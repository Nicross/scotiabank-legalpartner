﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace CL.Scotiabank.LegalPartner.SGL.Web
{
    public class ActionsFilter : Attribute, IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            var Configuration = builder.Build();

            if (Convert.ToBoolean(Configuration["ActionFilterStatus:Activated"]))
            {
                SessionLoginDTO session = context.HttpContext.Session.GetObjectFromJson<SessionLoginDTO>("sessionLogin");

                if (session == null)
                {
                    context.Result = new RedirectToActionResult("Index", "Login", new { area = "Login" });
                }
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
        }
    }
}
