﻿using CL.Scotiabank.LegalPartner.SGL.APP;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CL.Scotiabank.LegalPartner.SGL.Web.Areas.ConsultafirmasCaja.Controllers
{
    public class ConsultaFirmaCajaController : Controller
    {
        #region Members
        
        string baseUriConsultafirmasCaja = Resources.Consulta;

        private IConsultaFirmaCajaAPP _ConsultafirmasCajaAPP;
        
        #endregion

        #region Constructor

        public ConsultaFirmaCajaController(IConsultaFirmaCajaAPP ConsultafirmasCajaAPP)
        {
            this._ConsultafirmasCajaAPP = ConsultafirmasCajaAPP;
        }

        #endregion

        public IActionResult Index()
        {
            ConsultaFirmaCajaDTO ConsultafirmasCajaDTO = new ConsultaFirmaCajaDTO();

            return View(ConsultafirmasCajaDTO);
        }


        public JsonResult List()
        {
            IEnumerable<ConsultaFirmaCajaDTO> ConsultafirmasCajaRetorno = this._ConsultafirmasCajaAPP.Get();

            return this.Json(new
            {
                recordsTotal = ConsultafirmasCajaRetorno.Count(),
                data = ConsultafirmasCajaRetorno
            });
        }
   
        [HttpPost]
        public IActionResult Buscar(ConsultaFirmaCajaDTO ConsultafirmasCajaDTO)
        {
            ConsultaFirmaCajaDTO consultafirmasCajaRetorno = this._ConsultafirmasCajaAPP.Get(ConsultafirmasCajaDTO);

            if (consultafirmasCajaRetorno != null)
            {
                int codigoRetorno = consultafirmasCajaRetorno.CodigoRetorno;

                if (codigoRetorno == 0)
                {
                    return View(consultafirmasCajaRetorno); ;
                }
                else
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error";
                    @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                }
            }
            else
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
            }

            return View(consultafirmasCajaRetorno);
        }

        [HttpGet]
        public JsonResult ListDropdownCuentasCorrientes(string rut)
        {
            List<string> cuentas = this._ConsultafirmasCajaAPP.ObtenerProductosCliente(rut);

            List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

            Parallel.ForEach(cuentas, (cuenta) =>
            {
                listaDTO.Add(new DropDownListDTO { id = cuenta, text = cuenta });
            });

            return this.Json(listaDTO);
        }

        //public IActionResult Details(int id)
        //{
        //    ConsultafirmasCajaDTO ConsultafirmasCaja = this._ConsultafirmasCajaAPP.Get(id);
        //    return PartialView(ConsultafirmasCaja);
        //}

        //public IActionResult Edit(int id)
        //{
        //    ConsultafirmasCajaDTO ConsultafirmasCaja = this._ConsultafirmasCajaAPP.Get(id);
        //    return View(ConsultafirmasCaja);
        //}

        //[HttpPost]
        //public IActionResult Edit(ConsultafirmasCajaDTO ConsultafirmasCajaDTO)
        //{
        //    ConsultafirmasCajaDTO ConsultafirmasCajaRetorno = this._ConsultafirmasCajaAPP.Edit(ConsultafirmasCajaDTO);

        //    if (ConsultafirmasCajaRetorno != null)
        //    {
        //        int codigoRetorno = ConsultafirmasCajaRetorno.CodigoRetorno;


        //        if (codigoRetorno == 2)
        //        {
        //            @TempData["Exito"] = true;
        //            @TempData["TituloMensaje"] = "Edición Exitosa";
        //            @TempData["CuerpoMensaje"] = "Se ha editado el registro exitosamente.";
        //            return RedirectToAction("Index");
        //        }
        //        else if (codigoRetorno == 1)
        //        {
        //            @TempData["Error"] = true;
        //            @TempData["TituloMensaje"] = "Error - Edición de Registro No Existente";
        //            @TempData["CuerpoMensaje"] = "El registro que está intentando editar no existe, intente con otro.";
        //        }
        //        else
        //        {
        //            @TempData["Error"] = true;
        //            @TempData["TituloMensaje"] = "Error";
        //            @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
        //        }
        //    }
        //    else
        //    {
        //        @TempData["Error"] = true;
        //        @TempData["TituloMensaje"] = "Error";
        //        @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
        //    }

        //    return View(ConsultafirmasCajaDTO);            
        //}

        //public IActionResult Delete(int id)
        //{
        //    ConsultafirmasCajaDTO ConsultafirmasCaja = this._ConsultafirmasCajaAPP.Get(id);
        //    return View(ConsultafirmasCaja);
        //}

        //[HttpPost]
        //public IActionResult Delete(ConsultafirmasCajaDTO ConsultafirmasCajaDTO)
        //{
        //    ConsultafirmasCajaDTO ConsultafirmasCajaRetorno = this._ConsultafirmasCajaAPP.Delete(ConsultafirmasCajaDTO);

        //    if (ConsultafirmasCajaRetorno != null)
        //    {
        //        int codigoRetorno = ConsultafirmasCajaRetorno.CodigoRetorno;

        //        if (codigoRetorno == 2)
        //        {
        //            @TempData["Exito"] = true;
        //            @TempData["TituloMensaje"] = "Eliminación Exitosa";
        //            @TempData["CuerpoMensaje"] = "Se ha eliminado el registro exitosamente.";
        //            return RedirectToAction("Index");
        //        }
        //        else if (codigoRetorno == 1)
        //        {
        //            @TempData["Error"] = true;
        //            @TempData["TituloMensaje"] = "Error - Eliminación de Registro No Existente";
        //            @TempData["CuerpoMensaje"] = "El registro que está intentando eliminar no existe, intente con otro.";
        //        }
        //        else if (codigoRetorno == 3)
        //        {
        //            @TempData["Error"] = true;
        //            @TempData["TituloMensaje"] = "Error - Registro Utilizado por Tabla";
        //            @TempData["CuerpoMensaje"] = "No se ha podido eliminar el registro, porque está siendo utilizado por otra tabla.";
        //        }
        //        else
        //        {
        //            @TempData["Error"] = true;
        //            @TempData["TituloMensaje"] = "Error";
        //            @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
        //        }
        //    }
        //    else
        //    {
        //        @TempData["Error"] = true;
        //        @TempData["TituloMensaje"] = "Error";
        //        @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
        //    }

        //    return View();
        //}

    }
}