﻿using CL.Scotiabank.LegalPartner.SGL.APP;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace CL.Scotiabank.LegalPartner.SGL.Web.Areas.Consultas.Controllers
{
    public class BuscarClientesController : Controller
    {
        #region Members

        string baseUrlBuscarCliente = Resources.Consulta;

        private IBuscarClienteAPP _buscarClienteAPP;

        #endregion

        #region Constructor

        public BuscarClientesController(IBuscarClienteAPP BuscarClienteAPP)
        {
            this._buscarClienteAPP = BuscarClienteAPP;
        }

        #endregion

        #region Metodos

        public IActionResult Index()
        {
            BuscarClienteDTO buscarClienteDTO = new BuscarClienteDTO();

            return View(buscarClienteDTO);
        }

        public JsonResult ListNombreLegal(string nombre)
        {
            BasePropertiesDTO Retorno = this._buscarClienteAPP.GetListaNombreLegal(nombre);

            return this.Json(new
            {

                data = Retorno.Resultado
            });
        }

        public JsonResult ListNombreLegalAnterior(string nombre)
        {
            BasePropertiesDTO Retorno = this._buscarClienteAPP.GetListaNombreLegalAnterior(nombre);

            return this.Json(new
            {
                 
                data = Retorno.Resultado
            });
        }

        public JsonResult ListNombreFantasia(string nombre)
        {
            BasePropertiesDTO Retorno = this._buscarClienteAPP.GetListaClienteNombreFantasia(nombre);

            return this.Json(new
            {

                data = Retorno.Resultado
            });
        }

        public ActionResult ListMandatorios(int idSociedad, int IdSGL)
        {
            BasePropertiesDTO Retorno = this._buscarClienteAPP.GetListaMandatorios(idSociedad,IdSGL);

            //return PartialView((IEnumerable<MandatorioDTO>)Retorno.Resultado);

            return this.Json(new
            {

                data = Retorno.Resultado
            });
        }

        public ActionResult ModalMandatorio()
        {
            return PartialView("_ModalMandatorio");
        }

        public ActionResult GetSucursales(int rut, int idPersona)
        {
            BasePropertiesDTO Retorno = this._buscarClienteAPP.GetSucursales(rut, idPersona);

            return this.Json(new
            {
                data = Retorno.Resultado
            });
        }

        #endregion
    }
}