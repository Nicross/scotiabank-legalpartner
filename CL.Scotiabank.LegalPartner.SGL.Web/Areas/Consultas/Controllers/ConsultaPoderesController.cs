﻿
using CL.Scotiabank.LegalPartner.ConsultaAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.PoderYRevocacionesAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.APP;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resources = CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities.Resources;

namespace CL.Scotiabank.LegalPartner.SGL.Web.Areas.Consultas.Controllers
{
    /// <summary>
    /// Controlador  Consulta
    /// </summary>
    [ActionsFilter]
    public class ConsultaPoderesController : Controller
    {
        #region Members

        readonly string baseUriConservadores = Resources.ConsultaPoderesUrl;

        private IConsultaPoderesAPP _consultaPoderesAPP;

        #endregion

        #region Constructor

        public ConsultaPoderesController(IConsultaPoderesAPP consultaPoderesAPP)
        {
            this._consultaPoderesAPP = consultaPoderesAPP;
        }

        #endregion

        [HttpPost]
        public IActionResult Index(IngresarSGLDTO paramCargaIndex)
        {
            UsuarioConsultaDTORequest paramPersona = new UsuarioConsultaDTORequest
            {
                Rut = int.Parse(paramCargaIndex.Rut.Replace(".", "").Split("-")[0]),
                Nombre = string.Empty,
                ApellidoPaterno = string.Empty,
                ApellidoMaterno = string.Empty,
                CantidadRegistros = 0
            };

            BasePropertiesDTO datosPersonaRetorno = this._consultaPoderesAPP.ObtenerDatosPersona(paramPersona);
            ListaFacultades();

            ViewBag.IdSGL = paramCargaIndex.IdSGL;

            return View((UsuarioConsultaDTO)datosPersonaRetorno.Resultado);
        }
        
        public IActionResult BotonVolver(UsuarioConsultaDTO paramConsulta)
        {
            IngresarSGLDTO SGL = new IngresarSGLDTO();
            SGL.IdSGL = 4456426;

            return RedirectToAction("Index", "AsociarSociedadSGL", SGL);
        }

        /// <summary>
        /// Lista las facultades ordenadas por padre e hijo
        /// </summary>
        public void ListaFacultades()
        {
            StringBuilder htmlDropdown = new StringBuilder();
            BasePropertiesDTO facultadesRetorno = this._consultaPoderesAPP.ListarFacultadesPorPoder("15");

            ViewBag.lstFacultades = facultadesRetorno.Resultado;
        }

        /// <summary>
        /// Lista los mandatarios vigentes
        /// </summary>
        /// <param name="codigoParticipante"></param>
        /// <param name="tipoParticipante"></param>
        /// <param name="fechaConsulta"></param>
        /// <returns></returns>
        public JsonResult ListaMandatariosVigentes(int codigoParticipante, int tipoParticipante, DateTime fechaConsulta)
        {
            ConsultaParticipanteConPoderDTORequest paramParticipante = this._consultaPoderesAPP.ValidacionFecha(codigoParticipante, tipoParticipante, fechaConsulta);

            BasePropertiesDTO mandatariosRetorno = this._consultaPoderesAPP.ListarParticipanteConPoder(paramParticipante);

            return this.Json(new
            {
                data = mandatariosRetorno.Resultado
            });
        }

        public JsonResult CargaTablaComuneroModal(string codigoPersona, string codigoTipo)
        {
            BasePropertiesDTO participantesRetorno = this._consultaPoderesAPP.ObtenerListaParticipante(codigoPersona, codigoTipo);

            return this.Json(new
            {
                data = participantesRetorno.Resultado
            });
        }

        /// <summary>
        /// Boton "Ir a" datos estatuto
        /// </summary>
        /// <param name="codigoPersona"></param>
        /// <param name="tipoPersona"></param>
        /// <returns></returns>
        public IActionResult Details(string codigoPersona, string tipoPersona)
        {
            BasePropertiesDTO mandatariosRetorno = this._consultaPoderesAPP.ObtenerDatosCapital(codigoPersona, "1", tipoPersona);

            return PartialView("Details", (PatrimonioDTO)mandatariosRetorno.Resultado);
        }

        /// <summary>
        /// Json que recibe el objeto para luego enviarlo al modal Consulta Poder
        /// </summary>
        /// <param name="datosModal"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult EnviaDataModal([FromBody]MandatarioRutResponderDTO datosModal)
        {
            if (ModelState.IsValid)
            {
                datosModal.NombreFacultad = datosModal.NombreFacultad.TrimEnd().TrimStart();
                return Json(datosModal);
            }
            else
            {
                return Json("error create");
            }
        }

        /// <summary>
        /// Levanta Modal Responder Consulta Poder
        /// </summary>
        /// <param name="paramModal"></param>
        /// <returns></returns>
        public IActionResult ConsultaPoderesResponder(MandatarioRutResponderDTO paramModal)
        {
            int cantidadFacul = 1;
            string strHtml = string.Empty;

            paramModal.CantidadManda = paramModal.CodigosMandatarios.Remove(paramModal.CodigosMandatarios.Length - 1).Split(",").Length;
            paramModal.ArrRuts = paramModal.RutsMandatarios.Remove(paramModal.RutsMandatarios.Length - 1).Split(",");
            paramModal.ArrDvs = paramModal.Dvs.Remove(paramModal.Dvs.Length - 1).Split(",");
            paramModal.ArrNombres = paramModal.NombresMandatarios.Remove(paramModal.NombresMandatarios.Length - 1).Split(",");

            ConcatenaTexto(paramModal.CantidadManda, cantidadFacul);
            SeteaTipoPregunta(paramModal.CantidadManda, cantidadFacul);

            strHtml = _consultaPoderesAPP.ObtieneConsultaMandaFacul(paramModal);

            for (int i = 0; i < paramModal.ArrNombres.Length; i++)
            {
                strHtml = PintaMandatario(strHtml, paramModal.ArrNombres[i]);
            }

            ViewBag.respuesta = strHtml;

            return PartialView(paramModal);
        }

        private string PintaMandatario(string strHtml, string nombreMandatario)
        {
            if (strHtml.ToUpper().Contains(nombreMandatario.ToUpper()))
            {
                strHtml = strHtml.Replace(nombreMandatario, " <b><font color='green'>" + nombreMandatario + "</font></b> ");
            }

            return strHtml;
        }

        public IActionResult ResponderConsultaPoderesFacultad(MandatarioRutResponderDTO paramModal)
        {
            if (paramModal == null)
            {
                return PartialView(new MandatarioRutResponderDTO());
            }
            int codigoPersona = paramModal.CodigoPersona;
            int codigoFacultad = paramModal.CodigoFacultad;
            string strHtml = string.Empty;
            string fechaConsulta = Convert.ToDateTime(paramModal.Fecha).ToString("dd/MM/yyyy");

            ViewBag.FacultadConsultada = paramModal.NombreFacultad;

            strHtml = ObtieneConsultaFacultad(codigoPersona, codigoFacultad, fechaConsulta);

            ViewBag.strCadenaHtml = strHtml;

            return PartialView(paramModal);
        }

        public IActionResult ConsultaPoderesMandatarios(MandatarioRutResponderDTO paramModal)
        {
            int codigoPersona = paramModal.CodigoPersona;
            int codigoFacultad = paramModal.CodigoFacultad;
            string strHtml = string.Empty;
            string fechaConsulta = Convert.ToDateTime(paramModal.Fecha).ToString("dd/MM/yyyy");
            paramModal.ArrNombres = paramModal.NombresMandatarios.Remove(paramModal.NombresMandatarios.Length - 1).Split(",");

            strHtml = ObtieneConsultaFacultad(codigoPersona, codigoFacultad, fechaConsulta);

            for (int i = 0; i < paramModal.ArrNombres.Length; i++)
            {
                strHtml = PintaMandatario(strHtml, paramModal.ArrNombres[i]);
            }

            ViewBag.strCadenaHtml = strHtml;

            return PartialView(paramModal);
        }

        public IActionResult ConsultaSoloMandatarios(MandatarioRutResponderDTO paramModal)
        {
            int cantidadMand = 0;
            int cantidadFacul = 0;
            string strHtml = string.Empty;

            Tuple<MandatarioRutResponderDTO, string[]> respuesta = this._consultaPoderesAPP.RespConsultaSoloMandatarios(paramModal);

            paramModal.ArrNombres = paramModal.NombresMandatarios.Substring(0, paramModal.NombresMandatarios.Length).Split(",");

            strHtml = this._consultaPoderesAPP.ObtieneConsultaMandatario(paramModal);

            for (int i = 0; i < paramModal.ArrNombres.Length; i++)
            {
                strHtml = PintaMandatario(strHtml, paramModal.ArrNombres[i]);
            }

            ViewBag.cadenaHTML = strHtml;

            ViewBag.Mandatarios = respuesta.Item2;

            cantidadMand = respuesta.Item1.CantidadManda;
            cantidadFacul = respuesta.Item1.CodigoFacultad;

            ConcatenaTexto(cantidadMand, cantidadFacul);
            SeteaTipoPregunta(cantidadMand, cantidadFacul);

            return PartialView(paramModal);
        }



        private string ObtieneConsultaFacultad(int codigoPersona, int codigoFacultad, string fechaConsulta)
        {
            return this._consultaPoderesAPP.RespObtieneConsultaFacultad(codigoPersona, codigoFacultad, fechaConsulta);
        }

        private IList<GlosaResponderDTO> CargarGlosaPorFacultad(int codigoPersona, string fechaConsulta)
        {
            int tipopod = 0;
            string tipoRespu = "1";
            int estadoCsu = 0;

            IList<GlosaResponderDTO> lstGlosa = new List<GlosaResponderDTO>();
            GlosaResponderDTO paramGlosa = null;

            PoderVigenteDTORequest paramPoderesVig = new PoderVigenteDTORequest
            {
                CodigoPersona = codigoPersona,
                TipoPoder = tipopod,
                FechaBusqueda = fechaConsulta,
                TipoRespuesta = tipoRespu,
                EstadoConsulta = estadoCsu
            };

            BasePropertiesDTO PoderesVigentes = this._consultaPoderesAPP.ListarPoderesVigentes(paramPoderesVig);

            foreach (var item in PoderesVigentes.Resultado)
            {
                paramGlosa = new GlosaResponderDTO
                {
                    IdPoder = item.IdPoder,
                    Glosa = item.Glosa,
                    IdFactuar = item.FormaDeActuarId
                };

                lstGlosa.Add(paramGlosa);
            }
            return lstGlosa;
        }

        /// <summary>
        /// Obtiene Formas de actuar para mostrar en Modal 
        /// </summary>
        /// <param name="paramMandatarios"></param>
        private void ObtieneConsulta(MandatarioRutResponderDTO paramMandatarios)
        {

            int tipopod = 0;
            string tipoRespu = "1";
            int estadoCsu = 0;
            int codigoPersona = paramMandatarios.CodigoPersona;
            int codigoFacultad = paramMandatarios.CodigoFacultad;
            string fecha = paramMandatarios.Fecha;

            PoderFacultadVigenteDTORequest paramFacultadVig = new PoderFacultadVigenteDTORequest
            {
                CodigoPersona = codigoPersona,
                CodigoFacultad = codigoFacultad,
                FechaBusqueda = fecha
            };

            BasePropertiesDTO datosPersonaRetorno = this._consultaPoderesAPP.ListarPoderesFacultadesVigentes(paramFacultadVig);


            if (datosPersonaRetorno.Resultado.Count != 0)
            {
                ViewBag.FechaIngreso = datosPersonaRetorno.Resultado[0].FechaInicioDocumento;

                PoderVigenteDTORequest paramPoderesVig = new PoderVigenteDTORequest
                {
                    CodigoPersona = codigoPersona,
                    TipoPoder = tipopod,
                    FechaBusqueda = fecha,
                    TipoRespuesta = tipoRespu,
                    EstadoConsulta = estadoCsu
                };

                BasePropertiesDTO PoderesVigentes = this._consultaPoderesAPP.ListarPoderesVigentes(paramPoderesVig);

                IEnumerable<PoderVigentePorSociedadDTOResponse> lstPoderesVigentes = ((IEnumerable<PoderVigentePorSociedadDTOResponse>)PoderesVigentes.Resultado).Where(x => x.IdPoder == datosPersonaRetorno.Resultado[0].CodigoPoder);

                ViewBag.lstFormasActuar = this._consultaPoderesAPP.ObtieneFormaActuar(lstPoderesVigentes, paramMandatarios, fecha); //((IEnumerable<PoderVigentePorSociedadDTOResponse>)PoderesVigentes.Resultado).Where(x => x.IdPoder == datosPersonaRetorno.Resultado[0].CodigoPoder);

            }
            else
            {
                ViewBag.lstFormasActuar = null;
            }
        }

        /// <summary>
        /// Setea los mensajes a mostrar segun corresponda
        /// </summary>
        /// <param name="intTotPer"></param>
        /// <param name="intTotalFAcu"></param>
        private void SeteaTipoPregunta(int intTotPer, int intTotalFAcu)
        {
            string strTipoPregunta = string.Empty;

            if (intTotPer == 0 && intTotalFAcu == 1)
                strTipoPregunta = "Facultad";
            else if (intTotPer == 1 && intTotalFAcu == 0)
                strTipoPregunta = "Mandatarios";
            else if (intTotPer > 1 && intTotalFAcu == 0)
                strTipoPregunta = "Mandatarios";
            else if (intTotPer == 1 && intTotalFAcu == 1)
                strTipoPregunta = "MandatarioYfacu";
            else if (intTotPer > 1 && intTotalFAcu == 1)
                strTipoPregunta = "MandatarioYfacu";

            ViewBag.TipoPregunta = strTipoPregunta;
        }

        private void ConcatenaTextoMandatarios(int cantidadManda)
        {
            string strMandatarioEjerce = string.Empty;
            string strMandatarioNoEjerce = string.Empty;
            string strCampo = string.Empty;
            string strTxtResp = string.Empty;
            string strTexto = string.Empty;
            string strLasFacultades = string.Empty;
            string strConsulta = string.Empty;

            if (cantidadManda != 0)
            {
                if (cantidadManda > 1)
                {
                    strMandatarioEjerce = "Actuando conjuntamente pueden ejercer ";
                    strMandatarioNoEjerce = "No pueden ejercer ";
                    strCampo = "Los Mandatarios:";
                    strTxtResp = "SI";
                    strTexto = " pueden ejercer la facultad seleccionadas ";
                }
                else
                {
                    strMandatarioEjerce = "Actuando individualmente puede ejercer";
                    strMandatarioNoEjerce = "Actuando individualmente no puede ejercer";
                    strCampo = "El Mandatario:";
                    strTxtResp = "SI";
                    strTexto = " puede ejercer la facultad seleccionada ";
                }
            }

            ViewBag.strMandatarioEjerce = strMandatarioEjerce;
            ViewBag.strMandatarioNoEjerce = strMandatarioNoEjerce;
            ViewBag.strCampo = strCampo;
            ViewBag.strTxtResp = strTxtResp;
            ViewBag.strTexto = strTexto;
            ViewBag.strLasFacultades = strLasFacultades;
            ViewBag.strConsulta = strConsulta;
        }

        /// <summary>
        /// Setea los mensajes a mostrar segun corresponda
        /// </summary>
        /// <param name="cantidadMand"></param>
        /// <param name="cantidadFacul"></param>
        private void ConcatenaTexto(int cantidadMand, int cantidadFacul)
        {
            string strMandatarioEjerce = string.Empty;
            string strMandatarioNoEjerce = string.Empty;
            string strCampo = string.Empty;
            string strTxtResp = string.Empty;
            string strTexto = string.Empty;
            string strLasFacultades = string.Empty;
            string strConsulta = string.Empty;

            if (cantidadMand != 0)
            {
                if (cantidadMand > 1)
                {
                    strMandatarioEjerce = "Actuando conjuntamente pueden ejercer ";
                    strMandatarioNoEjerce = "No pueden ejercer ";
                    strCampo = "Los Mandatarios:";
                    strTxtResp = "SI";
                    strTexto = " pueden ejercer la facultad seleccionadas ";
                }
                else
                {
                    strMandatarioEjerce = "Actuando individualmente puede ejercer";
                    strMandatarioNoEjerce = "Actuando individualmente no puede ejercer";
                    strCampo = "El Mandatario:";
                    strTxtResp = "SI";
                    strTexto = " puede ejercer la facultad seleccionada ";
                }
            }
            else
            {
                if (cantidadFacul != 0)
                {
                    if (cantidadFacul > 0)
                    {
                        strCampo = "Facultades";
                        strLasFacultades = "las siguientes facultades:";
                        strConsulta = " consultadas";
                    }
                    else
                    {
                        strCampo = "Facultad";
                        strLasFacultades = "la siguiente facultad:";
                        strConsulta = " consultada";
                    }
                }
            }

            ViewBag.strMandatarioEjerce = strMandatarioEjerce;
            ViewBag.strMandatarioNoEjerce = strMandatarioNoEjerce;
            ViewBag.strCampo = strCampo;
            ViewBag.strTxtResp = strTxtResp;
            ViewBag.strTexto = strTexto;
            ViewBag.strLasFacultades = strLasFacultades;
            ViewBag.strConsulta = strConsulta;
        }

        /// <summary>
        /// Levanta Modal Responde cuando no se selecciona Mandatario ni Facultad
        /// </summary>
        /// <param name="rutSoc"></param>
        /// <param name="nombreSociedad"></param>
        /// <param name="fecha"></param>
        /// <param name="codigoPersona"></param>
        /// <returns></returns>
        public IActionResult RespondePoderesVigentes(string rutSoc, string nombreSociedad, string fecha, int codigoPersona)
        {
            RespondePoderesVigentesDTO paramPoderes = new RespondePoderesVigentesDTO
            {
                Nombre = nombreSociedad,
                Rut = rutSoc,
                FechaConsulta = fecha
            };

            CargaCabeceraTabla(codigoPersona, paramPoderes.FechaConsulta);

            return PartialView(paramPoderes);
        }

        /// <summary>
        /// Carga los datos del modal responder sin seleccionar opciones
        /// </summary>
        /// <param name="codigoPersona"></param>
        /// <param name="fecha"></param>
        private void CargaCabeceraTabla(int codigoPersona, string fecha)
        {
            int tipopod = 0;
            string tipoRespu = "1";
            int estadoCsu = 0;

            PoderVigenteDTORequest paramPoderesVig = new PoderVigenteDTORequest
            {
                CodigoPersona = codigoPersona,
                TipoPoder = tipopod,
                FechaBusqueda = fecha,
                TipoRespuesta = tipoRespu,
                EstadoConsulta = estadoCsu
            };

            BasePropertiesDTO PoderesVigentes = this._consultaPoderesAPP.ListarPoderesVigentes(paramPoderesVig);

            //ObtieneListaDocumentoLegal por fecha
            ViewBag.lstPoderesFecha = ObtieneListaDocumentoLegal(PoderesVigentes);

            ObtieneListaFacultades(PoderesVigentes, fecha);
            ViewBag.lstPoderesVigentes = PoderesVigentes.Resultado;

        }

        /// <summary>
        /// Obtiene Facultadades para modal
        /// </summary>
        /// <param name="PoderesVigentes"></param>
        /// <param name="fecha"></param>
        private void ObtieneListaFacultades(BasePropertiesDTO PoderesVigentes, string fecha)
        {
            IList<FacultadesLimitacionDTO> lstFaculVigentes = new List<FacultadesLimitacionDTO>();
            IList<PoderDTOResponse> lstPoderesGrupo = new List<PoderDTOResponse>();

            //Agrupamos por idPoder para luego llenar la lista de mandatarios y formas de actuar segun IdPoder
            var lstPoderesGrup = from s in (IEnumerable<PoderVigentePorSociedadDTOResponse>)PoderesVigentes.Resultado
                                 group s by s.IdPoder;

            foreach (var item in lstPoderesGrup)
            {
                BasePropertiesDTO Poderes = this._consultaPoderesAPP.ObtenerPoder(item.Key);
                lstPoderesGrupo.Add((PoderDTOResponse)Poderes.Resultado[0]);

                int codigo = Convert.ToInt32(Poderes.Resultado[0].Codigo);
                lstFaculVigentes = OrdenaListaFacultades(codigo, fecha, lstFaculVigentes);
            }

            ViewBag.lstFaculVigentes = lstFaculVigentes;
            ViewBag.lstPoderesGrup = (IEnumerable<PoderDTOResponse>)lstPoderesGrupo;

        }

        /// <summary>
        /// Ordena la facultades por padre e hijo
        /// </summary>
        /// <param name="codigo"></param>
        /// <param name="fecha"></param>
        /// <param name="lstFaculVigentes"></param>
        /// <returns></returns>
        private IList<FacultadesLimitacionDTO> OrdenaListaFacultades(int codigo, string fecha, IList<FacultadesLimitacionDTO> lstFaculVigentes)
        {
            FacultadesLimitacionDTO paramFaculVigentes = null;
            string descripGrupo = string.Empty;
            string DescripcionLim = string.Empty;

            BasePropertiesDTO facultadesVigentes = this._consultaPoderesAPP.ListarFacultadesVigentes(codigo, fecha);

            var lstPadresGrupo = from s in (IEnumerable<FacultadesVigentesPorPoderDTOResponse>)facultadesVigentes.Resultado
                                 group s by s.Grupo;

            foreach (var itemPadresGrupo in lstPadresGrupo)
            {
                descripGrupo = itemPadresGrupo.Key;

                paramFaculVigentes = new FacultadesLimitacionDTO
                {
                    IdPoder = codigo,
                    DescripcionFacultad = descripGrupo,
                    DescripcionLimitacion = string.Empty,
                    Tipo = "Padre"
                };

                lstFaculVigentes.Add(paramFaculVigentes);

                foreach (var itemFacVig in facultadesVigentes.Resultado)
                {
                    if (itemFacVig.Grupo == descripGrupo)
                    {
                        BasePropertiesDTO Limitacion = this._consultaPoderesAPP.ObtenerLimites(codigo, itemFacVig.CodigoFacultad, fecha);

                        if (Limitacion.Resultado.Count != 0)
                            DescripcionLim = Limitacion.Resultado[0].TextoLimite;
                        else
                            DescripcionLim = string.Empty;


                        paramFaculVigentes = new FacultadesLimitacionDTO
                        {
                            IdPoder = codigo,
                            DescripcionFacultad = itemFacVig.Descripcion,
                            DescripcionLimitacion = DescripcionLim,
                            Tipo = "Hijo"
                        };

                        lstFaculVigentes.Add(paramFaculVigentes);
                    }
                }
            }

            return lstFaculVigentes;
        }

        /// <summary>
        /// Ordena lista documentos legal para retornarla a la vista
        /// </summary>
        /// <param name="PoderesVigentes"></param>
        /// <returns></returns>
        private dynamic ObtieneListaDocumentoLegal(BasePropertiesDTO PoderesVigentes)
        {
            int codDocu = 0;
            PoderesVigentesResponderDTO paramDocLegal = null;
            IList<PoderesVigentesResponderDTO> lstDocumentoLegalViewBag = new List<PoderesVigentesResponderDTO>();

            var lstPoderesFec = from s in (IEnumerable<PoderVigentePorSociedadDTOResponse>)PoderesVigentes.Resultado
                                group s by new { s.DocumentoId, s.FechaDocumento };

            foreach (var itemPodFec in lstPoderesFec)
            {
                codDocu = itemPodFec.Key.DocumentoId;
                BasePropertiesDTO DocumentoLegal = this._consultaPoderesAPP.ObtenerDocumentoLegal(codDocu);

                DateTime FechaDocumento = Convert.ToDateTime(DocumentoLegal.Resultado[0].FechaDocumento);
                DateTime FechaActVigente = Convert.ToDateTime(DocumentoLegal.Resultado[0].FechaRenovacionDocumento);

                paramDocLegal = new PoderesVigentesResponderDTO
                {
                    FechaDocumentoString = Convert.ToDateTime(itemPodFec.Key.FechaDocumento).ToString("dd/MM/yyyy"),
                    FechaDocumento = Convert.ToDateTime(itemPodFec.Key.FechaDocumento),
                    CodigoDocumento = codDocu,
                    FechaRenovacionDocumentoString = FechaActVigente.ToString("dd/MM/yyyy"),
                    FechaRenovacionDocumento = FechaActVigente,
                    Notaria = DocumentoLegal.Resultado[0].Notaria
                };

                lstDocumentoLegalViewBag.Add(paramDocLegal);
            }

            return lstDocumentoLegalViewBag;
        }

        /// <summary>
        /// Obtiene Facultades y limitaciones cuando se selecciona solo mandatarios.
        /// </summary>
        /// <param name="intCodPoder"></param>
        /// <param name="fechaConsulta"></param>
        /// <param name="fechaIniPoder"></param>
        /// <returns></returns>
        private string ObtenerFacuYLim(int intCodPoder, string fechaConsulta, string fechaIniPoder)
        {
            string strCadena = string.Empty;
            string strLimit = string.Empty;
            int intCodGrp = 0;
            string grupo = string.Empty;
            int codFacultad = 0;

            BasePropertiesDTO FacultadesVigentes = this._consultaPoderesAPP.ObtenerFacultadesVigentes(intCodPoder, fechaConsulta);


            var lstGrupos = from s in (IEnumerable<FacultadVigenteDTOResponse>)FacultadesVigentes.Resultado
                            group s by s.CodigoGrupo;

            foreach (var itemFac in lstGrupos)
            {
                intCodGrp = itemFac.Key;
                grupo = ((IEnumerable<FacultadVigenteDTOResponse>)FacultadesVigentes.Resultado).Where(x => x.CodigoGrupo == intCodGrp).FirstOrDefault().Grupo;
                codFacultad = ((IEnumerable<FacultadVigenteDTOResponse>)FacultadesVigentes.Resultado).Where(x => x.CodigoGrupo == intCodGrp).FirstOrDefault().CodigoFacultad;

                strCadena = strCadena + "<tr class=\"bg-primary text-center text-light\">";
                strCadena = strCadena + "<td style=\"font-size: 15px\" colspan='3'>" + grupo.Trim() + "</td>";
                strCadena = strCadena + "</tr>";

                foreach (var itemFacAux in FacultadesVigentes.Resultado)
                {
                    if (!itemFacAux.CodigoGrupo.Equals(intCodGrp))
                        continue;

                    strCadena = strCadena + "<tr>";
                    strCadena = strCadena + "<td>" + itemFacAux.Descripcion.Trim() + "</td>";

                    strCadena = strCadena + "<td>";
                    strLimit = string.Empty;

                    strLimit = this._consultaPoderesAPP.ObtenerLimitacionesFacultad(intCodPoder, codFacultad, fechaConsulta);

                    if (!strLimit.Equals(string.Empty))
                        strCadena = strCadena + strLimit;
                    else
                        strCadena = strCadena + " ";

                    strCadena = strCadena + "</td>";

                    //Agregar fecha ini. poder
                    if (!fechaIniPoder.Equals(string.Empty))
                        strCadena = strCadena + "<td align=center>" + fechaIniPoder + "</td>";
                    else
                        strCadena = strCadena + "</tr>";
                }
            }

            return strCadena;
        }
    }
}