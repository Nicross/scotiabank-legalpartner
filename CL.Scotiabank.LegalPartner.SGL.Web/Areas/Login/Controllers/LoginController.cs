﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CL.Scotiabank.LegalPartner.SGL.APP;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace CL.Scotiabank.LegalPartner.SGL.Web.Areas.Login.Controllers
{
    public class LoginController : Controller
    {
        #region Members

        private ILoginAPP _loginAPP;
        private IMenuAPP _menuAPP;
        private ITokenAPP _tokenAPP;
        private IConfiguration _configuration;

        #endregion

        #region Constructor

        public LoginController(ILoginAPP loginAPP, IMenuAPP menuAPP, ITokenAPP tokenAPP, IConfiguration configuracion)
        {
            this._loginAPP = loginAPP;
            this._menuAPP = menuAPP;
            this._tokenAPP = tokenAPP;
            this._configuration = configuracion;
        }

        #endregion

        public IActionResult Index()
        {
            HttpContext.Session.Clear();
            return View();
        }

        [HttpPost]
        public IActionResult Index(LoginDTO loginDTO)
        {
            var rutSinFomatear = loginDTO.Rut;

            loginDTO.Rut = loginDTO.Rut.Replace(".", "");
            loginDTO.DigitoVerificador = loginDTO.Rut.Split('-')[1];
            loginDTO.Rut = loginDTO.Rut.Split('-')[0];

            TokenDTO token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });
            HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

            BasePropertiesDTO loginRetorno = this._loginAPP.Login(loginDTO);

            if (loginRetorno.CodigoRetorno == (int)LoginEstados.Exitoso)
            {
                BasePropertiesDTO menusRetorno = _menuAPP.GetMenusPorCodigoGrupoUsuario(loginRetorno.Resultado.GrupoPerfilId);

                HttpContext.Session.SetObjectAsJson<SessionLoginDTO>("sessionLogin", new SessionLoginDTO() {
                    CodigoUsuario = loginRetorno.Resultado.Codigo,
                    NombreUsuario = loginRetorno.Resultado.Nombre + " " + loginRetorno.Resultado.ApellidoPaterno + " " + loginRetorno.Resultado.ApellidoMaterno,
                    RutUsuario = loginRetorno.Resultado.Rut,
                    DigitoVerificador = loginRetorno.Resultado.DigitoVerificador,
                    menusDTO = menusRetorno.Resultado
                });

                return RedirectToAction("Home");
            }
            else if (loginRetorno.CodigoRetorno == (int)LoginEstados.ClaveIncorrecta)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Error - Clave Incorrecta";
                @TempData["CuerpoMensaje"] = "La clave ingresada no es correcta.";

                HttpContext.Session.Clear();
            }
            else if (loginRetorno.CodigoRetorno == (int)LoginEstados.PerfilInvalido)
            {
                //Si el usuario no posee un perfil valido entonces se le intenta loguear en el loguin de SGL.

                BasePropertiesDTO loginRetorno2 = this._loginAPP.LoginSGL(loginDTO);

                if (loginRetorno2.CodigoRetorno == (int)LoginEstados.Exitoso)
                {
                    BasePropertiesDTO menusRetorno = _menuAPP.GetMenusPorCodigoGrupoUsuario(loginRetorno2.Resultado.GrupoPerfilId);

                    HttpContext.Session.SetObjectAsJson<SessionLoginDTO>("sessionLogin", new SessionLoginDTO() {
                        CodigoUsuario = loginRetorno2.Resultado.Codigo,
                        NombreUsuario = loginRetorno2.Resultado.Nombre + " " + loginRetorno2.Resultado.ApellidoPaterno + " " + loginRetorno2.Resultado.ApellidoMaterno,
                        RutUsuario = loginRetorno.Resultado.Rut,
                        DigitoVerificador = loginRetorno.Resultado.DigitoVerificador,
                        menusDTO =  menusRetorno.Resultado
                    });

                    return RedirectToAction("Home");
                }
                else if (loginRetorno2.CodigoRetorno == (int)LoginEstados.ClaveIncorrecta)
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error - Clave Incorrecta";
                    @TempData["CuerpoMensaje"] = "Estimado Usuario: La clave ingresada no es correcta.";

                    HttpContext.Session.Clear();
                }
                else if (loginRetorno2.CodigoRetorno == (int)LoginEstados.PerfilInvalido)
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error - Perfil Invalido";
                    @TempData["CuerpoMensaje"] = "El usuario no posee un perfil valido para ingresar a este modulo.";

                    HttpContext.Session.Clear();
                }
                else if (loginRetorno2.CodigoRetorno == (int)LoginEstados.UsuarioNoRegistrado)
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error - Usuario No Registrado";
                    @TempData["CuerpoMensaje"] = "Estimado Usuario: Ud. no se encuentra registrado.";

                    HttpContext.Session.Clear();
                }
                else if (loginRetorno2.CodigoRetorno == (int)LoginEstados.UsuarioBloqueado)
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error - Usuario Bloqueado";
                    @TempData["CuerpoMensaje"] = "El usuario ha sido bloquedo debido a que fracasado en más de 3 intentos al ingresar la contraseña o bien un administrador lo ha bloqueado.";

                    HttpContext.Session.Clear();
                }
                else if (loginRetorno2.CodigoRetorno == (int)LoginEstados.AdvertenciaCambiarClave)
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Advertencia - Cambiar Clave";
                    @TempData["CuerpoMensaje"] = "Por su seguridad, le solicitamos cambiar su clave de acceso al sistema";

                    loginRetorno2.Resultado.Rut = rutSinFomatear;

                    BasePropertiesDTO menusRetorno = _menuAPP.GetMenusPorCodigoGrupoUsuario(loginRetorno2.Resultado.GrupoPerfilId);

                    HttpContext.Session.SetObjectAsJson<SessionLoginDTO>("sessionLogin", new SessionLoginDTO()
                    {
                        CodigoUsuario = loginRetorno2.Resultado.Codigo,
                        NombreUsuario = loginRetorno2.Resultado.Nombre + " " + loginRetorno2.Resultado.ApellidoPaterno + " " + loginRetorno2.Resultado.ApellidoMaterno,
                        RutUsuario = loginRetorno.Resultado.Rut,
                        DigitoVerificador = loginRetorno.Resultado.DigitoVerificador,
                        menusDTO = menusRetorno.Resultado
                    });

                    return RedirectToAction("ModificarClave", loginRetorno2.Resultado);
                }
                else if (loginRetorno2.CodigoRetorno == (int)LoginEstados.ExpiracionProntaClave)
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Advertencia - Clave Pronta a Expirar";
                    @TempData["CuerpoMensaje"] = "Su clave expirara en: " + loginRetorno2.Resultado.DiasRetorno + " día(s). Debe actualizar su clave.";

                    BasePropertiesDTO menusRetorno = _menuAPP.GetMenusPorCodigoGrupoUsuario(loginRetorno2.Resultado.GrupoPerfilId);

                    HttpContext.Session.SetObjectAsJson<SessionLoginDTO>("sessionLogin", new SessionLoginDTO()
                    {
                        CodigoUsuario = loginRetorno2.Resultado.Codigo,
                        NombreUsuario = loginRetorno2.Resultado.Nombre + " " + loginRetorno2.Resultado.ApellidoPaterno + " " + loginRetorno2.Resultado.ApellidoMaterno,
                        RutUsuario = loginRetorno.Resultado.Rut,
                        DigitoVerificador = loginRetorno.Resultado.DigitoVerificador,
                        menusDTO = menusRetorno.Resultado
                    });

                    return RedirectToAction("Home");
                }
                else if (loginRetorno2.CodigoRetorno == (int)LoginEstados.ClaveExpirada)
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Advertencia - Clave Expirada";
                    @TempData["CuerpoMensaje"] = "Su clave ha expirado. Por favor realice el cambio de clave a continuación. \n\n Atención: No podrá acceder al sistema si no se realiza el cambio.";

                    loginRetorno2.Resultado.Rut = rutSinFomatear;

                    HttpContext.Session.SetObjectAsJson<SessionLoginDTO>("sessionLogin", new SessionLoginDTO()
                    {
                        CodigoUsuario = loginRetorno2.Resultado.Codigo,
                        NombreUsuario = loginRetorno2.Resultado.Nombre + " " + loginRetorno2.Resultado.ApellidoPaterno + " " + loginRetorno2.Resultado.ApellidoMaterno,
                         RutUsuario = loginRetorno.Resultado.Rut,
                        DigitoVerificador = loginRetorno.Resultado.DigitoVerificador,
                    });

                    return RedirectToAction("ModificarClave", loginRetorno2.Resultado);
                }
            }
            else if (loginRetorno.CodigoRetorno == (int)LoginEstados.UsuarioNoRegistrado)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Error - Usuario No Registrado";
                @TempData["CuerpoMensaje"] = "El usuario no se encuentra registrado en el sistema.";

                HttpContext.Session.Clear();
            }
            else if (loginRetorno.CodigoRetorno == (int)LoginEstados.UsuarioBloqueado)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Error - Usuario Bloqueado";
                @TempData["CuerpoMensaje"] = "El usuario ha sido bloquedo debido a que fracasado en más de 3 intentos al ingresar la contraseña o bien un administrador lo ha bloqueado.";

                HttpContext.Session.Clear();
            }
            else if (loginRetorno.CodigoRetorno == (int)LoginEstados.AdvertenciaCambiarClave)
            {
                BasePropertiesDTO menusRetorno = _menuAPP.GetMenusPorCodigoGrupoUsuario(loginRetorno.Resultado.GrupoPerfilId);

                HttpContext.Session.SetObjectAsJson<SessionLoginDTO>("sessionLogin", new SessionLoginDTO()
                {
                    CodigoUsuario = loginRetorno.Resultado.Codigo,
                    NombreUsuario = loginRetorno.Resultado.Nombre + " " + loginRetorno.Resultado.ApellidoPaterno + " " + loginRetorno.Resultado.ApellidoMaterno,
                    RutUsuario = loginRetorno.Resultado.Rut,
                    DigitoVerificador = loginRetorno.Resultado.DigitoVerificador,
                    menusDTO = menusRetorno.Resultado
                });

                return RedirectToAction("Home");
            }
            else if (loginRetorno.CodigoRetorno == (int)LoginEstados.ExpiracionProntaClave)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Advertencia - Clave Pronta a Expirar";
                @TempData["CuerpoMensaje"] = "Su clave expirara en: " + loginRetorno.Resultado.DiasRetorno + " día(s). Debe actualizar su clave.";

                BasePropertiesDTO menusRetorno = _menuAPP.GetMenusPorCodigoGrupoUsuario(loginRetorno.Resultado.GrupoPerfilId);

                HttpContext.Session.SetObjectAsJson<SessionLoginDTO>("sessionLogin", new SessionLoginDTO()
                {
                    CodigoUsuario = loginRetorno.Resultado.Codigo,
                    NombreUsuario = loginRetorno.Resultado.Nombre + " " + loginRetorno.Resultado.ApellidoPaterno + " " + loginRetorno.Resultado.ApellidoMaterno,
                    RutUsuario = loginRetorno.Resultado.Rut,
                    DigitoVerificador = loginRetorno.Resultado.DigitoVerificador,
                    menusDTO = menusRetorno.Resultado
                });

                return RedirectToAction("Home");
            }
            else if (loginRetorno.CodigoRetorno == (int)LoginEstados.ClaveExpirada)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Advertencia - Clave Expirada";
                @TempData["CuerpoMensaje"] = "Su clave ha expirado. Por favor realice el cambio de clave a continuación. \n\n Atención: No podrá acceder al sistema si no se realiza el cambio.";

                loginRetorno.Resultado.Rut = rutSinFomatear;

                HttpContext.Session.SetObjectAsJson<SessionLoginDTO>("sessionLogin", new SessionLoginDTO()
                {
                    CodigoUsuario = loginRetorno.Resultado.Codigo,
                    NombreUsuario = loginRetorno.Resultado.Nombre + " " + loginRetorno.Resultado.ApellidoPaterno + " " + loginRetorno.Resultado.ApellidoMaterno,
                     RutUsuario = loginRetorno.Resultado.Rut,
                    DigitoVerificador = loginRetorno.Resultado.DigitoVerificador,
                });

                return RedirectToAction("ModificarClave", loginRetorno.Resultado);
            }
            else
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";

                HttpContext.Session.Clear();
            }

            loginDTO.Rut = rutSinFomatear;

            return View(loginDTO);
        }

        [ActionsFilter]
        public IActionResult Home()
        {
            return View();
        }

        [ActionsFilter]
        public IActionResult Prueba()
        {
            return View();
        }

        [ActionsFilter]
        public IActionResult ModificarClave(LoginDTO loginDTO)
        {
            BasePropertiesDTO loginRetorno = this._loginAPP.ObtenerInformacionModificarClave(loginDTO);

            return View(loginRetorno.Resultado);
        }

        [HttpPost]
        [ActionsFilter]
        public IActionResult ModificarClavePost(LoginDTO loginDTO)
        {
            var rutSinFomatear = loginDTO.Rut;

            loginDTO.Rut = loginDTO.Rut.Replace(".", "");
            loginDTO.DigitoVerificador = loginDTO.Rut.Split('-')[1];
            loginDTO.Rut = loginDTO.Rut.Split('-')[0];

            BasePropertiesDTO loginRetorno = this._loginAPP.ModificarClave(loginDTO);

            if (loginRetorno != null)
            {
                if (loginRetorno.CodigoRetorno == (int)LoginEstados.ClavePreviamenteIngresada)
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error - Clave ya ingresada";
                    @TempData["CuerpoMensaje"] = "Está clave ya ha sido ingresada con anterioridad, intente con otra clave.";

                    loginRetorno.Resultado.Rut = rutSinFomatear;

                    return RedirectToAction("ModificarClave", loginDTO);
                }
                else if (loginRetorno.CodigoRetorno == 1)
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error - Clave ya ingresada";
                    @TempData["CuerpoMensaje"] = "No se ha podido ingresar el usuario porque este ya se encuentra registrado.";

                    loginRetorno.Resultado.Rut = rutSinFomatear;

                    return RedirectToAction("ModificarClave", loginDTO);
                }
                else if (loginRetorno.CodigoRetorno == 2)
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error - Clave ya ingresada";
                    @TempData["CuerpoMensaje"] = "En estos momentos no lo podemos atender, por favor inténtelo más tarde.";

                    loginRetorno.Resultado.Rut = rutSinFomatear;

                    return RedirectToAction("ModificarClave", loginDTO);
                }
                else if (loginRetorno.CodigoRetorno == 3)
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error - No se ha podido cambiar la clave";
                    @TempData["CuerpoMensaje"] = "La contraseña anterior es incorrecta. La clave NO se ha cambiado.";

                    loginRetorno.Resultado.Rut = rutSinFomatear;

                    return RedirectToAction("ModificarClave", loginDTO);
                }
                else if (loginRetorno.CodigoRetorno == 4)
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error - Clave Bloqueada";
                    @TempData["CuerpoMensaje"] = "La contraseña anterior es incorrecta y es su tercer intento que ha fracasado.\nLa clave NO se ha cambiado y ademas ha sido BLOQUEADA.";

                    return RedirectToAction("Index");
                }
                else if (loginRetorno.CodigoRetorno == 5)
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Exito - Clave Cambiada con Exito";
                    @TempData["CuerpoMensaje"] = "El cambio de clave ha sido exitoso.";

                    return RedirectToAction("Index");
                }
            }
            else
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Error - No se ha podido cambiar la clave";
                @TempData["CuerpoMensaje"] = "En estos momentos no lo podemos atender, por favor inténtelo más tarde.";

                return RedirectToAction("ModificarClave", loginDTO);
            }

            return RedirectToAction("ModificarClave", loginDTO);
        }

        public IActionResult Download()
        {
            var path = Path.Combine(
                           Directory.GetCurrentDirectory(),
                           "wwwroot", "archivos", "Manual_Usuario_LegalPartner.pdf");

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                stream.CopyTo(memory);
            }
            memory.Position = 0;
            return File(memory, "application/pdf", Path.GetFileName(path));
        }
    }
}