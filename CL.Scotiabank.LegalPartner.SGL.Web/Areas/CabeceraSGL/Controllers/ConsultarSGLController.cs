﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CL.Scotiabank.LegalPartner.CabeceraSglAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.ConsultaAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.APP;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace CL.Scotiabank.LegalPartner.SGL.Web.Areas.CabeceraSGL.Controllers
{
    public class ConsultarSGLController : Controller
    {
        [ActionsFilter]
        #region Members

        private IConsultarSGLAPP _consultaSGLAPP;
        private dynamic logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

        #endregion

        #region Constructor

        public ConsultarSGLController(IConsultarSGLAPP consultarSGLAPP)
        {
            this._consultaSGLAPP = consultarSGLAPP;
        }

        #endregion

        #region Metodos base

        /// <summary>
        /// Recibe numero de SGL
        /// </summary>
        /// <returns></returns>        
        public IActionResult Index(int SglId)
        {
            try
            {
                //throw new Exception();
                ViewBag.Message = SglId;
                IngresarSGLDTO SGL = new IngresarSGLDTO
                {
                    Entrada = "N"
                };
                return View(SGL);
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Upsi: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");
                return RedirectToAction("Home", "Login", new {area = "Login" } );
            }

        }

        /// <summary>
        /// Recibe numero de SGL
        /// </summary>
        /// <param name="numeroSgl"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Index(IngresarSGLDTO paramAsociar)
        {
            try
            {
                throw new Exception();
                ViewBag.Message = paramAsociar.IdSGL == 0 ? paramAsociar.SglId : paramAsociar.IdSGL;
                return View(paramAsociar);
            }
            catch (Exception ex)
            {
                paramAsociar.IdSGL = 0;
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Upsi: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");
                return RedirectToAction("Index", new IngresarSGLDTO { IdSGL = 0 });
            }
        }

        /// <summary>
        /// Método que trae lista de Sucursales
        /// </summary>
        /// <param name="codigoSucursal"></param>
        /// <returns>Lista de Sucursales</returns>
        public JsonResult ListDropdownSucursales(int codigoSucursal)
        {
            //try
            //{
            BasePropertiesDTO sucursales = this._consultaSGLAPP.GetSucursalesConAbogados(codigoSucursal);

            if (sucursales != null && sucursales.Resultado.Count > 0)
            {
                List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

                Parallel.ForEach((IEnumerable<SucursalConAbogadosDTOResponse>)sucursales.Resultado, (registro) =>
                {
                    listaDTO.Add(new DropDownListDTO { id = registro.Codigo.ToString(), text = registro.OficinaPartesDescripcion + " / " + registro.Descripcion });
                });

                return Json(listaDTO);
            }
            else
            {
                return null;
            }

            //}
            //catch (Exception ex)
            //{

            //    return;
            //}
        }

        /// <summary>
        /// Método que trae lista materias
        /// </summary>
        /// <returns>Lista de materias</returns>
        public JsonResult ListDropdownMaterias()
        {
            //try
            //{
            BasePropertiesDTO materias = this._consultaSGLAPP.ObtenerMaterias();

            if (materias != null && materias.Resultado.Count > 0)
            {
                List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

                Parallel.ForEach((IEnumerable<MateriasDTOResponse>)materias.Resultado, (registro) =>
                {
                    listaDTO.Add(new DropDownListDTO { id = registro.Codigo.ToString(), text = registro.Descripcion });
                });

                return Json(listaDTO);
            }
            else
            {
                return null;
            }

            //}
            //catch (Exception ex)
            //{

            //    return;
            //}
        }

        /// <summary>
        /// Método que trae lista de Abogados
        /// </summary>
        /// <param name="codigoSucursal"></param>
        /// <returns>Lista de Abogados</returns>
        public JsonResult ListDropdownAbogado(int codigoSucursal)
        {
            //try
            //{
            BasePropertiesDTO abogados = this._consultaSGLAPP.GetAbogadosPorSucursal(codigoSucursal);

            if (abogados != null && abogados.Resultado.Count > 0)
            {
                List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

                Parallel.ForEach((IEnumerable<AbogadoDTOResponse>)abogados.Resultado, (registro) =>
                {
                    listaDTO.Add(new DropDownListDTO { id = registro.CodigoPersona.ToString(), text = registro.Nombre });
                });

                return Json(listaDTO);
            }
            else
            {
                return null;
            }

            //}
            //catch (Exception ex)
            //{

            //    return;
            //}
        }

        /// <summary>
        /// Método que trae lista de Tipos de materia
        /// </summary>
        /// <param name="materiaPadre"></param>
        /// <returns>Lista de Tipos de materia</returns>
        public JsonResult ListDropdownTipoMateria(int materiaPadre)
        {
            //try
            //{
            BasePropertiesDTO tipoMaterias = this._consultaSGLAPP.ObtenerTipoMateriaPorMateriaPadre(materiaPadre);

            if (tipoMaterias != null && tipoMaterias.Resultado.Count > 0)
            {
                List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

                Parallel.ForEach((IEnumerable<ObtenerTipoMateriaDTOResponse>)tipoMaterias.Resultado, (registro) =>
                {
                    listaDTO.Add(new DropDownListDTO { id = registro.IdMateria.ToString(), text = registro.DescripcionMateria });
                });

                return Json(listaDTO);
            }
            else
            {
                return null;
            }
            //}
            //catch (Exception ex)
            //{

            //    return;
            //}
        }

        /// <summary>
        /// Método que obtiene SGL filtrada por numero de sgl
        /// </summary>
        /// <param name="numeroSgl"></param>
        /// <returns>informacion de SGL</returns>
        public JsonResult GetSGLPorNumSgl(int numeroSgl)
        {
            //try
            //{
            dynamic data = "";

            BasePropertiesDTO Sgl = this._consultaSGLAPP.GetSGL(numeroSgl);

            if (Sgl.Resultado.NombrePersona != "na" && Sgl.Resultado.Dv != "na")
            {
                data = Sgl.Resultado;
            }

            return this.Json(new
            {
                data
            });
            //}
            //catch (Exception ex)
            //{

            //    return;
            //}
        }

        /// <summary>
        /// Método que busca Materias filtradas por codigo de materia
        /// </summary>
        /// <param name="codigoMateria"></param>
        /// <returns></returns>
        public JsonResult ListDropdownMateriasSeleccionada(int codigoMateriaPadre)
        {
            //try
            //{
            BasePropertiesDTO materias = this._consultaSGLAPP.ObtenerMateriaSeleccionada(codigoMateriaPadre);

            if (materias != null && materias.Resultado.Count > 0)
            {
                List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

                Parallel.ForEach((IEnumerable<MateriasDTOResponse>)materias.Resultado, (registro) =>
                {
                    listaDTO.Add(new DropDownListDTO { id = registro.Codigo.ToString(), text = registro.Descripcion });
                });

                return Json(listaDTO);
            }
            else
            {
                return null;
            }
            //}
            //catch (Exception ex)
            //{

            //    return;
            //}
        }

        public JsonResult ListDropdownTipoMateriaSeleccionada(int materiaId)
        {
            //try
            //{
            BasePropertiesDTO tiposMateria = this._consultaSGLAPP.ObtenerTipoMateriaSeleccionada(materiaId);

            if (tiposMateria != null && tiposMateria.Resultado.Count > 0)
            {
                List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

                Parallel.ForEach((IEnumerable<ObtenerTipoMateriaDTOResponse>)tiposMateria.Resultado, (registro) =>
                {
                    listaDTO.Add(new DropDownListDTO { id = registro.IdMateria.ToString(), text = registro.DescripcionMateria });
                });

                return Json(listaDTO);
            }
            else
            {
                return null;
            }
            //}
            //catch (Exception ex)
            //{

            //    return;
            //}
        }

        /// <summary>
        /// Método que buscar SGLs relacionadas al Rut Sociedad
        /// </summary>
        /// <param name="ModalSglxRutSociedad">Rut Sociedad</param>
        /// <returns></returns>
        public IActionResult ModalSglxRutSociedad(string ModalSglxRutSociedad)
        {
            //try
            //{
            var rutSinPuntos = ModalSglxRutSociedad.Replace(".", "");

            UsuarioConsultaDTORequest usuarioConsultaRequest = new UsuarioConsultaDTORequest
            {
                Rut = int.Parse(rutSinPuntos),
                Nombre = string.Empty,
                ApellidoPaterno = string.Empty,
                ApellidoMaterno = string.Empty,
                CantidadRegistros = 0
            };

            //Se consulta datos de persona por rut
            BasePropertiesDTO infoPersona = this._consultaSGLAPP.ConsultaPersonaDatos(usuarioConsultaRequest);
            List<SglsxRutDTO> listaSglsSelecciona = new List<SglsxRutDTO>();

            if (infoPersona != null)
            {
                //Se obtienes SGLs filtradas por codigo de persona
                BasePropertiesDTO sglsxCodigoPersona = this._consultaSGLAPP.GetSLGPorCodigoPersona(infoPersona.Resultado[0].CodigoPersona);
                if (sglsxCodigoPersona != null)
                {
                    IList<ConsultaSGLPorPersonaIdDTOResponse> listaSgls = sglsxCodigoPersona.Resultado;

                    if (listaSgls.Count > 0)
                    {
                        //Se obtiene usuario para obtener codigo de grupo
                        var usuario = HttpContext.Session.GetObjectFromJson<SessionLoginDTO>("sessionLogin");

                        LoginDTO loginDto = new LoginDTO
                        {
                            Rut = usuario.RutUsuario,
                            DigitoVerificador = usuario.DigitoVerificador
                        };

                        var grupoUsuario = this._consultaSGLAPP.GetGrupoUsuario(loginDto).Resultado;

                        if (grupoUsuario == null)
                        {
                            listaSglsSelecciona.Add(new SglsxRutDTO
                            {
                                mensajeError = "Usuario conectado no tiene Grupo Usuario asignado"
                            });
                        }
                        else
                        {
                            var codigoGrupo = grupoUsuario.GrupoPerfilId;

                            //Se obtiene Tipo Materia de cada SGL
                            foreach (ConsultaSGLPorPersonaIdDTOResponse sgl in listaSgls)
                            {
                                if (codigoGrupo == 3)
                                {
                                    if (sgl.MateriaId == 358)
                                    {
                                        BasePropertiesDTO materias = this._consultaSGLAPP.ObtenerTipoMateriaSeleccionada(sgl.MateriaId);

                                        if (materias.Resultado.Count > 0)
                                        {
                                            listaSglsSelecciona.Add(new SglsxRutDTO
                                            {
                                                SGLId = sgl.SGLId.ToString(),
                                                Fecha = sgl.FechaInicio.ToString().Substring(0, 10).Replace("-", "/"),
                                                Nombre = sgl.NombrePersona + ' ' + sgl.ApellidoPaternoPersona + ' ' + sgl.ApellidoMaternoPersona,
                                                TipoMateria = materias.Resultado[0].DescripcionMateria,
                                                Estado = sgl.DescripcionEstadoSGL
                                            });
                                        }
                                        else
                                        {
                                            listaSglsSelecciona.Add(new SglsxRutDTO
                                            {
                                                SGLId = sgl.SGLId.ToString(),
                                                Fecha = sgl.FechaInicio.ToString().Substring(0, 10).Replace("-", "/"),
                                                Nombre = sgl.NombrePersona + ' ' + sgl.ApellidoPaternoPersona + ' ' + sgl.ApellidoMaternoPersona,
                                                TipoMateria = "",
                                                Estado = sgl.DescripcionEstadoSGL
                                            });
                                        }
                                    }
                                }
                                else if (sgl.MateriaId != 358)
                                {
                                    BasePropertiesDTO materias = this._consultaSGLAPP.ObtenerTipoMateriaSeleccionada(sgl.MateriaId);

                                    if (materias.Resultado.Count > 0)
                                    {
                                        listaSglsSelecciona.Add(new SglsxRutDTO
                                        {
                                            SGLId = sgl.SGLId.ToString(),
                                            Fecha = sgl.FechaInicio.ToString().Substring(0, 10).Replace("-", "/"),
                                            Nombre = sgl.NombrePersona + ' ' + sgl.ApellidoPaternoPersona + ' ' + sgl.ApellidoMaternoPersona,
                                            TipoMateria = materias.Resultado[0].DescripcionMateria,
                                            Estado = sgl.DescripcionEstadoSGL
                                        });
                                    }
                                    else
                                    {
                                        listaSglsSelecciona.Add(new SglsxRutDTO
                                        {
                                            SGLId = sgl.SGLId.ToString(),
                                            Fecha = sgl.FechaInicio.ToString().Substring(0, 10).Replace("-", "/"),
                                            Nombre = sgl.NombrePersona + ' ' + sgl.ApellidoPaternoPersona + ' ' + sgl.ApellidoMaternoPersona,
                                            TipoMateria = "",
                                            Estado = sgl.DescripcionEstadoSGL
                                        });
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        listaSglsSelecciona.Add(new SglsxRutDTO
                        {
                            mensajeError = "No se han encontrado SGL"
                        });
                    }
                }
                else
                {
                    listaSglsSelecciona.Add(new SglsxRutDTO
                    {
                        mensajeError = "No se han encontrado SGL"
                    });
                }
            }
            else
            {
                listaSglsSelecciona.Add(new SglsxRutDTO
                {
                    mensajeError = "No se han encontrado SGL"
                });
            }

            return PartialView(listaSglsSelecciona);
            //}
            //catch (Exception ex)
            //{

            //    return;
            //}
        }

        public IActionResult BusquedaxNombreSociedad(string nombre)
        {
            //try
            //{
            BasePropertiesDTO listaClientesLegalDTOResponse = this._consultaSGLAPP.GetClienteNombreLegal(nombre);

            List<DatosSociedadDTO> listaDatosSociedadDto = new List<DatosSociedadDTO>();

            if (listaClientesLegalDTOResponse != null)
            {
                if (listaClientesLegalDTOResponse.Resultado.Count == 0)
                {
                    listaDatosSociedadDto.Add(new DatosSociedadDTO
                    {
                        mensajeError = "No se han encontrado SGL"
                    });
                }
                else
                {
                    foreach (ConsultaClienteNombreLegalDTOResponse clienteSociedad in listaClientesLegalDTOResponse.Resultado)
                    {
                        var numeroRut = clienteSociedad.Rut.ToString();
                        int cont = 0;
                        var numeroRutFormateado = "";

                        //Se formatea Rut sin digito verificador
                        for (int i = numeroRut.Length - 2; i >= 0; i--)
                        {
                            numeroRutFormateado = numeroRut.Substring(i, 1) + numeroRutFormateado;
                            cont++;
                            if (cont == 3 && i != 0)
                            {
                                numeroRutFormateado = "." + numeroRutFormateado;
                                cont = 0;
                            }
                        }

                        listaDatosSociedadDto.Add(new DatosSociedadDTO
                        {
                            Rut = numeroRutFormateado + '-' + clienteSociedad.DigitoVereficador,
                            NombreSociedad = clienteSociedad.Nombre
                        });
                    }
                }
            }
            else
            {
                listaDatosSociedadDto.Add(new DatosSociedadDTO
                {
                    mensajeError = "No se han encontrado SGL"
                });
            }

            return PartialView(listaDatosSociedadDto);
            //}
            //catch (Exception ex)
            //{

            //    return;
            //}
        }

        public IActionResult ModalBusquedaxFecha(string fechaBuscar)
        {
            //try
            //{
            List<SglsxFechaDTO> listaSglsFiltradas = new List<SglsxFechaDTO>();

            BasePropertiesDTO sglsXfecha = this._consultaSGLAPP.ObtenerListadoSGLPorFecha(fechaBuscar);

            if (sglsXfecha != null)
            {
                IList<SglDTOResponse> listaSGLs = sglsXfecha.Resultado;

                if (listaSGLs.Count > 0)
                {
                    //Se obtiene usuario para obtener codigo de grupo
                    var usuario = HttpContext.Session.GetObjectFromJson<SessionLoginDTO>("sessionLogin");

                    LoginDTO loginDto = new LoginDTO
                    {
                        Rut = usuario.RutUsuario,
                        DigitoVerificador = usuario.DigitoVerificador
                    };

                    var grupoUsuario = this._consultaSGLAPP.GetGrupoUsuario(loginDto).Resultado;

                    if (grupoUsuario == null)
                    {
                        listaSglsFiltradas.Add(new SglsxFechaDTO
                        {
                            mensajeError = "Usuario conectado no tiene Grupo Usuario asignado"
                        });
                    }
                    else
                    {
                        var codigoGrupo = grupoUsuario.GrupoPerfilId;

                        //Se obtiene Tipo Materia de cada SGL
                        foreach (SglDTOResponse sgl in listaSGLs)
                        {
                            if (codigoGrupo == 3)
                            {
                                if (sgl.MateriaId == 358)
                                {
                                    BasePropertiesDTO materias = this._consultaSGLAPP.ObtenerTipoMateriaSeleccionada(sgl.MateriaId);

                                    if (materias.Resultado.Count > 0)
                                    {
                                        listaSglsFiltradas.Add(new SglsxFechaDTO
                                        {
                                            SGLId = sgl.CodigoSGL.ToString(),
                                            Fecha = sgl.FechaInicioSGL.ToString().Substring(0, 10).Replace("-", "/"),
                                            Nombre = sgl.NombrePersonaParticipante + ' ' + sgl.ApellitoPaternoParticipante + ' ' + sgl.ApellidoMaternoParticipante,
                                            TipoMateria = materias.Resultado[0].DescripcionMateria,
                                            Estado = sgl.DescripcionEstado
                                        });
                                    }
                                    else
                                    {
                                        listaSglsFiltradas.Add(new SglsxFechaDTO
                                        {
                                            SGLId = sgl.CodigoSGL.ToString(),
                                            Fecha = sgl.FechaInicioSGL.ToString().Substring(0, 10).Replace("-", "/"),
                                            Nombre = sgl.NombrePersonaParticipante + ' ' + sgl.ApellitoPaternoParticipante + ' ' + sgl.ApellidoMaternoParticipante,
                                            TipoMateria = "",
                                            Estado = sgl.DescripcionEstado
                                        });
                                    }

                                }
                            }
                            else if (sgl.MateriaId != 358)
                            {
                                BasePropertiesDTO materias = this._consultaSGLAPP.ObtenerTipoMateriaSeleccionada(sgl.MateriaId);

                                if (materias.Resultado.Count > 0)
                                {
                                    listaSglsFiltradas.Add(new SglsxFechaDTO
                                    {
                                        SGLId = sgl.CodigoSGL.ToString(),
                                        Fecha = sgl.FechaInicioSGL.ToString().Substring(0, 10).Replace("-", "/"),
                                        Nombre = sgl.NombrePersonaParticipante + ' ' + sgl.ApellitoPaternoParticipante + ' ' + sgl.ApellidoMaternoParticipante,
                                        TipoMateria = materias.Resultado[0].DescripcionMateria,
                                        Estado = sgl.DescripcionEstado
                                    });
                                }
                                else
                                {
                                    listaSglsFiltradas.Add(new SglsxFechaDTO
                                    {
                                        SGLId = sgl.CodigoSGL.ToString(),
                                        Fecha = sgl.FechaInicioSGL.ToString().Substring(0, 10).Replace("-", "/"),
                                        Nombre = sgl.NombrePersonaParticipante + ' ' + sgl.ApellitoPaternoParticipante + ' ' + sgl.ApellidoMaternoParticipante,
                                        TipoMateria = "",
                                        Estado = sgl.DescripcionEstado
                                    });
                                }
                            }
                            //else
                            //{
                            //    listaSglsFiltradas.Add(new SglsxFechaDTO
                            //    {
                            //        SGLId = "",
                            //        Fecha = ""
                            //    });
                            //}
                        }

                        if (listaSglsFiltradas.Count == 0)
                        {
                            listaSglsFiltradas.Add(new SglsxFechaDTO
                            {
                                mensajeError = "No se han encontrado SGL"
                            });
                        }

                    }
                }
                else
                {
                    listaSglsFiltradas.Add(new SglsxFechaDTO
                    {
                        mensajeError = "No se han encontrado SGL"
                    });
                }
            }
            else
            {
                listaSglsFiltradas.Add(new SglsxFechaDTO
                {
                    mensajeError = "No se han encontrado SGL"
                });
            }

            return PartialView(listaSglsFiltradas);
            //}
            //catch (Exception ex)
            //{

            //    return;
            //}
        }

        public IActionResult ModalSeguimientoConsultarSGL(string sgl)
        {
            //try
            //{
            BasePropertiesDTO SGL = this._consultaSGLAPP.GetSGLPorCodigoSGL(sgl);
            ListadoSGLDTO infoSgl = null;

            if (SGL.Resultado.Count > 0)
            {
                infoSgl = SGL.Resultado[0];
            }
            else
            {
                infoSgl = new ListadoSGLDTO()
                {
                    SglId = 0,
                    Dv = "",
                    DvPersonaSolicitante = ""
                };
            }

            return PartialView(infoSgl);
            //}
            //catch (Exception ex)
            //{

            //    return;
            //}
        }

        public JsonResult ObtenerHistorialSGL(int codigoSGL)
        {
            //try
            //{
            BasePropertiesDTO historialSGL = this._consultaSGLAPP.ObtenerHistorialSGL(codigoSGL);

            var data = historialSGL.Resultado;

            return this.Json(new
            {
                data
            });
            //}
            //catch (Exception ex)
            //{

            //    return;
            //}
        }

        #endregion
    }
}