﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CL.Scotiabank.LegalPartner.CabeceraSglAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.ConsultaAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.APP;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace CL.Scotiabank.LegalPartner.SGL.Web.Areas.CabeceraSGL.Controllers
{
    public class ListadoSGLController : Controller
    {
        [ActionsFilter]
        #region Members

        private IListadoSGLAPP _listadoSGLAPP;

        #endregion

        #region Constructor

        public ListadoSGLController(IListadoSGLAPP listadoSGLAPP)
        {
            this._listadoSGLAPP = listadoSGLAPP;
        }

        #endregion

        #region Metodos base

        public IActionResult Index()
        {
            ListadoSGLDTO ObtCantSGL = new ListadoSGLDTO
            {
                Usuario = HttpContext.Session.GetObjectFromJson<SessionLoginDTO>("sessionLogin").CodigoUsuario
            };
            return View(ObtCantSGL);
        }

        public JsonResult ListDropdownUsuario(int codigoUsuario)
        {
            BasePropertiesDTO usuarios = this._listadoSGLAPP.GetUsuarios(codigoUsuario);

            List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

            Parallel.ForEach((IEnumerable<UsuarioDTOResponse>)usuarios.Resultado, (registro) =>
            {
                listaDTO.Add(new DropDownListDTO { id = registro.CodigoPersona.ToString(), text = registro.Nombre });
            });

            return Json(listaDTO);
        }

        public JsonResult ListDropdownSucursales(int codigoSucursal)
        {
            BasePropertiesDTO sucursales = this._listadoSGLAPP.GetSucursalesConAbogados(codigoSucursal);

            List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

            Parallel.ForEach((IEnumerable<SucursalConAbogadosDTOResponse>)sucursales.Resultado, (registro) =>
            {
                listaDTO.Add(new DropDownListDTO { id = registro.Codigo.ToString(), text = registro.OficinaPartesDescripcion + " / "+registro.Descripcion });
            });

            return Json(listaDTO);
        }

        public JsonResult ListDropdownAbogados(int codigoSucursal)
        {
            BasePropertiesDTO abogados = this._listadoSGLAPP.GetAbogadosPorSucursal(codigoSucursal);

            List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

            Parallel.ForEach((IEnumerable<AbogadoDTOResponse>)abogados.Resultado, (registro) =>
            {
                listaDTO.Add(new DropDownListDTO { id = registro.CodigoPersona.ToString(), text = registro.Nombre });
            });

            return Json(listaDTO);
        }

        public JsonResult ListDropdownEstados()
        {
            BasePropertiesDTO estados = this._listadoSGLAPP.GetListadoEstadosSGL();

            List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

            Parallel.ForEach((IEnumerable<EstadoSGLDTOResponse>)estados.Resultado, (registro) =>
            {
                switch (registro.IdEstadoSGL)
                {
                    case "1":
                    case "12": 
                    case "13":
                    case "14":
                    case "15":
                    case "16":
                    case "17":
                    case "20":
                    case "26":
                    case "27":
                    case "103":
                    case "104":
                    case "170":
                        listaDTO.Add(new DropDownListDTO { id = registro.IdEstadoSGL.ToString(), text = registro.DescripcionEstadoSGL });
                    break;
                }

            });

            return Json(listaDTO);
        }

        [HttpPost]
        public JsonResult BuscarCantidadSGLs(ObtenerCantidadSGLDTORequest listadoSglDTO)
        {
            BasePropertiesDTO cantidadSGL = this._listadoSGLAPP.ObtenerCantidadSGL(listadoSglDTO);

            if (cantidadSGL == null)
            {
                return Json("Na");
            }
            else
            {
                return Json(cantidadSGL.Resultado[0].Cantidad);
            }
        }

        public JsonResult ListDropdownUsuarioPorSucursal(string idSucursal)
        {
            BasePropertiesDTO estados = this._listadoSGLAPP.GetListadoUsuarioPorSucursal(idSucursal);

            List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

            Parallel.ForEach((IEnumerable<ObtenerUsuariosXsucursalDTOResponse>)estados.Resultado, (registro) =>
            {
                listaDTO.Add(new DropDownListDTO { id = registro.CodigoPersona.ToString(), text = registro.NombrePersona });
            });

            return Json(listaDTO);
        }

        [HttpPost]
        public JsonResult BuscarSGLs(ObtenerTodosSGLDTORequest listadoSglDTO)
        {
            BasePropertiesDTO listaSGLs = this._listadoSGLAPP.ObtenerTodasSGL(listadoSglDTO);

            return this.Json(new
            {
                data = listaSGLs.Resultado
            });
        }


        public IActionResult ModalSeguimiento(string sgl)
        {
            BasePropertiesDTO SGL = this._listadoSGLAPP.GetSGLPorCodigoSGL(sgl);

            return PartialView((ListadoSGLDTO)SGL.Resultado[0]);
        }

        public JsonResult ObtenerHistorial(int codigoSGL)
        {
            BasePropertiesDTO historialSGL = this._listadoSGLAPP.ObtenerHistorialSGL(codigoSGL);

            var data = historialSGL.Resultado;

            return this.Json(new
            {
                data 
            });
        }

        #endregion


    }
}