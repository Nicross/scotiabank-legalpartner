﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CL.Scotiabank.LegalPartner.CabeceraSglAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.APP;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;

namespace CL.Scotiabank.LegalPartner.SGL.Web.Areas.CabeceraSGL.Controllers
{
    public class CrearSGLController : Controller
    {
        #region Members

        string baseUrlBuscarCliente = Resources.Consulta;

        private ICabeceraSGLAPP _CabeceraSGLAPP;

        #endregion

        #region Constructor

        public CrearSGLController(ICabeceraSGLAPP CabeceraSGLAPP)
        {
            this._CabeceraSGLAPP = CabeceraSGLAPP;
        }

        #endregion

        #region Metodos

        public IActionResult Index()
        {
            IngresarSGLDTO SGL = new IngresarSGLDTO();
            return View(SGL);
        }

        public JsonResult ComboSucursales(int codigoSucursal)
        {
            BasePropertiesDTO sucursales = this._CabeceraSGLAPP.ObtenerSucursales(codigoSucursal);

            List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

            Parallel.ForEach((IEnumerable<SucursalConAbogadosDTOResponse>)sucursales.Resultado, (registro) =>
            {
                listaDTO.Add(new DropDownListDTO { id = registro.OficinaPartesCodigo.ToString() + ":" + registro.Codigo.ToString(),
                                                    text = registro.OficinaPartesDescripcion + "/" + registro.Descripcion });
            });

            return Json(listaDTO);
        }

        public JsonResult ComboMaterias(int codigoSucursal)
        {
            BasePropertiesDTO sucursales = this._CabeceraSGLAPP.ObtenerMaterias();

            List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

            Parallel.ForEach((IEnumerable<MateriasDTOResponse>)sucursales.Resultado, (registro) =>
            {
                listaDTO.Add(new DropDownListDTO { id = registro.Codigo.ToString(), text = registro.Descripcion });
            });

            return Json(listaDTO);
        }

        public JsonResult ComboAbogadosPorSucursal(int codigoSucursal)
        {
            BasePropertiesDTO sucursales = this._CabeceraSGLAPP.ObtenerAbogadoPorSucursal(codigoSucursal);

            List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

            Parallel.ForEach((IEnumerable<AbogadoDTOResponse>)sucursales.Resultado, (registro) =>
            {
                listaDTO.Add(new DropDownListDTO { id = registro.CodigoPersona.ToString(), text = registro.Nombre });
            });

            return Json(listaDTO);
        }

        public JsonResult CombotipoMaterias(int IdMateriapadre)
        {
            BasePropertiesDTO sucursales = this._CabeceraSGLAPP.ObtenerTipoMateriaPorPadre(IdMateriapadre);

            List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

            Parallel.ForEach((IEnumerable<ObtenerTipoMateriaDTOResponse>)sucursales.Resultado, (registro) =>
            {
                listaDTO.Add(new DropDownListDTO { id = registro.IdMateria.ToString(), text = registro.DescripcionMateria });
            });

            return Json(listaDTO);
        }

        public JsonResult DatosUsuario()
        {

            SessionLoginDTO session = HttpContext.Session.GetObjectFromJson<SessionLoginDTO>("sessionLogin");

            BasePropertiesDTO DatosUsuario = this._CabeceraSGLAPP.ObtenerDatosUsuario(session.CodigoUsuario);

            return this.Json(new
            {
                data = DatosUsuario.Resultado
            });
        }

        
        public JsonResult IngresarSGL(IngresarSGLDTO SGL)
        {
            SessionLoginDTO session = HttpContext.Session.GetObjectFromJson<SessionLoginDTO>("sessionLogin");

            SGL.CodigoUsuario = session.CodigoUsuario;

            BasePropertiesDTO ReturnEntity = this._CabeceraSGLAPP.IngresarSGL(SGL);
            
            int numeroSGL = ReturnEntity.Resultado.IdSGL;

            //return Redirect("~/CabeceraSGL/AsociarSociedadSGL/Index");
            //return RedirectToAction("Index", "AsociarSociedadSGL", new { IdSgl = numeroSGL }); 

            return Json(new
            {
                IdSgl = numeroSGL
            });
        }

        #endregion
    }
}