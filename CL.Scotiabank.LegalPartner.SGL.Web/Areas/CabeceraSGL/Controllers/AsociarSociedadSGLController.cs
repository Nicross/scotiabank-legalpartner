﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CL.Scotiabank.LegalPartner.ConsultaAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.APP;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace CL.Scotiabank.LegalPartner.SGL.Web.Areas.CabeceraSGL.Controllers
{
    public class AsociarSociedadSGLController : Controller
    {
        #region Members

        private readonly string baseUrlBuscarCliente = Resources.Consulta;

        private readonly ICabeceraSGLAPP _CabeceraSGLAPP;
        private readonly IConsultarSGLAPP _consultaSGLAPP;

        #endregion

        #region Constructor

        public AsociarSociedadSGLController(ICabeceraSGLAPP CabeceraSGLAPP, IConsultarSGLAPP consultarSGLAPP)
        {
            this._CabeceraSGLAPP = CabeceraSGLAPP;
            this._consultaSGLAPP = consultarSGLAPP;
        }

        #endregion
        [HttpPost]
        public IActionResult Index(IngresarSGLDTO SGL)
        {   
            var usuario = HttpContext.Session.GetObjectFromJson<SessionLoginDTO>("sessionLogin");

            LoginDTO loginDto = new LoginDTO
            {
                Rut = usuario.RutUsuario,
                DigitoVerificador = usuario.DigitoVerificador
            };

            var codigoGrupo = this._consultaSGLAPP.GetGrupoUsuario(loginDto).Resultado.GrupoPerfilId;
            ViewData["Grupo"] = codigoGrupo;
            ViewData["NumeroSGL"] = SGL.IdSGL;

            if (SGL.Rut != null || SGL.Nombre != null)
            {
                string[] rut = SGL.Rut.Replace(".", "").Split('-');
                BasePropertiesDTO Persona = this._CabeceraSGLAPP.BusquedaPorRut(int.Parse(rut[0]));
                BasePropertiesDTO sucursales = this._CabeceraSGLAPP.ObtenerListaTipoPersona();

                string tipoPerson = ((IEnumerable<TipoPersonaDTOResponse>)sucursales.Resultado).
                    Where(x => x.Codigo == Persona.Resultado[0].CodigoTipoPersona).FirstOrDefault().Descripcion;

                ViewBag.PersonaJuridica = tipoPerson;

                return View(SGL);
            }
            SGL.Rut = "";
            return View(SGL);
        }

        //public IActionResult Index()
        //{

        //    return View();
        //}

        public JsonResult ComboTipoPersona()
        {
            BasePropertiesDTO sucursales = this._CabeceraSGLAPP.ObtenerListaTipoPersona();
            IEnumerable<TipoPersonaDTOResponse> query = sucursales.Resultado;
            IEnumerable<TipoPersonaDTOResponse> ListaOrdenada = query.OrderBy(TipoPersona => TipoPersona.Descripcion);
            List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

            Parallel.ForEach(ListaOrdenada, (registro) =>
            {
                listaDTO.Add(new DropDownListDTO { id = registro.Codigo, text = registro.Descripcion });
            });

            return Json(listaDTO);
        }

        public JsonResult BusquedaPorRut(int rut)
        {
            if (rut.Equals(null))
            {
                return Json(0);
            }

            BasePropertiesDTO Persona = this._CabeceraSGLAPP.BusquedaPorRut(rut);
            if (Persona.Resultado.Count == 0)
            {
                return null;
            }

            return Json(Persona.Resultado);
        }

        /// <summary>
        /// Redirige a la pantalla de entrada completa y asocia la SGL a la sociedad ingresada.
        /// </summary>
        /// <param name="IdSGL">Id de la SGL.</param>
        /// <param name="IdSociedad">Id de la sociedad</param>
        /// <returns></returns>
        public ActionResult EntradaCompleta(int IdSGL, int IdSociedad, string Rut)
        {
            BasePropertiesDTO ReturnEntity = this._CabeceraSGLAPP.AsociarSociedadSGL(IdSGL,IdSociedad);

            //if (ReturnEntity.CodigoRetorno == 0)
            //{
            //    IngresarSGLDTO entrada = new IngresarSGLDTO { IdSGL = IdSGL, Rut = Rut };

            //    return RedirectToAction("Index", "EntradaCompleta", entrada); // 
            //}
            

            return View(); // cambiar a un error
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdSGL"></param>
        /// <param name="IdSociedad"></param>
        /// <returns></returns>
        public ActionResult Asociar(int IdSGL, int IdSociedad)
        {
            BasePropertiesDTO ReturnEntity = this._CabeceraSGLAPP.AsociarSociedadSGL(IdSGL, IdSociedad);
                        
            return View();
        }

        #region Eliminar SGL

        /// <summary>
        /// Elimina SGL Logicamente.
        /// </summary>
        /// <param name="idSgl"></param>
        /// <returns></returns>
        public JsonResult EliminaSGLPorSGLid(int idSgl)
        {
            int codigoUsuario = HttpContext.Session.GetObjectFromJson<SessionLoginDTO>("sessionLogin").CodigoUsuario;

            BasePropertiesDTO EliminacionSGL = this._CabeceraSGLAPP.ActualizarEstadoSGL(idSgl, codigoUsuario);

            return Json(EliminacionSGL.CuerpoRetorno);
        }

        #endregion

        #region CambiarSociedad

        /// <summary>
        /// Metodo orquestado para Obtener Lista 
        /// .0
        /// </summary>
        public JsonResult ListDropdownPerfil()
        {
            BasePropertiesDTO perfilRetorno = _CabeceraSGLAPP.ObtenerListaTipoPersona();
            List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

            Parallel.ForEach((IEnumerable<UsuarioDTO>)perfilRetorno.Resultado, (registro) =>
            {
                listaDTO.Add(new DropDownListDTO { id = Convert.ToString(registro.CodigoPerfil), text = registro.DescripcionPerfil });
            });

            return Json(listaDTO);
        }

        /// <summary>
        /// Metodo orquestado para Obtener Persona y datos de SGL para crear un objeto 
        /// </summary>
        public ActionResult CambiarSociedad(int rut, int idSgl, string nombre)
        {
            if (nombre == null)
            {
                nombre = "";
            }

            BasePropertiesDTO Persona = this._CabeceraSGLAPP.BusquedaPorRut(rut);
            BasePropertiesDTO sgl = this._consultaSGLAPP.GetSGLPorCodigoSGL(Convert.ToString(idSgl));

            IngresarSGLDTO sglDTO = new IngresarSGLDTO
            {
                Nombre = nombre.Trim(),
                CodigoPersona = Persona.Resultado[0].CodigoPersona,
                IdSGL = idSgl,
                Rut = Convert.ToString(sgl.Resultado[0].Rut),
                Dv = sgl.Resultado[0].Dv,
                RutPersonaAbogado = sgl.Resultado[0].RutPersonaAbogado,
                IdTipoPersona = Convert.ToInt32(Persona.Resultado[0].CodigoTipoPersona),

            };

            return View(sglDTO);
        }

        ///// <summary>
        ///// Metodo orquestado para Cambiar Persona Juridica
        ///// </summary>   
        public JsonResult CambioSociedad(IngresarSGLDTO sglDTO) {

            if (sglDTO == null)
            {
                return Json(0);
            }
            sglDTO.RutAbogado = sglDTO.RutPersonaAbogado;

            if (sglDTO.NombreActual != null)
            {
                sglDTO.NombreActual = sglDTO.NombreActual.TrimStart();
                sglDTO.NombreActual = sglDTO.NombreActual.TrimEnd();
            }

            if (sglDTO.VariableRespaldo == 1)
            {
                BasePropertiesDTO respaldoRetorno = this._CabeceraSGLAPP.RespaldarNombreAnterior(sglDTO);
            }

            sglDTO.ApellidoPaterno = "";
            sglDTO.ApellidoMaterno = "";
            sglDTO.Nombre = sglDTO.Nombre.TrimStart();
            sglDTO.Nombre = sglDTO.Nombre.TrimEnd();
               
            BasePropertiesDTO actualizarPersonaRetorno = this._CabeceraSGLAPP.ActualizarPersona(sglDTO);

            if (actualizarPersonaRetorno == null)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje de Advertencia";
                @TempData["CuerpoMensaje"] = "No se efectuó el cambio de Sociedad.";
            }
            else if (actualizarPersonaRetorno.CodigoRetorno == (int)MantenedoresEstados.Exitoso)
            {
                @TempData["Exito"] = true;
                @TempData["TituloMensaje"] = "Actualización Exitosa";
                @TempData["CuerpoMensaje"] = "Se ha actualizado el registro exitosamente.";
            }
            else if (actualizarPersonaRetorno.CodigoRetorno == (int)MantenedoresEstados.ErrorInesperado)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Actualización Errónea";
                @TempData["CuerpoMensaje"] = "Se ha detectado un error Inesperado.";
            }

            sglDTO.Rut = sglDTO.Rut.Replace(".", "");
            sglDTO.Rut = sglDTO.Rut.Split('-')[0];
            int rut = Convert.ToInt32(sglDTO.Rut);
           
            BasePropertiesDTO Persona = this._CabeceraSGLAPP.BusquedaPorRut(rut);
                      
            if (Persona.Resultado.Count == 0)
            {
                return null;
            }
            return Json(Persona.Resultado);            
        }
        #endregion
    }
}