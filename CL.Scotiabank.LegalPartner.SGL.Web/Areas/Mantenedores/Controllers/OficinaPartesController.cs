﻿using CL.Scotiabank.LegalPartner.SGL.APP;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CL.Scotiabank.LegalPartner.SGL.Web.Areas.Mantenedores.Controllers
{
    [ActionsFilter]
    public class OficinaPartesController : Controller
    {
        #region Members
        
        private readonly string baseUriOficinaPartess = Resources.OficinaPartesUrl;
        private dynamic logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
        private IOficinaPartesAPP _oficinaPartesAPP;
        
        #endregion

        #region Constructor

        public OficinaPartesController(IOficinaPartesAPP oficinaPartesAPP)
        {
            this._oficinaPartesAPP = oficinaPartesAPP;
        }

        #endregion

        public IActionResult Index()
        {
            try
            {
                return View();

            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

        public JsonResult List()
        {
            try
            {
                BasePropertiesDTO oficinaPartesRetorno = this._oficinaPartesAPP.Get();

                return this.Json(new
                {
                    data = oficinaPartesRetorno.Resultado
                });

            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return null;
            }
        }

        public JsonResult ListDropdown()
        {
            try
            {
                BasePropertiesDTO oficinaPartesRetorno = _oficinaPartesAPP.Get();

                List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

                Parallel.ForEach((IEnumerable<OficinaPartesDTO>)oficinaPartesRetorno.Resultado, (registro) => 
                {
                    listaDTO.Add(new DropDownListDTO { id = registro.Codigo.ToString(), text = registro.Descripcion });
                });

                return Json(listaDTO);

            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return null;
            }
        }

        public IActionResult Create()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public IActionResult Create(OficinaPartesDTO oficinaPartesDTO)
        {
            try
            {
                BasePropertiesDTO oficinaPartesRetorno = this._oficinaPartesAPP.Create(oficinaPartesDTO);

                if (oficinaPartesRetorno != null)
                {
                    int codigoRetorno = oficinaPartesRetorno.CodigoRetorno;

                    if (codigoRetorno == (int)MantenedoresEstados.Exitoso)
                    {
                        @TempData["Exito"] = true;
                        @TempData["TituloMensaje"] = "Creación Exitosa";
                        @TempData["CuerpoMensaje"] = "Se ha creado el registro exitosamente.";
                        return RedirectToAction("Index");
                    }
                    else if (codigoRetorno == (int)MantenedoresEstados.RegistroExistente)
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error - Registro Existente";
                        @TempData["CuerpoMensaje"] = "El registro que está intentando crear ya existe, intente con otra descripción.";
                    }
                    else
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error";
                        @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                    }
                }
                else
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error";
                    @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                }

                return View(oficinaPartesDTO);
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

        public IActionResult Details(int id)
        {
            try
            {
                BasePropertiesDTO oficinaPartes = this._oficinaPartesAPP.Get(id);

                return PartialView((OficinaPartesDTO)oficinaPartes.Resultado);
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

        public IActionResult Edit(int id)
        {
            try
            {
                BasePropertiesDTO oficinaPartes = this._oficinaPartesAPP.Get(id);
                return View((OficinaPartesDTO)oficinaPartes.Resultado);
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public IActionResult Edit(OficinaPartesDTO oficinaPartesDTO)
        {
            try
            {
                BasePropertiesDTO oficinaPartesRetorno = this._oficinaPartesAPP.Edit(oficinaPartesDTO);

                if (oficinaPartesRetorno != null)
                {
                    int codigoRetorno = oficinaPartesRetorno.CodigoRetorno;


                    if (codigoRetorno == (int)MantenedoresEstados.Exitoso)
                    {
                        @TempData["Exito"] = true;
                        @TempData["TituloMensaje"] = "Edición Exitosa";
                        @TempData["CuerpoMensaje"] = "Se ha editado el registro exitosamente.";
                        return RedirectToAction("Index");
                    }
                    else if (codigoRetorno == (int)MantenedoresEstados.RegistroNoExistente)
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error - Edición de Registro No Existente";
                        @TempData["CuerpoMensaje"] = "El registro que está intentando editar no existe, intente con otro.";
                    }
                    else
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error";
                        @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                    }
                }
                else
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error";
                    @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                }

                return View(oficinaPartesDTO);            
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }
        
        public IActionResult Delete(int id)
        {
            try
            {
                BasePropertiesDTO oficinaPartes = this._oficinaPartesAPP.Get(id);

                return View((OficinaPartesDTO)oficinaPartes.Resultado);
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public IActionResult Delete(OficinaPartesDTO oficinaPartesDTO)
        {
            try
            {
                BasePropertiesDTO oficinaPartesRetorno = this._oficinaPartesAPP.Delete(oficinaPartesDTO);

                if (oficinaPartesRetorno != null)
                {
                    int codigoRetorno = oficinaPartesRetorno.CodigoRetorno;

                    if (codigoRetorno == (int)MantenedoresEstados.Exitoso)
                    {
                        @TempData["Exito"] = true;
                        @TempData["TituloMensaje"] = "Eliminación Exitosa";
                        @TempData["CuerpoMensaje"] = "Se ha eliminado el registro exitosamente.";
                        return RedirectToAction("Index");
                    }
                    else if (codigoRetorno == (int)MantenedoresEstados.RegistroNoExistente)
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error - Eliminación de Registro No Existente";
                        @TempData["CuerpoMensaje"] = "El registro que está intentando eliminar no existe, intente con otro.";
                    }
                    else if (codigoRetorno == (int)MantenedoresEstados.RegistroUtilizado)
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error - Registro Utilizado por Tabla";
                        @TempData["CuerpoMensaje"] = "No se ha podido eliminar el registro, porque está siendo utilizado por otra tabla.";
                    }
                    else
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error";
                        @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                    }
                }
                else
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error";
                    @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                }

                return View(oficinaPartesDTO);

            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }
    }
}