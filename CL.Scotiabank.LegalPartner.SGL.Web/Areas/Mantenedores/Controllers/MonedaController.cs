﻿using CL.Scotiabank.LegalPartner.SGL.APP;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CL.Scotiabank.LegalPartner.SGL.Web.Areas.Mantenedores.Controllers
{
    [ActionsFilter]
    public class MonedaController : Controller
    {
        #region Members

        private readonly string baseUriMonedas = Resources.MonedaUrl;
        private dynamic logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

        private readonly IMonedaAPP _monedaAPP;
        
        #endregion

        #region Constructor

        public MonedaController(IMonedaAPP monedaAPP)
        {
            this._monedaAPP = monedaAPP;
        }

        #endregion

        public IActionResult Index()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

        public JsonResult List()
        {
            try
            {
                BasePropertiesDTO monedaRetorno = this._monedaAPP.Get();

                return this.Json(new
                {
                    data = monedaRetorno.Resultado
                });
              
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return null;
            }
        }
        

        public IActionResult Create()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public IActionResult Create(MonedaDTO monedaDTO)
        {
            try
            {
                BasePropertiesDTO monedaRetorno = this._monedaAPP.Create(monedaDTO);

                if (monedaRetorno != null)
                {
                    int codigoRetorno = monedaRetorno.CodigoRetorno;

                    if (codigoRetorno == (int)MantenedoresEstados.Exitoso)
                    {
                        @TempData["Exito"] = true;
                        @TempData["TituloMensaje"] = "Creación Exitosa";
                        @TempData["CuerpoMensaje"] = "Se ha creado el registro exitosamente.";
                        return RedirectToAction("Index");
                    }
                    else if (codigoRetorno == (int)MantenedoresEstados.RegistroExistente)
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error - Registro Existente";
                        @TempData["CuerpoMensaje"] = "El registro que está intentando crear ya existe, intente con otra descripción.";
                    }
                    else
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error";
                        @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                    }
                }
                else
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error";
                    @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                }

                return View(monedaDTO);
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

        public IActionResult Details(int id)
        {
            try
            {
                BasePropertiesDTO moneda = this._monedaAPP.Get(id);

                return PartialView((MonedaDTO)moneda.Resultado);
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

        public IActionResult Edit(int id)
        {
            try
            {
                BasePropertiesDTO moneda = this._monedaAPP.Get(id);

                return View((MonedaDTO)moneda.Resultado);
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public IActionResult Edit(MonedaDTO monedaDTO)
        {
            try
            {
                BasePropertiesDTO monedaRetorno = this._monedaAPP.Edit(monedaDTO);

                if (monedaRetorno != null)
                {
                    int codigoRetorno = monedaRetorno.CodigoRetorno;


                    if (codigoRetorno == (int)MantenedoresEstados.Exitoso)
                    {
                        @TempData["Exito"] = true;
                        @TempData["TituloMensaje"] = "Edición Exitosa";
                        @TempData["CuerpoMensaje"] = "Se ha editado el registro exitosamente.";
                        return RedirectToAction("Index");
                    }
                    else if (codigoRetorno == (int)MantenedoresEstados.RegistroNoExistente)
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error - Edición de Registro No Existente";
                        @TempData["CuerpoMensaje"] = "El registro que está intentando editar no existe, intente con otro.";
                    }
                    else
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error";
                        @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                    }
                }
                else
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error";
                    @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                }

                return View(monedaDTO);            
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }
        
        public IActionResult Delete(int id)
        {
            try
            {
                BasePropertiesDTO moneda = this._monedaAPP.Get(id);
                return View((MonedaDTO)moneda.Resultado);
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public IActionResult Delete(MonedaDTO monedaDTO)
        {
            try
            {
                BasePropertiesDTO monedaRetorno = this._monedaAPP.Delete(monedaDTO);

                if (monedaRetorno != null)
                {
                    int codigoRetorno = monedaRetorno.CodigoRetorno;

                    if (codigoRetorno == (int)MantenedoresEstados.Exitoso)
                    {
                        @TempData["Exito"] = true;
                        @TempData["TituloMensaje"] = "Eliminación Exitosa";
                        @TempData["CuerpoMensaje"] = "Se ha eliminado el registro exitosamente.";
                        return RedirectToAction("Index");
                    }
                    else if (codigoRetorno == (int)MantenedoresEstados.RegistroNoExistente)
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error - Eliminación de Registro No Existente";
                        @TempData["CuerpoMensaje"] = "El registro que está intentando eliminar no existe, intente con otro.";
                    }
                    else if (codigoRetorno == (int)MantenedoresEstados.RegistroUtilizado)
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error - Registro Utilizado por Tabla";
                        @TempData["CuerpoMensaje"] = "No se ha podido eliminar el registro, porque está siendo utilizado por otra tabla.";
                    }
                    else
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error";
                        @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                    }
                }
                else
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error";
                    @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                }

                return View(monedaDTO);

            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }
    }
}