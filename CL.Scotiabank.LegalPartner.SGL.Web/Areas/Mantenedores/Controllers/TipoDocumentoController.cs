﻿using CL.Scotiabank.LegalPartner.SGL.APP;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CL.Scotiabank.LegalPartner.SGL.Web.Areas.Mantenedores.Controllers
{
    public class TipoDocumentoController : Controller
    {
        [ActionsFilter]
        #region Members

        private readonly string baseUriTiposDocumentos = Resources.TipoDocumentoUrl;
        private dynamic logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
        private readonly ITipoDocumentoAPP _tipoDocumentoAPP;
        
        #endregion

        #region Constructor

        public TipoDocumentoController(ITipoDocumentoAPP tipoDocumentoAPP)
        {
            this._tipoDocumentoAPP = tipoDocumentoAPP;
        }

        #endregion

        public IActionResult Index()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

        public JsonResult List()
        {
            try
            {
                BasePropertiesDTO tipoDocumentoRetorno = this._tipoDocumentoAPP.Get();

                return this.Json(new
                {
                    data = tipoDocumentoRetorno.Resultado
                });               

            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return null;
            }
        }
        

        public IActionResult Create()
        {
            try
            {
                return View();

            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public IActionResult Create(TipoDocumentoDTO tipoDocumentoDTO)
        {
            try
            {
                
                BasePropertiesDTO tipoDocumentoRetorno = this._tipoDocumentoAPP.Create(tipoDocumentoDTO);

                if (tipoDocumentoRetorno != null)
                {
                    int codigoRetorno = tipoDocumentoRetorno.CodigoRetorno;

                    if (codigoRetorno == (int)MantenedoresEstados.Exitoso)
                    {
                        @TempData["Exito"] = true;
                        @TempData["TituloMensaje"] = "Creación Exitosa";
                        @TempData["CuerpoMensaje"] = "Se ha creado el registro exitosamente.";
                        return RedirectToAction("Index");
                    }
                    else if (codigoRetorno == (int)MantenedoresEstados.RegistroExistente)
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error - Registro Existente";
                        @TempData["CuerpoMensaje"] = "El registro que está intentando crear ya existe, intente con otra descripción.";
                    }
                    else
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error";
                        @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                    }
                }
                else
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error";
                    @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                }

                return View(tipoDocumentoDTO);

            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

        public IActionResult Details(int id)
        {
            try
            {
                BasePropertiesDTO tipoDocumento = this._tipoDocumentoAPP.Get(id);
                return PartialView((TipoDocumentoDTO)tipoDocumento.Resultado);                
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

        public IActionResult Edit(int id)
        {
            try
            {
                BasePropertiesDTO tipoDocumento = this._tipoDocumentoAPP.Get(id);
                return View((TipoDocumentoDTO)tipoDocumento.Resultado);              
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public IActionResult Edit(TipoDocumentoDTO tipoDocumentoDTO)
        {
            try
            {
                BasePropertiesDTO tipoDocumentoRetorno = this._tipoDocumentoAPP.Edit(tipoDocumentoDTO);

                if (tipoDocumentoRetorno != null)
                {
                    int codigoRetorno = tipoDocumentoRetorno.CodigoRetorno;


                    if (codigoRetorno == (int)MantenedoresEstados.Exitoso)
                    {
                        @TempData["Exito"] = true;
                        @TempData["TituloMensaje"] = "Edición Exitosa";
                        @TempData["CuerpoMensaje"] = "Se ha editado el registro exitosamente.";
                        return RedirectToAction("Index");
                    }
                    else if (codigoRetorno == (int)MantenedoresEstados.RegistroNoExistente)
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error - Edición de Registro No Existente";
                        @TempData["CuerpoMensaje"] = "El registro que está intentando editar no existe, intente con otro.";
                    }
                    else
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error";
                        @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                    }
                }
                else
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error";
                    @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                }

                return View(tipoDocumentoDTO);                            
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }
        
        public IActionResult Delete(int id)
        {
            try
            {
                BasePropertiesDTO tipoDocumento = this._tipoDocumentoAPP.Get(id);
                return View((TipoDocumentoDTO)tipoDocumento.Resultado);                

            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public IActionResult Delete(TipoDocumentoDTO tipoDocumentoDTO)
        {
            try
            {
                BasePropertiesDTO tipoDocumentoRetorno = this._tipoDocumentoAPP.Delete(tipoDocumentoDTO);

                if (tipoDocumentoRetorno != null)
                {
                    int codigoRetorno = tipoDocumentoRetorno.CodigoRetorno;

                    if (codigoRetorno == (int)MantenedoresEstados.Exitoso)
                    {
                        @TempData["Exito"] = true;
                        @TempData["TituloMensaje"] = "Eliminación Exitosa";
                        @TempData["CuerpoMensaje"] = "Se ha eliminado el registro exitosamente.";
                        return RedirectToAction("Index");
                    }
                    else if (codigoRetorno == (int)MantenedoresEstados.RegistroNoExistente)
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error - Eliminación de Registro No Existente";
                        @TempData["CuerpoMensaje"] = "El registro que está intentando eliminar no existe, intente con otro.";
                    }
                    else if (codigoRetorno == (int)MantenedoresEstados.RegistroUtilizado)
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error - Registro Utilizado por Tabla";
                        @TempData["CuerpoMensaje"] = "No se ha podido eliminar el registro, porque está siendo utilizado por otra tabla.";
                    }
                    else
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error";
                        @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                    }
                }
                else
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error";
                    @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                }

                return View(tipoDocumentoDTO);                
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

    }
}