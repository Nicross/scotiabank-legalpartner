﻿using CL.Scotiabank.LegalPartner.SGL.APP;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Mvc;
using System;

namespace CL.Scotiabank.LegalPartner.SGL.Web.Areas.Mantenedores.Controllers
{
    [ActionsFilter]
    public class ConservadorController : Controller
    {
        #region Members
        
        private readonly string baseUriConservadores = Resources.ConservadorUrl;
        private dynamic logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

        private readonly IConservadorAPP _conservadorAPP;
        
        #endregion

        #region Constructor

        public ConservadorController(IConservadorAPP conservadorAPP)
        {
            this._conservadorAPP = conservadorAPP;
        }

        #endregion

        //[ActionsFilter]
        public IActionResult Index()
        {
            return View();
        }

        public JsonResult List()
        {
            BasePropertiesDTO conservadorRetorno = this._conservadorAPP.Get();

            return this.Json(new
            {
                data = conservadorRetorno.Resultado
            });
        }


        //[ActionsFilter]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        //[ActionsFilter]
        public IActionResult Create(ConservadorDTO conservadorDTO)
        {
            try
            {
                BasePropertiesDTO conservadorRetorno = this._conservadorAPP.Create(conservadorDTO);

                if (conservadorRetorno != null)
                {
                    int codigoRetorno = conservadorRetorno.CodigoRetorno;

                    if (codigoRetorno == (int)MantenedoresEstados.Exitoso)
                    {
                        @TempData["Exito"] = true;
                        @TempData["TituloMensaje"] = "Creación Exitosa";
                        @TempData["CuerpoMensaje"] = "Se ha creado el registro exitosamente.";
                        return RedirectToAction("Index");
                    }
                    else if (codigoRetorno == (int)MantenedoresEstados.RegistroExistente)
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error - Registro Existente";
                        @TempData["CuerpoMensaje"] = "El registro que está intentando crear ya existe, intente con otra descripción.";
                    }
                    else
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error";
                        @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                    }
                }
                else
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error";
                    @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                }

                return View(conservadorDTO);

            }
            catch (Exception ex )
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

        public IActionResult Details(int id)
        {
            try
            {
                BasePropertiesDTO conservador = this._conservadorAPP.Get(id);

                return PartialView((ConservadorDTO)conservador.Resultado);
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

        public IActionResult Edit(int id)
        {
            try
            {
                BasePropertiesDTO conservador = this._conservadorAPP.Get(id);

                return View((ConservadorDTO)conservador.Resultado);
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }

        }

        [HttpPost]
        public IActionResult Edit(ConservadorDTO conservadorDTO)/*modificar el conservador DTO a base en la primera linea de codigo*/
        {
            try
            {
                BasePropertiesDTO conservadorRetorno = this._conservadorAPP.Edit(conservadorDTO);

                if (conservadorRetorno != null)
                {
                    int codigoRetorno = conservadorRetorno.CodigoRetorno;

                    if (codigoRetorno == (int)MantenedoresEstados.Exitoso)
                    {
                        @TempData["Exito"] = true;
                        @TempData["TituloMensaje"] = "Edición Exitosa";
                        @TempData["CuerpoMensaje"] = "Se ha editado el registro exitosamente.";
                        return RedirectToAction("Index");
                    }
                    else if (codigoRetorno == (int)MantenedoresEstados.RegistroNoExistente)
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error - Edición de Registro No Existente";
                        @TempData["CuerpoMensaje"] = "El registro que está intentando editar no existe, intente con otro.";
                    }
                    else
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error";
                        @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                    }
                }
                else
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error";
                    @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                }

                return View(conservadorDTO);
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
                   
        }
        
        public IActionResult Delete(int id)
        {
            try
            {
                BasePropertiesDTO conservador = this._conservadorAPP.Get(id);

                return View((ConservadorDTO)conservador.Resultado);
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }

            
        }

        [HttpPost]
        public IActionResult Delete(ConservadorDTO conservadorDTO)
        {
            try
            {
                BasePropertiesDTO conservadorRetorno = this._conservadorAPP.Delete(conservadorDTO);

                if (conservadorRetorno != null)
                {
                    int codigoRetorno = conservadorRetorno.CodigoRetorno;

                    if (codigoRetorno == (int)MantenedoresEstados.Exitoso)
                    {
                        @TempData["Exito"] = true;
                        @TempData["TituloMensaje"] = "Eliminación Exitosa";
                        @TempData["CuerpoMensaje"] = "Se ha eliminado el registro exitosamente.";
                        return RedirectToAction("Index");
                    }
                    else if (codigoRetorno == (int)MantenedoresEstados.RegistroNoExistente)
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error - Eliminación de Registro No Existente";
                        @TempData["CuerpoMensaje"] = "El registro que está intentando eliminar no existe, intente con otro.";
                    }
                    else if (codigoRetorno == (int)MantenedoresEstados.RegistroUtilizado)
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error - Registro Utilizado por Tabla";
                        @TempData["CuerpoMensaje"] = "No se ha podido eliminar el registro, porque está siendo utilizado por otra tabla.";
                    }
                    else
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error";
                        @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                    }
                }
                else
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error";
                    @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                }

                return View(conservadorDTO);
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }            
        }

    }
}