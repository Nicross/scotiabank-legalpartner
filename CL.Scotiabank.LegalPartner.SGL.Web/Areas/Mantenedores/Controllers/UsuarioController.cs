﻿using CL.Scotiabank.LegalPartner.SGL.APP;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace CL.Scotiabank.LegalPartner.SGL.Web.Areas.Mantenedores.Controllers
{
    [ActionsFilter]

    public class UsuarioController : Controller
    {
        #region Members


        private readonly string   baseUriUsuario = Resources.UsuarioUrl;
        private dynamic logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

        /// <summary>
        /// /
        /// </summary>
        private readonly IUsuarioAPP _usuarioAPP;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="usuarioAPP"></param>
        public UsuarioController(IUsuarioAPP usuarioAPP)
        {
            this._usuarioAPP = usuarioAPP;
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            UsuarioDTO UsuarioDTO = new UsuarioDTO();

            return View(UsuarioDTO);
        }

        /// <summary>
        /// /
        /// </summary>
        /// <returns></returns>
        public JsonResult List()
        {
            BasePropertiesDTO usuarioRetorno = this._usuarioAPP.GetUsuarios();

            if (usuarioRetorno == null)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "";
                @TempData["CuerpoMensaje"] = "No se registra información de Usuarios.";
            }

            return this.Json(new
            {
                data = usuarioRetorno.Resultado
            });
        }

        public JsonResult ListDropdownPerfil()
        {
            BasePropertiesDTO perfilRetorno = _usuarioAPP.GetPerfil();
            List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

            Parallel.ForEach((IEnumerable<UsuarioDTO>)perfilRetorno.Resultado, (registro) =>
            {
                listaDTO.Add(new DropDownListDTO { id = Convert.ToString(registro.CodigoPerfil), text = registro.DescripcionPerfil });
            });

            return Json(listaDTO);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public JsonResult ListDropdownSucursal()
        {
            BasePropertiesDTO sucursalRetorno = _usuarioAPP.GetSucursal();
            List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

            Parallel.ForEach((IEnumerable<UsuarioDTO>)sucursalRetorno.Resultado, (registro) =>
            {
                listaDTO.Add(new DropDownListDTO { id = registro.IdSucursal, text = registro.DescripcionSucursal });
            });

            return Json(listaDTO);
        }


        //Accion de consultar un Rut y validar si exite o no en el sistema para posterior creacion a sistema
        public IActionResult BusquedaUsuario()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="usuarioDTO"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult BusquedaUsuario(UsuarioDTO usuarioDTO)
        {
            usuarioDTO.RutUsuario = usuarioDTO.RutUsuario.Replace(".", "");
            usuarioDTO.DigitoVerificador = usuarioDTO.RutUsuario.Split('-')[1];
            usuarioDTO.RutUsuario = usuarioDTO.RutUsuario.Split('-')[0];

            BasePropertiesDTO usuarioRetorno = this._usuarioAPP.GetUsuario(usuarioDTO);

            SessionLoginDTO session = HttpContext.Session.GetObjectFromJson<SessionLoginDTO>("sessionLogin");
            usuarioDTO.SesionUsuario = String.Join(Convert.ToString(session.RutUsuario), session.DigitoVerificador);

            if (usuarioRetorno != null)
            {
                if (usuarioRetorno.Resultado != null && usuarioDTO.RutUsuario != null)
                {
                    var codigoVigencia = usuarioRetorno.Resultado.CodigoVigenciaConsulta;

                    if (codigoVigencia == 1)
                    {
                        /*Ingresar vista y controlador que pertenesca a syp_ReingresaUser.asp*/
                        return RedirectToAction("ReingresarUsuarioGet", usuarioDTO);
                    }
                    else
                    {
                        /*Ingresar vista y controlador que pertenesca a Syp_ModificarUserNew.asp*/
                        return RedirectToAction("EditUsuarioNuevoGet", usuarioDTO);
                    }
                }
                else
                {
                    return RedirectToAction("CreateGet", usuarioDTO);
                }
            }
            else
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Estimado usuario";
                @TempData["CuerpoMensaje"] = "En estos momentos no lo podemos atender, por favor intentelo más tarde.";
            }

            return View();
        }

        [HttpGet]
        public ActionResult CreateGet(UsuarioDTO usuarioDTO)
        {
            return View("Create", usuarioDTO);
        }

        [HttpPost]
        public ActionResult Create(UsuarioDTO usuarioDTO)
        {
            usuarioDTO.RutUsuario = usuarioDTO.RutUsuario.Replace(".", "");
            usuarioDTO.DigitoVerificador = usuarioDTO.RutUsuario.Split('-')[1];
            usuarioDTO.RutUsuario = usuarioDTO.RutUsuario.Split('-')[0];

            int rutUsuario = Convert.ToInt32(usuarioDTO.RutUsuario);

            SessionLoginDTO session = HttpContext.Session.GetObjectFromJson<SessionLoginDTO>("sessionLogin");
            usuarioDTO.SesionUsuario = string.Join(session.RutUsuario, session.DigitoVerificador);

            if (rutUsuario == 0)
            {
                rutUsuario = Convert.ToInt32(usuarioDTO.SesionUsuario);
            }

            if (rutUsuario == Convert.ToInt32(usuarioDTO.Contraseña))
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Error ";
                @TempData["CuerpoMensaje"] = "La contraseña no puede ser igual al Rut, favor corregir.";
            }
            BasePropertiesDTO usuarioRetorno = this._usuarioAPP.CreateUsuario(usuarioDTO);

            if (usuarioRetorno.Resultado != null)
            {
                int codigoRetorno = usuarioRetorno.Resultado.CodigoRetorno;

                      if (codigoRetorno == 0 || codigoRetorno == 1)
                      {
                        BasePropertiesDTO relacionPerfilRetorno = this._usuarioAPP.CreateRelacionPerfil(usuarioDTO);
                                               
                            if (codigoRetorno == (int)MantenedoresEstados.Exitoso)
                            {
                                @TempData["Exito"] = true;
                                @TempData["TituloMensaje"] = "Creación Exitosa";
                                @TempData["CuerpoMensaje"] = "Se ha creado el registro exitosamente.";
                                return RedirectToAction("Index");
                            }
                            else if (codigoRetorno == (int)MantenedoresEstados.RegistroExistente)
                            {
                                @TempData["Error"] = true;
                                @TempData["TituloMensaje"] = "Error - Registro Existente";
                                @TempData["CuerpoMensaje"] = "El registro que está intentando crear ya existe, intente con otra descripción.";
                            }
                            else if (codigoRetorno == (int)MantenedoresEstados.ErrorInesperado)
                            {
                                @TempData["Error"] = true;
                                @TempData["TituloMensaje"] = "Error";
                                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                            }                                                 

                }
            }
            else
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
            }


            return View((UsuarioDTO)usuarioRetorno.Resultado);
        }

        [HttpGet]
        public ActionResult ReingresarUsuarioGet(UsuarioDTO usuarioDTO)
        {
            BasePropertiesDTO usuarioRetorno = this._usuarioAPP.GetUsuario(usuarioDTO);

            if (usuarioRetorno == null)
            {
                BadRequest();
            }

            usuarioRetorno.Resultado.CodigoPerfil = 0;

            return View("ReingresarUsuario", (UsuarioDTO)usuarioRetorno.Resultado);
        }

        [HttpPost]
        public ActionResult ReingresarUsuario(UsuarioDTO usuarioDTO)
        {
            usuarioDTO.RutUsuario = usuarioDTO.RutUsuario.Replace(".", "");
            usuarioDTO.DigitoVerificador = usuarioDTO.RutUsuario.Split('-')[1];
            usuarioDTO.RutUsuario = usuarioDTO.RutUsuario.Split('-')[0];

            var digitoVerificador = usuarioDTO.DigitoVerificador;

            if (usuarioDTO.RutUsuario == usuarioDTO.Contraseña)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Error ";
                @TempData["CuerpoMensaje"] = "La contraseña no puede ser igual al Rut, favor corregir.";
            }

            BasePropertiesDTO usuarioRetorno = this._usuarioAPP.CreateReingresarUsuario(usuarioDTO);

            if (usuarioRetorno != null)
            {
                int codigoRetorno = usuarioRetorno.Resultado.CodigoRetorno;
                            
                    if (codigoRetorno == (int)MantenedoresEstados.Exitoso)
                    {
                        @TempData["Exito"] = true;
                        @TempData["TituloMensaje"] = "Creación Exitosa";
                        @TempData["CuerpoMensaje"] = "Se ha creado el registro exitosamente.";
                        return RedirectToAction("Index");
                    }
                    else if (codigoRetorno == (int)MantenedoresEstados.RegistroExistente)
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error - Registro Existente";
                        @TempData["CuerpoMensaje"] = "El registro que está intentando crear ya existe.";
                    }
                    else if (codigoRetorno == (int)MantenedoresEstados.ErrorInesperado)
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error";
                        @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                    }
                                    
            }
            else
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
            }


            return View((UsuarioDTO)usuarioRetorno.Resultado);
        }


        [HttpGet]
        public IActionResult Details(int codigo, string rut)
        {
            if (rut == null || Convert.ToString(codigo) == null)
            {
                return BadRequest();
            }

            rut = Convert.ToString(rut).Replace(".", "");
            rut = rut.Split('-')[0];

            UsuarioDTO usuario = new UsuarioDTO
            {
                RutUsuario = rut,
                Codigo = codigo
            };


            BasePropertiesDTO usuarioDtOVigencia = this._usuarioAPP.GetUsuarioVigencia(usuario);

            int codigoVigencia = usuarioDtOVigencia.Resultado.CodigoVigencia;

            if (usuarioDtOVigencia == null)
            {
                BadRequest();
            }
            else if (codigoVigencia == (int)MantenedoresEstados.RegistroExistente)
            {
                @TempData["ErrorVigencia"] = true;
                @TempData["TituloMensaje"] = "Error";
                @TempData["CuerpoMensaje"] = "El Usuario ha sido Bloqueado o Eliminado del Sistema.";

                return PartialView("DetailsBloqueado");
            }
            else
            {
                usuario.RutUsuario = rut;

                BasePropertiesDTO usuarioDtORut = this._usuarioAPP.GetUsuarioRut(usuario);

                if (usuarioDtORut == null)
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error";
                    @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";

                }
                else
                {
                    string rutSalida = usuarioDtORut.Resultado.RutUsuario;
                    string digitoSalida = usuarioDtORut.Resultado.DigitoVerificador;
                    usuarioDtORut.Resultado.RutUsuario = String.Join(rutSalida, digitoSalida);

                    return PartialView((UsuarioDTO)usuarioDtORut.Resultado);
                }

            }
            return PartialView();

        }


        [HttpGet]
        public IActionResult EditUser(int codigo, string rut)
        {
            if (rut == null || Convert.ToString(codigo) == null)
            {
                BadRequest();
            }

            UsuarioDTO usuarioDTO = new UsuarioDTO
            {
                Codigo = codigo,
                RutUsuario = rut
            };

            BasePropertiesDTO usuarioRetorno = this._usuarioAPP.GetUsuarioRut(usuarioDTO);

            if (usuarioRetorno == null)
            {
                BadRequest();
            }

            BasePropertiesDTO sucursalRetornoPorUsuario = this._usuarioAPP.GetSucursalPorCodigoUsuario(usuarioDTO);

            if (sucursalRetornoPorUsuario == null)
            {
                @TempData["ErrorSucursal"] = true;
                @TempData["TituloMensaje"] = "";
                @TempData["CuerpoMensaje"] = "No se registra Sucursales asociados al Usuario.";
            }

            else
            {

                ViewBag.listaSucursalesPorUsuario = sucursalRetornoPorUsuario.Resultado;
            }

            if (usuarioDTO == null)
            {
                BadRequest();
            }
            else
            {
                if (usuarioRetorno.Resultado.Telefono != null && usuarioRetorno.Resultado.Email != null)
                {
                    usuarioRetorno.Resultado.Telefono = usuarioRetorno.Resultado.Telefono.Trim();
                    usuarioRetorno.Resultado.Email = usuarioRetorno.Resultado.Email.Trim();
                }
            }
            return View("Edit", (UsuarioDTO)usuarioRetorno.Resultado);
        }

        [HttpPost]
        public IActionResult Edit(UsuarioDTO usuarioDTO)
        {
            if (usuarioDTO == null)
            {
                BadRequest();
            }

            usuarioDTO.RutUsuario = usuarioDTO.RutUsuario.Replace(".", "");
            usuarioDTO.DigitoVerificador = usuarioDTO.RutUsuario.Split('-')[1];
            usuarioDTO.RutUsuario = usuarioDTO.RutUsuario.Split('-')[0];




            if (Convert.ToInt32(usuarioDTO.IdNuevoSucursal) > 0)
            {
                BasePropertiesDTO sucursalRetorno = this._usuarioAPP.GetConsultaSucursal(usuarioDTO);

                var codigoRetornoSucursal = sucursalRetorno.Resultado.CodigoRetorno;


                if (sucursalRetorno != null)
                {
                    if (codigoRetornoSucursal == (int)MantenedoresEstados.PerfilExistente)
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error";
                        @TempData["CuerpoMensaje"] = "No se pudo relacionar al Usuario con la Sucursal debido a que la nueva asignación ya existe.";
                    }

                }
                else
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error";
                    @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                }
            }

            BasePropertiesDTO usuarioRetorno = this._usuarioAPP.UpdateUsuario(usuarioDTO);

            if (Convert.ToInt32(usuarioDTO.IdNuevoSucursal) > 0)
            {
                BasePropertiesDTO sucursalUpdateRetorno = this._usuarioAPP.UpdateUsuarioSucursal(usuarioDTO);

                usuarioRetorno.Resultado.CodigoRetorno = sucursalUpdateRetorno.Resultado.CodigoRetorno;
            }

            var codigoRetorno = usuarioRetorno.Resultado.CodigoRetorno;

            if (usuarioRetorno != null)
            {
                if (usuarioRetorno.Resultado.CodigoRetorno == 0)
                {
                    BasePropertiesDTO usuarioPerfilRetorno1 = null;

                    if (usuarioDTO.CodigoPerfil != usuarioRetorno.Resultado.CodigoPerfil)
                    {
                        BasePropertiesDTO usuarioPerfilRetorno = this._usuarioAPP.UpdateUsuarioPerfil(usuarioDTO);

                        usuarioRetorno.Resultado.CodigoRetorno = usuarioPerfilRetorno.Resultado.codigoRetorno;
                    }
                    else
                    { 
                        usuarioPerfilRetorno1.Resultado.codigoRetorno = 1;

                        usuarioRetorno.Resultado.CodigoRetorno = usuarioPerfilRetorno1.Resultado.codigoRetorno;

                    }

                    if (usuarioRetorno != null)
                    {
                        if (codigoRetorno == (int)MantenedoresEstados.RegistroNoExistente)
                        {
                            @TempData["Error"] = true;
                            @TempData["TituloMensaje"] = "Error";
                            @TempData["CuerpoMensaje"] = "Ha ocurridso un error al modificar al Usuario debido a que este no se encuentra registrado.";
                        }
                        else if (codigoRetorno == (int)MantenedoresEstados.Exitoso)
                        {
                            @TempData["Exito"] = true;
                            @TempData["TituloMensaje"] = "Edición Exitosa";
                            @TempData["CuerpoMensaje"] = "Usuario ha sido modificado exitosamente.";
                            return RedirectToAction("Index");
                        }
                        else if (codigoRetorno == (int)MantenedoresEstados.ErrorInesperado)
                        {
                            @TempData["Error"] = true;
                            @TempData["TituloMensaje"] = "Error";
                            @TempData["CuerpoMensaje"] = "No se pudo modificar el Perfil al usuario.";

                        }
                        else if (codigoRetorno == (int)MantenedoresEstados.PerfilExistente)
                        {
                            @TempData["Error"] = true;
                            @TempData["TituloMensaje"] = "Error";
                            @TempData["CuerpoMensaje"] = " No se pudo modificar el Perfil porque la nueva asignación ya existe..";

                        }
                    }
                    else
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error";
                        @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                    }
                }

                else
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error";
                    @TempData["CuerpoMensaje"] = "Ha ocurridso un error al modificar al Usuario debido a que este no se encuentra registrado.";
                }
            }

            else
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";

            }

            return View((UsuarioDTO)usuarioRetorno.Resultado);
        }


        [HttpGet]
        public ActionResult EditUsuarioNuevoGet(UsuarioDTO usuarioDTO)
        {

            BasePropertiesDTO usuarioRetorno = this._usuarioAPP.GetUsuario(usuarioDTO);

            if (usuarioRetorno == null)
            {
                BadRequest();
            }

            usuarioRetorno.Resultado.CodigoPerfil = 0;

            return View("EditUsuarioNuevo", (UsuarioDTO)usuarioRetorno.Resultado);
        }

        [HttpPost]
        public ActionResult EditUsuarioNuevo(UsuarioDTO usuarioDTO)
        {
            usuarioDTO.RutUsuario = usuarioDTO.RutUsuario.Replace(".", "");
            usuarioDTO.DigitoVerificador = usuarioDTO.RutUsuario.Split('-')[1];
            usuarioDTO.RutUsuario = usuarioDTO.RutUsuario.Split('-')[0];

            var digitoVerificador = usuarioDTO.DigitoVerificador;

            if (usuarioDTO.RutUsuario == usuarioDTO.Contraseña)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Error ";
                @TempData["CuerpoMensaje"] = "La contraseña no puede ser igual al Rut, favor corregir.";
            }

            BasePropertiesDTO usuarioRetorno = this._usuarioAPP.UpdateUsuarioNuevo(usuarioDTO);


            if (usuarioRetorno != null)
            {
                int codigoRetorno = usuarioRetorno.Resultado.CodigoRetorno;

                if (codigoRetorno == (int)MantenedoresEstados.Exitoso)
                {
                    @TempData["Exito"] = true;
                    @TempData["TituloMensaje"] = "Creación Exitosa";
                    @TempData["CuerpoMensaje"] = "Se ha editado el registro exitosamente.";
                    return RedirectToAction("Index");
                }
                else if (codigoRetorno == (int)MantenedoresEstados.RegistroNoExistente)
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error - Registro No Existente";
                    @TempData["CuerpoMensaje"] = "El registro que está intentando editar no está registrado.";
                    return RedirectToAction("Index");
                }
                else if (codigoRetorno == (int)MantenedoresEstados.RegistroExistente)
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error - Registro Existente";
                    @TempData["CuerpoMensaje"] = "El registro que está intentando editar ya está registrado.";
                    return RedirectToAction("Index");
                }
                else if (codigoRetorno == (int)MantenedoresEstados.SucursalExistente)
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error - Asignación Existente";
                    @TempData["CuerpoMensaje"] = "No se pudo relacionar al Usuario con la Sucursal porque la nueva asignación ya existe.";
                }

            }
            else
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
            }


            return View((UsuarioDTO)usuarioRetorno.Resultado);
        }


        [HttpGet]
        public IActionResult Delete(int codigo, string rut, string digito)
        {
            try
            {

                UsuarioDTO usuarioDTO = new UsuarioDTO
                {
                    Codigo = codigo,
                    RutUsuario = rut,
                    DigitoVerificador = digito
                };


                BasePropertiesDTO usuarioRetorno = this._usuarioAPP.GetUsuarioRut(usuarioDTO);

                var codigoRetorno = usuarioRetorno.Resultado;

                BasePropertiesDTO SucursalRetorno = this._usuarioAPP.GetSucursalPorCodigoUsuario(usuarioDTO);

                if (SucursalRetorno == null)
                {
                    @TempData["ErrorSucursal"] = true;
                    @TempData["TituloMensaje"] = "";
                    @TempData["CuerpoMensaje"] = "No se registra Sucursales asociados al Usuario.";
                }
                else
                {
                    ViewBag.listaSucursales = SucursalRetorno.Resultado;

                    @TempData["ErrorSeleccionSucursal"] = true;
                    @TempData["TituloMensaje"] = "Alerta";
                    @TempData["CuerpoMensaje"] = "Debe selecciona una Sucursal.";

                }

                if (usuarioRetorno == null)
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error";
                    @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";

                }
                if (codigoRetorno.Email == null && codigoRetorno.Telefono == null)
                {
                    BadRequest();
                }
                else
                {
                    codigoRetorno.Email = codigoRetorno.Email.Trim();
                    codigoRetorno.Telefono = codigoRetorno.Telefono.Trim();
                }


                return View((UsuarioDTO)usuarioRetorno.Resultado);
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Error";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public IActionResult Delete(UsuarioDTO usuarioDTO)
        {
            usuarioDTO.RutUsuario = usuarioDTO.RutUsuario.Split('-')[0];

            if (usuarioDTO.IdSucursal == null)
            {
                BasePropertiesDTO usuarioRetorno = this._usuarioAPP.DeleteUsuario(usuarioDTO);

                int codigoRetorno = usuarioRetorno.Resultado.CodigoRetorno;

                if (usuarioRetorno.Resultado != null)
                {
                    if (codigoRetorno == (int)MantenedoresEstados.Exitoso)
                    {
                        @TempData["Exito"] = true;
                        @TempData["TituloMensaje"] = "Eliminación Exitosa";
                        @TempData["CuerpoMensaje"] = "Se ha eliminado el registro exitosamente.";
                        return RedirectToAction("Index");
                    }
                    else if (codigoRetorno == (int)MantenedoresEstados.RegistroNoExistente)
                    {

                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error";
                        @TempData["CuerpoMensaje"] = "Ha ocurrido un error al eliminar el usuario debido a que no se encuentra registrado.";
                    }

                }
                else
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error";
                    @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                }
            }
            else
            {
                BasePropertiesDTO usuarioSucursalRetorno = this._usuarioAPP.DeleteUsuarioConSucursal(usuarioDTO);

                int codigoRetorno = usuarioSucursalRetorno.Resultado.CodigoRetorno;

                if (usuarioSucursalRetorno.Resultado != null)
                {
                    if (codigoRetorno == (int)MantenedoresEstados.Exitoso)
                    {
                        @TempData["Exito"] = true;
                        @TempData["TituloMensaje"] = "Eliminación Exitosa";
                        @TempData["CuerpoMensaje"] = "Se ha eliminado la sucursal exitosamente.";
                        return RedirectToAction("Index");
                    }
                    else if (codigoRetorno == (int)MantenedoresEstados.RegistroNoExistente)
                    {

                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Error";
                        @TempData["CuerpoMensaje"] = "Ha ocurrido un error al eliminar el usuario debido a que no se encuentra registrado.";
                    }

                }
                else
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Error";
                    @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado, inténtelo de nuevo más tarde o conéctese con el administrador del sistema.";
                }
            }

            return View();
        }
    }
}