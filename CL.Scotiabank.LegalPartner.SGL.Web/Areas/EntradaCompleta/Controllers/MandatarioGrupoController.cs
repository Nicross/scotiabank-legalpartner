﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.APP;
using CL.Scotiabank.LegalPartner.ConsultaAPI.Transversal.DTO;

namespace CL.Scotiabank.LegalPartner.SGL.Web.Areas.PoderesYRevocaciones.MandatarioGrupo.Controllers
{
    /// <summary>
    /// Controlador Para Mandatario, Grupo y Asociacion de Mandatarios a un Grupo
    /// </summary>
    [ActionsFilter]
    public class MandatarioGrupoController : Controller
    {
        #region Members
             
        private IMandatarioGrupoAPP _mandatarioGrupoAPP;

        #endregion

        #region Constructor

        public MandatarioGrupoController(IMandatarioGrupoAPP mandatarioGrupoAPP)
        {
            this._mandatarioGrupoAPP = mandatarioGrupoAPP;
        }

        #endregion

        public IActionResult Index()
        {
            MandatarioGrupoDTO mandatarioGrupoDTO = new MandatarioGrupoDTO();
            HttpContext.Session.SetString("_CodigoSociedad", "4148196");
            HttpContext.Session.SetString("_CodigoPersona", "4148196");
            HttpContext.Session.SetString("_CodigoTipo", "3");
            //Mandatario
            HttpContext.Session.SetString("_CodigoPersonaM", "7");
            HttpContext.Session.SetString("_CodigoTipoM", "3"); 
            HttpContext.Session.SetString("_CodigoDocumentoM", "1673385");
            HttpContext.Session.SetString("_SglId", "4456415"); 


            return View(mandatarioGrupoDTO);
        }

        /// <summary>
        /// Region Para métodos del area de Mandatarios 
        /// </summary>
        #region Mandatarios

        #region LISTAR

        /// <summary>
        /// Método para listar los mandatarios.
        /// </summary>
        /// <returns></returns>
        public JsonResult ListaMandatario(int codigoPersona, string codigoTipo)
        {
            codigoPersona = Convert.ToInt32(HttpContext.Session.GetString("_CodigoPersonaM"));
            codigoTipo = Convert.ToString(HttpContext.Session.GetString("_CodigoTipoM"));

            if (codigoPersona == Convert.ToInt32(Resources.valorCero) || codigoTipo == string.Empty || codigoTipo == null)
            {
                BadRequest();
            }

            GetMandatarioDTO ParametroGetMandatario = new GetMandatarioDTO
            {
                CodigoPersona = codigoPersona,
                CodigoTipo = codigoTipo
            };

            BasePropertiesDTO mandatarioRetorno = this._mandatarioGrupoAPP.ListarMandatarios(ParametroGetMandatario);

            return this.Json(new
            {
                data = mandatarioRetorno.Resultado
            });
        }

        #endregion

        #region INGRESAR      

        /// <summary>
        /// Método que consulta si un mandatario ya existe.
        /// </summary>
        /// <param name="datosModal"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ConsultaMandatario([FromBody]MandatarioModalIngresarDTO pConsultaMandatarioDTO)
        {
            if (ModelState.IsValid)
            {
                MandatarioGrupoDTO mandatarioGrupoDTO = new MandatarioGrupoDTO
                {
                    Rut = pConsultaMandatarioDTO.Rut,
                    Nombre = pConsultaMandatarioDTO.Nombre,
                    ApellidoPaterno = pConsultaMandatarioDTO.ApellidoPaterno,
                    ApellidoMaterno = pConsultaMandatarioDTO.ApellidoMaterno,
                    CantidadRegistros = pConsultaMandatarioDTO.CantidadRegistros
                };

                BasePropertiesDTO consultaMandatarioRetorno = this._mandatarioGrupoAPP.ConsultaMandatario(mandatarioGrupoDTO);

                MandatarioGrupoDTO mandatarioConsultaGrupoDTO = new MandatarioGrupoDTO();

                if (consultaMandatarioRetorno.Resultado.Count != 0)
                {
                    mandatarioConsultaGrupoDTO.DvPersona = consultaMandatarioRetorno.Resultado[0].Dv;
                    mandatarioConsultaGrupoDTO.Rut = Convert.ToString(consultaMandatarioRetorno.Resultado[0].Rut);
                    mandatarioConsultaGrupoDTO.Nombre = consultaMandatarioRetorno.Resultado[0].NombrePersona;
                    mandatarioConsultaGrupoDTO.ApellidoPaterno = consultaMandatarioRetorno.Resultado[0].ApellidoPaternoPersona;
                    mandatarioConsultaGrupoDTO.ApellidoMaterno = consultaMandatarioRetorno.Resultado[0].ApellidoMaternoPersona;
                    mandatarioConsultaGrupoDTO.CodigoPersona = consultaMandatarioRetorno.Resultado[0].CodigoPersona;
                    mandatarioConsultaGrupoDTO.CodigoTipoPersona = Convert.ToInt32(consultaMandatarioRetorno.Resultado[0].CodigoTipoPersona);
                }
                else
                {
                    mandatarioConsultaGrupoDTO = null;
                }


                return Json(mandatarioConsultaGrupoDTO);
            }
            else
            {
                return Json("error create");
            }
        }
         
        /// <summary>
        /// Método que ingresa a un Mandatario.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult IngresarMandatario()
        {
            MandatarioGrupoDTO parametroMandatario = new MandatarioGrupoDTO();

            return PartialView(parametroMandatario);
        }

        /// <summary>
        /// Método que ingresa las observaciones y otros datos del mandatario.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult IngresarMandatario(MandatarioGrupoDTO pIngresaMandarioDTO)
        {
            if (pIngresaMandarioDTO.Rut == string.Empty || pIngresaMandarioDTO.DvPersona == string.Empty)
            {
                BadRequest();
            }

            BasePropertiesDTO retornoIngresoMandatario = this._mandatarioGrupoAPP.IngresarMandatario(pIngresaMandarioDTO);
                        
            pIngresaMandarioDTO.CodigoPersona = Convert.ToInt32(HttpContext.Session.GetString("_CodigoPersonaM"));                        
            pIngresaMandarioDTO.CodigoParticipante = retornoIngresoMandatario.Resultado[0].CodigoPersona;                        
            pIngresaMandarioDTO.CodigoDocumento = Convert.ToInt32(HttpContext.Session.GetString("_CodigoDocumentoM"));            
            pIngresaMandarioDTO.CodigoTipoParticipante = 3;
            pIngresaMandarioDTO.Monto = 0;
            pIngresaMandarioDTO.Porcentaje = 0;
            pIngresaMandarioDTO.FechaIniciadora = "31/12/3000";
            pIngresaMandarioDTO.FechaFin = "31/12/3000";
            pIngresaMandarioDTO.CodigoRevocacion = 0;
                       
            if (string.IsNullOrEmpty(pIngresaMandarioDTO.Limitacion))
            {
                pIngresaMandarioDTO.Limitacion = string.Empty;
            }
            
            BasePropertiesDTO retornoIngresarObservaciones = this._mandatarioGrupoAPP.IngresarObservacionMandatario(pIngresaMandarioDTO);

            pIngresaMandarioDTO.SglId = Convert.ToInt32(HttpContext.Session.GetString("_SglId"));
            pIngresaMandarioDTO.Estado = 8;
            pIngresaMandarioDTO.CodigoPersonaModificador = 717;
           
            BasePropertiesDTO retornoActualizaEstadoSGL = this._mandatarioGrupoAPP.ActualizarEstadoSGL(pIngresaMandarioDTO);

            if (retornoIngresoMandatario != null || retornoIngresarObservaciones != null)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Ingresar Mandatario";
                @TempData["CuerpoMensaje"] = "El registro fue añadido exitosamente";
            }

            return View("Index", new MandatarioGrupoDTO());
        }

        #endregion

        #region ELIMINAR

        /// <summary>
        /// Método que elimina el Mandatario
        /// </summary>
        /// <param name="codigoPersona"></param>
        /// <param name="codigoParticipante"></param>
        /// <param name="codigoTipo"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult EliminarMandatario(int codigoPersona, int codigoParticipante, int codigoTipo)
        {
            codigoPersona = Convert.ToInt32(HttpContext.Session.GetString("_CodigoPersonaM"));
            codigoTipo = Convert.ToInt32(HttpContext.Session.GetString("_CodigoTipoM"));

            MandatarioGrupoDTO mandatarioGrupoDTO = new MandatarioGrupoDTO
            {
                CodigoPersona = codigoPersona,

                CodigoParticipante = codigoParticipante,
                CodigoTipo = codigoTipo
            };

            return PartialView(mandatarioGrupoDTO);
        }

        /// <summary>
        /// Método para eliminar a un mandatario vigente.
        /// </summary>
        /// <param name="pEliminarMandatario"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EliminarMandatario(MandatarioGrupoDTO pEliminarMandatario)
        {
            BasePropertiesDTO retornoObtenerMandatarioVigente = this._mandatarioGrupoAPP.ObtenerMandatarioVigente(pEliminarMandatario);

            if (retornoObtenerMandatarioVigente != null)
            {
                var codigoRetorno = retornoObtenerMandatarioVigente.Resultado;

                if (!string.IsNullOrEmpty(retornoObtenerMandatarioVigente.ToString()))
                {
                    if (codigoRetorno[0].Estado == 0)
                    {
                        BasePropertiesDTO retornoEliminarMandatario = this._mandatarioGrupoAPP.EliminarMandatario(pEliminarMandatario);

                        var codRetorno = retornoEliminarMandatario.CodigoRetorno;

                        if (codRetorno == (int)MantenedoresEstados.Exitoso)
                        {
                            @TempData["Error"] = true;
                            @TempData["TituloMensaje"] = "Mensaje: Eliminar Mandatario";
                            @TempData["CuerpoMensaje"] = "El registro se eliminó exitosamente";

                            return RedirectToAction("Index", retornoEliminarMandatario.Resultado);
                        }
                        else if (codRetorno == (int)MantenedoresEstados.ErrorInesperado)
                        {
                            @TempData["Error"] = true;
                            @TempData["TituloMensaje"] = "Mensaje: Eliminar Mandatario";
                            @TempData["CuerpoMensaje"] = "Error inesperado al eliminar al Mandatario";
                        }
                    }
                    else if (codigoRetorno[0].Estado > 0)
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "Registro Utilizado";
                        @TempData["CuerpoMensaje"] = "MANDATARIO Activo, no se puede BORRAR";
                    }
                }
            }

            return View("Index", pEliminarMandatario);
        }

        #endregion

        #region EDITAR

        /// <summary>
        /// Método que toma objeto de JS y retorna un JSON
        /// </summary>
        /// <param name="datosModal"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult EnviarEditarMandatario([FromBody]MandatarioModalEditarDTO pDatosModal)
        {
            if (ModelState.IsValid)
            {
                return Json(pDatosModal);
            }
            else
            {
                return Json("error create");
            }
        }

        /// <summary>
        /// Método que envía un modal con los datos de un Mandatario para Editar.
        /// </summary>
        /// <param name="pEditar"></param>
        /// <returns></returns>
        public ActionResult EditaMandatario(MandatarioModalEditarDTO pEditar)
        {
            MandatarioGrupoDTO mandatarioGrupoDTO = new MandatarioGrupoDTO
            {
                //Para mabos métodos de APP
                CodigoParticipante = pEditar.CodParticipante,
                //EditarMandatario
                CodigoTipoPersona = pEditar.TipoPersona,
                Nombre = pEditar.Nombre,
                ApellidoPaterno = pEditar.ApellPaterno,
                ApellidoMaterno = pEditar.ApellMaterno,
                //Vista
                Rut = pEditar.Rut,
                DvPersona = pEditar.Dv,
                //EditarObservacionesMandatario
                CodigoPersona = pEditar.CodPersona,
                Limite = pEditar.Observaciones,
                CodigoTipoParticipante = pEditar.TipoParticipante,
                Monto = pEditar.Monto,
                Porcentaje = pEditar.Porcentaje
            };

            return PartialView(mandatarioGrupoDTO);
        }

        /// <summary>
        /// Método que permite editar al Mandatario.
        /// </summary>
        /// <param name="mandatarioGrupoDTO"></param>
        /// <returns></returns>
        public ActionResult EditarMandatario(MandatarioGrupoDTO mandatarioGrupoDTO)
        {
            if (mandatarioGrupoDTO.CodigoParticipante == 0)
            {
                BadRequest();
            }

            BasePropertiesDTO retornoEditarMandatario = this._mandatarioGrupoAPP.EditarMandatario(mandatarioGrupoDTO);

            if (retornoEditarMandatario == null)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Mensaje: Editar Mandatario";
                @TempData["CuerpoMensaje"] = "No se pudo editar";
            }
            else
            {
                BasePropertiesDTO retornoEditarObservacionesMandatario = this._mandatarioGrupoAPP.EditarObservacionesMandatario(mandatarioGrupoDTO);

                if (retornoEditarObservacionesMandatario != null)
                {
                    BasePropertiesDTO retornoActualizaEstadoMandatario = this._mandatarioGrupoAPP.ActualizarEstadoSGL(mandatarioGrupoDTO);

                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Mensaje: Editar Mandatario";
                    @TempData["CuerpoMensaje"] = "El registro fue editado exitosamente";

                    return View("Index", retornoEditarObservacionesMandatario.Resultado);                                       
                }
                else
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Mensaje: Editar Mandatario";
                    @TempData["CuerpoMensaje"] = "No se pudo editar la observación del Mandatario.";
                }

                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "Editar Mandatario";
                @TempData["CuerpoMensaje"] = "El Mandatario fue editado exitosamente.";
            }

            return View("Index");
        }

        #endregion

        #region OBSERVACION

        /// <summary>
        /// Método que toma objeto de JS y retorna un JSON
        /// </summary>
        /// <param name="datosModal"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult EnviarObservacionesMandatario([FromBody]ObservacionMandatario pDatosModal)
        {
            if (ModelState.IsValid)
            {
                return Json(pDatosModal);
            }
            else
            {
                return Json("error create");
            }
        }

        /// <summary>
        /// Método para ver la observación de un Mandatario.
        /// </summary>
        /// <param name="pObservacionMandatarioDTO"></param>
        /// <returns></returns>
        public ActionResult VerObservacionMandatario(ObservacionMandatario VerObservacionMandatarioDTO)
        {
            MandatarioGrupoDTO mandatarioGrupoDTO = new MandatarioGrupoDTO
            {
                Nombre = VerObservacionMandatarioDTO.Nombre,
                Limite = VerObservacionMandatarioDTO.Limite,
                Rut = VerObservacionMandatarioDTO.Rut,
                DvPersona = VerObservacionMandatarioDTO.DvPersona
            };

            return PartialView(mandatarioGrupoDTO);
        }

        #endregion

        #endregion

        /// <summary>
        /// Región para métodos del area de Grupo 
        /// </summary>
        #region Grupos

        /// <summary>
        /// Metodo orquestado para Listar Grupos de Sociedades de Mandatarios Filtrados por el Tipo de Grupo
        /// </summary>
        public JsonResult ListarGrupo()
        {
            var id = Convert.ToString(HttpContext.Session.GetString("_CodigoSociedad"));

            BasePropertiesDTO grupoRetorno = this._mandatarioGrupoAPP.ConsultaSociedadGrupoMandatario(id);

            return this.Json(new
            {
                data = grupoRetorno.Resultado
            });
        }

        /// <summary>
        /// Metodo orquestado para Obtener Lista de Mandatarios filtrado por Sociedad
        /// </summary>
        [HttpGet]
        public ActionResult IngresarGrupo()
        {
            return PartialView();
        }

        /// <summary>
        /// Metodo orquestado para Ingresar un Grupo de Mandatarios
        /// </summary>
        [HttpPost]
        public ActionResult IngresarGrupo(MandatarioGrupoDTO mandatarioGrupoDTO)
        {
            mandatarioGrupoDTO.CodigoSociedad = Convert.ToInt32(HttpContext.Session.GetString("_CodigoSociedad"));

            if (mandatarioGrupoDTO.NombreGrupo == null || mandatarioGrupoDTO.CodigoSociedad == 0)
            {
                BadRequest();
            }

            mandatarioGrupoDTO.TipoGrupo = Convert.ToInt32(Resources.tipoGrupo);
            mandatarioGrupoDTO.NombreGrupo = mandatarioGrupoDTO.NombreGrupo.TrimStart();
            mandatarioGrupoDTO.NombreGrupo = mandatarioGrupoDTO.NombreGrupo.TrimEnd();

            BasePropertiesDTO grupoRetorno = this._mandatarioGrupoAPP.IngresarGrupo(mandatarioGrupoDTO);

            if (grupoRetorno == null)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "";
                @TempData["CuerpoMensaje"] = "No se registro el Grupo.";
            }
            else if (grupoRetorno.CodigoRetorno == (int)MantenedoresEstados.Exitoso)
            {
                @TempData["Exito"] = true;
                @TempData["TituloMensaje"] = "Creación Exitosa";
                @TempData["CuerpoMensaje"] = "Se ha creado el registro exitosamente.";
                return RedirectToAction("Index", grupoRetorno.Resultado);
            }


            return View("Index", grupoRetorno.Resultado);
        }

        /// <summary>
        /// Metodo orquestado para Modificar un Grupo de Mandatarios
        /// </summary>
        [HttpGet]
        public ActionResult EditarGrupo(int codigoGrupo, string nombreGrupo, int tipoGrupo)
        {
            MandatarioGrupoDTO mandatarioGrupoDTO = new MandatarioGrupoDTO();
            mandatarioGrupoDTO.CodigoGrupo = codigoGrupo;
            mandatarioGrupoDTO.NombreGrupo = nombreGrupo.Trim();
            mandatarioGrupoDTO.TipoGrupo = tipoGrupo;

            return PartialView(mandatarioGrupoDTO);
        }

        [HttpPost]
        public ActionResult EditarGrupo(MandatarioGrupoDTO mandatarioGrupoDTO)
        {
            if (mandatarioGrupoDTO.CodigoGrupo == 0)
            {
                BadRequest();
            }

            mandatarioGrupoDTO.CodigoPer = Convert.ToInt32(HttpContext.Session.GetString("_CodigoSociedad"));

            mandatarioGrupoDTO.NombreGrupo = mandatarioGrupoDTO.NombreGrupo.TrimStart();
            mandatarioGrupoDTO.NombreGrupo = mandatarioGrupoDTO.NombreGrupo.TrimEnd();

            BasePropertiesDTO grupoRetorno = this._mandatarioGrupoAPP.EditarGrupo(mandatarioGrupoDTO);

            if (grupoRetorno == null)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "";
                @TempData["CuerpoMensaje"] = "No se pudo editar el Grupo.";
            }
            else if (grupoRetorno.CodigoRetorno == (int)MantenedoresEstados.Exitoso)
            {
                @TempData["Exito"] = true;
                @TempData["TituloMensaje"] = "Edición Exitosa";
                @TempData["CuerpoMensaje"] = "Se ha editado el registro exitosamente.";
                return RedirectToAction("Index", grupoRetorno.Resultado);
            }
            return View("Index", grupoRetorno.Resultado);
        }

        /// <summary>
        /// Metodo orquestado para Eliminar un Grupo de Mandatarios
        /// </summary>
        [HttpGet]
        public ActionResult EliminarGrupo(int codigoGrupo)
        {
            MandatarioGrupoDTO mandatarioGrupoDTO = new MandatarioGrupoDTO
            {
                CodigoGrupo = codigoGrupo
            };

            return PartialView(mandatarioGrupoDTO);
        }

        [HttpPost]
        public ActionResult EliminarGrupo(MandatarioGrupoDTO mandatarioGrupoDTO)
        {
            if (mandatarioGrupoDTO.CodigoGrupo == 0)
            {
                BadRequest();
            }
            string codigoGrupo = Convert.ToString(mandatarioGrupoDTO.CodigoGrupo);

            BasePropertiesDTO grupoVigenteRetorno = this._mandatarioGrupoAPP.ObtenerGruposVigentes(codigoGrupo);

            if (grupoVigenteRetorno != null)
            {
                var codigoRetornoVigencia = grupoVigenteRetorno.Resultado;

                if (codigoRetornoVigencia[0].Estado == 0)
                {
                    BasePropertiesDTO grupoRetorno = this._mandatarioGrupoAPP.EliminarGruposVigentes(codigoGrupo);

                    var codigoRetorno = grupoRetorno.CodigoRetorno;

                    if (codigoRetorno == (int)MantenedoresEstados.Exitoso)
                    {

                        @TempData["Exito"] = true;
                        @TempData["TituloMensaje"] = "Eliminación Exitosa";
                        @TempData["CuerpoMensaje"] = "Se ha eliminado el registro exitosamente.";
                        return RedirectToAction("Index", grupoRetorno.Resultado);
                    }
                    else if (codigoRetorno == (int)MantenedoresEstados.ErrorInesperado)
                    {
                        @TempData["Error"] = true;
                        @TempData["TituloMensaje"] = "";
                        @TempData["CuerpoMensaje"] = "Error Inesperado al eliminar Grupo.";
                    }
                }
                else if (codigoRetornoVigencia[0].Estado > 0)
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "Mensaje: Advertencia";
                    @TempData["CuerpoMensaje"] = "No se puede eliminar Grupo por asociación Activa";

                }
            }
            return View("Index", mandatarioGrupoDTO);
        }

        #endregion

        /// <summary>
        /// Región para métodos del área de la Asociacion de Mandatarios
        /// </summary>
        #region Asociacion

        /// <summary>
        /// Metodo orquestado para Listar Grupos de Sociedades de Mandatarios Filtrados por el Tipo de Grupo
        /// </summary>
        /// 
        [HttpGet]
        public ActionResult ListarMandatariosPorAsignar()
        {

            return View();
        }


        public JsonResult ListarMandatariosSociedad(string id)
        {

            if (id == null)
            {
                BadRequest();
            }

            GetMandatarioDTO ParametroGetMandatario = new GetMandatarioDTO
            {
                CodigoPersona = Convert.ToInt32(HttpContext.Session.GetString("_CodigoPersona")),
                CodigoTipo = Convert.ToString(HttpContext.Session.GetString("_CodigoTipo"))
            };

            BasePropertiesDTO mandatariosSociedadRetorno = this._mandatarioGrupoAPP.ListarMandatarios(ParametroGetMandatario);
            List<DatosParticipantesDTOResponse> listaTodosMandatarios = mandatariosSociedadRetorno.Resultado;

            BasePropertiesDTO mandatariosPorGrupoRetorno = this._mandatarioGrupoAPP.ConsultarListaGrupoMandatorio(id);
            List<MandatarioGrupoDTO> listaMandatariosAsignadosGrupo = mandatariosPorGrupoRetorno.Resultado;
            var listaMandatariosVigentes = listaMandatariosAsignadosGrupo.Where(x => x.CodigoRevocacion == 0);
            List<DatosParticipantesDTOResponse> lstauxiliar = new List<DatosParticipantesDTOResponse>();

            foreach (var item in listaMandatariosVigentes)
            {
                lstauxiliar.Add(new DatosParticipantesDTOResponse
                {
                    Rut = item.RutPersona
                });
            }

            var listaMandatariosSinAsignacion = listaTodosMandatarios.Where(x => lstauxiliar.FirstOrDefault(a => a.Rut == x.Rut) != null);

            var arregloRutMandatarios = listaMandatariosSinAsignacion.ToArray();

            var listamandatariosSinAsignacion = listaTodosMandatarios.Except(arregloRutMandatarios);


            return this.Json(new
            {
                data = listamandatariosSinAsignacion
            });

        }


        /// <summary>
        /// Json que recibe un array de objetos de Mandatarios
        /// </summary>
        /// <param name="mandatarioGrupoDTO"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult IngresarAsociacion([FromBody]IEnumerable<MandatarioGrupoDTO> mandatarioGrupoDTO)
        {
            if (mandatarioGrupoDTO == null)
            {
                BadRequest();
            }

            string CodigoDocumento = Convert.ToString(mandatarioGrupoDTO.FirstOrDefault().CodigoDocumento);

            BasePropertiesDTO retornoDocumentoPoder = this._mandatarioGrupoAPP.GetEscrituraPoder(CodigoDocumento);

            if (retornoDocumentoPoder == null)
            {
                BadRequest();
            }

            string fechaInicio = (retornoDocumentoPoder.Resultado[0].FechaInicial).ToString("dd/MM/yyyy");

            var asociacionRetorno = new List<BasePropertiesDTO>();

            foreach (var item in mandatarioGrupoDTO)
            {
                item.FechaInicio = fechaInicio;
                item.FechaFinal = "31/12/3000";
                var resultado = this._mandatarioGrupoAPP.IngresarAsociacion(item);

                if (resultado.CodigoRetorno == (int)MantenedoresEstados.ErrorInesperado)
                {
                    @TempData["Error"] = true;
                    @TempData["TituloMensaje"] = "";
                    @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado al  Asociar  Mandatario(os) al Grupo.";
                }

                asociacionRetorno.Add(resultado);

            }

            return Json(asociacionRetorno);
        }

        /// <summary>
        /// Metodo orquestado para Listar Grupos de Sociedades de Mandatarios Filtrados por el Tipo de Grupo
        /// </summary>
        /// 
        [HttpGet]
        public ActionResult ListarMandatariosAsociados()
        {

            return View();
        }


        public JsonResult ListarMandatariosDeGrupo(string id)
        {
            if (id == null)
            {
                BadRequest();
            }

            GetMandatarioDTO ParametroGetMandatario = new GetMandatarioDTO
            {
                CodigoPersona = Convert.ToInt32(HttpContext.Session.GetString("_CodigoPersona")),
                CodigoTipo = Convert.ToString(HttpContext.Session.GetString("_CodigoTipo"))
            };

            BasePropertiesDTO mandatariosSociedadRetorno = this._mandatarioGrupoAPP.ListarMandatarios(ParametroGetMandatario);
            List<DatosParticipantesDTOResponse> listaTodosMandatarios = mandatariosSociedadRetorno.Resultado;

            BasePropertiesDTO mandatariosPorGrupoRetorno = this._mandatarioGrupoAPP.ConsultarListaGrupoMandatorio(id);
            List<MandatarioGrupoDTO> listaMandatariosAsignadosGrupo = mandatariosPorGrupoRetorno.Resultado;

            var listaMandatariosVigentes = listaMandatariosAsignadosGrupo.Where(x => x.CodigoRevocacion == 0);

            return this.Json(new
            {
                data = listaMandatariosVigentes
            });

        }


        /// <summary>
        /// Metodo orquestado para Listar Mandatarios Asociados en un Grupo de la Sociedad para Retirar Asociación
        /// </summary>
        /// 
        [HttpGet]
        public ActionResult ListarMandatariosAsociadosParaRetirar()
        {

            return View();
        }


        public JsonResult ListarMandatariosRetiroAsociacion(string id)
        {
            if (id == null)
            {
                BadRequest();
            }

            GetMandatarioDTO ParametroGetMandatario = new GetMandatarioDTO
            {
                CodigoPersona = Convert.ToInt32(HttpContext.Session.GetString("_CodigoPersona")),
                CodigoTipo = Convert.ToString(HttpContext.Session.GetString("_CodigoTipo"))
            };

            BasePropertiesDTO mandatariosSociedadRetorno = this._mandatarioGrupoAPP.ListarMandatarios(ParametroGetMandatario);
            List<DatosParticipantesDTOResponse> listaTodosMandatarios = mandatariosSociedadRetorno.Resultado;

            BasePropertiesDTO mandatariosPorGrupoRetorno = this._mandatarioGrupoAPP.ConsultarListaGrupoMandatorio(id);
            List<MandatarioGrupoDTO> listaMandatariosAsignadosGrupo = mandatariosPorGrupoRetorno.Resultado;

            var listaMandatariosVigentes = listaMandatariosAsignadosGrupo.Where(x => x.CodigoRevocacion == 0);

            return this.Json(new
            {
                data = listaMandatariosVigentes
            });

        }

        /// <summary>
        /// Json que recibe un array de objetos de Mandatarios
        /// </summary>
        /// <param name="mandatarioGrupoDTO"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult EliminarAsociacion([FromBody]MandatarioGrupoDTO mandatarioGrupoDTO)
        {
            if (mandatarioGrupoDTO == null)
            {
                BadRequest();
            }

            BasePropertiesDTO retornoAsociacion = this._mandatarioGrupoAPP.EliminarAsociacion(mandatarioGrupoDTO);

            if (retornoAsociacion == null)
            {
                BadRequest();
            }

            if (retornoAsociacion.CodigoRetorno == (int)MantenedoresEstados.ErrorInesperado)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = "";
                @TempData["CuerpoMensaje"] = "Ha ocurrido un error inesperado al  Asociar  Mandatario(os) al Grupo.";
            }

            return Json(retornoAsociacion);
        }

        #endregion

    }
}