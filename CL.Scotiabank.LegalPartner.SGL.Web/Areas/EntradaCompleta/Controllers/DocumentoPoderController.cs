﻿using CL.Scotiabank.LegalPartner.SGL.APP;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CL.Scotiabank.LegalPartner.SGL.Web.Areas.EntradaCompleta.Controllers
{
    //[Produces("application/json")]
    //[Route("api/DocumentoPoder")]

    public class DocumentoPoderController : Controller
    {
        #region Members

        private IDocumentoPoderAPP _documentoPoderAPP;
        private dynamic logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
        private readonly string tituloMensajeError = Resources.TituloMensajeError;
        private readonly string cuerpoMensajeError = Resources.CuerpoMensajeError;


        #endregion

        #region Constructor

        public DocumentoPoderController(IDocumentoPoderAPP documentoPoderApp)
        {
            this._documentoPoderAPP = documentoPoderApp;
        }

        #endregion

        public ActionResult Index(EntradaCompletaDTO paramUsuario)
        {
            DocumentoPoderDTO documentoPoderDTO = new DocumentoPoderDTO();

            documentoPoderDTO.IdSGL = paramUsuario.Sgl;
            documentoPoderDTO.CodigoTipoPersona = Convert.ToInt32(paramUsuario.CodigoTipoPersona);
            documentoPoderDTO.CodigoPersona = paramUsuario.CodigoPersona;

            return View(documentoPoderDTO);
        }

        #region LISTAR        

        /// <summary>
        /// Método para listar los documentos.
        /// </summary>
        /// /// <param name="id"> Codigo de persona</param>
        /// <returns> Lista de documentos de poder</returns>
        public JsonResult ListarDocumentos(int id)
        {
            bool exitoso = false;
            
            try
            {
                if (id == Convert.ToInt32(Resources.valorCero))
                {
                    BadRequest();
                }

                BasePropertiesDTO mandatarioRetorno = this._documentoPoderAPP.ListarDocumentos(id);
                exitoso = true;

                return this.Json(new
                {
                    data = mandatarioRetorno.Resultado,
                    Exitoso = exitoso
                });
            }
            catch (Exception ex)
            {                
                logger.Error(ex, "Stopped program because of exception");

                return Json(new
                {
                    Exitoso = exitoso,
                    Titulo = tituloMensajeError,
                    Mensaje = cuerpoMensajeError
                });
            }
        }

        /// <summary>
        /// Método para obtener los poderes de lso documentos poder.
        /// </summary>
        /// <param name="id"> Codigo de persona</param>
        /// <param name="checkIn"> Valor que trae el checkbox de la lista</param>
        /// <param name="sgl">Id SGL </param>
        /// <returns>Lista de poderes de documentos de poder</returns>
        public JsonResult ObtenerDatosPoderes(int id, bool checkIn, int sgl)
        {
            bool exitoso = false;

            try
            {
                if (id == Convert.ToInt32(Resources.valorCero))
                {
                    BadRequest();
                }

                IList<DatosPoderesAndDesMatDTOResponse> mandatarioRetorno = this._documentoPoderAPP.ObtenerDatosPoderes(id, checkIn, sgl);

                return this.Json(new
                {
                    data = mandatarioRetorno
                });
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Stopped program because of exception");

                return Json(new
                {
                    Exitoso = exitoso,
                    Titulo = tituloMensajeError,
                    Mensaje = cuerpoMensajeError
                });
            }
        }

        #endregion

        #region Grabar Documento de Poder

        /// <summary>
        /// Json que recibe el objeto para luego enviarlo a la paágina párcial Documento Poder
        /// </summary>
        /// <param name="datosModal"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult EnviaDataVistaParcial([FromBody]DocumentoPoderDTO datosModal)
        {
            if (ModelState.IsValid)
            {
                return Json(datosModal);
            }
            else
            {
                return Json("Error");
            }
        }
     //GET
        public ActionResult IngresarDocumento(DocumentoPoderDTO documentoPoderDTO)
        {
            //documentoPoderDTO.CodigoDocumentoRequest = 1442083;
            documentoPoderDTO.TipoDocumento = 1;

            if (documentoPoderDTO != null)
            {
                string codigoDocumento = Convert.ToString(documentoPoderDTO.CodigoDocumentoRequest);
                BasePropertiesDTO obtenerDocumentoRetorno = this._documentoPoderAPP.ObtenerDocumentoLegal(codigoDocumento);

                if (obtenerDocumentoRetorno != null)
                {
                    documentoPoderDTO.FechaDocumento = "";
                }
                else
                {

                }
                string codigoDocumentoRespuesta = Convert.ToString(documentoPoderDTO.CodigoDocumentoRequest);
                BasePropertiesDTO respuestLegalRetorno = this._documentoPoderAPP.ObtenerRespuestaLegal(codigoDocumento);

            }


            return View(documentoPoderDTO);
        }



        [HttpPost]
        ActionResult IngresarDocumentoGrabar(DocumentoPoderDTO documentoPoderDTO)
        {
            BasePropertiesDTO DocumentoRetorno = this._documentoPoderAPP.IngresarDocumentoPoder(documentoPoderDTO);

            return View();
        }

        public JsonResult ComboTipoDocumento()
        {
            IList<DropDownListDTO> lstDocumentos = new List<DropDownListDTO>();
            BasePropertiesDTO tipoDocumentoRetorno = this._documentoPoderAPP.ObtenerListaTipoDocumento();
            List<DocumentoPoderDTO> tipoDocumentoRetornofiltro = tipoDocumentoRetorno.Resultado;
            var listaTipoDocumentoPoderFiltro = tipoDocumentoRetornofiltro.Where(x => x.Codigo == 1 || x.Codigo == 2 || x.Codigo == 3 || x.Codigo == 8);

            Parallel.ForEach((IEnumerable<DocumentoPoderDTO>)listaTipoDocumentoPoderFiltro, (registro) =>
            {
                lstDocumentos.Add(new DropDownListDTO { id = registro.Codigo.ToString(), text = registro.Descripcion });
            });

            return Json(lstDocumentos);
        }

        public JsonResult ComboNotaria()
        {
            IList<DropDownListDTO> lstDocumentos = new List<DropDownListDTO>();
            BasePropertiesDTO notariaRetorno = this._documentoPoderAPP.ObtenerListaNotaria();
            //List<DocumentoPoderDTO> tipoDocumentoRetornofiltro = tipoDocumentoRetorno.Resultado;
            // var listaTipoDocumentoPoderFiltro = tipoDocumentoRetornofiltro.Where(x => x.Codigo == 1 || x.Codigo == 2 || x.Codigo == 3 || x.Codigo == 8);

            Parallel.ForEach((IEnumerable<DocumentoPoderDTO>)notariaRetorno.Resultado, (registro) =>
            {
                lstDocumentos.Add(new DropDownListDTO { id = registro.Codigo.ToString(), text = registro.Descripcion });
            });

            return Json(lstDocumentos);
        }


        public JsonResult ComboConservador()
        {
            IList<DropDownListDTO> lstDocumentos = new List<DropDownListDTO>();
            BasePropertiesDTO notariaRetorno = this._documentoPoderAPP.ObtenerConservadores();         

            Parallel.ForEach((IEnumerable<DocumentoPoderDTO>)notariaRetorno.Resultado, (registro) =>
            {
                lstDocumentos.Add(new DropDownListDTO { id = registro.Codigo.ToString(), text = registro.Descripcion });
            });

            return Json(lstDocumentos);
        }

        #endregion

        #region EDITAR Documento Poder
        public ActionResult EditarDocumento(DocumentoPoderDTO documentoPoderDTO)
        {
         

            if (documentoPoderDTO.CodigoDocumentoRequest != 0)
            {
                string codigoDocumento = Convert.ToString(documentoPoderDTO.CodigoDocumentoRequest);
                BasePropertiesDTO obtenerDocumentoRetorno = this._documentoPoderAPP.ObtenerDocumentoLegal(codigoDocumento);

                if (obtenerDocumentoRetorno != null)
                {
                    documentoPoderDTO.FechaDocumento = "";
                }
                else
                {

                }
                string codigoDocumentoRespuesta = Convert.ToString(documentoPoderDTO.CodigoDocumentoRequest);
                BasePropertiesDTO respuestLegalRetorno = this._documentoPoderAPP.ObtenerRespuestaLegal(codigoDocumento);

            }


            return View(documentoPoderDTO);
        }


        #endregion 

    }
}