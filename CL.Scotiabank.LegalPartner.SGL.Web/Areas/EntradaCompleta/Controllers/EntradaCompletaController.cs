﻿using CL.Scotiabank.LegalPartner.CabeceraSglAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.ConsultaAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.PoderYRevocacionesAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.APP;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO.DTO.EntradaCompleta;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Mvc;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Resources = CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities.Resources;

namespace CL.Scotiabank.LegalPartner.SGL.Web.Areas.EntradaCompleta.Controllers
{

    [ActionsFilter]
    public class EntradaCompletaController : Controller
    {
        #region Members

        private IEntradaCompletaAPP _entradaCompletaAPP;
        private dynamic logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
        private readonly string tituloMensajeError = Resources.TituloMensajeError;
        private readonly string cuerpoMensajeError = Resources.CuerpoMensajeError;
        #endregion

        #region Constructor

        public EntradaCompletaController(IEntradaCompletaAPP consultaPoderesAPP)
        {
            this._entradaCompletaAPP = consultaPoderesAPP;
        }

        #endregion

        /// <summary>
        /// Index de entrada completa
        /// </summary>
        /// <param name="paramCargaIndex"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Index(IngresarSGLDTO paramCargaIndex)
        {

            ConsultaPersonaDatosDTO paramPersonaRetorno = new ConsultaPersonaDatosDTO();

            try
            {
                int codEstadoLegal = 0;
                VerInformeCapitalDTO InformeDto = new VerInformeCapitalDTO();

                UsuarioConsultaDTORequest paramPersona = new UsuarioConsultaDTORequest
                {
                    Rut = int.Parse(paramCargaIndex.Rut.Replace(".", "").Split("-")[0]),//17310107,// 76005049, //76005049, //1, 
                    Nombre = string.Empty,
                    ApellidoPaterno = string.Empty,
                    ApellidoMaterno = string.Empty,
                    CantidadRegistros = 0
                };

                BasePropertiesDTO datosPersonaRetorno = this._entradaCompletaAPP.ObtenerDatosPersona(paramPersona);

                BasePropertiesDTO datosPersonaJuridicaRetorno = this._entradaCompletaAPP.ConsultaPersonaJuridicaDatos(datosPersonaRetorno.Resultado[0].CodigoPersona);
                ViewBag.NombreLegal = datosPersonaJuridicaRetorno.Resultado[0].NombreLegal;
                ViewBag.CodigoTipoPersona = datosPersonaJuridicaRetorno.Resultado[0].CodigoTipoPersona;
                codEstadoLegal = Convert.ToInt32(datosPersonaJuridicaRetorno.Resultado[0].CodigoEstadoLegal).Equals(0) ? 1 : Convert.ToInt32(datosPersonaJuridicaRetorno.Resultado[0].CodigoEstadoLegal);
                ViewBag.CodigoEstadoLegal = codEstadoLegal;

                BasePropertiesDTO datosComboRetorno = this._entradaCompletaAPP.GetListaEstadoLegal();
                ViewBag.DescEstadoLegal = ((IEnumerable<ConsultaListaEstadoLegalDTOResponse>)datosComboRetorno.Resultado).Where(x => x.Codigo == codEstadoLegal).FirstOrDefault().Descripcion;
                ViewBag.Sgl = paramCargaIndex.IdSGL;
                InformeDto.SGL = paramCargaIndex.IdSGL;
                if (paramCargaIndex.Entrada == "C")
                {
                    InformeDto.Entrada = "EC";
                }
                else
                {
                    InformeDto.Entrada = "N";
                }
                ViewBag.Entrada = InformeDto.Entrada;

                return View("Index", (ConsultaPersonaDatosDTO)datosPersonaRetorno.Resultado[0]);

            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = tituloMensajeError;
                @TempData["CuerpoMensaje"] = cuerpoMensajeError;
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Home", "Login", new { area = "Login" });
            }
           
        }
        
        /// <summary>
        /// Lista los documentos en el DataTable de legalizacion.
        /// </summary>
        /// <param name="codigoPersona"></param>
        /// <returns></returns>
        public JsonResult ListaDocumentos(int codigoPersona)
        {
            IList<ListaDocumentoDTO> lstDocumentos = new List<ListaDocumentoDTO>();
            BasePropertiesDTO datosPersonaRetorno = this._entradaCompletaAPP.ObtenerListadoDocumentos(codigoPersona);

            if (datosPersonaRetorno != null)
            {
                lstDocumentos = _entradaCompletaAPP.MapeaListaDocumentos((IEnumerable<DocumentoDTOResponse>)datosPersonaRetorno.Resultado);
            }

            return this.Json(new
            {
                data = lstDocumentos
            });
        }

        /// <summary>
        /// Json que recibe el objeto para luego enviarlo al modal Consulta Poder
        /// </summary>
        /// <param name="datosModal"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult EnviaDataVistaParcial([FromBody]EntradaCompletaDTO datosModal)
        {
            bool exitoso = false;
            if (ModelState.IsValid)
            {
                exitoso = true;
                return Json(new { Exitoso = exitoso, Data = datosModal });                
            }
            else
            {
                return Json(new { Exitoso = exitoso, Data = datosModal });
            }
        }

        [HttpPost]
        public JsonResult EnviaDataVistaParcialAgregar([FromBody]DatosModalAgregarDocumEscriDTO datosModal)
        {
            bool exitoso = false;
            if (ModelState.IsValid)
            {
                exitoso = true;
                return Json(new { Exitoso = exitoso, Data = datosModal });
            }
            else
            {
                return Json(new { Exitoso = exitoso, Data = datosModal });
            }
        }

        /// <summary>
        /// Carga inicial del Tab de Constitucion y legalizacion
        /// </summary>
        /// <param name="paramUsuario"></param>
        /// <returns></returns>
        public IActionResult ConstitucionYLegalizacion(EntradaCompletaDTO paramUsuario)
        {
            ConstitucionYLegalizacionDTO paramConsLEg = new ConstitucionYLegalizacionDTO();
            string codTipoPersonaFLEG = string.Empty;
            int codTipoPersona = 0;
            int codEPV = 0;
            //Cargar Documentos

            try
            {
                BasePropertiesDTO datosPersonaRetorno = this._entradaCompletaAPP.ObtenerListadoDocumentos(paramUsuario.CodigoPersona);

                if (datosPersonaRetorno.Resultado.Count == 0)
                {
                    //Insertar el registro
                    IngresarDatosDocumentosDTO paramDocumento = new IngresarDatosDocumentosDTO
                    {
                        CodigoPersona = paramUsuario.CodigoPersona,
                        IdSGL = paramUsuario.Sgl
                    };

                    paramDocumento = LlenaInsertarVacio(paramDocumento);

                    BasePropertiesDTO documentoRetorno = this._entradaCompletaAPP.IngresarDatosDocumento(paramDocumento);
                    datosPersonaRetorno = this._entradaCompletaAPP.ObtenerListadoDocumentos(paramUsuario.CodigoPersona);
                }

                BasePropertiesDTO datosPersonaJuridicaRetorno = this._entradaCompletaAPP.ConsultaPersonaJuridicaDatos(paramUsuario.CodigoPersona);
                codTipoPersona = Convert.ToInt32(datosPersonaJuridicaRetorno.Resultado[0].CodigoTipoPersona);
                paramConsLEg.CodTipoPersona = codTipoPersona;

                BasePropertiesDTO datosTipoPersonaRetorno = this._entradaCompletaAPP.GetListadoTipoPersonaDetalle();
                if (codTipoPersona != 0)
                {
                    codTipoPersonaFLEG = ((IEnumerable<TipoPersonaDetalleDTOResponse>)datosTipoPersonaRetorno.Resultado).Where(x => x.Codigo == codTipoPersona.ToString()).FirstOrDefault().CodigoLegal;
                }


                paramConsLEg.TipoPersonaFLEG = codTipoPersonaFLEG.Trim();
                paramConsLEg.Sgl = paramUsuario.Sgl;
                paramConsLEg.intTipoLeg = ((IEnumerable<DocumentoDTOResponse>)datosPersonaRetorno.Resultado).Where(x => x.CodigoTipoLegal.Equals(1)).Count() > 0 ? 1 : 0;
                paramConsLEg.CodEPV = codEPV;
                paramConsLEg.CodigoEstadoLegal = datosPersonaJuridicaRetorno.Resultado[0].CodigoEstadoLegal;
                paramConsLEg.Estado = datosPersonaJuridicaRetorno.Resultado[0].Estado;
                paramConsLEg.FechaConsulta = datosPersonaRetorno.Resultado[0].FechaDocumento.ToString("dd/MM/yyyy").Replace('-', '/');
                paramConsLEg.IdPersona = datosPersonaRetorno.Resultado[0].CodigoPersona;
                ViewBag.lstDocumentos = datosPersonaRetorno.Resultado;

                return PartialView(paramConsLEg);

            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = tituloMensajeError;
                @TempData["CuerpoMensaje"] = cuerpoMensajeError;
                logger.Error(ex, "Stopped program because of exception");

                return PartialView(paramConsLEg);
                //return RedirectToAction("Home", "Login", new { area = "Login" });
            }
           
        }

        /// <summary>
        /// Llena objeto para ingresar un registro cuanto la SGL no contiene documentos
        /// </summary>
        /// <param name="paramDocumento"></param>
        /// <returns></returns>
        private IngresarDatosDocumentosDTO LlenaInsertarVacio(IngresarDatosDocumentosDTO paramDocumento)
        {
            paramDocumento.CodigoTipoLegal = 1;
            paramDocumento.FechaDocumento = "31/12/3000";
            paramDocumento.NumeroDocumento = string.Empty;
            paramDocumento.CodigoNotaria = "0";
            paramDocumento.FechaInscripcion = "31/12/3000";

            paramDocumento.AnnoInscripcion = "0";
            paramDocumento.Fojas = "31/12/3000";
            paramDocumento.NumeroFoja = "31/12/3000";
            paramDocumento.CodigoConservador = 0;
            paramDocumento.FechaPublicacion = "31/12/3000";
            paramDocumento.FechaSesionDirectorio = "31/12/3000";
            paramDocumento.FechaJuntaAccionista = "31/12/3000";
            paramDocumento.CodigoRectificacion = 0;
            paramDocumento.FechaDecreto = "31/12/3000";
            paramDocumento.NumeroDecreto = string.Empty;
            paramDocumento.FechaPublicacionDecreto = "31/12/3000";
            paramDocumento.FechaProtocolarizacion = "31/12/3000";

            paramDocumento.CodigoNotariaProtocolo = "0";
            paramDocumento.CodigoDocumentoOrigen = 0;
            paramDocumento.NombreOrigen = string.Empty;
            paramDocumento.LugarOrigen = string.Empty;
            paramDocumento.Observaciones = string.Empty;
            paramDocumento.ObservacionPoder = string.Empty;
            paramDocumento.EstadoConsulta = "1";
            paramDocumento.FechaInicio = DateTime.Now.ToString("dd/MM/yyyy");

            paramDocumento.FechaFinal = "31/12/3000";
            paramDocumento.CodigoRevocacion = 0;
            paramDocumento.FechaResolucionQuiebra = "31/12/3000";
            paramDocumento.CodigoTribunal = 0;
            paramDocumento.FechaPublicacionQuiebra = "31/12/3000";
            paramDocumento.ResponsableDocumento = string.Empty;
            paramDocumento.LugarDocumento = string.Empty;

            paramDocumento.NombreInforme = string.Empty;
            paramDocumento.TipoDocumento = 1;
            paramDocumento.FechaRenovacion = "31/12/3000";
            paramDocumento.FechaActVigencia = "31/12/3000";
            paramDocumento.FechaTerminaVigencia = "31/12/3000";

            return paramDocumento;
        }

        public IActionResult DatosEstatutos(EntradaCompletaDTO paramUsuario)
        {
            return PartialView();
        }

        public IActionResult PoderesYRevocaciones(EntradaCompletaDTO paramUsuario)
        {
            return PartialView();
        }

        public IActionResult CrearDatoEstatuto(EntradaCompletaDTO paramUsuario)
        {
            try
            {
                List<string> ObservacionesPredefinidas = new List<string>();
                DateTime FechaActualizacion;
                string FechaActualizacionString = string.Empty;
                string[] condicion = { "Sociedad Constituida", "Fundación Constituida", "Sociedad Cerrada Constituida", "Sociedad Abierta Constituida", "Informe Pendiente", "Texto Libre" };
                List<string> lista = new List<string>(condicion);


                if (paramUsuario.CodigoTipoPersona == "2")
                {
                    lista.RemoveAt(1);
                }
                else if (paramUsuario.CodigoTipoPersona == "7")
                {
                    lista.RemoveAt(0);
                    lista.RemoveAt(2);
                    lista.RemoveAt(3);
                }
                else
                {
                    lista.RemoveAt(1);
                    lista.RemoveAt(2);
                    lista.RemoveAt(3);
                }

                BasePropertiesDTO datoEstatutoRetorno = this._entradaCompletaAPP.ObtenerDatosEstatutos(paramUsuario.CodigoPersona);
                ViewBag.FormatoPredefinido = lista;
                FechaActualizacion = datoEstatutoRetorno.Resultado[0].FechaUltimoEts;
                FechaActualizacionString = FechaActualizacion.ToString("dd/MM/yyyy");
                FechaActualizacionString = FechaActualizacionString.Replace('-', '/');
                ViewBag.FechaActualiacion = FechaActualizacionString;
                ViewBag.SGL = paramUsuario.Sgl;
                ViewBag.Entrada = paramUsuario.Entrada;
                datoEstatutoRetorno.Resultado[0].CodigoTipoPersona = paramUsuario.CodigoTipoPersona;
                return PartialView("CrearDatoEstatuto", (ConsultaPersonaJuridicaDatosDTOResponse)datoEstatutoRetorno.Resultado[0]);
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = tituloMensajeError;
                @TempData["CuerpoMensaje"] = cuerpoMensajeError;
                logger.Error(ex, "Stopped program because of exception");

                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public JsonResult EnviarDatoEstatuto(UsuarioConsultaDTO paramDTO)
        {
            try
            {
                BasePropertiesDTO datoEstatutoRetorno = this._entradaCompletaAPP.AgregarDatoEstatuto(paramDTO);
                
                return Json(paramDTO);
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = tituloMensajeError;
                @TempData["CuerpoMensaje"] = cuerpoMensajeError;
                logger.Error(ex, "Stopped program because of exception");

                return Json(new { code = 1 });
            }
        }

        /// <summary>
        /// Carga Combo estado
        /// </summary>
        /// <returns></returns>
        public JsonResult CargaComboEstado()
        {
            BasePropertiesDTO datosComboRetorno = this._entradaCompletaAPP.GetListaEstadoLegal();

            if (datosComboRetorno == null)
                return Json(null);

            List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

            Parallel.ForEach((IEnumerable<ConsultaListaEstadoLegalDTOResponse>)datosComboRetorno.Resultado, (registro) =>
            {
                listaDTO.Add(new DropDownListDTO { id = registro.Codigo.ToString(), text = registro.Descripcion });
            });

            return Json(listaDTO);
        }

        /// <summary>
        /// Carga Combo Tipo Persona juridica
        /// </summary>
        /// <returns></returns>
        public JsonResult CargaComboTipoPersona()
        {
            BasePropertiesDTO datosComboRetorno = this._entradaCompletaAPP.GetListadoTipoPersonaDetalle();

            if (datosComboRetorno == null)
                return Json(null);

            List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

            Parallel.ForEach((IEnumerable<TipoPersonaDetalleDTOResponse>)datosComboRetorno.Resultado, (registro) =>
            {
                listaDTO.Add(new DropDownListDTO { id = registro.Codigo.ToString(), text = registro.Descripcion });
            });

            return Json(listaDTO);
        }

        /// <summary>
        /// Levanta modal para agregar nuevo documento
        /// </summary>
        /// <param name="paramModal"></param>
        /// <returns></returns>
        public IActionResult ModalAgregarLegalizacion(DatosModalAgregarDocumEscriDTO paramModal)
        {
            IngresarDatosDocumentosDTO paramDocumento = new IngresarDatosDocumentosDTO();

            try
            {
                paramDocumento.TipoSoc = paramModal.TipoSoc;
                paramDocumento.EstadoLegal = paramModal.EstadoLegal;
                paramDocumento.NumFilas = paramModal.NumFilas;
                paramDocumento.CodDocumentos = paramModal.CodDoctos;
                paramDocumento.FechaCons = paramModal.FechaCons.ToString();
                paramDocumento.IdSGL = paramModal.Sgl;
                paramDocumento.CodigoPersona = paramModal.PerId;

                return PartialView(paramDocumento);
            }
            catch (Exception ex)
            {

                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = tituloMensajeError;
                @TempData["CuerpoMensaje"] = cuerpoMensajeError;
                logger.Error(ex, "Stopped program because of exception");

                //return RedirectToAction("Home", "Login", new { area = "Login" });
                return PartialView(false);
            }
           
        }

        /// <summary>
        /// Levanta Modal para Ver un documento
        /// </summary>
        /// <param name="paramModal"></param>
        /// <returns></returns>
        public IActionResult ModalVerLegalizacion(DatosModalAgregarDocumEscriDTO paramModal)
        {
            string origen = string.Empty;
            string rectificacion = string.Empty;
            IngresarDatosDocumentosDTO paramDocumento = new IngresarDatosDocumentosDTO();

            try
            {
                BasePropertiesDTO datosDocumentoRetorno = this._entradaCompletaAPP.ObtenerDocumentoLegal(paramModal.CodDocumento);

                //Obtener Tipo Legalizacion
                //Obtener Notaria
                BasePropertiesDTO datosNotariaRetorno = this._entradaCompletaAPP.GetListaNotaria();


                //Obtener Registro
                //Obtener Conservador
                BasePropertiesDTO datosRegistroRetorno = this._entradaCompletaAPP.ObtenerRegistros();

                //Obtener Escritura Rectificada
                // es el numero
                List<DropDownListDTO> listaRectificacion = ObtieneListaRectificacion(paramModal.CodDoctos);

            if (!datosDocumentoRetorno.Resultado[0].CodRct.Equals("-1"))
                rectificacion = listaRectificacion.Where(x => x.id == datosDocumentoRetorno.Resultado[0].CodRct.ToString()).FirstOrDefault().text;

                //Obtener Notaría Protocolización
                //Es 0
                //Obtener Origen
                BasePropertiesDTO datosOrigenRetorno = this._entradaCompletaAPP.GetDatosOrigen(paramModal.CodDocumento.ToString(), paramModal.PerId.ToString());
                if (datosOrigenRetorno.Resultado.Count != 0)
                    origen = ((IEnumerable<ObtenerDatosOrigenDTOResponse>)datosOrigenRetorno.Resultado).Where(x => x.CodigoOrigen == datosDocumentoRetorno.Resultado[0].CodigoDocumentoOrigen).FirstOrDefault().Descripcion;

               
                paramDocumento.TipoSoc = paramModal.TipoSoc;
                paramDocumento.EstadoLegal = paramModal.EstadoLegal;
                paramDocumento.NumFilas = paramModal.NumFilas;
                paramDocumento.CodDocumentos = paramModal.CodDoctos;
                paramDocumento.FechaCons = paramModal.FechaCons.ToString();
                paramDocumento.IdSGL = paramModal.Sgl;
                paramDocumento.CodigoPersona = paramModal.PerId;

                paramDocumento.CodigoTipoLegal = datosDocumentoRetorno.Resultado[0].CodigoTipoLegalizacion;
                paramDocumento.FechaDocumento = datosDocumentoRetorno.Resultado[0].FechaDocumento.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocumento.CodigoNotaria = datosDocumentoRetorno.Resultado[0].CodigoNotaria.ToString();
                paramDocumento.Notaria = ((IEnumerable<ObtenerListaNotariaDTOResponse>)datosNotariaRetorno.Resultado).Where(x => x.Codigo == datosDocumentoRetorno.Resultado[0].CodigoNotaria).FirstOrDefault().Descripcion;

                paramDocumento.NumeroDocumento = datosDocumentoRetorno.Resultado[0].NumeroDocumento;
                paramDocumento.FechaInscripcion = datosDocumentoRetorno.Resultado[0].FechaInp.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocumento.Fojas = datosDocumentoRetorno.Resultado[0].FojaDocumento;

                paramDocumento.NumeroFoja = datosDocumentoRetorno.Resultado[0].NumeroFojaDocumento;
                paramDocumento.CodigoConservador = datosDocumentoRetorno.Resultado[0].CodigoConservador;
                paramDocumento.Conservador = ((IEnumerable<ObtenerRegistrosDTO>)datosRegistroRetorno.Resultado).Where(x => x.Codigo == datosDocumentoRetorno.Resultado[0].CodigoConservador).FirstOrDefault().Descripcion;

                paramDocumento.FechaSesionDirectorio = datosDocumentoRetorno.Resultado[0].FechaSesionDirectorio.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocumento.FechaJuntaAccionista = datosDocumentoRetorno.Resultado[0].FechaJuntaExtraordinariaAccionistas.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocumento.CodigoRectificacion = Convert.ToInt32(datosDocumentoRetorno.Resultado[0].CodRct);
                paramDocumento.Rectificacion = rectificacion;
                paramDocumento.FechaDecreto = datosDocumentoRetorno.Resultado[0].FechaDecreto.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocumento.NumeroDecreto = datosDocumentoRetorno.Resultado[0].NumeroDecreto;
                //paramDocumento.FechaPublicacionDecreto = datosDocumentoRetorno.Resultado[0].FechaDecreto.ToString("dd/MM/yyyy").Replace("-", "/");

                paramDocumento.DocumentoOrigen = origen;
                paramDocumento.FechaPublicacionDecreto = datosDocumentoRetorno.Resultado[0].FechaPublicacionDocumento.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocumento.FechaPublicacion = datosDocumentoRetorno.Resultado[0].FechaPublicacion.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocumento.FechaResolucionQuiebra = datosDocumentoRetorno.Resultado[0].FechaResolucionQuiebra.ToString("dd/MM/yyyy").Replace("-", "/");
                //paramDocumento.FechaTerminaVigencia = datosDocumentoRetorno.Resultado[0].FechaTerminaVigencia.ToString("dd/MM/yyyy");
                paramDocumento.CodigoNotariaProtocolo = datosDocumentoRetorno.Resultado[0].CodigoNotariaProtocolizacion;
                paramDocumento.NombreOrigen = datosDocumentoRetorno.Resultado[0].NombreOrigen;
                paramDocumento.LugarOrigen = datosDocumentoRetorno.Resultado[0].LugarOrigen;
                paramDocumento.Observaciones = datosDocumentoRetorno.Resultado[0].Observaciones;

                return PartialView(paramDocumento);
            }
            catch (Exception ex)
            {

                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = tituloMensajeError;
                @TempData["CuerpoMensaje"] = cuerpoMensajeError;
                logger.Error(ex, "Stopped program because of exception");

                //return RedirectToAction("Home", "Login", new { area = "Login" });
                return PartialView(paramDocumento);
            }


        }

        /// <summary>
        /// Levanta modal para editar un documento
        /// </summary>
        /// <param name="paramModal"></param>
        /// <returns></returns>
        public IActionResult ModalEditarLegalizacion(DatosModalAgregarDocumEscriDTO paramModal)
        {
            string origen = string.Empty;
            string rectificacion = string.Empty;
            IngresarDatosDocumentosDTO paramDocumento = new IngresarDatosDocumentosDTO();

            try
            {
                BasePropertiesDTO datosDocumentoRetorno = this._entradaCompletaAPP.ObtenerDocumentoLegal(paramModal.CodDocumento);

                //Obtener Tipo Legalizacion
                //Obtener Notaria
                BasePropertiesDTO datosNotariaRetorno = this._entradaCompletaAPP.GetListaNotaria();


                //Obtener Registro
                //Obtener Conservador
                BasePropertiesDTO datosRegistroRetorno = this._entradaCompletaAPP.ObtenerRegistros();

                //Obtener Escritura Rectificada
                // es el numero
                List<DropDownListDTO> listaRectificacion = ObtieneListaRectificacion(paramModal.CodDoctos);
                rectificacion = listaRectificacion.Where(x => x.id == paramModal.CodDocumento.ToString()).FirstOrDefault().text;

                //Obtener Notaría Protocolización
                //Es 0
                //Obtener Origen
                BasePropertiesDTO datosOrigenRetorno = this._entradaCompletaAPP.GetDatosOrigen(paramModal.CodDocumento.ToString(), paramModal.PerId.ToString());
                if (datosOrigenRetorno.Resultado.Count != 0)
                    origen = ((IEnumerable<ObtenerDatosOrigenDTOResponse>)datosOrigenRetorno.Resultado).Where(x => x.CodigoOrigen == datosDocumentoRetorno.Resultado[0].CodigoDocumentoOrigen).FirstOrDefault().Descripcion;

               
                paramDocumento.CodDocumento = paramModal.CodDocumento;
                paramDocumento.TipoSoc = paramModal.TipoSoc;
                paramDocumento.EstadoLegal = paramModal.EstadoLegal;
                paramDocumento.NumFilas = paramModal.NumFilas;
                paramDocumento.CodDocumentos = paramModal.CodDoctos;
                paramDocumento.FechaCons = paramModal.FechaCons.ToString();
                paramDocumento.IdSGL = paramModal.Sgl;
                paramDocumento.CodigoPersona = paramModal.PerId;

                paramDocumento.CodigoTipoLegal = datosDocumentoRetorno.Resultado[0].CodigoTipoLegalizacion;
                paramDocumento.FechaDocumento = datosDocumentoRetorno.Resultado[0].FechaDocumento.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocumento.CodigoNotaria = datosDocumentoRetorno.Resultado[0].CodigoNotaria.ToString();
                paramDocumento.Notaria = ((IEnumerable<ObtenerListaNotariaDTOResponse>)datosNotariaRetorno.Resultado).Where(x => x.Codigo == datosDocumentoRetorno.Resultado[0].CodigoNotaria).FirstOrDefault().Descripcion;

                paramDocumento.NumeroDocumento = datosDocumentoRetorno.Resultado[0].NumeroDocumento;
                paramDocumento.FechaInscripcion = datosDocumentoRetorno.Resultado[0].FechaInp.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocumento.Fojas = datosDocumentoRetorno.Resultado[0].FojaDocumento;

                paramDocumento.NumeroFoja = datosDocumentoRetorno.Resultado[0].NumeroFojaDocumento;
                paramDocumento.CodigoConservador = datosDocumentoRetorno.Resultado[0].CodigoConservador;
                paramDocumento.Conservador = ((IEnumerable<ObtenerRegistrosDTO>)datosRegistroRetorno.Resultado).Where(x => x.Codigo == datosDocumentoRetorno.Resultado[0].CodigoConservador).FirstOrDefault().Descripcion;

                paramDocumento.FechaSesionDirectorio = datosDocumentoRetorno.Resultado[0].FechaSesionDirectorio.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocumento.FechaJuntaAccionista = datosDocumentoRetorno.Resultado[0].FechaJuntaExtraordinariaAccionistas.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocumento.CodigoRectificacion = Convert.ToInt32(datosDocumentoRetorno.Resultado[0].CodRct);
                paramDocumento.Rectificacion = rectificacion;
                paramDocumento.FechaDecreto = datosDocumentoRetorno.Resultado[0].FechaDecreto.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocumento.NumeroDecreto = datosDocumentoRetorno.Resultado[0].NumeroDecreto;
                //paramDocumento.FechaPublicacionDecreto = datosDocumentoRetorno.Resultado[0].FechaDecreto.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocumento.FechaPublicacionDecreto = datosDocumentoRetorno.Resultado[0].FechaPublicacionDocumento.ToString("dd/MM/yyyy").Replace("-", "/");

                paramDocumento.DocumentoOrigen = origen;
                paramDocumento.FechaPublicacion = datosDocumentoRetorno.Resultado[0].FechaPublicacion.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocumento.FechaResolucionQuiebra = datosDocumentoRetorno.Resultado[0].FechaResolucionQuiebra.ToString("dd/MM/yyyy").Replace("-", "/");
                //paramDocumento.FechaTerminaVigencia = datosDocumentoRetorno.Resultado[0].FechaTerminaVigencia.ToString("dd/MM/yyyy");
                paramDocumento.CodigoNotariaProtocolo = datosDocumentoRetorno.Resultado[0].CodigoNotariaProtocolizacion;
                paramDocumento.NombreOrigen = datosDocumentoRetorno.Resultado[0].NombreOrigen;
                paramDocumento.LugarOrigen = datosDocumentoRetorno.Resultado[0].LugarOrigen;
                paramDocumento.Observaciones = datosDocumentoRetorno.Resultado[0].Observaciones;

                return PartialView(paramDocumento);
            }
            catch (Exception ex)
            {

                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = tituloMensajeError;
                @TempData["CuerpoMensaje"] = cuerpoMensajeError;
                logger.Error(ex, "Stopped program because of exception");

                //return RedirectToAction("Home", "Login", new { area = "Login" });
                return PartialView(paramDocumento);
            }

           
        }

        [HttpPost]
        public JsonResult EditaDocumento([FromBody] IngresarDatosDocumentosDTO paramDocumento)
        {
            string mensaje = string.Empty;
            bool exitoso = false;

            if (ModelState.IsValid)
            {
                return Json(new { Exitoso = exitoso, Mensaje = mensaje });
            }
            else
            {
                return Json(new { Exitoso = exitoso, Mensaje = mensaje });
            }
        }

        [HttpPost]
        public JsonResult CreateDocumento([FromBody] IngresarDatosDocumentosDTO paramDocumento)
        {
            string mensaje = string.Empty;
            bool exitoso = false;
            int codDoc = 0;

            try
            {
                DateTime fecha1 = new DateTime();
                DateTime fecha2 = new DateTime();
                DateTime fecha3 = new DateTime(3000, 12, 31);

                //Validamos los campos
                paramDocumento = ValidaCampos(paramDocumento);

                //Termino Fecha
                fecha1 = Convert.ToDateTime(paramDocumento.FechaDocumento);
                fecha2 = Convert.ToDateTime(paramDocumento.FechaCons);

                if (paramDocumento.Observaciones.Length > 6450)
                {
                    mensaje = "Observaciones ha superado el tamaño máximo de caracteres permitido.";
                }
                else if (fecha1 < fecha2 && fecha2 != fecha3)
                {
                    mensaje = "Observaciones ha superado el tamaño máximo de caracteres permitido.";
                }
                else if (fecha1 < fecha2 && fecha2 != fecha3)
                {
                    mensaje = "La fecha de la escritura es anterior a la de constitución.";
                }
                else
                {
                    if (paramDocumento.Modo.Equals("A"))
                    {
                        if (paramDocumento.EstadoLegal == 3)
                        {
                            //Actualizar Tribunal (Juzgado)
                            BasePropertiesDTO TribunalRetorno = this._entradaCompletaAPP.AgregarTribunal(paramDocumento.Juzgado);
                        }

                        BasePropertiesDTO documentoRetorno = this._entradaCompletaAPP.IngresarDatosDocumento(paramDocumento);

                        if (documentoRetorno != null)
                        {
                            int codigoPersonaRetorno = documentoRetorno.CodigoRetorno;

                            if (codigoPersonaRetorno == (int)MantenedoresEstados.Exitoso)
                            {
                                mensaje = "Se ha creado el registro exitosamente.";
                                exitoso = true;
                                codDoc = documentoRetorno.Resultado[0].CodigoDocumento;
                                //return RedirectToAction("Index");
                            }
                            else if (codigoPersonaRetorno == (int)MantenedoresEstados.RegistroExistente)
                            {
                                mensaje = "El registro que esta intentando crear ya existe, intente con otro";
                            }
                            else
                            {
                                mensaje = cuerpoMensajeError;
                            }
                        }
                    }
                    else if (paramDocumento.Modo.Equals("E"))
                    {
                        if (paramDocumento.EstadoLegal == 3)
                        {
                            //Actualizar Tribunal (Juzgado)
                            BasePropertiesDTO TribunalRetorno = this._entradaCompletaAPP.ActualizaTribunal(paramDocumento.CodDocumento.ToString(), paramDocumento.Juzgado);
                        }
                        else
                        {
                            //Juzgado = 0
                        }

                        BasePropertiesDTO documentoRetorno = this._entradaCompletaAPP.ActualizarDatosDocumento(paramDocumento);

                        if (documentoRetorno != null)
                        {
                            int codigoPersonaRetorno = documentoRetorno.CodigoRetorno;

                            if (codigoPersonaRetorno == (int)MantenedoresEstados.Exitoso)
                            {
                                mensaje = "Se ha actualizado el registro exitosamente.";
                                exitoso = true;
                                //return RedirectToAction("Index");
                            }
                            else
                            {
                                mensaje = cuerpoMensajeError;
                            }
                        }
                    }
                }
                return Json(new { Exitoso = exitoso, Mensaje = mensaje, CodiDocumento = codDoc });
                //return Json(mensaje);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Stopped program because of exception");
                mensaje = cuerpoMensajeError;
                return Json(new { Exitoso = exitoso, Mensaje = mensaje, CodiDocumento = codDoc });
            }
        }

        private IngresarDatosDocumentosDTO ValidaCampos(IngresarDatosDocumentosDTO paramDocumento)
        {
            paramDocumento.CodigoTipoLegal = paramDocumento.EstadoLegal;

            if (paramDocumento.NombreOrigen == null) paramDocumento.NombreOrigen = string.Empty;

            if (paramDocumento.LugarOrigen == null) paramDocumento.LugarOrigen = string.Empty;

            if (paramDocumento.Observaciones == null) paramDocumento.Observaciones = string.Empty;

            paramDocumento.EstadoConsulta = "1";

            if (paramDocumento.ResponsableDocumento == null) paramDocumento.ResponsableDocumento = string.Empty;

            if (paramDocumento.LugarDocumento == null) paramDocumento.LugarDocumento = string.Empty;

            if (paramDocumento.NombreInforme == null) paramDocumento.NombreInforme = string.Empty;

            if (paramDocumento.ObservacionPoder == null) paramDocumento.ObservacionPoder = string.Empty;

            if (paramDocumento.CodigoNotariaProtocolo == null) paramDocumento.CodigoNotariaProtocolo = string.Empty;

            if (paramDocumento.NumeroDocumento == null) paramDocumento.NumeroDocumento = string.Empty;

            if (paramDocumento.CodigoNotaria == null) paramDocumento.CodigoNotaria = string.Empty;

            if (paramDocumento.AnnoInscripcion == null) paramDocumento.AnnoInscripcion = "0";

            if (paramDocumento.Fojas == null) paramDocumento.Fojas = string.Empty;

            if (paramDocumento.NumeroFoja == null) paramDocumento.NumeroFoja = string.Empty;

            if (paramDocumento.NumeroDecreto == null) paramDocumento.NumeroDecreto = string.Empty;

            if (paramDocumento.Juzgado == null) paramDocumento.Juzgado = "Sin Información";

            //Fechas
            if (string.IsNullOrEmpty(paramDocumento.FechaActVigencia))
                paramDocumento.FechaActVigencia = "31/12/3000";

            if (string.IsNullOrEmpty(paramDocumento.FechaDecreto))
                paramDocumento.FechaDecreto = "31/12/3000";

            if (string.IsNullOrEmpty(paramDocumento.FechaDocumento))
                paramDocumento.FechaDocumento = "31/12/3000";

            if (string.IsNullOrEmpty(paramDocumento.FechaFinal))
                paramDocumento.FechaFinal = "31/12/3000";

            if (string.IsNullOrEmpty(paramDocumento.FechaInicio))
                paramDocumento.FechaInicio = DateTime.Now.ToString("dd/MM/yyyy").Replace("-", "/");

            if (string.IsNullOrEmpty(paramDocumento.FechaInscripcion))
                paramDocumento.FechaInscripcion = "31/12/3000";

            if (string.IsNullOrEmpty(paramDocumento.FechaJuntaAccionista))
                paramDocumento.FechaJuntaAccionista = "31/12/3000";

            if (string.IsNullOrEmpty(paramDocumento.FechaProtocolarizacion))
                paramDocumento.FechaProtocolarizacion = "31/12/3000";

            if (string.IsNullOrEmpty(paramDocumento.FechaPublicacion))
                paramDocumento.FechaPublicacion = "31/12/3000";

            if (string.IsNullOrEmpty(paramDocumento.FechaPublicacionDecreto))
                paramDocumento.FechaPublicacionDecreto = "31/12/3000";

            if (string.IsNullOrEmpty(paramDocumento.FechaPublicacionQuiebra))
                paramDocumento.FechaPublicacionQuiebra = "31/12/3000";

            if (string.IsNullOrEmpty(paramDocumento.FechaRenovacion))
                paramDocumento.FechaRenovacion = "31/12/3000";

            if (string.IsNullOrEmpty(paramDocumento.FechaResolucionQuiebra))
                paramDocumento.FechaResolucionQuiebra = "31/12/3000";

            if (string.IsNullOrEmpty(paramDocumento.FechaSesionDirectorio))
                paramDocumento.FechaSesionDirectorio = "31/12/3000";

            if (string.IsNullOrEmpty(paramDocumento.FechaTerminaVigencia))
                paramDocumento.FechaTerminaVigencia = "31/12/3000";

            if (paramDocumento.Observaciones == null) paramDocumento.Observaciones = string.Empty;

            return paramDocumento;
        }

        [HttpPost]
        public JsonResult TransformaSoc([FromBody] GrabaRevocacionDTO paramGrabaRevo)
        {
            bool exitoso = false;
            MandatarioGrupoDTO mandatarioGrupoDTO = null;
            try
            {
                if (ModelState.IsValid)
                {
                    VerInformeCapitalDTO paramActSocio = new VerInformeCapitalDTO()
                    {
                        CodigoPersona = paramGrabaRevo.CodPer,
                        MontoCapital = 0,
                        CodigoMoneda = "999",
                        CantidadTotalAcciones = 0,
                        ValorNominalAccion = 0,
                        Descripcion = string.Empty
                    };

                    this._entradaCompletaAPP.IngresarCapitalSocio(paramActSocio);

                    IngresarSGLDTO paramActPersona = new IngresarSGLDTO
                    {
                        CodigoPersona = paramGrabaRevo.CodPer,
                        IdTipoPersona = paramGrabaRevo.TipoPer,
                        Nombre = paramGrabaRevo.NombrePer,
                        ApellidoPaterno = string.Empty,
                        ApellidoMaterno = string.Empty
                    };

                    BasePropertiesDTO actualizarPersonaRetorno = this._entradaCompletaAPP.ActualizarPersona(paramActPersona);

                    if (paramGrabaRevo.EliminaSoc == 1)
                    {
                        BasePropertiesDTO obtieneDatosParticRetorno = this._entradaCompletaAPP.ConsultaDatosParticipante(paramGrabaRevo.CodPer, paramGrabaRevo.TipoPer.ToString());

                        foreach (var item in obtieneDatosParticRetorno.Resultado)
                        {
                            mandatarioGrupoDTO = new MandatarioGrupoDTO
                            {
                                CodigoPersona = paramGrabaRevo.CodPer,
                                CodigoParticipante = item.CodigoParticipante,
                                CodigoTipo = 1
                            };

                            BasePropertiesDTO retornoEliminarMandatario = this._entradaCompletaAPP.EliminarMandatario(mandatarioGrupoDTO);
                        }
                    }

                    exitoso = true;

                    return Json(new { Exitoso = exitoso });
                }
                else
                {
                    return Json(new { Exitoso = exitoso });
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Stopped program because of exception");
                return Json(new { Exitoso = exitoso });
            }
        }

        [HttpPost]
        public JsonResult GrabaRevocGral([FromBody] GrabaRevocacionDTO paramGrabaRevo)
        {
            string mensaje = string.Empty;
            bool exitoso = false;
            int codDoc = 0;
            string fechaFinal = string.Empty;
            string codRevRet = string.Empty;

            IngresarDatosDocumentosDTO paramDocumento = new IngresarDatosDocumentosDTO();
            RevocacionEscrituraDTO paramRevocacion = null;

            try
            {
                if (ModelState.IsValid)
                {
                    BasePropertiesDTO datosDocumentoRetorno = this._entradaCompletaAPP.ObtenerDocumentoLegal(paramGrabaRevo.CodDocu);

                    paramDocumento = MapeaDatosIngresarDocumento((ConsultaAPI.Transversal.DTO.DocumentoLegalDTOResponse)datosDocumentoRetorno.Resultado[0]);

                    paramDocumento = ValidaCampos(paramDocumento);

                    BasePropertiesDTO documentoRetorno = this._entradaCompletaAPP.IngresarDatosDocumento(paramDocumento);

                    if (documentoRetorno != null)
                    {
                        codDoc = documentoRetorno.Resultado[0].CodigoDocumento;
                        fechaFinal = documentoRetorno.Resultado[0].FechaDocumento.ToString();
                    }

                    codRevRet = GrabarRevocacion(4, "Revocado por " + paramGrabaRevo.Texto, codDoc);

                    BasePropertiesDTO datosPoderesRetorno = this._entradaCompletaAPP.ConsultarDatosPoderes(codRevRet);

                    foreach (var item in datosPoderesRetorno.Resultado)
                    {
                        if (codDoc != item.CodigoDocumento && item.rev_cod == 0)
                        {
                            paramRevocacion = new RevocacionEscrituraDTO
                            {
                                CodigoDocumento = item.CodigoDocumento,
                                CodigoRevocacion = Convert.ToInt32(codRevRet),
                                Estado = "1",
                                FechaFinal = fechaFinal
                            };

                            BasePropertiesDTO datosGrabaRevoRetorno = this._entradaCompletaAPP.GrabarRevocacionEscritura(paramRevocacion);
                        }
                    }

                    exitoso = true;

                    return Json(new { Exitoso = exitoso });
                }
                else
                {
                    return Json(new { Exitoso = exitoso });
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Stopped program because of exception");
                return Json(new { Exitoso = exitoso });
            }


        }

        private string GrabarRevocacion(int tipoRevocacion, string descripcion, int codDoc)
        {
            BasePropertiesDTO datosAgreRevRetorno = this._entradaCompletaAPP.AgregarRevocacion(tipoRevocacion, descripcion, codDoc);

            if (datosAgreRevRetorno.Resultado != null)
                return datosAgreRevRetorno.Resultado.CodigoRevocacion.ToString();
            else
                return "-1";
        }

        private IngresarDatosDocumentosDTO MapeaDatosIngresarDocumento(ConsultaAPI.Transversal.DTO.DocumentoLegalDTOResponse documentoLegal)
        {
            IngresarDatosDocumentosDTO paramDocumento = new IngresarDatosDocumentosDTO
            {
                CodigoTipoLegal = 15,
                FechaDocumento = documentoLegal.FechaDocumento.ToString(),
                NumeroDocumento = documentoLegal.NumeroDocumento,
                CodigoNotaria = documentoLegal.CodigoNotaria.ToString(),
                FechaInscripcion = documentoLegal.FechaInp.ToString(),
                AnnoInscripcion = documentoLegal.AnioInp,
                Fojas = documentoLegal.FojaDocumento,
                NumeroFoja = documentoLegal.NumeroFojaDocumento,
                CodigoConservador = documentoLegal.CodigoConservador,
                FechaPublicacion = documentoLegal.FechaPublicacion.ToString(),
                FechaSesionDirectorio = documentoLegal.FechaSesionDirectorio.ToString(),
                FechaJuntaAccionista = documentoLegal.FechaJuntaExtraordinariaAccionistas.ToString(),
                CodigoRectificacion = 0,
                FechaDecreto = documentoLegal.FechaDecreto.ToString(),
                NumeroDecreto = documentoLegal.NumeroDecreto,
                FechaPublicacionDecreto = documentoLegal.FechaPublicacionDocumento.ToString(),
                FechaProtocolarizacion = documentoLegal.FechaProtocolizacion.ToString(),
                CodigoNotariaProtocolo = documentoLegal.CodigoNotariaProtocolizacion,
                CodigoDocumentoOrigen = Convert.ToInt32(documentoLegal.CodigoOrigenDocumento),
                NombreOrigen = documentoLegal.NombreOrigen,
                LugarOrigen = string.Empty, // documentoLegal.LugarOrigen;
                NombreInforme = string.Empty,
                TipoDocumento = 1,
                FechaRenovacion = string.Empty,
                FechaActVigencia = string.Empty,
                FechaTerminaVigencia = string.Empty,
                EstadoConsulta = documentoLegal.EstadoCsu,
                FechaInicio = documentoLegal.FechaInicio.ToString(),
                //paramDocumento.
                CodigoRevocacion = Convert.ToInt32(documentoLegal.CodigoRevocacion),
                FechaResolucionQuiebra = documentoLegal.FechaResolucionQuiebra.ToString(),
                Juzgado = documentoLegal.Juzgado
            };
            paramDocumento.FechaPublicacion = documentoLegal.FechaPublicacionQuiebra.ToString();
            paramDocumento.ResponsableDocumento = documentoLegal.UsuarioLogin;

            return paramDocumento;
        }

        [HttpPost]
        public JsonResult EnviaDatosInforme([FromBody] VerInformeCapitalDTO dto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    return Json(dto);
                }
                else
                {
                    return Json("Error");
                }
            }
            catch (Exception)
            {

                return Json(new { code = 1 });
            }
        }

        public ActionResult VerInforme(VerInformeCapitalDTO dto)
        {
            try
            {
                dto.CodigoTipoParticipante = "1";
                dto = this._entradaCompletaAPP.ConsultaDatosPersonaInforme(dto);
                dto = this._entradaCompletaAPP.ConsultaPersonaJuridicaDatosInforme(dto);
                dto = this._entradaCompletaAPP.ObtenerSGLUSuarioInforme(dto);
                dto = this._entradaCompletaAPP.ConsultaDatosParticipanteInforme(dto);
                dto = this._entradaCompletaAPP.ConsultaDocumentosCapitalInforme(dto);
                dto = this._entradaCompletaAPP.ObtenerAbogado(dto);
                dto = this._entradaCompletaAPP.ConsultaDocumentosGeneralInforme(dto);
                dto = this._entradaCompletaAPP.ObtenerDatosEstatutosInforme(dto);
                return View(dto);

            }
            catch (Exception)
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public JsonResult IngresarInforme(IngresarDocDTO dto)
        {
            try
            {
                //byte[] array = Encoding.UTF8.GetBytes(html);

                //dto.DocumentoHtml = array;

                this._entradaCompletaAPP.IngresarInforme(dto);
                return Json(new {code = 2 });
            }
            catch (Exception)
            {
                return Json(new {code = 1 });
            }
        }

        public ActionResult InformeCapitalSocio(VerInformeCapitalDTO dto)
        {
            dto.CodigoTipoParticipante = "1";
            dto = this._entradaCompletaAPP.ConsultaDatosPersonaInforme(dto);
            dto = this._entradaCompletaAPP.ConsultaPersonaJuridicaDatosInforme(dto);
            dto = this._entradaCompletaAPP.ConsultaDocumentosCapitalInforme(dto);
            dto = this._entradaCompletaAPP.ObtenerListadoMoneda(dto);
            //if (dto.Formato != 0)
            //{
            dto = this._entradaCompletaAPP.ConsultaDatosParticipanteInforme(dto);
            //}
            return View(dto);

        }

        [HttpPost]
        public ActionResult AgregarCapitalSocio([FromBody] VerInformeCapitalDTO dto)
        {
            try
            {
                //dto = this._entradaCompletaAPP.ConsultaDocumentosCapitalInforme(dto);
                this._entradaCompletaAPP.IngresarCapitalSocio(dto);
                return Json(new { Success = "True" });
            }
            catch (Exception ex)
            {
                @TempData["Error"] = true;
                @TempData["TituloMensaje"] = tituloMensajeError;
                @TempData["CuerpoMensaje"] = cuerpoMensajeError;
                logger.Error(ex, "Stopped program because of exception");

                return Json(new { code = 1 });
            }
        }

        public JsonResult CargaComboTipoLegalizacionVerModal(int tipoSoc, int estadoLegal, int codTipoLega)
        {
            string formato = string.Empty;
            string codTipoLeg = string.Empty;
            string codTipoDoc = string.Empty;
            string value = string.Empty;

            BasePropertiesDTO datosTipoLegaRetorno = this._entradaCompletaAPP.GetTipoLegalizacion(tipoSoc.ToString(), estadoLegal.ToString());

            if (datosTipoLegaRetorno == null)
                return Json(null);

            if (codTipoLega != 0)
                datosTipoLegaRetorno.Resultado = ((IEnumerable<TipoLegalizacionDTO>)datosTipoLegaRetorno.Resultado).Where(x => x.CodigoTipoLeg == codTipoLega.ToString());

            var lstTipoLega = from s in (IEnumerable<TipoLegalizacionDTO>)datosTipoLegaRetorno.Resultado
                              group s by new { s.DescripcionTipoLeg, s.CodigoTipoLeg, s.CodigoInvVis, s.CodigoTipoDoc };

            var lstTipoLegaAux = from s in (IEnumerable<TipoLegalizacionDTO>)datosTipoLegaRetorno.Resultado
                                 group s by new { s.CodigoFto, s.CodigoTipoLeg };

            List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();
            List<DropDownListDTO> listaDTOAux = new List<DropDownListDTO>();
            DropDownListDTO paramListaDTOAuc = null;

            foreach (var item in lstTipoLega)
            {
                codTipoLeg = item.Key.CodigoTipoLeg;
                codTipoDoc = item.Key.CodigoTipoDoc;

                if (!item.Key.CodigoTipoLeg.Equals("2") && !item.Key.CodigoInvVis.Equals("1") && !codTipoLeg.Equals("1") && !codTipoLeg.Equals("4"))
                {
                    paramListaDTOAuc = new DropDownListDTO();

                    value = codTipoLeg + "-" + codTipoDoc + "|";
                    foreach (var itemValue in lstTipoLegaAux)
                    {
                        if (codTipoLeg.Equals(itemValue.Key.CodigoTipoLeg))
                        {
                            value = value + itemValue.Key.CodigoFto.Trim() + ";";
                        }
                    }

                    paramListaDTOAuc.id = value;
                    paramListaDTOAuc.text = item.Key.DescripcionTipoLeg;

                    listaDTOAux.Add(paramListaDTOAuc);
                }
            }

            Parallel.ForEach(listaDTOAux, (registro) =>
            {
                listaDTO.Add(new DropDownListDTO { id = registro.id, text = registro.text });
            });

            return Json(listaDTO);
        }

        public JsonResult CargaComboNotariaModal()
        {
            BasePropertiesDTO datosNotariaRetorno = this._entradaCompletaAPP.GetListaNotaria();

            if (datosNotariaRetorno == null)
                return Json(null);

            List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

            Parallel.ForEach((IEnumerable<ObtenerListaNotariaDTOResponse>)datosNotariaRetorno.Resultado, (registro) =>
            {
                listaDTO.Add(new DropDownListDTO { id = registro.Codigo.ToString(), text = registro.Descripcion.ToString() });
            });

            return Json(listaDTO);
        }

        public JsonResult CargaComboRegistrosModal()
        {
            BasePropertiesDTO datosRegistroRetorno = this._entradaCompletaAPP.ObtenerRegistros();

            if (datosRegistroRetorno == null)
                return Json(null);

            List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

            Parallel.ForEach((IEnumerable<ObtenerRegistrosDTO>)datosRegistroRetorno.Resultado, (registro) =>
            {
                listaDTO.Add(new DropDownListDTO { id = registro.Codigo.ToString(), text = registro.Descripcion.ToString() });
            });

            return Json(listaDTO);
        }

        public JsonResult CargaComboOrigenModal(string codigoTipoDocumento, string codigoTipoPersona)
        {
            BasePropertiesDTO datosOrigenRetorno = this._entradaCompletaAPP.GetDatosOrigen(codigoTipoDocumento, codigoTipoPersona);

            if (datosOrigenRetorno == null)
                return Json(null);

            List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

            Parallel.ForEach((IEnumerable<ObtenerDatosOrigenDTOResponse>)datosOrigenRetorno.Resultado, (registro) =>
            {
                listaDTO.Add(new DropDownListDTO { id = registro.CodigoOrigen.ToString(), text = registro.Descripcion.ToString() });
            });

            return Json(listaDTO);
        }

        public JsonResult CargaComboRectificadoModal(string codDocumentos)
        {
            List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

            listaDTO = ObtieneListaRectificacion(codDocumentos);

            return Json(listaDTO);
        }

        private List<DropDownListDTO> ObtieneListaRectificacion(string codDocumentos)
        {
            List<DropDownListDTO> listaDTO = new List<DropDownListDTO>();

            if (codDocumentos != null)
            {
                string[] arrDoc = codDocumentos.Split(";");
                int contFilas = 1;

                DropDownListDTO paramCombo = null;

                for (int i = 0; i < arrDoc.Length - 1; i++)
                {
                    paramCombo = new DropDownListDTO();
                    paramCombo.id = arrDoc[i];
                    paramCombo.text = contFilas.ToString();

                    listaDTO.Add(paramCombo);
                    contFilas++;
                }
            }

            return listaDTO;
        }

        public string ValidaEliminacion(string codDocumento)
        {
            string mensaje = string.Empty;

            try
            {
                BasePropertiesDTO datosPoderesRetorno = this._entradaCompletaAPP.ConsultarListaPoderes(codDocumento);

                BasePropertiesDTO datosCantGruposPoderesRetorno = this._entradaCompletaAPP.ObtieneCantidadGruposDocumento(codDocumento, 1);

                if (datosCantGruposPoderesRetorno.Resultado[0].Contador == 0)
                {
                    BasePropertiesDTO datosCantGruposMandatariosRetorno = this._entradaCompletaAPP.ObtieneCantidadGruposDocumento(codDocumento, 2);

                    if (datosCantGruposMandatariosRetorno.Resultado[0].Contador > 0)
                    {
                        mensaje = "Debe eliminar Grupos de Mandatarios.";
                    }
                }
                else
                {
                    mensaje = "Debe eliminar Poderes.";
                }

                BasePropertiesDTO datosParticipantesDocumentoRetorno = this._entradaCompletaAPP.GetCantidadParticipantesDocumento(codDocumento, "3");

                if (datosParticipantesDocumentoRetorno.Resultado[0].CantParticipantesDoc > 0)
                {
                    mensaje = "Debe eliminar Mandatarios.";
                }

                BasePropertiesDTO datosListaRevocacionesRetorno = this._entradaCompletaAPP.ConsultarListaRevocaciones(codDocumento);

                if (datosListaRevocacionesRetorno.Resultado.Count > 0)
                {
                    mensaje = "Debe eliminar Revocaciones.";
                }

                return mensaje;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Stopped program because of exception");
                mensaje = cuerpoMensajeError;
                return mensaje;
            }           
        }

        [HttpGet]
        public ActionResult ModalEliminaDocumento(string codDocumento)
        {
            EliminarDocumentoDTO paramDocumento = new EliminarDocumentoDTO
            {
                CodigoDocumento = codDocumento
            };

            return PartialView(paramDocumento);
        }

        [HttpPost]
        public JsonResult EliminaDocumento([FromBody] string codDocumento)
        {
            string mensaje = string.Empty;
            bool exitoso = false;

            try
            {
                if (ModelState.IsValid)
                {
                    BasePropertiesDTO datosListaRevocacionesRetorno = this._entradaCompletaAPP.DeleteDocumento(codDocumento);

                    if (datosListaRevocacionesRetorno != null)
                    {
                        if (datosListaRevocacionesRetorno.CodigoRetorno == 0)
                        {
                            mensaje = "Se ha eliminado el documento exitosamente.";
                            exitoso = true;
                        }
                        else
                        {
                            mensaje = cuerpoMensajeError;
                        }
                    }

                    return Json(new { Exitoso = exitoso, Mensaje = mensaje });
                }
                else
                {
                    mensaje = cuerpoMensajeError;
                    return Json(new { Exitoso = exitoso, Mensaje = mensaje });
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Stopped program because of exception");
                mensaje = cuerpoMensajeError;
                return Json(new { Exitoso = exitoso, Mensaje = mensaje });
            }
            
        }

        #region Cambiar Estado SGL

        public JsonResult GetSGLPorId(string codigoSGL)
        {
            EstadoSGLDTO SGLconEstado = this._entradaCompletaAPP.GetSGL(codigoSGL);

            return Json(SGLconEstado);
        }

        public IActionResult ModalCambiarEstadoSGL(int codigoEstado, int idSgl)
        {
            List<EstadoSGLDTO> estadosSGL = new List<EstadoSGLDTO>();

            if (codigoEstado == 13 || codigoEstado == 14)
            {
                estadosSGL.Add(new EstadoSGLDTO { CodigoEstadoSGL = 1, DescripcionEstadoSGL = "Creada", CodigoSGL = idSgl });
                estadosSGL.Add(new EstadoSGLDTO { CodigoEstadoSGL = 2, DescripcionEstadoSGL = "Abogado asignado", CodigoSGL = idSgl });
                estadosSGL.Add(new EstadoSGLDTO { CodigoEstadoSGL = 3, DescripcionEstadoSGL = "Ingresados constitucion", CodigoSGL = idSgl });
                estadosSGL.Add(new EstadoSGLDTO { CodigoEstadoSGL = 4, DescripcionEstadoSGL = "Ingresados socios/capital", CodigoSGL = idSgl });
                estadosSGL.Add(new EstadoSGLDTO { CodigoEstadoSGL = 5, DescripcionEstadoSGL = "Ingresados datos estatutos", CodigoSGL = idSgl });
                estadosSGL.Add(new EstadoSGLDTO { CodigoEstadoSGL = 6, DescripcionEstadoSGL = "Sociedad ingresada", CodigoSGL = idSgl });
                estadosSGL.Add(new EstadoSGLDTO { CodigoEstadoSGL = 7, DescripcionEstadoSGL = "Ingresado doc. poder", CodigoSGL = idSgl });
                estadosSGL.Add(new EstadoSGLDTO { CodigoEstadoSGL = 8, DescripcionEstadoSGL = "Ingresado mandatarios", CodigoSGL = idSgl });
                estadosSGL.Add(new EstadoSGLDTO { CodigoEstadoSGL = 9, DescripcionEstadoSGL = "Ingresado formas de actuar", CodigoSGL = idSgl });
                estadosSGL.Add(new EstadoSGLDTO { CodigoEstadoSGL = 10, DescripcionEstadoSGL = "Ingresando facultades", CodigoSGL = idSgl });
                estadosSGL.Add(new EstadoSGLDTO { CodigoEstadoSGL = 11, DescripcionEstadoSGL = "Ingresados poderes", CodigoSGL = idSgl });
                estadosSGL.Add(new EstadoSGLDTO { CodigoEstadoSGL = 12, DescripcionEstadoSGL = "SGL en Revisión", CodigoSGL = idSgl });
            }

            estadosSGL.Add(new EstadoSGLDTO { CodigoEstadoSGL = 13, DescripcionEstadoSGL = "Devolución antecedentes", CodigoSGL = idSgl });
            estadosSGL.Add(new EstadoSGLDTO { CodigoEstadoSGL = 14, DescripcionEstadoSGL = "Terminada con reparos", CodigoSGL = idSgl });

            return PartialView(estadosSGL);
        }

        [HttpPost]
        public JsonResult ModificarEstadoSGL(int codigoSGL, int codigoEstado)
        {
            string mensaje = string.Empty;
            bool exitoso = false;
            int codigoUsuario = HttpContext.Session.GetObjectFromJson<SessionLoginDTO>("sessionLogin").CodigoUsuario;

            BasePropertiesDTO ModificacionSGL = this._entradaCompletaAPP.ActualizarEstadoSGL(codigoSGL, codigoEstado, codigoUsuario);

            if (ModificacionSGL != null)
            {
                int codigoRetorno = ModificacionSGL.CodigoRetorno;

                if (codigoRetorno == 0)
                {
                    exitoso = true;
                    mensaje = "El estado ha sido actualizado.";
                    return Json(new { Exitoso = exitoso, Mensaje = mensaje });
                }
            }
            else
            {
                mensaje = "El registro no fue actualizado.";
                return Json(new { Exitoso = exitoso, Mensaje = mensaje });
            }

            return Json(new { Exitoso = exitoso, Mensaje = mensaje });
        }


        #endregion

        #region TerminarSGL

        [HttpGet]
        public ActionResult TerminarSGL(IngresarSGLDTO EntitytoUse)
        {
            try
            {
                int estado = 0;
                int codigoUsuario = HttpContext.Session.GetObjectFromJson<SessionLoginDTO>("sessionLogin").CodigoUsuario;

                this._entradaCompletaAPP.TerminarSGL(EntitytoUse.IdSGL);

                if (EntitytoUse.Entrada == "EC")
                {
                    estado = 15;
                }
                else if (EntitytoUse.Entrada == "EP")
                {
                    estado = 27;
                }

                BasePropertiesDTO ModificacionSGL = this._entradaCompletaAPP.ActualizarEstadoSGL(EntitytoUse.IdSGL, estado, codigoUsuario);

                //gran validacion de asp
                
                return RedirectToAction("Index", "EntradaCompleta", EntitytoUse);
            }
            catch (Exception)
            {
                //cambiar algun mensaje de error
                return RedirectToAction("Index", "EntradaCompleta", EntitytoUse);
            }
        }

        #endregion
    }
}