using CL.Scotiabank.LegalPartner.SGL.APP;
using CL.Scotiabank.LegalPartner.SGL.BLL;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO.Compression;

namespace CL.Scotiabank.LegalPartner.SGL.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IConservadorAPP, ConservadorAPP>();
            services.AddScoped<INotariaAPP, NotariaAPP>();
            services.AddScoped<IFacultadAPP, FacultadAPP>();
            services.AddScoped<ITipoPersonaAPP, TipoPersonaAPP>();
            services.AddScoped<ITipoDocumentoAPP, TipoDocumentoAPP>();
            services.AddScoped<ITipoPoderAPP, TipoPoderAPP>();
            services.AddScoped<ITipoParticipanteAPP, TipoParticipanteAPP>();
            services.AddScoped<IMonedaAPP, MonedaAPP>();
            services.AddScoped<IGrupoFacultadAPP, GrupoFacultadAPP>();
            services.AddScoped<IMateriaBancoAPP, MateriaBancoAPP>();
            services.AddScoped<ISucursalBancoAPP, SucursalBancoAPP>();
            services.AddScoped<IOficinaPartesAPP, OficinaPartesAPP>();
            services.AddScoped<IGrupoUsuarioAPP, GrupoUsuarioAPP>();
            services.AddScoped<IPerfilUsuarioAPP, PerfilUsuarioAPP>();
            services.AddScoped<ILoginAPP, LoginAPP>();
            services.AddScoped<IUsuarioAPP, UsuarioAPP>();
            services.AddScoped<ILoginBLL, LoginBLL>();
            services.AddScoped<IConsultaFirmaCajaAPP, ConsultaFirmaCajaAPP>();
            services.AddScoped<ITokenAPP, TokenAPP>();
            services.AddScoped<IMenuAPP, MenuAPP>();
            services.AddScoped<IConsultaPoderesAPP, ConsultaPoderesAPP>();
            services.AddScoped<IEntradaCompletaAPP, EntradaCompletaAPP>();
            services.AddScoped<IDocumentoPoderAPP, DocumentoPoderAPP>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            

            services.AddDistributedMemoryCache();
            services.AddSession();

            services.AddScoped<ICabeceraSGLAPP, CabeceraSGLAPP>();
            services.AddScoped<IListadoSGLAPP, ListadoSGLAPP>();
            services.AddScoped<IConsultarSGLAPP, ConsultarSGLAPP>();


            services.AddScoped<IBuscarClienteAPP, BuscarClienteAPP>();

            services.AddScoped<IMandatarioGrupoAPP, MandatarioGrupoAPP>();
            services.AddMvc();
            services.AddResponseCompression();

            services.Configure<GzipCompressionProviderOptions>(options => {
                options.Level = CompressionLevel.Fastest;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Shared/Error");
            }

            app.UseStaticFiles();

            app.UseSession();

            app.UseMvc(routes =>
            {
                //routes.MapRoute(
                //  name: "areas",
                //  template: "{area:exists}/{controller=Login}/{action=Index}/{id?}"
                //);
                routes.MapRoute(
                    name: "default",
                    template: "{area=Login}/{controller=Login}/{action=Index}/{id?}"
                );
            });
        }
    }
}
