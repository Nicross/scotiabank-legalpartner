﻿// Write your Javascript code.
$.fn.FormatearRut = function () {
    $(this).on('keyup', function (e) {
        var variable = false;

        var num = e.target.value.replace(/\./g, '');
        num = num.replace(/\-/g, '');
        var num2 = null;

        if (num.length > 1) {

            variable = true
            num2 = num.substring(num.length - 1, num.length);
            num = num.substring(0, num.length - 1);
        }

        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
        num = num.split('').reverse().join('').replace(/^[\.]/, '');

        if (variable) {

            if (num2 === null) {

                e.target.value = num.toString() + "-";
            }
            else {

                e.target.value = num.toString() + "-" + num2;
            }
        }
        else {

            e.target.value = num
        }
    });
}

$.fn.CampoTipoRut = function () {
    $(this).on('keypress', function (e) {
        var a = [];
        var k = e.which;

        for (i = 48; i < 58; i++)  //digitos 0-9
            a.push(i);

        a.push(75, 107);

        if (!(a.indexOf(k) >= 0))
            e.preventDefault();
    });
}

$.fn.ValidarRut = function () {
    $(this).on('blur', function () {

        var rutC = $(this).val();

        if (rutC.length === 0) {
            return false;
        }

        rutC = rutC.replace('-', '')
        rutC = rutC.replace(/\./g, '')

        if (parseInt(rutC) === 0) {
            return false;
        }

        var suma = 0;
        var caracteres = "1234567890kK";
        var contador = 0;

        for (var i = 0; i < rutC.length; i++) {
            u = rutC.substring(i, i + 1);
            if (caracteres.indexOf(u) !== -1)
                contador++;
        }

        if (contador === 0) { return false }

        var rut = rutC.substring(0, rutC.length - 1)
        var drut = rutC.substring(rutC.length - 1)
        var dvr = '0';
        var mul = 2;

        for (i = rut.length - 1; i >= 0; i--) {
            suma = suma + rut.charAt(i) * mul
            if (mul === 7) mul = 2
            else mul++
        }

        res = suma % 11

        if (res === 1) dvr = 'k'
        else if (res === 0) dvr = '0'
        else {
            dvi = 11 - res
            dvr = dvi + ""
        }

        if (dvr !== drut.toLowerCase()) {
            $('#titulo-modal-mensaje').html('Error - Rut');
            $('#cuerpo-modal-mensaje').html('El valor ingresado no corresponde a un R.U.T valido');

            $('#modal-mensaje').modal('show');
            
            $(this).val('');
            return false;
        }
        else {
            return true;
        }
    })
};

function Numeros(string) {//Solo numeros
    var out = '';
    var filtro = '1234567890';//Caracteres validos

    //Recorrer el texto y verificar si el caracter se encuentra en la lista de validos 
    for (var i = 0; i < string.length; i++)
        if (filtro.indexOf(string.charAt(i)) != -1)
            //Se añaden a la salida los caracteres validos
            out += string.charAt(i);

    //Retornar valor filtrado
    return out;
}