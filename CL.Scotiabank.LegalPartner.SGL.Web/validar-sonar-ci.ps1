  # Step 1. Create a username:password pair
  $credPair = "tfs:Indra2019"

  # Step 2. Encode the pair to Base64 string
  $encodedCredentials = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($credPair))

  # Step 3. Form the header and add the Authorization attribute to it
  $headers = @{ Authorization = "Basic $encodedCredentials" }

  # Step 4. Make the GET request to api
  $responseData = Invoke-WebRequest -Uri http://172.17.206.17:9000/api/qualitygates/project_status?projectKey=LegalPartner-WEB -Method Get -Headers $headers -UseBasicParsing

  #write-host $responseData.content

  $x = $responseData.content | ConvertFrom-Json
  $sonarQualityGateResult = $x.projectStatus.status

  if($sonarQualityGateResult -eq "ERROR")
    {
        write-host "CI failed due to Sonarqube quality Gate"
        exit 1
    }