using System.Net.Http;
using System.Net.Http.Headers;

namespace CL.Scotiabank.LegalPartner.SGL.DAL
{
    public class NotariaDTORequest
    {
        /// <summary>
        /// Código del DTO de Notaria.
        /// </summary>
        public string Codigo { get; set; }

        /// <summary>
        /// Descripción del DTO de Notaria.
        /// </summary>
        public string Descripcion { get; set; }
    }
}