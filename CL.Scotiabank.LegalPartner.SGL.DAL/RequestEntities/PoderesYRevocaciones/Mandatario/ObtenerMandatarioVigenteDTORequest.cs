﻿namespace CL.Scotiabank.LegalPartner.SGL.DAL
{
    /// <summary>
    /// Clase de parametros request para GetMandatario.
    /// </summary>
    public class ObtenerMandatarioVigenteDTORequest
    {
        /// <summary>
        /// Codigo persona.
        /// </summary>        
        public int CodigoPersona { get; set; }

        /// <summary>
        /// Codigo Tipo.
        /// </summary>
        public int CodigoParticipante { get; set; }
    }
}
