﻿namespace CL.Scotiabank.LegalPartner.SGL.DAL
{
    /// <summary>
    /// Clase de parámetros request para AddMandatario.
    /// </summary>
    public class ModificarParticipanteBDTORequest
    {
        /// <summary>
        /// Identificador de Persona del DTO de Socio.
        /// </summary>
        public int CodigoPersona { get; set; }

        /// <summary>
        /// Codigo de ParticipanteL  del DTO de Socio.
        /// </summary>
        public int CodigoParticipante { get; set; }

        /// <summary>
        /// Identificador de Tipo Persona  del DTO de Socio.
        /// </summary>
        public int CodigoTipoParticipante { get; set; }

        /// <summary>
        /// Monto de Capital del DTO de Socio.
        /// </summary>
        public decimal Monto { get; set; }

        /// <summary>
        /// Porcentaje de Capital  del DTO de Socio.
        /// </summary>
        public decimal Porcentaje { get; set; }

        /// <summary>
        /// Limitacion  del DTO de Socio.
        /// </summary>
        public string Limitacion { get; set; }
    }
}
