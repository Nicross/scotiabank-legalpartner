﻿using System.Net.Http;
using System.Net.Http.Headers;

namespace CL.Scotiabank.LegalPartner.SGL.DAL
{
    /// <summary>
    /// Clase Request del Método para Agregar Datos Estatuto.
    /// </summary>
    public class DatosEstatutosCreateDTORequest
    {
        /// <summary>
        /// Identificador del DTO de cliente.
        /// </summary>
       
        public int CodigoCliente { get; set; }

        /// <summary>
        /// Identificador del DTO del estado legal.
        /// </summary>
        
        public int EstadoLegal { get; set; }

        /// <summary>
        /// Identificador del DTO de nombre de fantasia.
        /// </summary>
        public string NombreFantasia { get; set; }

        /// <summary>
        /// Identificador del DTO de nombre legal anterior.
        /// </summary>
        
        public string NombreLegalAnterior { get; set; }

        /// <summary>
        /// Identificador del DTO de objeto.
        /// </summary>
        
        public string Objeto { get; set; }

        /// <summary>
        /// Identificador del DTO del domicilio
        /// </summary>
        
        public string Domicilio { get; set; }

        /// <summary>
        /// Identificador del DTO de la duracion.
        /// </summary>
       
        public string Duracion { get; set; }

        /// <summary>
        /// Identificador del DTO del administrador.
        /// </summary>
        
        public string Administracion { get; set; }

        /// <summary>
        /// Identificador del DTO de la Fuente.
        /// </summary>
        
        public string Fuente { get; set; }

        /// <summary>
        /// Identificador del DTO de conclusiones.
        /// </summary>
        
        public string Conclusiones { get; set; }

        /// <summary>
        /// Identificador del DTO del estado de cuenta.
        /// </summary>
        
        public string EstadoConsulta { get; set; }

        /// <summary>
        /// Identificador del DTO Fecha del ultimo estudio.
        /// </summary>
        
        public string FechaUltimoEstudio { get; set; }

        /// <summary>
        /// Identificador del DTO de la proxima fecha de vigencia.
        /// </summary>
        
        public string FechaProximaVigencia { get; set; }

        /// <summary>
        /// Identificador del DTO del informe.
        /// </summary>
        
        public string IndicadorInforme { get; set; }
    }
}
