﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.DAL.RequestEntities
{
    public class ConsultaPersonaDatosDTORequest
    {
        /// <summary>
        /// Rut.
        /// </summary>
        public string Rut { get; set; }

        /// <summary>
        /// Descripción del DTO de Nombre.
        /// </summary>
        public string Nombre { get; set; }
        /// <summary>
        /// Código del DTO de Apellido Paterno.
        /// </summary>
        public string ApellidoPaterno { get; set; }

        /// <summary>
        /// Descripción del DTO de Apellido Materno.
        /// </summary>
        public string ApellidoMaterno { get; set; }
        /// <summary>
        /// Código del DTO de Cantidad Registros.
        /// </summary>
        public int CantidadRegistros { get; set; }

    }
}
