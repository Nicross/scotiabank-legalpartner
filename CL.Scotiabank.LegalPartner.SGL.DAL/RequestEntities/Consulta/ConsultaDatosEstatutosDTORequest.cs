﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.DAL
{
   public class ConsultaDatosEstatutosDTORequest
    {
        public string NombreLegalAnterior { get; set; }

        public string NombreFantasia { get; set; }

        public string Objeto { get; set; }

        public string Domicilio { get; set; }

        public string Duracion { get; set; }

        public string Conclusion { get; set; }

        public string FechaActualizacion { get; set; }
    }
}
