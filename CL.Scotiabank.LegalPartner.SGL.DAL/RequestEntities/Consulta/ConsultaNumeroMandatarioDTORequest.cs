﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.DAL
{
    public class ConsultaNumeroMandatarioDTORequest
    {
        /// <summary>
        /// Total
        /// </summary>
        public int Total { get; set; }
    }
}
