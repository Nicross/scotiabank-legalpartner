using System.Net.Http;
using System.Net.Http.Headers;

namespace CL.Scotiabank.LegalPartner.SGL.DAL
{
    public class PerfilUsuarioDTORequest
    {
        /// <summary>
        /// Código del DTO de PerfilUsuario.
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Descripción del DTO de PerfilUsuario.
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Grupo codigo del DTO perfil usuario
        /// </summary>
        public string GrupoCodigo { get; set; }
    }
}