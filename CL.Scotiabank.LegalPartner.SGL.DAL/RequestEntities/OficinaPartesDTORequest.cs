namespace CL.Scotiabank.LegalPartner.SGL.DAL
{
    public class OficinaPartesDTORequest
    {
        /// <summary>
        /// Código del DTO de OficinaPartes.
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Descripción del DTO de OficinaPartes.
        /// </summary>
        public string Descripcion { get; set; }
    }
}