﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.DAL
{
    public class UsuarioDTORequest
    {
        /// <summary>
        /// Identificador del DTO de Conservador.
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Rut del DTO de Usuario.
        /// </summary>
        public int RutUsuario { get; set; }

        /// <summary>
        /// Digito Verificador del DTO de Usuario.
        /// </summary>
        public string DigitoVerificador { get; set; }

        /// <summary>
        /// Nombre del DTO de Usuario.
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Apewllido Paterno del DTO de Usuario.
        /// </summary>
        public string ApellidoPaterno { get; set; }

        /// <summary>
        /// Apellido MAterno del DTO de Usuario.
        /// </summary>
        public string ApellidoMaterno { get; set; }

        /// <summary>
        /// Rut Usuario que inicia session del DTO de Usuario.
        /// </summary>
        public string UsuarioSession { get; set; }

        /// <summary>
        /// Clave del DTO de Usuario.
        /// </summary>
        public string Contraseña { get; set; }          

        /// <summary>
        /// Telefono del DTO de Usuario.
        /// </summary>
        public string Telefono { get; set; }

        /// <summary>
        /// Email del DTO de Usuario.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Codigo Perfil del DTO de Usuario.
        /// </summary>
        public int CodigoPerfil { get; set; }

        /// <summary>
        /// Codigo Perfil del DTO de Usuario.
        /// </summary>
        public int CodigoPerfilNUevo{ get; set; }

        /// <summary>
        /// Identificador de Sucursal del DTO de Usuario.
        /// </summary>
        public string IdSucursal { get; set; }

        /// <summary>
        /// Identificador de Sucursal del DTO de Usuario.
        /// </summary>
        public string IdNuevoSucursal { get; set; }
    }
}
