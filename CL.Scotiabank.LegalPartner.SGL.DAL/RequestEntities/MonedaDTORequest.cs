using System.Net.Http;
using System.Net.Http.Headers;

namespace CL.Scotiabank.LegalPartner.SGL.DAL
{
    public class MonedaDTORequest
    {
        /// <summary>
        /// Código del DTO de Moneda.
        /// </summary>
        public string Codigo { get; set; }

        /// <summary>
        /// Descripción del DTO de Moneda.
        /// </summary>
        public string Descripcion { get; set; }
    }
}