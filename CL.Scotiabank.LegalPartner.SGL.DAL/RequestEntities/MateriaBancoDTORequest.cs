using System.Net.Http;
using System.Net.Http.Headers;

namespace CL.Scotiabank.LegalPartner.SGL.DAL
{
    public class MateriaBancoDTORequest
    {
        /// <summary>
        /// Código del DTO de MateriaBanco.
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Descripción del DTO de MateriaBanco.
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Materia padre de la  materia banco.
        /// </summary>
        public int? MateriaPadre { get; set; }
    }
}