using System.Net.Http;
using System.Net.Http.Headers;

namespace CL.Scotiabank.LegalPartner.SGL.DAL
{
    public class SucursalBancoDTORequest
    {
        /// <summary>
        /// Código del DTO de SucursalBanco.
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Descripción del DTO de SucursalBanco.
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// oficina de parte de la sucursal
        /// </summary>
        public int OficinaPartesCodigo { get; set; }
            
        /// <summary>
        /// Codigo de cuenta de la sucursal
        /// </summary>
        public string SucursalCodigoCuenta { get; set; }
    }
}