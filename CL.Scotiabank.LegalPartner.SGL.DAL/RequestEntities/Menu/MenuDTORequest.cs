using System.Net.Http;
using System.Net.Http.Headers;

namespace CL.Scotiabank.LegalPartner.SGL.DAL
{
    public class MenuDTORequest
    {
        /// <summary>
        /// Código Grupo Usuario del DTO de Login.
        /// </summary>
        public int CodigoGrupoUsuario { get; set; }
    }
}