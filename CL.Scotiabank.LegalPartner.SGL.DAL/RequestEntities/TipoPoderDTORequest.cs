using System.Net.Http;
using System.Net.Http.Headers;

namespace CL.Scotiabank.LegalPartner.SGL.DAL
{
    public class TipoPoderDTORequest
    {
        /// <summary>
        /// Código del DTO de Tipo de Poder.
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Descripción del DTO de Tipo de Poder.
        /// </summary>
        public string Descripcion { get; set; }
    }
}