namespace CL.Scotiabank.LegalPartner.SGL.DAL
{
    public class ConservadorDTORequest
    {
        /// <summary>
        /// Código del DTO de Conservador.
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Descripción del DTO de Conservador.
        /// </summary>
        public string Descripcion { get; set; }
    }
}