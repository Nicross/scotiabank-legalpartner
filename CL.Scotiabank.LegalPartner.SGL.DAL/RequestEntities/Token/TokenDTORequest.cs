﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.DAL
{
    public class TokenDTORequest
    {
        public string ClaveToken { get; set; }
    }
}
