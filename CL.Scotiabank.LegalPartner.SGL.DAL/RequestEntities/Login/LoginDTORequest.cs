using System.Net.Http;
using System.Net.Http.Headers;

namespace CL.Scotiabank.LegalPartner.SGL.DAL
{
    public class LoginDTORequest
    {
        /// <summary>
        /// Código del DTO de Login.
        /// </summary>
        public int CodigoUsuario { get; set; }

        /// <summary>
        /// Rut del DTO de Login.
        /// </summary>
        public string RutUsuario { get; set; }

        /// <summary>
        /// Digito Verificador del DTO de Login.
        /// </summary>
        public string DigitoVerificador { get; set; }

        /// <summary>
        /// Password del DTO de Login.
        /// </summary>
        public string PasswordUsuario { get; set; }
    }
}