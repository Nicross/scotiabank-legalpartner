﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// DTO utilizado para el ingreso de SGL.
    /// </summary>
    public class IngresarSGLDTORequest
    {
        /// <summary>
        /// Id sgl.
        /// </summary>
        public int IdSGL { get; set; }

        /// <summary>
        /// Fecha.
        /// </summary>
        public string Fecha { get; set; }

        /// <summary>
        /// Materia.
        /// </summary>
        public int Materia { get; set; }

        /// <summary>
        /// Estado.
        /// </summary>
        public int Estado { get; set; }

        /// <summary>
        /// .
        /// </summary>
        public int of_partes { get; set; }

        /// <summary>
        /// Id de sucursal.
        /// </summary>
        public int IdSucursal { get; set; }

        /// <summary>
        /// Codigo usuario.
        /// </summary>
        public int CodigoUsuario { get; set; }
    }
}
