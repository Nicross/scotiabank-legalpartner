using System.Net.Http;
using System.Net.Http.Headers;

namespace CL.Scotiabank.LegalPartner.SGL.DAL
{
    public class GrupoUsuarioDTORequest
    {
        /// <summary>
        /// Código del DTO de GrupoUsuario.
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Descripción del DTO de GrupoUsuario.
        /// </summary>
        public string Descripcion { get; set; }
    }
}