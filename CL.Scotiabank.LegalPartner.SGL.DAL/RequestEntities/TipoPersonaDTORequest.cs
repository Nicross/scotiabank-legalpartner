using System.Net.Http;
using System.Net.Http.Headers;

namespace CL.Scotiabank.LegalPartner.SGL.DAL
{
    public class TipoPersonaDTORequest
    {
        /// <summary>
        /// Código del DTO de Tipo de Persona.
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Descripción del DTO de Tipo de Persona.
        /// </summary>
        public string Descripcion { get; set; }
    }
}