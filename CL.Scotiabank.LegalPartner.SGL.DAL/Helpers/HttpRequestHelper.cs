using System.Net.Http;
using System.Net.Http.Headers;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;

namespace CL.Scotiabank.LegalPartner.SGL.DAL
{
    public static class HttpRequestHelper
    {
        public static HttpResponseMessage Get(string requestUri)
            => Get(requestUri, "");

        public static HttpResponseMessage Get(string requestUri, string bearerToken)
        {
            var builder = new HttpRequestBuilder()
                                .AddMethod(HttpMethod.Get)
                                .AddRequestUri(requestUri)
                                .AddBearerToken(bearerToken);

            return builder.SendAsync();
        }

        public static HttpResponseMessage Post(string requestUri, object value)
            => Post(requestUri, value, "");

        public static HttpResponseMessage Post(string requestUri, object value, string bearerToken)
        {
            var builder = new HttpRequestBuilder()
                                .AddMethod(HttpMethod.Post)
                                .AddRequestUri(requestUri)
                                .AddContent(new JsonContent(value))
                                .AddBearerToken(bearerToken);

            return builder.SendAsync();
        }

        public static HttpResponseMessage Put(string requestUri, object value)
            => Put(requestUri, value, "");

        public static HttpResponseMessage Put(string requestUri, object value, string bearerToken)
        {
            var builder = new HttpRequestBuilder()
                                .AddMethod(HttpMethod.Put)
                                .AddRequestUri(requestUri)
                                .AddContent(new JsonContent(value))
                                .AddBearerToken(bearerToken);

            return builder.SendAsync();
        }

        public static HttpResponseMessage Delete(string requestUri)
            => Delete(requestUri, "");

        public static HttpResponseMessage Delete(string requestUri, string bearerToken)
        {
            var builder = new HttpRequestBuilder()
                                .AddMethod(HttpMethod.Delete)
                                .AddRequestUri(requestUri)
                                .AddBearerToken(bearerToken);

            return builder.SendAsync();
        }
    }
}
