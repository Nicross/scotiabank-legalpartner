﻿using System;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class ListadoSGLDTO
    {
        /// <summary>
        /// Obtiene o modifica Usuario
        /// </summary>
        public int Usuario { get; set; }

        /// <summary>
        /// Obtiene o modifica Identificador SGL
        /// </summary>
        public int SglId { get; set; }

        /// <summary>
        /// Obtiene o modifica FechaInicial
        /// </summary>
        public DateTime FechaInicioSGL { get; set; }

        /// <summary>
        /// Obtiene o modifica Descripcion Estado SGL
        /// </summary>
        public string DescripcionEstadoSGL { get; set; }

        /// <summary>
        /// Obtiene o modifica Rut Persona Solicitante
        /// </summary>
        public string RutPersonaSolicitante { get; set; }

        /// <summary>
        /// Obtiene o modifica DvPersona Solicitante
        /// </summary>
        public string DvPersonaSolicitante { get; set; }

        /// <summary>
        /// Obtiene o modifica Nombre Persona Solicitante
        /// </summary>
        public string NombrePersonaSolicitante { get; set; }

        /// <summary>
        /// Obtiene o modifica Apellido Paterno Persona Solicitante
        /// </summary>
        public string ApellidoPaternoPersonaSolicitante { get; set; }

        /// <summary>
        /// Obtiene o modifica Apellido Materno Persona Solicitante
        /// </summary>
        public string ApellidoMaternoPersonaSolicitante { get; set; }

        /// <summary>
        /// Obtiene o modifica Rut
        /// </summary>
        public int Rut { get; set; }

        /// <summary>
        /// Obtiene o modifica Digito Verificador
        /// </summary>
        public string Dv { get; set; }

        /// <summary>
        /// Obtiene o modifica Nombre Persona
        /// </summary>
        public string NombrePersona { get; set; }

        /// <summary>
        /// Obtiene o modifica Apellido Paterno Persona
        /// </summary>
        public string ApellidoPaternoPersona { get; set; }

        /// <summary>
        /// Obtiene o modifica Apellido Materno Persona
        /// </summary>
        public string ApellidoMaternoPersona { get; set; }

        /// <summary>
        /// Obtiene o modifica Apellido Materno Persona
        /// </summary>
        public string RutPersonaAbogado { get; set; }
    }
}
