﻿using System;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class HistorialSGLDTO
    {
        /// <summary>
        /// Codigo de la SGL.
        /// </summary>
        public int CodigoSGL { get; set; }

        /// <summary>
        /// Fecha seguimiento.
        /// </summary>
        public DateTime FechaSeguimiento { get; set; }

        /// <summary>
        /// Estado SGL
        /// </summary>
        public string EstadoDescripcionSGL { get; set; }

        /// <summary>
        /// Codigo persona
        /// </summary>
        public int CodigoPersona { get; set; }

        /// <summary>
        /// Responsable
        /// </summary>
        public string Responsable { get; set; }
    }
}
