﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class IngresarSGLDTO
    {
        /// <summary>
        /// Id sgl.
        /// </summary>
        public int IdSGL { get; set; }

        /// <summary>
        /// Fecha.
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Fecha solicitud")]
        public string Fecha { get; set; }

        /// <summary>
        /// Materia.
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Materia")]
        public int Materia { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Tipo de Materia")]
        /// <summary>
        /// Tipo de materia.
        /// </summary>
        public int TipoMateria { get; set; }

        /// <summary>
        /// Estado.
        /// </summary>
        public int Estado { get; set; }

        /// <summary>
        /// Oficina de Partes.
        /// </summary>
        public int of_partes { get; set; }

        /// <summary>
        /// Id de sucursal.
        /// </summary>
        [Required(ErrorMessage ="El campo {0} es requerido")]
        [Display(Name = "Sucursal")]
        public int IdSucursal { get; set; }

        /// <summary>
        /// Codigo usuario Session.
        /// </summary>
        public int CodigoUsuario { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Rut")]
        public string Rut { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Abogado")]
        public int IdAbogado { get; set; }
        
        /// <summary>
        /// Id Tipo Persona.
        /// </summary>
        public int IdTipoPersona { get; set; }

        public int SglId { get; set; }

        public string Entrada { get; set; }

        #region Cambiar Sociedad 

        /// <summary>
        /// Codigo Persona Sociedad.
        /// </summary>
        public int CodigoPersona { get; set; }

        /// <summary>
        /// Apellido Paterno de Persona.
        /// </summary>
        public string ApellidoPaterno { get; set; }

        /// <summary>
        /// Apellido Materno de Persona.
        /// </summary>
        public string ApellidoMaterno { get; set; }

        /// <summary>
        /// Paremetro Codigo de Grupo para Validar la auterizacion de usuario de session para cambiar Persona Jurídica.
        /// </summary>
        public int CodigoGrupo { get; set; }

        /// <summary>
        /// Paremetro Rut Abogado para cambiar Persona Jurídica.
        /// </summary>
        public string RutAbogado { get; set; }

        /// <summary>
        /// Paremetro Nombre Actual para cambiar Persona Jurídica.
        /// </summary>
        public string NombreActual { get; set; }

        /// <summary>
        /// Paremetro Variable de Respaldo necesaria .
        /// </summary>
        public int VariableRespaldo { get; set; }

        /// <summary>
        /// Paremetro Digito Verificador de Persona .
        /// </summary>
        public string Dv { get; set; }

        /// <summary>
        /// Paremetro Rut de Abogado de Persona .
        /// </summary>
        public string RutPersonaAbogado { get; set; }


        #endregion

    }
}
