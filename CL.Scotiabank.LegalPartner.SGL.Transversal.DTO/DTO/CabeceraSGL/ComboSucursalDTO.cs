﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class ComboSucursalDTO
    {
        /// <summary>
        /// Columna BD: "suc_id"
        /// Codigo de la sucursal.
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Columna BD: "suc_des"
        /// Descripcion de la  sucursal.
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Columna BD: "of_partes_id"
        /// id de la  Oficina Partes.
        /// </summary>
        public int OficinaPartesCodigo { get; set; }

        /// <summary>
        /// Columna BD: "of_partes_des"
        /// Descripcion de la  Oficina Partes.
        /// </summary>
        public string OficinaPartesDescripcion { get; set; }
    }
}
