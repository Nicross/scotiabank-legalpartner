﻿using System;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// Entidad de negocio representativa de SGL
    /// </summary>
    public class SGLDTO
    {
        /// <summary>
        /// Estado SGL.
        /// </summary>
        public int EstadoSGL { get; set; }

        /// <summary>
        /// Columna BD: "sgl_id"
        /// Codigo del Usuario del Consulta.
        /// </summary>
        public int SGLId { get; set; }

        /// <summary>
        /// Rut solicitante.
        /// </summary>
        public string RutPersonaSolicitante { get; set; }

        /// <summary>
        /// Dv Solicitante.
        /// </summary>
        public string DvPersonaSolicitante { get; set; }

        /// <summary>
        /// Nombre Persona Solicitante.
        /// </summary>
        public string NombrePersonaSolicitante { get; set; }

        /// <summary>
        /// Apellido Paterno Persona Solicitante.
        /// </summary>
        public string ApellidoPaternoPersonaSolicitante { get; set; }

        /// <summary>
        /// Apellido Materno Persona Solicitante.
        /// </summary>
        public string ApellidoMaternoPersonaSolicitante { get; set; }

        /// <summary>
        /// Rut.
        /// </summary>
        public int Rut { get; set; }

        /// <summary>
        /// Digito Verificador.
        /// </summary>
        public string Dv { get; set; }

        /// <summary>
        /// Nombre Persona.
        /// </summary>
        public string NombrePersona { get; set; }

        /// <summary>
        /// Apellido Paterno Persona.
        /// </summary>
        public string ApellidoPaternoPersona { get; set; }

        /// <summary>
        /// Apellido Materno Persona.
        /// </summary>
        public string ApellidoMaternoPersona { get; set; }

        /// <summary>
        /// Sucursal Id.
        /// </summary>
        public int SucursalId { get; set; }

        /// <summary>
        /// Fecha Inicio SGL
        /// </summary>
        public DateTime FechaInicioSGL { get; set; }

        /// <summary>       
        /// Descripcion estado SGL.
        /// </summary>
        public string DescripcionEstadoSGL { get; set; }

        /// <summary>
        /// Columna BD: "per_cod_abg"
        /// Nombre del Usuario del Consulta.
        /// </summary>
        public int CodigoPersonaAbogadoSGL { get; set; }

        //if strCboAbogados = ""

        /// <summary>
        /// Codigo Persona Abogado.
        /// </summary>
        //public int CodigoPersonaAbogado { get; set; }

        /// <summary>
        /// Nombre Persona Abogado.
        /// </summary>
        //public string NombrePersonaAbogado { get; set; }

        /// <summary>
        /// Apellido Persona Abogado.
        /// </summary>
        //public string ApellidoPaternoPersonaAbogado { get; set; }

        /// <summary>
        /// .
        /// </summary>
        //public string ApellidoMaternoPersonaAbogado { get; set; }

        //

        //Para HTML_ComboBox_MateriaSeleccionada_New
        /// <summary>
        /// materia padre de la SGL.
        /// </summary>
        public string MateriaPadre { get; set; }

        //Para HTML_ComboBox_TipoMateriaSeleccionada_New
        /// <summary>
        /// Columna BD: "mat_id"
        /// Materia Id.
        /// </summary>
        public int MateriaId { get; set; }

        //Para sglIngreso
        /// <summary>
        /// Codigo Persona Solicitante.
        /// </summary>
        public int CodigoPersonaSolicitante { get; set; }

        /// <summary>
        /// Codigo Persona. 
        /// </summary>
        public int CodigoPersona { get; set; }
    }
}
