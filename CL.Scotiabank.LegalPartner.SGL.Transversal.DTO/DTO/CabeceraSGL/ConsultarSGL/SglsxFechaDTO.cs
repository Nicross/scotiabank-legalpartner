﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// Dto utilizado para Modal de busqueda SGLs por fecha
    /// </summary>
    public class SglsxFechaDTO
    {
        /// <summary>
        /// Codigo identificador del SGL
        /// </summary>
        public string SGLId { get; set; }

        /// <summary>
        /// Fecha de SGL
        /// </summary>
        public string Fecha { get; set; }

        /// <summary>
        /// Nombre de Sociedad
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Tipo de materia
        /// </summary>
        public string TipoMateria { get; set; }

        /// <summary>
        /// Estado de SGL
        /// </summary>
        public string Estado { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string mensajeError { get; set; }
    }
}
