﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
//TODO: Agregar restricciones a los campos.
namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// DTO para la tabla de SucursalBanco
    /// </summary>
    public class SucursalBancoDTO
    {
        /// <summary>
        /// Código del SucursalBanco
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Descripción del SucursalBanco
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Descripción")]
        [MaxLength(80)]
        public string Descripcion { get; set; }

        /// <summary>
        /// Codigo de la oficina de partes de la sucursal
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Oficina de Partes")]
        public int OficinaPartesCodigo { get; set; }

        /// <summary>
        /// Codigo de la oficina de partes de la sucursal
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Oficina de Partes")]
        public string OficinaPartesDescripcion { get; set; }

        /// <summary>
        /// Codigo de cuenta de la sucursal
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Cuenta")]
        [MaxLength(50)]
        public string SucursalCodigoCuenta { get; set; }
    }
}
