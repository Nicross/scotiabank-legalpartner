﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class UsuarioClavesDTO
    {
        /// <summary>
        /// Código del Usuario (per_cod)
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Identificador de Session de Usuario (cla_login) que en sistema se representa por ser el rut sin digito verificador
        /// </summary>
        public string UsuarioSession { get; set; }

        /// <summary>
        /// Clave activa de l Usuario (cla_clave_act) 
        /// </summary>
        public string ClaveActiva { get; set; }

        /// <summary>
        /// Clave anterior de usuario (cla_clave_ant) 
        /// </summary>
        public string ClaveAnterior { get; set; }

        /// <summary>
        /// Clave Bloqueada (cla_bloqueado) 
        /// </summary>
        public string ClaveBloqueada { get; set; }

        /// <summary>
        /// Numero de intentos de ingreso de clave del Usuario (cla_intentos) 
        /// </summary>
        public int IntentosClave { get; set; }

        /// <summary>
        /// Clave Inicio (cla_inicio) 
        /// </summary>
        public string ClaveInicio { get; set; }

        /// <summary>
        /// CFecha de Inicio de clave (cla_fec_ini) 
        /// </summary>
        public DateTime FechaInicio { get; set; }

        /// <summary>
        /// Segunda Clave Activa (cla_clave_act_2) 
        /// </summary>
        public string SugundaClaveActiva { get; set; }
        
        public ICollection<UsuarioDTO> Usuario { get; set; }



    }
}
