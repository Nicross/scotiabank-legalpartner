﻿using System.ComponentModel.DataAnnotations;

//TODO: Agregar restricciones a los campos.
namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// DTO para la tabla de GrupoUsuario
    /// </summary>
    public class GrupoUsuarioDTO
    {
        /// <summary>
        /// Código del GrupoUsuario
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Descripción del GrupoUsuario
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Descripción")]
        [MaxLength(30)]
        public string Descripcion { get; set; }
    }
}
