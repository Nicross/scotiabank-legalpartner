using System.ComponentModel.DataAnnotations;
/// <summary>
//TODO:Validar restricciones de los campos.
/// </summary>
namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// DTO utilizado para la tabla de Tipo de Poder.
    /// </summary>
    public class TipoPoderDTO
    {
        /// <summary>
        /// Código del Tipo de Poder (cbr_cod)
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Descripción del Tipo de Poder (cbr_des)
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Descripción")]
        [MaxLength(80)]
        public string Descripcion { get; set; }
    }
}
