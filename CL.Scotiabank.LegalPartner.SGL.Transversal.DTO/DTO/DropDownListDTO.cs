﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// DTO utilizado para desplegar los DropDownList del sistema.
    /// </summary>
    public class DropDownListDTO
    {
        public string id { get; set; }

        public string text {   get; set; }
    }
}
