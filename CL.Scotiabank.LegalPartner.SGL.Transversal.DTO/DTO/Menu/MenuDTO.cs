﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class MenuDTO
    {
        /// <summary>
        /// 
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? CodigoMenuPadre { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool EsMenuCabecera { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? GrupoUsuarioCodigo { get; set; }

        public string Area { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }
    }
}
