﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
//TODO: Agregar restricciones a los campos.
namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// DTO para la tabla de MateriaBanco
    /// </summary>
    public class MateriaBancoDTO
    {
        /// <summary>
        /// Código del MateriaBanco 
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Descripción del MateriaBanco
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Descripción")]
        [MaxLength(80)]
        public string Descripcion { get; set; }
        
        /// <summary>
        /// Materia padre de la  materia banco.
        /// </summary>
        public int? MateriaPadre { get; set; }

        /// <summary>
        /// Materia padre de la  materia banco.
        /// </summary>
        public string MateriaPadreDescripcion { get; set; }
    }
}
