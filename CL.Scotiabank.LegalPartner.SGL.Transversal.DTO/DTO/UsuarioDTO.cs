﻿using System.ComponentModel.DataAnnotations;
/// <summary>
//TODO:Validar restricciones de los campos.
/// </summary>
namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// DTO utilizado para la tabla de Usuario.
    /// </summary>
    public class UsuarioDTO
    {
        /// <summary>
        /// Codigo del Usuario (per_cod)
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Rut del Usuario (per_nrt)
        /// validacion Rut completo @"\b\d{1,8}\-[K|k|0-9]$
        /// </summary>
        [Required(ErrorMessage = "El campo Rut es requerido")]
        [Display(Name = "Rut Usuario")]
        //[RegularExpression(@"[0-9]", ErrorMessage = "Debe ingresar un Rut válido")]
        [MaxLength(80)]
        public string RutUsuario { get; set; }
       

        /// <summary>
        /// Digito Verificador de Usuario (per_drt)
        /// </summary>
        [Required(ErrorMessage = "El campo Digito Verificador es requerido")]
        [Display(Name = "Digito Verificador")]
        [RegularExpression(@"\d{1,}[0-9kK]$", ErrorMessage = "Debe ingresar un Digito Verificador válido")]
        [MaxLength(1)]
        public string DigitoVerificador { get; set; }

        /// <summary>
        /// Nombre del Usuario (per_nom)
        /// </summary>
        [Required(ErrorMessage = "El campo Nombre es requerido")]
        [Display(Name = "Nombre")]
        [MaxLength(260)]
        public string Nombre { get; set; }

        /// <summary>
        /// Apellido Paterno de Usuario (per_apl_ptn)
        /// </summary>
        [Required(ErrorMessage = "El campo Apellido Paterno es requerido")]
        [Display(Name = "Apellido Paterno")]
        [MaxLength(30)]
        public string ApellidoPaterno { get; set; }

        /// <summary>
        /// Apellido Materno del Usuario (per_apl_mtn)
        /// </summary>
        [Required(ErrorMessage = "El campo Apeliido Materno es requerido")]
        [Display(Name = "Apellido Materno")]
        [MaxLength(30)]
        public string ApellidoMaterno { get; set; }

        /// <summary>
        /// Telefono del Usuario (per_tel)
        /// </summary>
        [Required(ErrorMessage = "El campo Telefono es requerido")]
        [RegularExpression(@"\d{1,}[0-9]$",ErrorMessage = "Debe ingresar un número telefónico válido")]
        [Display(Name = "Telefono")]
       
        public string Telefono { get; set; }

        /// <summary>
        /// Email de Usuario (per_email)
        /// </summary>
        [Required(ErrorMessage = "El campo Email es requerido")]
        [Display(Name = "Email")]
        [MaxLength(80)]
        [RegularExpression(@"\b[a-zA-Z0-9._-]+@[a-zA-Z0-9]+.[a-zA-Z]{2,4}$\b", ErrorMessage = "Mail inválido")]
        public string Email { get; set; }

        /// <summary>
        /// Clave de Usuario (cla_clave_act)
        /// </summary>
        [Display(Name = "Contraseña")]
        [Required(ErrorMessage = "El campo Constraseña es requerido")]
        [DataType(DataType.Password)]
        [StringLength(16, MinimumLength = 8, ErrorMessage = "Debe ingresar entre 8 y 16 carácteres")]

        public string Contraseña { get; set; }

        /// <summary>
        /// Clave de Confirmación de Usuario 
        /// </summary>
        [Display(Name = "Contraseña de confirmación")]
        [Required(ErrorMessage = "Debe confirmar Contraseña")]
        [DataType(DataType.Password)]
        [StringLength(16, MinimumLength = 8, ErrorMessage = "Debe ingresar entre 8 y 16 carácteres")]
        [System.ComponentModel.DataAnnotations.Compare("Contraseña", ErrorMessage = "Las contraseñas no coinciden")]   
        public string ContraseñaConfirmacion { get; set; }

        /// <summary>
        /// Sesión Usuario 
        /// </summary>
        [Display(Name = "Sesión Usuario")]
        public string SesionUsuario { get; set; }


        /// <summary>
        /// Codigo de Vigencia (cod_usr_vgt)
        /// </summary>
        public int CodigoVigencia { get; set; }

        /// <summary>
        /// Codigo de Vigencia (cod_usr_vgt)
        /// </summary>
        public int CodigoVigenciaConsulta { get; set; }

        /// <summary>
        /// Id Sucursal (suc_id)
        /// </summary>
        public string IdSucursal { get; set; }

        /// <summary>
        /// Id Sucursal (suc_id)
        /// </summary>
        public string IdNuevoSucursal { get; set; }

        /// <summary>
        /// Descripción Sucursal (suc_des)
        /// </summary>
        public string DescripcionSucursal { get; set; }

        /// <summary>
        /// Oficina de Partes Sucursal (of_partes_id)
        /// </summary>
        public int IdOficinaPartes { get; set; }

        /// <summary>
        /// Codigo de Perfil de usuario (perf_cod)
        /// </summary>
        public int CodigoPerfil { get; set; }

        /// <summary>
        /// Codigo de Perfil de usuario (perf_cod)
        /// </summary>
        public int CodigoNuevoPerfil { get; set; }

        /// <summary>
        /// Codigo de Perfil de usuario (perf_des)
        /// </summary>
        public string DescripcionPerfil { get; set; }

        /// <summary>
        /// Codigo de Grupo de usuario (grup_cod)
        /// </summary>
        public int CodigoGrupo { get; set; }

        /// <summary>
        /// Descripción de Grupo (grup_des)
        /// </summary>
        public string DescripcionGrupo { get; set; }

        /// <summary>
        /// Clave Bloqueada(cla_bloqueado)
        /// </summary>
        public string ClaveBloqueada { get; set; }

        /// <summary>
        /// Codigo de retorno tras la utilización del objeto.
        /// </summary>
        public int CodigoRetorno { get; set; }

        /// <summary>
        /// Filas afectadas tras la utilización del objeto.
        /// </summary>
        public int FilasAfectadas { get; set; }
            
        public string ListaDropDownSucursal { get; set; }

        public string ListaDropDownPerfil { get; set; }

    }
}
