﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO.DTO.EntradaCompleta
{
   public class IngresarDocDTO
    {
        public string Fecha { get; set; }

        public int IdSgl { get; set; }

        public string DocumentoHtml { get; set; }

        public int TipoDocumentoLegal { get; set; }

        public int CodigoDocumneto { get; set; }
    }
}
