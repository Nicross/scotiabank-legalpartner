﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class EstadoSGLDTO
    {
        public int CodigoEstadoSGL{get; set;} 
        public string DescripcionEstadoSGL{get; set;} 
        public int CodigoSGL{get; set;} 
    }
}
