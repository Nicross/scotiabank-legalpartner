﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class DocumentoLegalEntradaCompletaDTO
    {
        public string NotariaProtocolizacion { get; set; }
        public string CodigoOrigenDocumento { get; set; }
        public string OrigenDocumento { get; set; }
        public string NombreOrigen { get; set; }
        public string LugarOrigen { get; set; }
        public string Observaciones { get; set; }
        public string EstadoCsu { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFinal { get; set; }
        public string CodigoRevocacion { get; set; }
        public DateTime FechaResolucionQuiebra { get; set; }
        public int CodigoJuzgado { get; set; }
        public string Juzgado { get; set; }
        public DateTime FechaPublicacionQuiebra { get; set; }
        public string UsuarioLogin { get; set; }
        public string sol_cod_uni_ofi { get; set; }
        public string NombreInforme { get; set; }
        public string CodRct { get; set; }
        public string CodigoNotariaProtocolizacion { get; set; }
        public DateTime FechaProtocolizacion { get; set; }
        public DateTime FechaPublicacionDocumento { get; set; }
        public string NumeroDecreto { get; set; }
        public int CodigoDocumento { get; set; }
        public int CodigoPersona { get; set; }
        public int CodigoTipoDocumento { get; set; }
        public int CodigoTipoLegalizacion { get; set; }
        public string TipoLegalizacion { get; set; }
        public DateTime FechaDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public int CodigoNotaria { get; set; }
        public string Notaria { get; set; }
        public DateTime FechaInp { get; set; }
        public string AnioInp { get; set; }
        public string FojaDocumento { get; set; }
        public string NumeroFojaDocumento { get; set; }
        public int CodigoConservador { get; set; }
        public string Conservador { get; set; }
        public DateTime FechaPublicacion { get; set; }
        public DateTime FechaSesionDirectorio { get; set; }
        public DateTime FechaJuntaExtraordinariaAccionistas { get; set; }
        public DateTime FechaDecreto { get; set; }
        public string TipoDocumentoPod { get; set; }
        public DateTime FechaRenovacionDocumento { get; set; }
    }
}
