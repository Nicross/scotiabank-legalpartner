﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class EliminarDocumentoDTO
    {
        public string CodigoDocumento { get; set; }
    }
}
