﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class RevocacionEscrituraDTO
    {
        public int CodigoDocumento { get; set; }
        public int CodigoRevocacion { get; set; }
        public string Estado { get; set; }
        public string FechaFinal { get; set; }
    }
}
