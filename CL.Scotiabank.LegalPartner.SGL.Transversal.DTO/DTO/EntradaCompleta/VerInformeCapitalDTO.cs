﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class VerInformeCapitalDTO
    {
        public int SGL { get; set; }
        public string Rut { get; set; }
        public string Dv { get; set; }
        public string RazonSocial { get; set; }
        public string CodigoPersonaJuridica { get; set; }
        public string CodigoTipoParticipante { get; set; } 
        public int CodigoPersonaAbogado { get; set; }
        public string TipoPersonaJuridica { get; set; }
        public string NombreFantasia { get; set; }
        public string Domicilio { get; set; }
        public string Administracion { get; set; }
        public string Objeto { get; set; }
        public string Duracion { get; set; }
        public string Monto { get; set; }
        public string Moneda { get; set; }
        public string NombreSocio { get; set; }
        public int CapitalAportado { get; set; }
        public decimal MontoCapital { get; set; }
        public string MontoCapitalIngresa { get; set; }
        public string CodigoMoneda { get; set; }
        public string CodigoMonedaSeleccionado { get; set; }
        public string DescripcionMoneda { get; set; }
        public decimal CantidadTotalAcciones { get; set; }
        public decimal CantidadTotalAccionesIngresa { get; set; }
        public decimal ValorNominalAccion { get; set; }
        public decimal ValorNominalAccionIngresa { get; set; }
        public string Descripcion { get; set; }
        public int CodigoPersona { get; set; }
        public string NombreAbogado { get; set; }
        public string ApellidoAbogado { get; set; }
        public string NombreCompleto { get; set; }
        public string CodigoPersonaSGL { get; set; }
        public string Observaciones { get; set; }
        public string FechaActual = DateTime.Now.ToString("dd/MM/yyyy").Replace("-","/");
        public bool DatosValidos { get; set; }
        public int Formato { get; set; } 
        public string Entrada { get; set; }
        public DateTime FechaActualizacion = DateTime.Now;
        public string FechaActualizacionString { get; set; }
        public IEnumerable<ParticipanteDTO> listaParticipantes;
        public IEnumerable<DocumentoDTO> ListaDocumentos;
        public IEnumerable<MonedaDTO> listaMoneda;
    }
}
