﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class DocumentoDTO
    {
        public int CodigoTipoLegal { get; set; }
        public string DescripcionTipoLegal { get; set; }
        public DateTime FechaResolucionQuiebra { get; set; }
        
        public string FechaResolucionQuiebraString { get; set; }
        public DateTime FechaDocumento { get; set; }
        
        public string FechaDocumentoString { get; set; }
        public string Juzgado { get; set; }
        public string Notaria { get; set; }
        public string NumeroDocumento { get; set; }
        public DateTime FechaIncorporacion { get; set; }
        
        public string FechaIncorporacionString { get; set; }
        public string Foja { get; set; }
        public string FojaNumero { get; set; }
        public string FechaDecreto { get; set; }
        public string NumeroDecreto { get; set; }
        public DateTime FechaPublicacionQuiebra { get; set; }
        
        public string FechaPublicacionQuiebraString { get; set; }
        public DateTime FechaPublicacionDecreto { get; set; }
       
        public string FechaPublicacionDecretoString { get; set; }
        public string Observaciones { get; set; }
        public string Conservador { get; set; }
        public DateTime FechaPublicacion { get; set; }
        
        public string FechaPublicacionString { get; set; }
    }
}
