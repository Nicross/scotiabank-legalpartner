﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class DatosModalAgregarDocumEscriDTO
    {
        [Required]
        public int Sgl { get; set; }

        [Required]
        public int EstadoLegal { get; set; }

        [Required]
        public int TipoSoc { get; set; }
        public int NumFilas { get; set; }

        public string CodDoctos { get; set; }

        [Required]
        public int TipoSociedad { get; set; }

        public int EscConst { get; set; }

        [Required]
        public int TipoPod { get; set; }

        public int CodEpv { get; set; }

        public int CodReg { get; set; }

        [Required]
        public string FechaCons { get; set; }

        [Required]
        public int PerId { get; set; }

        public int CodDocumento { get; set; }
    }
}
