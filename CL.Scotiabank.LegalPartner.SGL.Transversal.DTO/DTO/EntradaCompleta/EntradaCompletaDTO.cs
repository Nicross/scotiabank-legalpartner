﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class EntradaCompletaDTO
    {
        public int Rut { get; set; }
        public int Sgl { get; set; }
        public int CodigoPersona { get; set; }
        public string CodigoTipoPersona { get; set; }
        public string Entrada { get; set; }
    }
}
