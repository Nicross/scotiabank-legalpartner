﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class GrabaRevocacionDTO
    {
        public int IdSgl { get; set; }
        public int CodPer { get; set; }
        public string Texto { get; set; }
        public int CodDocu { get; set; }

        public int EliminaSoc { get; set; }
        public int TipoPer { get; set; }
        public string NombrePer { get; set; }
        public string Tipopod { get; set; }
    }
}
