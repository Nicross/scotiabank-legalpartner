﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class ConstitucionYLegalizacionDTO
    {
        public string TipoPersonaFLEG { get; set; }
        public int CodTipoPersona { get; set; }
        public int Sgl { get; set; }
        public int intTipoLeg { get; set; }
        public int CodEPV { get; set; }
        public string CodigoEstadoLegal { get; set; }
        public string Estado { get; set; }
        public string FechaConsulta { get; set; }
        public int IdPersona { get; set; }
    }
}
