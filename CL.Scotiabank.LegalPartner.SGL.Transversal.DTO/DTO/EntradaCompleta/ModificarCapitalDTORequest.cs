﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
   public class ModificarCapitalDTORequest
    {
       
        public int CodigoPersona { get; set; }

        
        public decimal MontoCapital { get; set; }

       
        public string CodigoMoneda { get; set; }

       
        public decimal CantidadTotal { get; set; }

        
        public decimal ValorNominal { get; set; }

       
        public string ObservacionCapital { get; set; }
    }
}
