﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class ObtenerCantParticipantesDocDTO
    {
        public int CantParticipantesDoc { get; set; }
    }
}
