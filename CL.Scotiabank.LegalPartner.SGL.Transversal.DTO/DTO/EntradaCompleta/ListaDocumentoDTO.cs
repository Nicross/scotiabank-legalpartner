﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class ListaDocumentoDTO
    {
        public int contador { get; set; }
        public string FechaFinal { get; set; }
        public string CodigoRevocacion { get; set; }
        public string FechaResolucionQuiebra { get; set; }
        public string Juzgado { get; set; }
        public string FechaPublicacionQuiebra { get; set; }
        public string UsuarioLogin { get; set; }
        public string sol_cod_uni_ofi { get; set; }
        public string NombreInforme { get; set; }
        public string CodigoConservador { get; set; }
        public string FechaRenovacionDocumento { get; set; }
        public string IdSGL { get; set; }
        public string Notaria { get; set; }
        public string NumeroDocumento { get; set; }
        public string FechaDocumento { get; set; }
        public string DescripcionTipoLegal { get; set; }
        public int CodigoTipoLegal { get; set; }
        public string FechaInicio { get; set; }
        public string EstadoCsu { get; set; }
        public string Observaciones { get; set; }
        public string LugarOrigen { get; set; }
        public string FechaIncorporacion { get; set; }
        public string AnioIncorporacion { get; set; }
        public string Foja { get; set; }
        public string FojaNumero { get; set; }
        public string Conservador { get; set; }
        public string FechaPublicacion { get; set; }
        public string FechaSesionDirectorio { get; set; }
        public int CodigoDocumento { get; set; }
        public string FechaJuntaExtraordinariaAccionistas { get; set; }
        public string NumeroDecreto { get; set; }
        public string FechaPublicacionDecreto { get; set; }
        public string FechaProtocolizacion { get; set; }
        public string NombreNotariaProtocolizacion { get; set; }
        public string CodigoOrigen { get; set; }
        public string DescripcionOrigen { get; set; }
        public string NombreOrigen { get; set; }
        public string FechaDecreto { get; set; }
        public int CodigoPersona { get; set; }
    }
}
