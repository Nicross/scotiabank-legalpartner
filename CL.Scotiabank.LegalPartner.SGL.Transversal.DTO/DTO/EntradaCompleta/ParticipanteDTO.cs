﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
   public class ParticipanteDTO
    {
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Rut { get; set; }
        public string Dv { get; set; }
        public string MontoAporte { get; set; }
        public string PorcentajeAporte { get; set; }

       
    }
}
