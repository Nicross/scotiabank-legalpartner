﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class TipoLegalizacionDTO
    {
        /// <summary>
        /// Codigo Tipo Legalizacion.
        /// </summary>
        public string CodigoTipoLeg { get; set; }

        /// <summary>
        /// Codigo Tipo Documento.
        /// </summary>
        public string CodigoTipoDoc { get; set; }

        /// <summary>
        /// Descripcion Tipo Legalizacion
        /// </summary>
        public string DescripcionTipoLeg { get; set; }

        /// <summary>
        /// Codigo Fto.
        /// </summary>
        public string CodigoFto { get; set; }

        /// <summary>
        /// Codigo Ind Vis.
        /// </summary>
        public string CodigoInvVis { get; set; }

        /// <summary>
        /// Estado correspondiente al insertar un tribunal (0 exitoso)
        /// </summary>
        public int Estado { get; set; }

        /// <summary>
        /// Codigo correspondiente al agregar un tribunal
        /// </summary>
        public int CodigoCreacion { get; set; }

        /// <summary>
        /// Codigo correspondiente al agregar un tribunal
        /// </summary>
        public int CodigoTipo { get; set; }

        /// <summary>
        /// Codigo correspondiente al agregar un tribunal
        /// </summary>
        public string TipoTexto { get; set; }

        /// <summary>
        /// Codigo correspondiente al Orden
        /// </summary>
        public int Orden { get; set; }

        /// <summary>
        /// Codigo correspondiente al Nombre Parrafo
        /// </summary>
        public string NombreParrafo { get; set; }

        /// <summary>
        /// Codigo correspondiente al Comportamiento
        /// </summary>
        public int Comportamiento { get; set; }

        /// <summary>
        /// Codigo correspondiente al Texto
        /// </summary>
        public string Texto { get; set; }
    }
}
