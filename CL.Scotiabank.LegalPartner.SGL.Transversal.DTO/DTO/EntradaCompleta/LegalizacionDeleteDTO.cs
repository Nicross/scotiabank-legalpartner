﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class LegalizacionDeleteDTO
    {
        /// <summary>
        /// Columna BD: "cbr_cod"
        /// Codigo del Conservador.
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Columna BD: "cbr_des"
        /// Descripcion del Conservador.
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Codigo de Retorno devuelto de la ejecucion del SP ejecutado.
        /// </summary>
        public int CodigoRetorno { get; set; }

        /// <summary>
        /// Cuerpo a retornar en caso de ser necesario
        /// </summary>
        public string CuerpoRetorno { get; set; }

        /// <summary>
        /// Filas afectadas durante la ejecuccion del procedimiento almacenado.
        /// </summary>
        public int FilasAfectadas { get; set; }
    }
}
