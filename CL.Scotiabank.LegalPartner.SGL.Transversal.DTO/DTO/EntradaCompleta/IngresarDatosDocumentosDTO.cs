﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class IngresarDatosDocumentosDTO
    {
        /// <summary>
        /// Codigo correspondiente al documento
        /// </summary>
        public int CodDocumento { get; set; }

        /// <summary>
        /// Juzgado
        /// </summary>
        public string Juzgado { get; set; }

        /// <summary>
        /// Depende si es Edicion o Agregar un Documento
        /// </summary>
        public string Modo { get; set; }

        /// <summary>
        /// Tipo Sociedad
        /// </summary>
        public int TipoSoc { get; set; }

        /// <summary>
        /// Estado Legalizacion
        /// </summary>
        public int EstadoLegal { get; set; }

        /// <summary>
        /// Cantidad de filas en la tabla documentos
        /// </summary>
        public int NumFilas { get; set; }

        /// <summary>
        /// Codigo Documento
        /// </summary>
        public string CodDocumentos { get; set; }

        /// <summary>
        /// Fecha Consulta
        /// </summary>
        public string FechaCons { get; set; }

        /// <summary>
        /// Codigo de la persona.
        /// </summary>
        ////[PropertyName("intPerCod")]
        public int CodigoPersona { get; set; }

        /// <summary>
        /// Id de SGL.
        /// </summary>
        ////[PropertyName("intSGL")]
        public int IdSGL { get; set; }

        /// <summary>
        /// Codigo de tipo legal.
        /// </summary>
        ////[PropertyName("intTipoLegCod")]
        public int CodigoTipoLegal { get; set; }

        /// <summary>
        /// Fecha de documento.
        /// </summary>
        ////[PropertyName("dtmFecDoc")]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Fecha Documento")]
        [MaxLength(10)]
        public string FechaDocumento { get; set; }

        /// <summary>
        /// Numero del documento.
        /// </summary>
        ////[PropertyName("strNumDoc")]
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Numero Documento")]
        [MaxLength(10)]
        public string NumeroDocumento { get; set; }

        /// <summary>
        /// Codigo de la notaria.
        /// </summary>
        //[PropertyName("strNotCod")]
        public string CodigoNotaria { get; set; }

        /// <summary>
        /// Nombre Notaria
        /// </summary>
        public string Notaria { get; set; }

        /// <summary>
        /// Fecha inscripción.
        /// </summary>
        //[PropertyName("dtmFecInscrip")]
        public string FechaInscripcion { get; set; }

        /// <summary>
        /// Año de inscripción.
        /// </summary>
        //[PropertyName("strAnoInscrip")]
        public string AnnoInscripcion { get; set; }

        /// <summary>
        /// Descripción de Fojas.
        /// </summary>
        //[PropertyName("strFojas")]
        public string Fojas { get; set; }

        /// <summary>
        /// Numero de foja.
        /// </summary>
        //[PropertyName("strNumFoja")]
        public string NumeroFoja { get; set; }

        /// <summary>
        /// Codigo del conservador.
        /// </summary>
        //[PropertyName("smaConservCod")]
        public int CodigoConservador { get; set; }

        /// <summary>
        /// Nombre del Conservador
        /// </summary>
        public string Conservador { get; set; }

        /// <summary>
        /// Fecha publicación.
        /// </summary>
        //[PropertyName("dtmFecPublic")]
        public string FechaPublicacion { get; set; }

        /// <summary>
        /// .
        /// </summary>
        //[PropertyName("dtmFecSesDire")]
        public string FechaSesionDirectorio { get; set; }

        /// <summary>
        /// .
        /// </summary>
        //[PropertyName("dtmFecJunAcc")]
        public string FechaJuntaAccionista { get; set; }

        /// <summary>
        /// Codigo rectificación.
        /// </summary>
        //[PropertyName("intCodRectifacion")]
        public int CodigoRectificacion { get; set; }

        /// <summary>
        /// rectificación
        /// </summary>
        public string Rectificacion { get; set; }

        /// <summary>
        /// Fecha decreto.
        /// </summary>
        //[PropertyName("dtmFecDecreto")]
        public string FechaDecreto { get; set; }

        /// <summary>
        /// Numero decreto.
        /// </summary>
        //[PropertyName("strNumDecreto")]
        public string NumeroDecreto { get; set; }

        /// <summary>
        /// Fecha publicación decreto.
        /// </summary>
        //[PropertyName("dtmFecPblDecreto")]
        public string FechaPublicacionDecreto { get; set; }

        /// <summary>
        /// Fecha Protocolo.
        /// </summary>
        //[PropertyName("dtmFecProtoc")]
        public string FechaProtocolarizacion { get; set; }

        /// <summary>
        /// Codigo notaria protocolo.
        /// </summary>
        //[PropertyName("strNotProtocCod")]
        public string CodigoNotariaProtocolo { get; set; }

        /// <summary>
        /// Codigo origen documento.
        /// </summary>
        //[PropertyName("intOrigenDocCod")]
        public int CodigoDocumentoOrigen { get; set; }

        /// <summary>
        /// origen documento.
        /// </summary>
        //[PropertyName("intOrigenDocCod")]
        public string DocumentoOrigen { get; set; }

        /// <summary>
        /// Nombre de origen.
        /// </summary>
        //[PropertyName("strNomOrigen")]
        public string NombreOrigen { get; set; }

        /// <summary>
        /// Lugar de origen.
        /// </summary>
        //[PropertyName("strLugOrigen")]
        public string LugarOrigen { get; set; }

        /// <summary>
        /// Oberservaciones.
        /// </summary>
        //[PropertyName("strObserv")]
        public string Observaciones { get; set; }

        /// <summary>
        /// Estado de consulta.
        /// </summary>
        //[PropertyName("strEstConsulta")]
        public string EstadoConsulta { get; set; }

        /// <summary>
        /// Fecha inicio.
        /// </summary>
        //[PropertyName("dtmFecInicio")]
        public string FechaInicio { get; set; }

        /// <summary>
        /// Fecha final.
        /// </summary>
        //[PropertyName("dtmFecFinal")]
        public string FechaFinal { get; set; }

        /// <summary>
        /// Codigo de revocación.
        /// </summary>
        //[PropertyName("intRevocionCod")]
        public int CodigoRevocacion { get; set; }

        /// <summary>
        /// Fecha de resolución de quiebra.
        /// </summary>
        //[PropertyName("dtmFecRslQuiebra")]
        public string FechaResolucionQuiebra { get; set; }

        /// <summary>
        /// Codigo Tribunal.
        /// </summary>
        //[PropertyName("intTribunalCod")]
        public int CodigoTribunal { get; set; }

        /// <summary>
        /// Fecha publicación quiebra.
        /// </summary>
        //[PropertyName("dtmFecPblQuiebra")]
        public string FechaPublicacionQuiebra { get; set; }

        /// <summary>
        /// Responable del documento.
        /// </summary>
        //[PropertyName("strResponsableDoc")]
        public string ResponsableDocumento { get; set; }

        /// <summary>
        /// Lugar del documento.
        /// </summary>
        //[PropertyName("strLugarDoc")]
        public string LugarDocumento { get; set; }

        /// <summary>
        /// Nombre de informe.
        /// </summary>
        //[PropertyName("strNomInforme")]
        public string NombreInforme { get; set; }

        /// <summary>
        /// Tipo de documento.
        /// </summary>
        //[PropertyName("intTipoDocum")]
        public int TipoDocumento { get; set; }

        /// <summary>
        /// Fecha renovación.
        /// </summary>
        //[PropertyName("dtmFecRenov")]
        public string FechaRenovacion { get; set; }

        /// <summary>
        /// Observaciones del poder.
        /// </summary>
        //[PropertyName("ObsPod")]
        public string ObservacionPoder { get; set; }

        /// <summary>
        /// Fecha vigencia.
        /// </summary>
        //[PropertyName("strFecActVig")]
        public string FechaActVigencia { get; set; }

        /// <summary>
        /// Fecha de termino vigencia.
        /// </summary>
        //[PropertyName("strFecTerVig")]
        public string FechaTerminaVigencia { get; set; }
    }
}
