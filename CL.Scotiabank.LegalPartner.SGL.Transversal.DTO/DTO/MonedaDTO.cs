﻿using System.ComponentModel.DataAnnotations;
//TODO: Agregar restricciones a los campos.
namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// DTO para la tabla de Moneda
    /// </summary>
    public class MonedaDTO
    {
        /// <summary>
        /// Código del Moneda
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Codigo")]
        [MaxLength(3)]
        public string Codigo { get; set; }

        /// <summary>
        /// Descripción del Moneda
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Descripción")]
        [MaxLength(40)]
        public string Descripcion { get; set; }
    }
}
