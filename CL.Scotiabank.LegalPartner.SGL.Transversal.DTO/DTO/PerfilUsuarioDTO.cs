﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
//TODO: Agregar restricciones a los campos.
namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// DTO para la tabla de PerfilUsuario
    /// </summary>
    public class PerfilUsuarioDTO
    {
        /// <summary>
        /// Código del PerfilUsuario
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Descripción del PerfilUsuario
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Descripción")]
        [MaxLength(80)]
        public string Descripcion { get; set; }

        /// <summary>
        /// Grupo codigo del DTO perfil usuario
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Grupo")]
        public string GrupoCodigo { get; set; }

        /// <summary>
        /// Grupo Descripcion del DTO perfil usuario
        /// </summary>
        [Display(Name = "Grupo")]
        public string GrupoDescripcion { get; set; }
    }
}
