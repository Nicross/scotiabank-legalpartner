﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
//TODO: Agregar restricciones a los campos.
namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// DTO para la tabla de GrupoFacultad
    /// </summary>
    public class GrupoFacultadDTO
    {
        /// <summary>
        /// Codigo del GrupoFacultad (cbr_cod)
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Descripcion del GrupoFacultad (cbr_des)
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Descripción")]
        [MaxLength(80)]
        public string Descripcion { get; set; }
    }
}
