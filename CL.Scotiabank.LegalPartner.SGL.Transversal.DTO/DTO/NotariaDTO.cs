﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
//TODO: Agregar restricciones a los campos.
namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// DTO para la tabla de Notaria
    /// </summary>
    public class NotariaDTO
    {
        /// <summary>
        /// Código del Conservador (cbr_cod)
        /// </summary>
        public string Codigo { get; set; }

        /// <summary>
        /// Descripción del Conservador (cbr_des)
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Descripción")]
        [MaxLength(75)]
        public string Descripcion { get; set; }
    }
}
