﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class SessionLoginDTO
    {
        public int CodigoUsuario {get; set;}

        public string NombreUsuario { get; set; }

        public string RutUsuario { get; set; }

        public string DigitoVerificador { get; set; }

        public IList<MenuDTO> menusDTO { get; set; }
    }
}
