using System.ComponentModel.DataAnnotations;
/// <summary>
//TODO:Validar restricciones de los campos.
/// </summary>
namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// DTO utilizado para la tabla de Conservador.
    /// </summary>
    public class ConservadorDTO
    {
        /// <summary>
        /// Codigo del Conservador (cbr_cod)
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Descripcion del Conservador (cbr_des)
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Descripción")]
        [MaxLength(510)]
        public string Descripcion { get; set; }
    }
}
