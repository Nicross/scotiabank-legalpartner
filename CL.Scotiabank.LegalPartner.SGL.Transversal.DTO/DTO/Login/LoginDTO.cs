using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
/// <summary>
//TODO:Validar restricciones de los campos.
/// </summary>
namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// DTO utilizado para la tabla de Conservador.
    /// </summary>
    public class LoginDTO
    {
        /// <summary>
        /// Codigo del Usuario (per_cod)
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Descripcion del Usuario (per_nrt)
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Rut")]
        [MaxLength(80)]
        public string Rut { get; set; }

        /// <summary>
        /// per_drt
        /// </summary>
        public string DigitoVerificador { get; set; }

        /// <summary>
        /// per_nom"
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        /// <summary>
        /// per_apl_ptn
        /// </summary>
        [Display(Name = "Apellido Paterno")]
        public string ApellidoPaterno { get; set; }

        /// <summary>
        /// per_apl_mtn
        /// </summary>
        [Display(Name = "Apellido Materno")]
        public string ApellidoMaterno { get; set; }

        /// <summary>
        /// per_tel
        /// </summary>
        [Display(Name = "Telefono")]
        public string Telefono { get; set; }

        /// <summary>
        /// per_email
        /// </summary>
        [Display(Name = "Email")]
        public string Email { get; set; }

        /// <summary>
        /// suc_id
        /// </summary>
        [Display(Name = "Id Sucursal")]
        public string SucursalId { get; set; }

        public string PerfilId { get; set; }

        public string PerfilDescripcion { get; set; }

        public int GrupoPerfilId { get; set; }

        public string GrupoPerfilDescripcion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Contraseña")]
        [MaxLength(16, ErrorMessage = "El campo {0} no puede superar los 16 caracteres")]
        [MinLength(8, ErrorMessage = "El campo {0} no puede ser inferior a los 8 caracteres")]
        public string Password { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Confirmar Contraseña")]
        [MaxLength(16, ErrorMessage = "El campo {0} no puede superar los 16 caracteres")]
        [MinLength(8, ErrorMessage = "El campo {0} no puede ser inferior a los 8 caracteres")]
        public string ConfirmacionPassword { get; set; }


        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Contraseña Anterior")]
        public string PasswordAnterior { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CadenaPasswordsAnteriores { get; set; }

        /// <summary>
        /// dias de retorno tras la utilización del objeto.
        /// </summary>
        public int DiasRetorno { get; set; }

        public IEnumerable<string> ListaPasswordAntiguas { get; set; }
    }
}
