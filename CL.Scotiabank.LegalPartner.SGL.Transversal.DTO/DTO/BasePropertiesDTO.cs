﻿namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class BasePropertiesDTO
    {
        /// <summary>
        /// Codigo de Retorno devuelto de la ejecucion del SP ejecutado.
        /// </summary>
        public int CodigoRetorno { get; set; }

        /// <summary>
        /// Cuerpo a retornar en caso de ser necesario
        /// </summary>
        public string CuerpoRetorno { get; set; }

        /// <summary>
        /// Filas afectadas durante la ejecuccion del procedimiento almacenado.
        /// </summary>
        public int FilasAfectadas { get; set; }

        /// <summary>
        /// Resultado obtenidos a ser devueltos
        /// </summary>
        public dynamic Resultado { get; set; }
    }
}
