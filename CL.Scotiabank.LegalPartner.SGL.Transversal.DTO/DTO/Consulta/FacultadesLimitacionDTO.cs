﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class FacultadesLimitacionDTO
    {
        public int IdPoder { get; set; }
        public string DescripcionFacultad { get; set; }
        public string DescripcionLimitacion { get; set; }
        public string Tipo { get; set; }


    }
}
