﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class DocumentoLegalizacionDTO
    {
        [Display(Name = "Tipo Documento")]
        public string TipoDocumento { get; set; }

        [Display(Name = "Fecha Escritura")]
        public string FechaEscritura { get; set; }

        [Display(Name = "Notaría")]
        public string Notaria { get; set; }

        [Display(Name = "NºRep.")]
        public string NumeroRepertorio { get; set; }

        [Display(Name = "Fecha Decreto")]
        public string FechaDecreto { get; set; }

        [Display(Name = "N° Decreto")]
        public string NumeroDecreto { get; set; }

        [Display(Name = "Fecha Public. Decreto")]
        public string FechaPublicacionDecreto { get; set; }

    }
}