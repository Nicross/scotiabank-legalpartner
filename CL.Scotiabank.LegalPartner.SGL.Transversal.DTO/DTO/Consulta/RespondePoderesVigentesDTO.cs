﻿namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class RespondePoderesVigentesDTO
    {
        public string Nombre { get; set; }
        public string Rut { get; set; }
        public string FechaConsulta { get; set; }
       public string[] MandatariosRut { get; set; }
        public string[] MandatariosNombre { get; set; }
        public string Facultad { get; set; }
    }
}
