﻿using System;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class SucursalDTO
    {
        /// <summary>
        /// Columna BD: "suc_id"
        /// Codigo de la sucursalBanco.
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Columna BD: "suc_des"
        /// Descripcion de la  sucursalBanco.
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Columna BD: "of_partes_des"
        /// Descripcion de la  sucursalBanco.
        /// </summary>
        public string OficinaPartesDescripcion { get; set; }

        /// <summary>
        /// Columna BD: "of_partes_id"
        /// Descripcion de la  sucursalBanco.
        /// </summary>
        public int OficinaPartesCodigo { get; set; }

        /// <summary>
        /// Columna BD: "of_partes_des"
        /// Descripcion de la  sucursalBanco.
        /// </summary>
        public string SucursalCodigoCuenta { get; set; }
    }
}
