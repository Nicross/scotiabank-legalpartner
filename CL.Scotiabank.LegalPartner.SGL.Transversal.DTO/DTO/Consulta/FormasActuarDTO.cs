﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class FormasActuarDTO
    {
        public string Glosa { get; set; }
        public List<Limitaciones> LstLimitaciones { get; set; }
    }

    public class Limitaciones
    {
        public string Limitacion { get; set; }
        public string LimitCta { get; set; }
        public string DescripcionMoneda { get; set; }
        public string NumeroOperacion { get; set; }
        public string LimitMonto { get; set; }
        public string Monto { get; set; }
        public string LimitText { get; set; }
    }
}
