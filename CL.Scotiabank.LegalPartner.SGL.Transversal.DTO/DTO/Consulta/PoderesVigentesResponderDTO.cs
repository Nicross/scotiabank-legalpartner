﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class PoderesVigentesResponderDTO
    {
        public int CodigoDocumento { get; set; }
        public string FechaDocumentoString { get; set; }
        public string FechaRenovacionDocumentoString { get; set; }
        public DateTime FechaDocumento { get; set; }
        public DateTime FechaRenovacionDocumento { get; set; }
        public string Notaria { get; set; }
    }
}
