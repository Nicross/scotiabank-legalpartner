﻿using System.ComponentModel.DataAnnotations;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class DatosPersonaDTO
    {
        /// <summary>
        /// Rut
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Rut")]
        [MaxLength(12)]
        public string Rut { get; set; }

        public string Dv { get; set; }

        /// <summary>
        /// Cuenta corriente
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Cuenta Corriente")]
        [MaxLength(12)]
        public string CuentaCorriente { get; set; }

        /// <summary>
        /// Codigo Persona
        /// </summary>
        public int CodigoPersona { get; set; }

        /// <summary>
        /// Nombre Persona
        /// </summary>
        [Display(Name = "Nombre o Razón Social")]
        public string NombrePersona { get; set; }

        /// <summary>
        /// Apellido Materno Persona
        /// </summary>
        public string ApellidoMaternoPersona { get; set; }

        /// <summary>
        /// Apellido Paterno Persona
        /// </summary>
        public string ApellidoPaternoPersona { get; set; }

        /// <summary>
        /// codigo Tipo Persona
        /// </summary>
        public string CodigoTipoPersona { get; set; }

        /// <summary>
        /// Codigo SGL
        /// </summary>
        public int SglId { get; set; }

        /// <summary>
        /// Tipo Persona
        /// </summary>
        [Display(Name = "Tipo Persona Jurídica")]
        public int TipoPersona { get; set; }
    }
}