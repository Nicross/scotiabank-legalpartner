using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
/// <summary>
//TODO:Validar restricciones de los campos.
/// </summary>
namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// DTO utilizado para la tabla de Conservador.
    /// </summary>
    public class ConsultaFirmaCajaDTO
    {
        public DatosPersonaDTO DatosPersona { get; set; }

        public IEnumerable<DatosSGLDTO> ListadoDatosSGL { get; set; }

        #region Pestaña modificacion y legalizacion

        public IEnumerable<DocumentoLegalizacionDTO> ListadoDocumentosLegalizado { get; set; }

        public string ObservacionesDocumentos { get; set; }

        #endregion

        #region Pestaña Datos Estatutos

        public string NombreAnteriorHistorico { get; set; }
        public string NombreDeFantasia { get; set; }
        public string Objeto { get; set; }
        public string Domicilio { get; set; }
        public string Duracion { get; set; }
        public string Administracion { get; set; }
        public string FuenteComunidad { get; set; }
        public string Conclusion { get; set; }
        public string FechaProximoEstatutoVigente { get; set; }

        #endregion

        #region Pestaña Consulta Poder 

        public string FechaConsulta { get; set; }
        public IEnumerable<MandatarioVigenteDTO> ListadoMandatariosVigentes { get; set; }
        public IEnumerable<FacultadesPoderDTO> ListadoFacultadesPoder { get; set; }

        #endregion

        #region datos base
        /// <summary>
        /// Cantidad registros
        /// </summary>
        public int CantidadRegistros { get; set; }

        /// <summary>
        /// Codigo de retorno tras la utilización del objeto.
        /// </summary>
        public int CodigoRetorno { get; set; }

        /// <summary>
        /// Filas afectadas tras la utilización del objeto.
        /// </summary>
        public int FilasAfectadas { get; set; }
        #endregion
    }
}
