﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
  public class DocumentoCapitalDTO
    {
        public string Id { get; set; }

        public decimal MontoCapital { get; set; }

        public string CodigoMoneda { get; set; }

        public string DescripcionMoneda { get; set; }

        public string Descripcion { get; set; }

        public List<Participante> ListaParticipantes { get; set; }

    
    }
}
