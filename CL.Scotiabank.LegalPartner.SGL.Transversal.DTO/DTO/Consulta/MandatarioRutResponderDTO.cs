﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
  public  class MandatarioRutResponderDTO
    {
        public int CodigoSGL { get; set; }
        public string RutSoc { get; set; }
        public string NombreSociedad { get; set; }
        public int CodigoPersona { get; set; }
        public int CantidadManda { get; set; }
        public string NombresMandatarios { get; set; }
        public string RutsMandatarios { get; set; }
        public string Dvs { get; set; }
        public string Fecha { get; set; }
        public int CantidadFacul { get; set; }
        public string NombreFacultad { get; set; }
        public int CodigoGrupoFacultad { get; set; }
        public int CodigoFacultad { get; set; }
        public string CodigosMandatarios { get; set; }

        //Datos de carga 
        public string[] ArrDvs { get; set; }
        public string[] ArrNombres { get; set; }
        public string[] ArrRuts { get; set; }
        public string Nombre { get; set; }
        public string Rut { get; set; }
        public string Facultad { get; set; }
    }
}
