﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
   public class Participante
    {
       public string Rut { get; set; }

        public string Dv { get; set; }

        public string Nombre { get; set; }

       public string MontoAporte { get; set; }

        public string PorcentajeAporte { get; set; } 
    }
}
