﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class EstadoLegalizacionDTO
    {
        public int Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}
