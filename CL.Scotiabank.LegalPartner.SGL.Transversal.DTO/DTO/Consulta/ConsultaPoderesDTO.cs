﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class ConsultaPoderesDTO
    {
        /// <summary>
        /// Codigo Persona.
        /// </summary>
        public int CodigoPersona { get; set; }

        /// <summary>
        /// TipoPoder.
        /// </summary>
        public int TipoPoder { get; set; }

        /// <summary>
        /// Tipo Respuesta.
        /// </summary>
        public string TipoRespuesta { get; set; }

        /// <summary>
        /// Estado Consulta.
        /// </summary>
        public int EstadoConsulta { get; set; }

        /// <summary>
        /// Fecha Busqueda.
        /// </summary>
        public string FechaBusqueda { get; set; }

        /// <summary>
        /// Total
        /// </summary>
        public int Total { get; set; }

        /// <summary>
        /// Id SGL
        /// </summary>
        public int CodigoParticipante { get; set; }

        /// <summary>
        /// Tipo Participante
        /// </summary>
        public int TipoParticipante { get; set; }

        /// <summary>
        /// Fecha Consulta
        /// </summary>
        public string FechaConsulta { get; set; }

      
    }
}
