﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// Clase DTO para Documento de Poder (Legalización).
    /// </summary>
    public class DocumentoLegalDTO
    {
        /// <summary>
        /// CodigoDocumento del DTO de Documento.
        /// </summary>
        [Display(Name = "Codigo Documento")]
        public int CodigoDocumento { get; set; }

        /// <summary>
        /// CodigoPersona del DTO de Documento.
        /// </summary>
        [Display(Name = "Codigo Persona")]
        public int CodigoPersona { get; set; }

        /// <summary>
        /// CodigoTipoLegalizacion del DTO de Documento.
        /// </summary>
        [Display(Name = "Codigo Tipo Legalizacion")]
        public int CodigoTipoLegalizacion { get; set; }

        /// <summary>
        /// TipoLegalizacion del DTO de Documento.
        /// </summary>
        [Display(Name = "Tipo Legalizacion")]
        public string TipoLegalizacion { get; set; }

        /// <summary>
        /// FechaDocumento del DTO de Documento.
        /// </summary>
        [Display(Name = "Fecha Documento")]
        public string FechaDocumento { get; set; }

        /// <summary>
        /// NumeroDocumento del DTO de Documento.
        /// </summary>
        [Display(Name = "Numero Documento")]
        public string NumeroDocumento { get; set; }

        /// <summary>
        /// DescripcionNotaria del DTO de Documento.
        /// </summary>
        [Display(Name = "Descripcion Notaria")]
        public string DescripcionNotaria { get; set; }

        /// <summary>
        /// FechaDocumentoINP del DTO de Documento.
        /// </summary>
        [Display(Name = "Fecha DocumentoINP")]/*averiguar doc_fec_inp */
        public string FechaDocumentoINP { get; set; }

        /// <summary>
        /// AnyoDocumentoINP del DTO de Documento.
        /// </summary>
        [Display(Name = "Año Documento")]
        public string AnyoDocumentoINP { get; set; }

        /// <summary>
        /// NumeroFoja del DTO de Documento.
        /// </summary>
        [Display(Name = "Número Foja")]
        public string NumeroFoja { get; set; }

        /// <summary>
        /// Foja del DTO de Documento.
        /// </summary>
        [Display(Name = "Foja")]
        public string Foja { get; set; }

        /// <summary>
        /// DescripcionConservador del DTO de Documento.
        /// </summary>
        [Display(Name = "Conservador")]
        public string DescripcionConservador { get; set; }

        /// <summary>
        /// FechaPBL del DTO de Documento.
        /// </summary>
        [Display(Name = "Fecha")]/*averiguar doc_fec_pbl*/
        public string FechaPBL { get; set; }

        /// <summary>
        /// FechaSession del DTO de Documento.
        /// </summary>
        [Display(Name = "Fecha Session")]
        public string FechaSession { get; set; }

        /// <summary>
        /// FechaJuntaAccion del DTO de Documento.
        /// </summary>
        [Display(Name = "Fecha")]/*averiguar doc_fec_jun_exa_aci*/
        public string FechaJuntaAccion { get; set; }

        /// <summary>
        /// Fecha del DTO de Documento.
        /// </summary>
        [Display(Name = "Fecha")]
        public DateTime Fecha { get; set; }

        /// <summary>
        /// NumeroDocumento del DTO de Documento.
        /// </summary>
        [Display(Name = "Número Documento")]
        public string NumeroDCO { get; set; }

        /// <summary>
        /// FechaPublicacion del DTO de Documento.
        /// </summary>
        [Display(Name = "Fecha Publicacion")]
        public string FechaPublicacion { get; set; }

        /// <summary>
        /// FechaPublicacionDCO del DTO de Documento.
        /// </summary>
        [Display(Name = "Publicación Documento")]
        public string FechaPublicacionDCO { get; set; }

        /// <summary>
        /// FechaProtocolo del DTO de Documento.
        /// </summary>
        [Display(Name = "Fecha Protocolo")]
        public string FechaProtocolo { get; set; }

        /// <summary>
        /// NotariaProtocolo del DTO de Documento.
        /// </summary>
        [Display(Name = "Notaria")]
        public string NotariaProtocolo { get; set; }

        /// <summary>
        /// CodigoOrigen del DTO de Documento.
        /// </summary>
        [Display(Name = "Codigo Origen")]
        public int CodigoOrigen { get; set; }

        /// <summary>
        /// OrigenDocumento del DTO de Documento.
        /// </summary>
        [Display(Name = "Origen Documento")]
        public string OrigenDocumento { get; set; }

        /// <summary>
        /// OrigenNombre del DTO de Documento.
        /// </summary>
        [Display(Name = "Origen")]
        public string OrigenNombre { get; set; }

        /// <summary>
        /// LugarOrigen del DTO de Documento.
        /// </summary>
        [Display(Name = "Lugar")]
        public string LugarOrigen { get; set; }

        /// <summary>
        /// ObservacionesDocumento del DTO de Documento.
        /// </summary>
        [Display(Name = "Observaciones")]
        public string ObservacionesDocumento { get; set; }

        /// <summary>
        /// EstadoDocumento del DTO de Documento.
        /// </summary>
        [Display(Name = "Estado")]
        public int EstadoDocumento { get; set; }

        /// <summary>
        /// FechaInicial del DTO de Documento.
        /// </summary>
        [Display(Name = "Fecha Inicial")]
        public DateTime FechaInicial { get; set; }

        /// <summary>
        /// FechaFinal del DTO de Documento.
        /// </summary>
        [Display(Name = "Fecha Final")]
        public DateTime FechaFinal { get; set; }

        /// <summary>
        /// CodigoRevocacion del DTO de Documento.
        /// </summary>
        [Display(Name = "Codigo Revocacion")]
        public int CodigoRevocacion { get; set; }

        /// <summary>
        /// FechaQuiebra del DTO de Documento.
        /// </summary>
        [Display(Name = "Fecha Quiebra")]
        public DateTime FechaQuiebra { get; set; }

        /// <summary>
        /// Juzgado del DTO de Documento.
        /// </summary>
        [Display(Name = "Juzgado")]
        public string Juzgado { get; set; }

        /// <summary>
        /// FechaQuiebraPBL del DTO de Documento.
        /// </summary>
        [Display(Name = "Codigo Persona")]/*Averiguar doc_fec_pbl_qbr*/
        public DateTime FechaQuiebraPBL { get; set; }

        /// <summary>
        /// Identificador del DTO de Documento.
        /// </summary>
        [Display(Name = "Usuario")]
        public string UsuarioLogin { get; set; }
        /// <summary>
        /// CodigoOficina del DTO de Documento.
        /// </summary>
        [Display(Name = "Codigo Oficina")]
        public string CodigoOficina { get; set; }

        /// <summary>
        /// NombreDocumnetoINF del DTO de Documento.
        /// </summary>
       // [PropertyName("doc_nom_inf")]Averiguar
        [Display(Name = "Documento Nombre")]
        public string NombreDocumnetoINF { get; set; }

        /// <summary>
        /// CodigoConservador del DTO de Documento.
        /// </summary>
        [Display(Name = "Codigo Conservador")]
        public int CodigoConservador { get; set; }

        /// <summary>
        /// FechaRenovacion del DTO de Documento.
        /// </summary>
        [Display(Name = "Fecha Renovacion")]
        public DateTime FechaRenovacion { get; set; }

        /// <summary>
        /// IdSGL del DTO de Documento.
        /// </summary>
        [Display(Name = "Id SGL")]
        public int IdSGL { get; set; }

        /// <summary>
        /// TipoLegalizacionCodigo del DTO de Documento.
        /// </summary>
        [Display(Name = "Codigo Tipo Legalización")]
        public int TipoLegalizacionCodigo { get; set; }

        /// <summary>
        /// CodigoTipoDocumneto del DTO de Documento.
        /// </summary>
        [Display(Name = "Codigo Tipo Documento")]
        public int CodigoTipoDocumento { get; set; }

        /// <summary>
        /// CodigoNotaria del DTO de Documento.
        /// </summary>
        [Display(Name = "Codigo Notaria")]
        public string CodigoNotaria { get; set; }

        /// <summary>
        /// CodigoRectificacion del DTO de Documento.
        /// </summary>
        [Display(Name = "Codigo Rectificacion")]
        public int CodigoRectificacion { get; set; }

        /// <summary>
        /// CodigoProtocoloNotaria del DTO de Documento.
        /// </summary>
        [Display(Name = "Codigo Protocolo")]
        public string CodigoProtocoloNotaria { get; set; }

        /// <summary>
        /// CodigoOrigenDocumento del DTO de Documento.
        /// </summary>
        [Display(Name = "Codigo Origen")]
        public int CodigoOrigenDocumento { get; set; }

        /// <summary>
        /// CodigoTarjeton del DTO de Documento.
        /// </summary>
        [Display(Name = "Codigo Tarjeton")]
        public int CodigoTarjeton { get; set; }

        /// <summary>
        /// Tarjeton del DTO de Documento.
        /// </summary>
        [Display(Name = "Tarjeton")]
        public string Tarjeton { get; set; }
    }
}
