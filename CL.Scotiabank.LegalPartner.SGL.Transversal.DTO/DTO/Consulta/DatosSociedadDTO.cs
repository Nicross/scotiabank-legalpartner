﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// Dto utilizado para recibir datod de busqueda por nombre de sociedad.
    /// </summary>
    public class DatosSociedadDTO
    {
        /// <summary>
        /// Rut sociedad 
        /// </summary>
        public string Rut { get; set; }

        /// <summary>
        /// Nombre de Sociedad
        /// </summary>
        public string NombreSociedad { get; set; }

        public string mensajeError { set; get; }
    }
}
