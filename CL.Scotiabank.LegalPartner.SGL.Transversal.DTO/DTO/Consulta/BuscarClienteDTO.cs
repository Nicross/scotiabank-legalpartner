﻿using System.ComponentModel.DataAnnotations;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// DTO utilizado para la busqueda de clientes.
    /// </summary>
    public class BuscarClienteDTO
    {
        /// <summary>
        /// Codigo de persona.
        /// </summary>
        public int CodigoPersona { get; set; }

        /// <summary>
        /// Rut del cliente.
        /// </summary>
        public string Rut { get; set; }

        /// <summary>
        /// Digito Vereficador
        /// </summary>
        public string DigitoVereficador { get; set; }

        /// <summary>
        /// Nombre de Cliente.
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Nombre")]
        [MaxLength(200,ErrorMessage = "Por favor no ingrese más de {0} caracteres.")]
        [MinLength(4,ErrorMessage = "Por favor ingrese al menos {0} caracteres.")]
        public string Nombre { get; set; }
    }
}
