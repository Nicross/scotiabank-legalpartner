﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class GlosaResponderDTO
    {
        public int IdPoder { get; set; }
        public string Glosa { get; set; }
        public int IdFactuar { get; set; }
    }
}
