﻿using System.ComponentModel.DataAnnotations;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class MandatarioVigenteDTO
    {
        [Display(Name = "Rut")]
        public string Rut { get; set; }

        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        [Display(Name = "Apellido Paterno")]
        public string ApellidoPaterno { get; set; }

        [Display(Name = "Apellido Materno")]
        public string ApellidoMaterno { get; set; }

        [Display(Name = "Codigo Persona")]
        public string CodigoPersona { get; set; }
    }
}