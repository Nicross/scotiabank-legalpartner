﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
   public class PatrimonioDTO
    {
        public string Id { get; set; }

        public string CodigoTipo { get; set; }

        public decimal Monto { get; set; }

        public string Moneda { get; set; }

        public decimal CantidadTotalAcciones { get; set; }

        public decimal ValorNominalAccion { get; set; }

        public string Descripcion { get; set; }

        public List<Participante> ListaParticipante { get; set; }
    }
}
