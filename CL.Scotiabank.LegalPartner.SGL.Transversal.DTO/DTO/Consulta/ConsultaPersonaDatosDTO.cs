﻿namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{

    /// <summary>
    /// DTO de Response para la Entidad de Consulta.
    /// Aqui se declaran los parametros de Salida del StoreProcedure (Output).
    /// y se mapean las propiedades contra los nombres de las columnas de la BD.
    /// </summary>
    public class ConsultaPersonaDatosDTO
    {
        /// <summary>
        /// Id SGL
        /// </summary>
        public int IdSgl { get; set; }

        /// <summary>
        /// Rut.
        /// </summary>            
        public int Rut { get; set; }

        /// <summary>
        /// Rut.
        /// </summary>            
        public string RutString { get; set; }

        /// <summary>
        /// Digito Verificador.
        /// </summary>
        public string Dv { get; set; }

        /// <summary>
        /// Codigo Persona. 
        /// </summary>
        public int CodigoPersona { get; set; }

        /// <summary>
        /// Nombre Persona.
        /// </summary>
        public string NombrePersona { get; set; }

        /// <summary>
        /// Apellido Paterno Persona.
        /// </summary>
        public string ApellidoPaternoPersona { get; set; }

        /// <summary>
        /// Apellido Materno Persona.
        /// </summary>
        public string ApellidoMaternoPersona { get; set; }

        /// <summary>
        /// Tipo codigo.
        /// </summary>
        public string CodigoTipoPersona { get; set; }

        //#region Datos para Documento Poder

        ///// <summary>
        ///// Usuario.
        ///// </summary>
        //public string Usuario { get; set; }

        ///// <summary>
        ///// TipoPoder.
        ///// </summary>
        //public string TipoPoder { get; set; }

        //#endregion

    }

}
