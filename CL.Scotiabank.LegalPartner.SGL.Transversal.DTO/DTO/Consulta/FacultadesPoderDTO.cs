﻿using System.ComponentModel.DataAnnotations;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class FacultadesPoderDTO
    {
        /// <summary>
        /// Codigo facultad
        /// </summary>
        [Display(Name ="Codigo Facultad")]
        public int CodigoFacultad { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Display(Name = "Nombre Facultad")]
        public string NombreFacultad { get; set; }

        /// <summary>
        /// Descripcion grupo
        /// </summary>
        [Display(Name = "Descripcion Grupo")]
        public string DescripcionGrupo { get; set; }

        /// <summary>
        /// Codigo grupo
        /// </summary>
        [Display(Name = "Codigo Grupo")]
        public int CodigoGrupo { get; set; }

        /// <summary>
        /// Tipo relacion
        /// </summary>
        [Display(Name = "Tipo Relacion")]
        public string TipoRelacion { get; set; }

    }
}