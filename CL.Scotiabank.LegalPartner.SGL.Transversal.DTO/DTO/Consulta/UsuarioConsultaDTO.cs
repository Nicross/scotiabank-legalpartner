﻿using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class UsuarioConsultaDTO
    {
        public int Id { get; set; }

        public int IdSgl { get; set; }

        public string Rut { get; set; }

        public string Dv { get; set; }

        public int CodigoPersona { get; set; }

        public int CodigoLegal { get; set; }

        public string NombrePersona { get; set; }

        public string ApellidoPaternoPersona { get; set; }

        public string ApellidoMaternoPersona { get; set; }

        public string CodigoTipoPersona { get; set; }

        public int EstadoLegal { get; set; }

        public string EstadoLegalDescripcion { get; set; }

        public string TipoPersonaJuridica { get; set; }

        public string TipoPersonaDescripcion { get; set; }

        public string FechaConsulta { get; set; }

        #region lista documentos
        public List<ListadoDocumentosDTO> ListaDocumentos { get; set; }
        #endregion

        #region Estatutos

        public string NombreLegalAnterior { get; set; }

        public string NombreFantasia { get; set; }

        public string NombreFantasiaAnt { get; set; }

        public string Objeto { get; set; }

        public string Domicilio { get; set; }

        public string ComunidadFuente { get; set; }

        public string Duracion { get; set; }

        public string Administracion { get; set; }

        public string Conclusion { get; set; }

        public string FechaActualizacion { get; set; }

        public string FechaUltimoEstudio { get; set; }

        public string IndicadorInforme { get; set; }

        public string EstadoConsulta { get; set; }
        #endregion
    }
}
