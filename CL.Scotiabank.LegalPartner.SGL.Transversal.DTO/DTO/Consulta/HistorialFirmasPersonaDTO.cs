﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO.Consulta
{
    /// <summary>
    /// DTO de Response para la Entidad de Consulta.
    /// Aqui se declaran los parametros de Salida del StoreProcedure (Output).
    /// y se mapean las propiedades contra los nombres de las columnas de la BD.
    /// </summary>
    public class HistorialFirmasPersonaDTOResponse 
    {
        public HistorialFirmasPersonaDTOResponse()
        {
        }
        

        /// <summary>
        /// Codigo Firma
        /// </summary>        
        public string CodigoFirma { get; set; }

        /// <summary>
        /// Control Firma
        /// </summary>        
        public string ControlFirma { get; set; }

        /// <summary>
        /// Fecha Firma
        /// </summary>
        public DateTime FechaFirma { get; set; }

    }
}