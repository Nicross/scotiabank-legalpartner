﻿using System;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class SGLPorPersonaDTO
    {
        /// <summary>
        /// Columna BD: "sgl_id"
        /// Codigo del Usuario del Consulta.
        /// </summary>
        public int SGLId { get; set; }

        /// <summary>
        /// Columna BD: "sgl_fec_ini"
        /// Fecha Inicio SGL.
        /// </summary>
        public DateTime FechaInicio { get; set; }

        /// <summary>
        /// Columna BD: "mat_id"
        /// Materia Id.
        /// </summary>
        public int MateriaId { get; set; }

        /// <summary>
        /// Columna BD: "per_cod_abg"
        /// Nombre del Usuario del Consulta.
        /// </summary>
        public int CodigoPersonaAbogadoSGL { get; set; }

        /// <summary>
        /// Descripcion Materia SGL.
        /// </summary>
        public string DescripcionMateria { get; set; }

        /// <summary>
        /// Estado SGL.
        /// </summary>
        public int EstadoSGL { get; set; }

        /// <summary>       
        /// Descripcion estado SGL.
        /// </summary>
        public string DescripcionEstadoSGL { get; set; }

        /// <summary>
        /// Sucursal Id.
        /// </summary>
        public int SucursalId { get; set; }

        /// <summary>
        /// Observaciones de la SGL.
        /// </summary>
        public string ObservacionSGL { get; set; }

        /// <summary>
        /// OficinaPartesId.
        /// </summary>
        public int OficinaPartesId { get; set; }

        /// <summary>
        /// Oficina Partes Descripcion.
        /// </summary>
        public string OficinaPartesDescripcion { get; set; }

        /// <summary>
        /// Rut.
        /// </summary>
        public int Rut { get; set; }

        /// <summary>
        /// Digito Verificador.
        /// </summary>
        public string Dv { get; set; }

        /// <summary>
        /// Codigo Persona. 
        /// </summary>
        public int CodigoPersona { get; set; }

        /// <summary>
        /// Nombre Persona.
        /// </summary>
        public string NombrePersona { get; set; }

        /// <summary>
        /// Apellido Paterno Persona.
        /// </summary>
        public string ApellidoPaternoPersona { get; set; }

        /// <summary>
        /// Apellido Materno Persona.
        /// </summary>
        public string ApellidoMaternoPersona { get; set; }

        /// <summary>
        /// Tipo codigo.
        /// </summary>
        public string tipoCodigo { get; set; }


        /// <summary>
        /// Rut solicitante.
        /// </summary>
        public string RutPersonaSolicitante { get; set; }

        /// <summary>
        /// Dv Solicitante.
        /// </summary>
        public string DvPersonaSolicitante { get; set; }

        /// <summary>
        /// Codigo Persona Solicitante.
        /// </summary>
        public int CodigoPersonaSolicitante { get; set; }

        /// <summary>
        /// Nombre Persona Solicitante.
        /// </summary>
        public string NombrePersonaSolicitante { get; set; }

        /// <summary>
        /// Apellido Paterno Persona Solicitante.
        /// </summary>
        public string ApellidoPaternoPersonaSolicitante { get; set; }

        /// <summary>
        /// Apellido Materno Persona Solicitante.
        /// </summary>
        public string ApellidoMaternoPersonaSolicitante { get; set; }

        /// <summary>
        /// Rut Persona Abogado.
        /// </summary>
        public string RutPersonaAbogado { get; set; }

        /// <summary>
        /// Dv Abogado.
        /// </summary>
        public string DvAbogado { get; set; }

        /// <summary>
        /// Codigo Persona Abogado.
        /// </summary>
        public int CodigoPersonaAbogado { get; set; }

        /// <summary>
        /// Nombre Persona Abogado.
        /// </summary>
        public string NombrePersonaAbogado { get; set; }

        /// <summary>
        /// Apellido Persona Abogado.
        /// </summary>
        public string ApellidoPaternoPersonaAbogado { get; set; }

        /// <summary>
        /// .
        /// </summary>
        public string ApellidoMaternoPersonaAbogado { get; set; }

        /// <summary>
        /// .
        /// </summary>
        public int CodigoPersonaSolicitanteSGL { get; set; }

        /// <summary>
        /// Nombre Sucursal
        /// </summary>
        public string Sucursal { get; set; }
    }
}
