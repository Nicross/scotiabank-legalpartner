﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class MandatorioDTO
    {
        /// <summary>
        /// Codigo cliente.
        /// </summary>
        public int CodigoCliente { get; set; }

        /// <summary>
        /// CodPcp.
        /// </summary>
        public int CodPcp { get; set; }

        /// <summary>
        /// Rut.
        /// </summary>
        public int Rut { get; set; }

        /// <summary>
        /// Digito vereficador.
        /// </summary>
        public string DigitoVereficador { get; set; }

        /// <summary>
        /// Nombre.
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Codigo de tipo.
        /// </summary>
        public int CodigoTipo { get; set; }

        /// <summary>
        /// Descripcion de tipo.
        /// </summary>
        public string DescripcionTipo { get; set; }

        /// <summary>
        /// Apellido paterno.
        /// </summary>
        public string ApellidoPaterno { get; set; }

        /// <summary>
        /// Apellido materno.
        /// </summary>
        public string ApellidoMaterno { get; set; }

        /// <summary>
        /// Nombres.
        /// </summary>
        public string Nombres { get; set; }

        /// <summary>
        /// Codigo documento.
        /// </summary>
        public int CodigoDocumento { get; set; }

        /// <summary>
        /// Fecha inicio.
        /// </summary>
        public DateTime FechaInicio { get; set; }

        /// <summary>
        /// Fecha final.
        /// </summary>
        public string FechaFinal { get; set; }

        /// <summary>
        /// Estado.
        /// </summary>
        public string Estado { get; set; }
    }
}
