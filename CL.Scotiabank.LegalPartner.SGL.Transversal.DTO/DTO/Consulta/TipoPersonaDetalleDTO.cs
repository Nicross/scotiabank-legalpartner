﻿namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class TipoPersonaDetalleDTO
    {
        /// <summary>
        /// Codigo Firma
        /// </summary>
        public string Codigo { get; set; }

        /// <summary>
        /// Codigo Firma
        /// </summary>
        public string CodigoGeneral { get; set; }

        /// <summary>
        /// Codigo Firma
        /// </summary>
        public string CodigoCapital { get; set; }

        /// <summary>
        /// Codigo Firma
        /// </summary>
        public string CodigoLegal { get; set; }

        /// <summary>
        /// Control Firma
        /// </summary>
        public string Descripcion { get; set; }
    }
}
