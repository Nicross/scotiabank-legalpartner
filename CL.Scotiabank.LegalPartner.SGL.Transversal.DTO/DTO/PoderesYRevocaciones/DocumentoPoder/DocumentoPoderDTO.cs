﻿using System.ComponentModel.DataAnnotations;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class DocumentoPoderDTO
    {
        #region Listar Documento Poder

        /// <summary>
        /// Id SGL
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// Id de tipo de materia.
        /// </summary>
        public int TipoMateriaId { get; set; }

        #endregion



        #region Datos Request desde index Documento hacia Ingresar Documento

        /// <summary>
        /// Codigo tipo de documento.
        /// </summary>
        public int TipoPoder { get; set; }

        /// <summary>
        /// Codigo tipo de documento.
        /// </summary>
        public int CodigoDocumentoRequest { get; set; }

        /// <summary>
        /// Codigo tipo de documento.
        /// </summary>
        public int EstadoCsu { get; set; }

        /// <summary>
        /// Codigo tipo persona.
        /// </summary>
        public int CodigoTipoPersona { get; set; }


        #endregion



        #region ObtenerListaTipoDocumentoDTOResponse

        /// <summary>
        /// Codigo tipo de documento.
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Descripcioón tipo de documento.
        /// </summary>
        public string Descripcion { get; set; }

        #endregion

        #region ObtenerRespuestaLegalDTOResponse
        /// <summary>
        /// Descripcion Documento
        /// </summary>
        public int CodigoPersona { get; set; }

        /// <summary>
        /// Descripcion Documento
        /// </summary>
        public int Rut { get; set; }

        /// <summary>
        /// Descripcion Documento
        /// </summary>
        public string Verificador { get; set; }

        /// <summary>
        /// Descripcion Documento
        /// </summary>
        public string Nombre { get; set; }
        #endregion

        #region ConsultaEstadoSGLDTORequest

        /// <summary>
        /// Id SGL
        /// </summary>
        public int IdSGL { get; set; }

        /// <summary>
        /// Estado Id
        /// </summary>
        public int EstadoId { get; set; }

        #endregion

        #region IngresarDatosDocumentosDTORequest
     
        /// <summary>
        /// Codigo de tipo legal.
        /// </summary>
        public int CodigoTipoLegal { get; set; }

        /// <summary>
        /// Fecha de documento.
        /// </summary>
        public string FechaDocumento { get; set; }

        /// <summary>
        /// Numero del documento.
        /// </summary>
        public string NumeroDocumento { get; set; }

        /// <summary>
        /// Codigo de la notaria.
        /// </summary>
        public string CodigoNotaria { get; set; }

        /// <summary>
        /// Fecha inscripción.
        /// </summary>
        public string FechaInscripcion { get; set; }

        /// <summary>
        /// Año de inscripción.
        /// </summary>
        public string AnnoInscripcion { get; set; }

        /// <summary>
        /// Descripción de Fojas.
        /// </summary>
        public string Fojas { get; set; }

        /// <summary>
        /// Numero de foja.
        /// </summary>
        public string NumeroFoja { get; set; }

        /// <summary>
        /// Codigo del conservador.
        /// </summary>
        public int CodigoConservador { get; set; }

        /// <summary>
        /// Fecha publicación.
        /// </summary>
        public string FechaPublicacion { get; set; }

        /// <summary>
        /// .
        /// </summary>
        public string FechaSesionDirectorio { get; set; }

        /// <summary>
        /// .
        /// </summary>
        public string FechaJuntaAccionista { get; set; }

        /// <summary>
        /// Codigo rectificación.
        /// </summary>
        public int CodigoRectificacion { get; set; }

        /// <summary>
        /// Fecha decreto.
        /// </summary>
        public string FechaDecreto { get; set; }

        /// <summary>
        /// Numero decreto.
        /// </summary>
        public string NumeroDecreto { get; set; }

        /// <summary>
        /// Fecha publicación decreto.
        /// </summary>
        public string FechaPublicacionDecreto { get; set; }

        /// <summary>
        /// Fecha Protocolo.
        /// </summary>
        public string FechaProtocolarizacion { get; set; }

        /// <summary>
        /// Codigo notaria protocolo.
        /// </summary>
        public string CodigoNotariaProtocolo { get; set; }

        /// <summary>
        /// Codigo origen documento.
        /// </summary>
        public int CodigoDocumentoOrigen { get; set; }

        /// <summary>
        /// Nombre de origen.
        /// </summary>
        public string NombreOrigen { get; set; }

        /// <summary>
        /// Lugar de origen.
        /// </summary>
        public string LugarOrigen { get; set; }

        /// <summary>
        /// Oberservaciones.
        /// </summary>
        public string Observaciones { get; set; }

        /// <summary>
        /// Estado de consulta.
        /// </summary>
        public string EstadoConsulta { get; set; }

        /// <summary>
        /// Fecha inicio.
        /// </summary>
        public string FechaInicio { get; set; }

        /// <summary>
        /// Fecha final.
        /// </summary>
        public string FechaFinal { get; set; }

        /// <summary>
        /// Codigo de revocación.
        /// </summary>
        public int CodigoRevocacion { get; set; }

        /// <summary>
        /// Fecha de resolución de quiebra.
        /// </summary>
        public string FechaResolucionQuiebra { get; set; }

        /// <summary>
        /// Codigo Tribunal.
        /// </summary>
        public int CodigoTribunal { get; set; }

        /// <summary>
        /// Fecha publicación quiebra.
        /// </summary>
        public string FechaPublicacionQuiebra { get; set; }

        /// <summary>
        /// Responable del documento.
        /// </summary>
        public string ResponsableDocumento { get; set; }

        /// <summary>
        /// Lugar del documento.
        /// </summary>
        public string LugarDocumento { get; set; }

        /// <summary>
        /// Nombre de informe.
        /// </summary>
        public string NombreInforme { get; set; }

        /// <summary>
        /// Tipo de documento.
        /// </summary>
        public int TipoDocumento { get; set; }

        /// <summary>
        /// Fecha renovación.
        /// </summary>
        public string FechaRenovacion { get; set; }

        /// <summary>
        /// Observaciones del poder.
        /// </summary>
        public string ObservacionPoder { get; set; }

        /// <summary>
        /// Fecha vigencia.
        /// </summary>
        public string FechaActVigencia { get; set; }

        /// <summary>
        /// Fecha de termino vigencia.
        /// </summary>
        public string FechaTerminaVigencia { get; set; } 

        #endregion

    }
}
