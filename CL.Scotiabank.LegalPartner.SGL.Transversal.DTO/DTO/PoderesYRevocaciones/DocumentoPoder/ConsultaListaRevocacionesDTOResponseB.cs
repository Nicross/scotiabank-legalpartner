﻿
namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// DTO de response para la entidad de consulta de revocaciones.
    /// </summary>
    public class ConsultaListaRevocacionesDTOResponseB
    {
        public ConsultaListaRevocacionesDTOResponseB()
        {
        }        

        /// <summary>
        /// Código.
        /// </summary>
        public int Codigo { get; set; }

        /// <summary> 
        /// Tipo de revocación.
        /// </summary>
        public int TipoRevocacion { get; set; }

        /// <summary>
        /// Descripción tipo.
        /// </summary>
        public string DesTipo { get; set; }

        /// <summary>
        /// Tipo.
        /// </summary>
        public string Nombre { get; set; }

        /// <summary> 
        /// Codigo.
        /// </summary>
        public int EstCsu { get; set; }

        /// <summary>
        /// Nombre.
        /// </summary>
        public string CodDocu { get; set; }
    }
}
