﻿using System;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// Objeto creado en base al objeto completo de ConsultaDatosPoderDTOResponse +
    /// y el parámetro DescripcionMateria ConsultaSGLPorSGLIdDTOResponse
    /// </summary>
    public class DatosPoderesAndDesMatDTOResponse
    {
        #region ConsultaDatosPoderDTOResponse
        ///CodNotProtoc
        public int CodNotProtoc { get; set; }
        ///CodOrigen
        public int CodigoOrigen { get; set; }
        ///OrigenDoc        
        public string Origen { get; set; }
        ///doc_nom_org
        public string NombreOrigen { get; set; }
        ///doc_lug_org
        public string LugarOrigen { get; set; }
        ///doc_obs
        public string Observaciones { get; set; }
        ///doc_est_csu
        public string doc_est_csu { get; set; }
        ///doc_fec_ini
        public DateTime FechaInicio { get; set; }
        ///doc_fec_fnl
        public string FechaTerminoPoder { get; set; }
        ///rev_cod
        public int rev_cod { get; set; }
        ///doc_fec_rsl_qbr
        public DateTime doc_fec_rsl_qbr { get; set; }
        ///Juzgado
        public string Juzgado { get; set; }
        ///doc_fec_pbl_qbr
        public DateTime doc_fec_pbl_qbr { get; set; }
        ///usr_lgn
        public string UsuarioResponasble { get; set; }
        ///sol_cod_uni_ofi
        public string sol_cod_uni_ofi { get; set; }
        ///doc_nom_inf
        public string doc_nom_inf { get; set; }
        ///rev_est_csu
        public string EstadoPoderes { get; set; }
        ///NotProtoc
        public string NotariaProtocolizacion { get; set; }
        ///doc_fec_pro
        public DateTime FechaProtocolizacion { get; set; }
        ///doc_fec_pbl_dco
        public DateTime FechaPublicacionDecreto { get; set; }
        ///doc_num_dco
        public string NumeroDecreto { get; set; }
        ///sgl_id
        public int SglId { get; set; }
        ///doc_cod
        public int CodigoDocumento { get; set; }
        ///per_cod
        public int CodigoPersona { get; set; }
        ///CodTipLeg
        public int CodogioTipoLegalizacion { get; set; }
        ///TipLeg
        public string TipoLegalizacion { get; set; }
        ///doc_fec_doc

            ////
        public string FechaDocumento { get; set; }
        ///doc_num_doc
        public string NumeroDocumento { get; set; }
        ///CodNotaria
        public int CodigoNotaria { get; set; }
        ///TipoDocumPod
        public int TipoDocumentoPoder { get; set; }
        ///Notaria
        public string Notaria { get; set; }
        ///doc_ano_inp
        public string AnnoInscripcion { get; set; }
        ///doc_fja
        public string Fojas { get; set; }
        ///doc_fja_num
        public string FojaNumero { get; set; }
        ///Conservador
        public string Conservador { get; set; }
        ///doc_fec_pbl
        public DateTime FechaPublicacion { get; set; }
        ///doc_fec_ses_dio
        public string FechaSesionDirectorio { get; set; }
        ///doc_fec_jun_exa_aci
        public DateTime doc_fec_jun_exa_aci { get; set; }
        ///doc_fec_dco
        public DateTime FechaDecreto { get; set; }
        ///doc_fec_inp
        public DateTime FechaInscripcion { get; set; }
        ///TipoDocumPodDes
        public string DescripcionTipoPoder { get; set; }

        #endregion

        #region ConsultaSGLPorSGLIdDTOResponse

        ///mat_des
        public string DescripcionMateria { get; set; }

        #endregion
    }
}
