﻿namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// DTO para AddMandatario.
    /// </summary>
    public class AddMandatarioDTO
    {
        #region Datos Mandatario

        /// <summary>
        /// Tipo de persona.
        /// </summary>
        public int CodigoTipoPersona { get; set; }

        /// <summary>
        /// Rut de mandatario.
        /// </summary>
        public int RutPersona { get; set; }

        /// <summary>
        /// Díugito ve4rificador de rut de mandatario.
        /// </summary>
        public string DvPersona { get; set; }

        /// <summary>
        /// Nombre mandatario.
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Apellido paterno de mandatario.
        /// </summary>
        public string ApellidoPaterno { get; set; }

        /// <summary>
        /// Apellido materno de mandatario.
        /// </summary>
        public string ApellidoMaterno { get; set; }

        #endregion

        #region Observaciones Mandatario

        /// <summary>
        /// Identificador de Persona del DTO de Socio.
        /// </summary>
        public int CodigoPersona { get; set; }

        /// <summary>
        /// Codigo de ParticipanteL  del DTO de Socio.
        /// </summary>
        public int CodigoParticipante { get; set; }

        /// <summary>
        /// Identificador de Tipo Persona  del DTO de Socio.
        /// </summary>
        public int CodigoTipoParticipante { get; set; }

        /// <summary>
        /// Monto de Capital del DTO de Socio.
        /// </summary>
        public decimal Monto { get; set; }

        /// <summary>
        /// Porcentaje de Capital  del DTO de Socio.
        /// </summary>
        public decimal Porcentaje { get; set; }

        /// <summary>
        /// Limitacion  del DTO de Socio.
        /// </summary>
        public string Limitacion { get; set; }

        /// <summary>
        /// Identificador de Tipo Persona  del DTO de Socio.
        /// </summary>
        public int CodigoDocumento { get; set; }

        /// <summary>
        /// Monto de Capital del DTO de Socio.
        /// </summary>
        public string FechaInicio { get; set; }

        /// <summary>
        /// Porcentaje de Capital  del DTO de Socio.
        /// </summary>
        public string FechaFinal { get; set; }

        /// <summary>
        /// Limitacion  del DTO de Socio.
        /// </summary>
        public int CodigoRevocacion { get; set; }

        #endregion
    }
}
