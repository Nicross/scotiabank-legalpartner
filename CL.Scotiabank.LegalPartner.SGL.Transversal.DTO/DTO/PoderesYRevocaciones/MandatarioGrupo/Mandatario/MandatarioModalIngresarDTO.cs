﻿
namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class MandatarioModalIngresarDTO
    {
        /// <summary>
        /// Rut de persona.
        /// </summary>
        public string Rut { get; set; }

        /// <summary>
        /// Nombre de persona.
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Apellido paterno de persona.
        /// </summary>
        public string ApellidoPaterno { get; set; }

        /// <summary>
        /// Apellido materno de persona.
        /// </summary>
        public string ApellidoMaterno { get; set; }

        /// <summary>
        /// Cantidad de registrois de persona
        /// </summary>
        public int CantidadRegistros { get; set; }
    }
}
