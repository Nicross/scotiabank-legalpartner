﻿using System.ComponentModel.DataAnnotations;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// DTO para GetMandatario.
    /// </summary>
    public class GetMandatarioDTO
    {
        /// <summary>
        /// Codigo persona.
        /// </summary>     
      
        [Display(Name = "CodigoPersona")]       
        public int CodigoPersona { get; set; }

        /// <summary>
        /// Codigo Tipo.
        /// </summary>            
        [Display(Name = "CodigoTipo")]
        [MaxLength(1)]
        public string CodigoTipo { get; set; }
    }
}
