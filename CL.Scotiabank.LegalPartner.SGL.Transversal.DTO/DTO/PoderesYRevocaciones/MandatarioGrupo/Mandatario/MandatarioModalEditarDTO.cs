﻿namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class MandatarioModalEditarDTO
    {
        public int CodParticipante { get; set; }
        public int TipoPersona { get; set; }
        public string Nombre { get; set; }
        public string ApellPaterno { get; set; }
        public string ApellMaterno { get; set; }
        public int CodPersona { get; set; }
        public string Rut { get; set; }
        public string Dv { get; set; }
        public string Observaciones { get; set; }
        public int TipoParticipante { get; set; }
        public int Monto { get; set; }
        public decimal Porcentaje { get; set; }
    }
}
