﻿namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class NuevoEstadoSGLDTO
    {
        /// <summary>
        /// Id de SGL.
        /// </summary>        
        public int SglId { get; set; }

        /// <summary>
        /// Estado.
        /// </summary>       
        public int Estado { get; set; }

        /// <summary>
        /// Código de persona.
        /// </summary>       
        public int CodigoPersonaModificador { get; set; }

    }
}
