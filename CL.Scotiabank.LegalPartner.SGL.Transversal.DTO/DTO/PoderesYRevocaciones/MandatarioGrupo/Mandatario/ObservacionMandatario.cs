﻿
namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// Clase DTO de observación de mandatario
    /// </summary>
    public class ObservacionMandatario
    {
        /// <summary>
        /// Rut de mandatario.
        /// </summary>
        public string Rut { get; set; }

        /// <summary>
        /// Díugito ve4rificador de rut de mandatario.
        /// </summary>
        public string DvPersona { get; set; }

        /// <summary>
        /// Observación del Mandatario.
        /// </summary>
        public string Limite { get; set; }

        /// <summary>
        /// Nombre del Mandatario
        /// </summary>       
        public string Nombre { get; set; }
    }
}
