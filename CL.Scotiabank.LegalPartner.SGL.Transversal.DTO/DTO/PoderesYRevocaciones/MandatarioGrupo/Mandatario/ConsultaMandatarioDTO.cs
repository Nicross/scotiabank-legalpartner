﻿using System.ComponentModel.DataAnnotations;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// DTO utilizado para Consutamandatario.
    /// </summary>
    public class ConsultaMandatarioDTO
    {
        /// <summary>
        /// Rut sin DV.
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Rut")]
        public int Rut { get; set; }

        /// <summary>
        /// Nombre.
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Nombre")]
        [MaxLength(255)]
        public string Nombre { get; set; }

        /// <summary>
        /// Apellido Paterno.
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "ApellidoPaterno")]
        [MaxLength(255)]
        public string ApellidoPaterno { get; set; }

        /// <summary>
        /// Apellido Materno.
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "ApellidoMJaterno")]
        [MaxLength(255)]
        public string ApellidoMaterno { get; set; }

        /// <summary>
        /// Cantidad Registros.
        /// </summary>
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "CantidadRegistros")]        
        public int CantidadRegistros { get; set; }
    }
}
