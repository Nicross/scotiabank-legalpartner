﻿namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class IngresarBParticipanteDTORequest
    {
        /// <summary>
        /// Identificador de Persona del DTO de Socio.
        /// </summary>
        //[PropertyName("intCodPer")]
        public int CodigoPersona { get; set; }

        /// <summary>
        /// Codigo de ParticipanteL  del DTO de Socio.
        /// </summary>
        //[PropertyName("intCodPerPcp")]
        public int CodigoParticipante { get; set; }

        /// <summary>
        /// Identificador de Tipo Persona  del DTO de Socio.
        /// </summary>
        //[PropertyName("intTipoPcp")]
        public int CodigoTipoParticipante { get; set; }

        /// <summary>
        /// Monto de Capital del DTO de Socio.
        /// </summary>
        //[PropertyName("decMontoApr")]
        public decimal Monto { get; set; }

        /// <summary>
        /// Porcentaje de Capital  del DTO de Socio.
        /// </summary>
        //[PropertyName("numPorcentajeApr")]
        public decimal Porcentaje { get; set; }

        /// <summary>
        /// Limitacion  del DTO de Socio.
        /// </summary>
        //[PropertyName("strLimitacion")]
        public string Limitacion { get; set; }

        /// <summary>
        /// Identificador de Tipo Persona  del DTO de Socio.
        /// </summary>
        //[PropertyName("intCodDoc")]
        public int CodigoDocumento { get; set; }

        /// <summary>
        /// Monto de Capital del DTO de Socio.
        /// </summary>
        //[PropertyName("strFecInicio")]
        public string FechaInicio { get; set; }

        /// <summary>
        /// Porcentaje de Capital  del DTO de Socio.
        /// </summary>
        //[PropertyName("strFecFinal")]
        public string FechaFinal { get; set; }

        /// <summary>
        /// Limitacion  del DTO de Socio.
        /// </summary>
        //[PropertyName("intCodRevoc")]
        public int CodigoRevocacion { get; set; }
    }
}
