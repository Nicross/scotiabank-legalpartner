﻿using System.ComponentModel.DataAnnotations;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class MandatarioGrupoDTO
    {
        #region Mandatario

        /// <summary>
        /// Id de SGL
        /// </summary>
        public int SglId { get; set; }

        /// <summary>
        /// Código persona
        /// </summary>
        public int CodigoPersonaModificador { get; set; }

        /// <summary>
        /// Observación del Mandatario.
        /// </summary>
        public string Limite { get; set; }

        /// <summary>
        /// Descripción del tipo
        /// </summary>
        public string DescripcionTipo { get; set; }

        /// <summary>
        /// Estado de revocación.
        /// </summary>
        public int EstadoRevocacion { get; set; }

        /// <summary>
        /// Cantidad Registros.
        /// </summary>
        public int CantidadRegistros { get; set; }

        /// <summary>
        /// Monto de Capital del DTO de Socio.
        /// </summary>
        public string FechaIniciadora { get; set; }

        /// <summary>
        /// Porcentaje de Capital  del DTO de Socio.
        /// </summary>
        public string FechaFin { get; set; }

        /// <summary>
        /// Código Tipo.
        /// </summary>     
        public int CodigoTipo { get; set; }

        /// <summary>
        /// Tipo de persona.
        /// </summary>
        public int CodigoTipoPersona { get; set; }

        /// <summary>
        /// Rut de mandatario.
        /// </summary>
        public string Rut { get; set; }

        /// <summary>
        /// Dígito verificador de rut de mandatario.
        /// </summary>
        public string DvPersona { get; set; }

        /// <summary>
        /// Apellido paterno de mandatario.
        /// </summary>
        public string ApellidoPaterno { get; set; }

        /// <summary>
        /// Apellido materno de mandatario.
        /// </summary>
        public string ApellidoMaterno { get; set; }

        /// <summary>
        /// Código persona.
        /// </summary> 
        public int CodigoPersona { get; set; }

        /// <summary>
        /// Código de Participante del DTO de Socio.
        /// </summary>
        public int CodigoParticipante { get; set; }

        /// <summary>
        /// Identificador de Tipo Persona  del DTO de Socio.
        /// </summary>
        public int CodigoTipoParticipante { get; set; }

        /// <summary>
        /// Monto de Capital del DTO de Socio.
        /// </summary>
        public decimal Monto { get; set; }

        /// <summary>
        /// Porcentaje de Capital  del DTO de Socio.
        /// </summary>
        public decimal Porcentaje { get; set; }

        /// <summary>
        /// Limitación  del DTO de Socio.
        /// </summary>
        public string Limitacion { get; set; }

        #endregion

        #region Grupo

        #region ListarGrupo

        /// <summary>
        /// Codigo del Grupo (grp_cod)
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Codigo del Grupo (grp_nom)
        /// </summary>       
        public string Nombre { get; set; }

        /// <summary>
        /// Codigo del Grupo (gfc_tip)
        /// </summary>
        public int Tipo { get; set; }

        #endregion

        #region CreateGrupo

        /// <summary>
        /// Identificador del DTO Codigo Correlativo que retorna el SP.
        /// </summary>
        public int CodigoCorrelativoGrupo { get; set; }

        /// <summary>
        /// Identificador del DTO Estado que retorna el SP.
        /// </summary>
        public int Estado { get; set; }

        /// <summary>
        /// Identificador del DTO Tipo Grupo.
        /// </summary>
        public int TipoGrupo { get; set; }

        /// <summary>
        /// Identificador del DTO nombre grupo.
        /// </summary>
        [Required(ErrorMessage = "Debe Ingresar nombre de Grupo")]
        [RegularExpression(@"^[0-9a-zA-Z]+$", ErrorMessage = "Debe ingresar un Nombre válido sin carácteres especiales")]
        [Display(Name = "Nombre de Grupo")]
        [MaxLength(80, ErrorMessage = "El nombre de Grupo  excede el largo ")]
        public string NombreGrupo { get; set; }

        /// <summary>
        /// Identificador del DTO Codigo Sociedad.
        /// </summary>
        public int CodigoSociedad { get; set; }

        #endregion

        #region UpdateGrupo       

        /// <summary>
        /// Identificador del DTO Codigo Persona.
        /// </summary>
        public int CodigoPer { get; set; }

        /// <summary>
        /// Identificador del DTO Codigo Grupo.
        /// </summary>
        public int CodigoGrupo { get; set; }

        #endregion

        #endregion

        #region DocumentoLegal
        /// <summary>
        /// CodigoDocumento del DTO de Documento.
        /// </summary>
        [Display(Name = "Codigo Documento")]
        public int CodigoDocumento { get; set; }

        #endregion

        #region Asociacion

        /// <summary>
        /// FechaInicial del DTO de Documento.
        /// </summary>
        public string FechaInicio { get; set; }

        /// <summary>
        /// FechaFinal del DTO de Documento.
        /// </summary>
        public string FechaFinal { get; set; }

        /// <summary>
        /// CodigoRevocacion del DTO de Documento.
        /// </summary>
        public int CodigoRevocacion { get; set; }

        /// <summary>
        /// CodigoDocumento del DTO de Documento.
        /// </summary>
        //public int CodigoDocumento { get; set; } 

        /// <summary>
        /// Codigo del DTO de Asociacion.
        /// </summary>
        [Display(Name = "Codigo Cliente")]
        public int CodigoCliente { get; set; }

        #region ListarMandatarioGrupo             

        /// <summary>
        /// Rut persona.
        /// </summary>
        public string RutPersona { get; set; }

        /// <summary>
        /// Digito Vereficador.
        /// </summary>
        public string DigitoVereficador { get; set; }

        /// <summary>
        /// Nombre completo.
        /// </summary>
        public string NombreCompleto { get; set; }

        /// <summary>
        /// Fecha Inicio.
        /// </summary>
        //public DateTime FechaInicio { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int EstCsu { get; set; }

        #endregion

        #endregion
    }
}
