﻿namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// DTO de Grupo de Mandatarios.
    /// </summary>
    public class GetGrupoDTO
    {
        /// <summary>
        /// Codigo del Grupo (grp_cod)
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Codigo del Grupo (grp_nom)
        /// </summary>       
        public string Nombre{ get; set; }

        /// <summary>
        /// Codigo del Grupo (gfc_tip)
        /// </summary>
        public int Tipo { get; set; }
    }
}
