﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class UpdateGrupoDTO
    {
        /// <summary>
        /// Identificador del DTO Codigo Correlativo que retorna el SP.
        /// </summary>
        public int CodigoCorrelativoGrupo { get; set; }

        /// <summary>
        /// Identificador del DTO Estado que retorna el SP.
        /// </summary>
        public int Estado { get; set; }

        /// <summary>
        /// Identificador del DTO Codigo Persona.
        /// </summary>
        public int CodigoPer { get; set; }

        /// <summary>
        /// Identificador del DTO Codigo Grupo.
        /// </summary>
        public int CodigoGrupo { get; set; }

        /// <summary>
        /// Identificador del DTO Tipo grupo.
        /// </summary>
        public int TipoGrupo { get; set; }

        /// <summary>
        /// Identificador del DTO nombre grupo.
        /// </summary>
        [Required(ErrorMessage = "El campo Nombre de Grupo es requerido")]
        [Display(Name = "Grupos")]
        public string NombreGrupo { get; set; }
    }
}
