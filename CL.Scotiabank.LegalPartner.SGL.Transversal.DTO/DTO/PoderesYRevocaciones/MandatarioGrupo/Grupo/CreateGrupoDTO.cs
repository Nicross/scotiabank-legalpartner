﻿using System.ComponentModel.DataAnnotations;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    /// <summary>
    /// 
    /// </summary>
    public class CreateGrupoDTO
    {
        /// <summary>
        /// Identificador del DTO Codigo Correlativo que retorna el SP.
        /// </summary>
        public int CodigoCorrelativoGrupo { get; set; }

        /// <summary>
        /// Identificador del DTO Estado que retorna el SP.
        /// </summary>
        public int Estado { get; set; }

        /// <summary>
        /// Identificador del DTO Tipo Grupo.
        /// </summary>
        public int TipoGrupo { get; set; }

        /// <summary>
        /// Identificador del DTO nombre grupo.
        /// </summary>
        [Required(ErrorMessage = "El campo Nombre de Grupo es requerido")]
        [Display(Name = "Grupos")]
        public string NombreGrupo { get; set; }

        /// <summary>
        /// Identificador del DTO Codigo Sociedad.
        /// </summary>
        public int CodigoSociedad { get; set; }
    }
}
