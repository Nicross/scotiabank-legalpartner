﻿using System.ComponentModel.DataAnnotations;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class AsociacionDTO
    {
        /// <summary>
        /// Codigo del DTO de Asociacion.
        /// </summary>
        [Display(Name = "Codigo Cliente")]
        public int CodigoCliente { get; set; }

        /// <summary>
        /// Codigo de Participante DTO de Asociación.
        /// </summary>
        [Display(Name = "Codigo Participante")]
        public int CodigoParticipante { get; set; }

        /// <summary>
        /// Codigo de Tipo del DTO de Asociación.
        /// </summary>
        [Display(Name = "Codigo Tipo")]
        public int CodigoTipo { get; set; }

        /// <summary>
        /// Campo Estado del DTO de Socio.
        /// </summary>
        [Display(Name = "Estado")]
        public int Estado { get; set; }
    }
}
