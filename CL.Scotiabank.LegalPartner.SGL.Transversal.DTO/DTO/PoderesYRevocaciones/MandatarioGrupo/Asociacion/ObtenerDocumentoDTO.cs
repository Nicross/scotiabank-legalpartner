﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class ObtenerDocumentoDTO
    {

        /// <summary>
        /// CodigoDocumento del DTO de Documento.
        /// </summary>
        public int CodigoDocumento { get; set; }

        /// <summary>
        /// CodigoPersona del DTO de Documento.
        /// </summary>
        public int CodigoPersona { get; set; }

        /// <summary>
        /// CodigoTipoLegalizacion del DTO de Documento.
        /// </summary>
        public int CodigoTipoLegalizacion { get; set; }

        /// <summary>
        /// TipoLegalizacion del DTO de Documento.
        /// </summary>
        public string TipoLegalizacion { get; set; }

        /// <summary>
        /// FechaDocumento del DTO de Documento.
        /// </summary>
        public DateTime FechaDocumento { get; set; }

        /// <summary>
        /// NumeroDocumento del DTO de Documento.
        /// </summary>
        public string NumeroDocumento { get; set; }

        /// <summary>
        /// DescripcionNotaria del DTO de Documento.
        /// </summary>
        public string DescripcionNotaria { get; set; }

        /// <summary>
        /// FechaDocumentoINP del DTO de Documento.
        /// </summary>
        public DateTime FechaDocumentoINP { get; set; }

        /// <summary>
        /// AnyoDocumentoINP del DTO de Documento.
        /// </summary>
        public string AnyoDocumentoINP { get; set; }

        /// <summary>
        /// NumeroFoja del DTO de Documento.
        /// </summary>
        public string NumeroFoja { get; set; }

        /// <summary>
        /// Foja del DTO de Documento.
        /// </summary>
        public string Foja { get; set; }

        /// <summary>
        /// DescripcionConservador del DTO de Documento.
        /// </summary>
        public string DescripcionConservador { get; set; }

        /// <summary>
        /// FechaPBL del DTO de Documento.
        /// </summary>
        public DateTime FechaPublicacion { get; set; }

        /// <summary>
        /// FechaSession del DTO de Documento.
        /// </summary>
        public DateTime FechaSession { get; set; }

        /// <summary>
        /// FechaJuntaAccion del DTO de Documento.
        /// </summary>
        public DateTime FechaJuntaAccion { get; set; }

        /// <summary>
        /// Fecha del DTO de Documento.
        /// </summary>
        public DateTime Fecha { get; set; }

        /// <summary>
        /// NumeroDocumento del DTO de Documento.
        /// </summary>
        public string NumeroDCO { get; set; }


        /// <summary>
        /// FechaPublicacionDCO del DTO de Documento.
        /// </summary>
        public DateTime FechaPublicacionDCO { get; set; }

        /// <summary>
        /// FechaProtocolo del DTO de Documento.
        /// </summary>
        public DateTime FechaProtocolo { get; set; }

        /// <summary>
        /// NotariaProtocolo del DTO de Documento.
        /// </summary>
        public string NotariaProtocolo { get; set; }

        /// <summary>
        /// CodigoOrigen del DTO de Documento.
        /// </summary>
        public int CodigoOrigen { get; set; }

        /// <summary>
        /// OrigenDocumento del DTO de Documento.
        /// </summary>
        public string OrigenDocumento { get; set; }

        /// <summary>
        /// OrigenNombre del DTO de Documento.
        /// </summary>
        public string OrigenNombre { get; set; }

        /// <summary>
        /// LugarOrigen del DTO de Documento.
        /// </summary>
        public string LugarOrigen { get; set; }

        /// <summary>
        /// ObservacionesDocumento del DTO de Documento.
        /// </summary>
        public string ObservacionesDocumento { get; set; }

        /// <summary>
        /// EstadoDocumento del DTO de Documento.
        /// </summary>
        public int EstadoDocumento { get; set; }

        /// <summary>
        /// FechaInicial del DTO de Documento.
        /// </summary>
        public DateTime FechaInicial { get; set; }

        /// <summary>
        /// FechaFinal del DTO de Documento.
        /// </summary>
        public DateTime FechaFinal { get; set; }

        /// <summary>
        /// CodigoRevocacion del DTO de Documento.
        /// </summary>
        public int CodigoRevocacion { get; set; }

        /// <summary>
        /// FechaQuiebra del DTO de Documento.
        /// </summary>
        public DateTime FechaQuiebra { get; set; }

        /// <summary>
        /// Juzgado del DTO de Documento.
        /// </summary>
        public string Juzgado { get; set; }

        /// <summary>
        /// FechaQuiebraPBL del DTO de Documento.
        /// </summary>
        public DateTime FechaQuiebraPBL { get; set; }

        /// <summary>
        /// Identificador del DTO de Documento.
        /// </summary>
        public string UsuarioLogin { get; set; }
        /// <summary>
        /// CodigoOficina del DTO de Documento.
        /// </summary>
        public string CodigoOficina { get; set; }

        /// <summary>
        /// NombreDocumnetoINF del DTO de Documento.
        /// </summary>
        public string NombreDocumnetoINF { get; set; }

        /// <summary>
        /// CodigoConservador del DTO de Documento.
        /// </summary>
        public int CodigoConservador { get; set; }

        /// <summary>
        /// FechaRenovacion del DTO de Documento.
        /// </summary>
        public DateTime FechaRenovacion { get; set; }

        /// <summary>
        /// IdSGL del DTO de Documento.
        /// </summary>
        public int IdSGL { get; set; }

        /// <summary>
        /// TipoLegalizacionCodigo del DTO de Documento.
        /// </summary>
        public int TipoLegalizacionCodigo { get; set; }

        /// <summary>
        /// CodigoTipoDocumneto del DTO de Documento.
        /// </summary>
        public int CodigoTipoDocumneto { get; set; }

        /// <summary>
        /// CodigoNotaria del DTO de Documento.
        /// </summary>
        public string CodigoNotaria { get; set; }

        /// <summary>
        /// CodigoRectificacion del DTO de Documento.
        /// </summary>
        public int CodigoRectificacion { get; set; }

        /// <summary>
        /// CodigoProtocoloNotaria del DTO de Documento.
        /// </summary>
        public string CodigoProtocoloNotaria { get; set; }

        /// <summary>
        /// CodigoOrigenDocumento del DTO de Documento.
        /// </summary>
        public int CodigoOrigenDocumento { get; set; }

        /// <summary>
        /// CodigoTarjeton del DTO de Documento.
        /// </summary>
        public int CodigoTarjeton { get; set; }

        /// <summary>
        /// Tarjeton del DTO de Documento.
        /// </summary>
        public string Tarjeton { get; set; }
    }
}
