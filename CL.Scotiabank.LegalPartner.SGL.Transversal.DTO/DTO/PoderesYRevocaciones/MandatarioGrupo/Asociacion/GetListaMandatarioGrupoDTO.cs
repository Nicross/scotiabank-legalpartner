﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class GetListaMandatarioGrupoDTO
    {
        /// <summary>
        /// Codigo de grupo.
        /// </summary>
        public int CodigoGrupo { get; set; }

        /// <summary>
        /// Codigo persona.
        /// </summary>
        public int CodigoPersona { get; set; }

        /// <summary>
        /// Rut persona.
        /// </summary>
        public string RutPersona { get; set; }

        /// <summary>
        /// Digito Vereficador.
        /// </summary>
        public string DigitoVereficador { get; set; }

        /// <summary>
        /// Nombre completo.
        /// </summary>
        public string NombreCompleto { get; set; }

        /// <summary>
        /// Fecha Inicio.
        /// </summary>
        public DateTime FechaInicio { get; set; }

        /// <summary>
        /// Fecha Final.
        /// </summary>
        public DateTime FechaFinal { get; set; }

        /// <summary>
        /// Codigo documento.
        /// </summary>
        public int CodigoDocumento { get; set; }

        /// <summary>
        /// Codigo Revocación.
        /// </summary>
        public int CodigoRevocacion { get; set; }

        /// <summary>
        /// Nombre de grupo.
        /// </summary>
        public string NombreGrupo { get; set; }

        /// <summary>
        /// Tipo de Grupo.
        /// </summary>
        public int TipoGrupo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int EstCsu { get; set; }
    }
}
