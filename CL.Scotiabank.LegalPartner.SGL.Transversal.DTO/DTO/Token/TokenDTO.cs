﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.DTO
{
    public class TokenDTO
    {
        public string ClaveSecreta { get; set; }

        public string access_token { get; set; }
    }
}
