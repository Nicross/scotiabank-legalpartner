﻿using CL.Scotiabank.LegalPartner.CabeceraSglAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.ConsultaAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.DAL;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    public class ListadoSGLAPP : IListadoSGLAPP
    {
        #region Memebers

        private ITokenAPP _tokenAPP;
        private IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string urlCabeceraSGL = Resources.CabeceraSGL;
        private readonly string urlTransversal = Resources.TransversalUrl;
        private readonly string urlConsulta = Resources.Consulta;

        #endregion

        #region Constructor

        public ListadoSGLAPP(ITokenAPP tokenAPP, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            this._tokenAPP = tokenAPP;
            this._configuration = configuration;
            this._httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="codigoUsuario"></param>
        /// <returns>Listado de UsuarioDTOResponse</returns>
        public BasePropertiesDTO GetUsuarios(int codigoUsuario)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerUsuario?codigoUsuario=" + codigoUsuario, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerUsuario?codigoUsuario=" + codigoUsuario, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IList<UsuarioDTOResponse>>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="codigoSucursal"></param>
        /// <returns>Listado de SucursalConAbogadosDTOResponse</returns>
        public BasePropertiesDTO GetSucursalesConAbogados(int codigoSucursal)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerSucursalesConAbogados?codigoSucursal=" + codigoSucursal, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerSucursalesConAbogados?codigoSucursal=" + codigoSucursal, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IList<SucursalConAbogadosDTOResponse>>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="codigoSucursal"></param>
        /// <returns>Listado de AbogadoDTOResponse</returns>
        public BasePropertiesDTO GetAbogadosPorSucursal(int codigoSucursal)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerAbogadosPorSucursal?codigoSucursal=" + codigoSucursal, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerAbogadosPorSucursal?codigoSucursal=" + codigoSucursal, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IList<AbogadoDTOResponse>>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web ConsultasApi.
        /// </summary>
        /// <param></param>
        /// <returns>Listado de EstadoSGLDTOResponse</returns>
        public BasePropertiesDTO GetListadoEstadosSGL()
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/GetListadoEstadosSGL", token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlTransversal + "/GetListadoEstadosSGL", token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IList<EstadoSGLDTOResponse>>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web CabeceraAPI. 
        /// </summary>
        /// <param name="listadoSglDTO"></param>
        /// <returns>Cantidad de SGLs</returns>
        public BasePropertiesDTO ObtenerCantidadSGL(ObtenerCantidadSGLDTORequest listadoSglDTO)
        {
            if (listadoSglDTO.FechaInicial==null)
            {
                listadoSglDTO.FechaInicial = DateTime.Today.ToString("dd/MM/yyyy");
            }

            if (listadoSglDTO.FechaFinal == null)
            {
                listadoSglDTO.FechaFinal = DateTime.Now.ToString("dd/MM/yyyy");
            }

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlCabeceraSGL + "/ObtenerCantidadSGL", listadoSglDTO, token.access_token);


            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Post(urlCabeceraSGL + "/ObtenerCantidadSGL", listadoSglDTO, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IList<ObtenerCantidadSGLDTOResponse>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web CabeceraAPI. 
        /// </summary>
        /// <param name="listadoSglDTO"></param>
        /// <returns>Lista de SGLs</returns>
        public BasePropertiesDTO ObtenerTodasSGL(ObtenerTodosSGLDTORequest listadoSglDTO)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");
            
            var responseContent = HttpRequestHelper.Post(urlCabeceraSGL + "/ObtenerTodosSGL", listadoSglDTO, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Post(urlCabeceraSGL + "/ObtenerTodosSGL", listadoSglDTO, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IList<ObtenerTodosSGLDTOResponse>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web ConsultaAPI.
        /// </summary>
        /// <param name="idSucursal"></param>
        /// <returns>Lista de usuarios por suscursal</returns>
        public BasePropertiesDTO GetListadoUsuarioPorSucursal(string idSucursal)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerUsuariosXsucursal?idSucursal=" + idSucursal, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerUsuariosXsucursal?idSucursal=" + idSucursal, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IList<ObtenerUsuariosXsucursalDTOResponse>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web ConsultaAPI.
        /// </summary>
        /// <param name="sgl"></param>
        /// <returns>Sgl filtrada por codio</returns>
        public BasePropertiesDTO GetSGLPorCodigoSGL(string sgl)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlConsulta + "/GetSGLPorCodigoSGL?id=" + sgl, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlConsulta + "/GetSGLPorCodigoSGL?id=" + sgl, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                if (responseContent.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    returnEntity = responseContent.ContentAsType<IList<ListadoSGLDTO>>();
                }
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web CVabeceraSGLAPI.
        /// </summary>
        /// <param name="idSgl"></param>
        /// <returns>Listado con historial de SGL solicitado</returns>
        public BasePropertiesDTO ObtenerHistorialSGL(int idSgl)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerHistorialEstadosSGL?codigoSGL=" + idSgl, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerHistorialEstadosSGL?codigoSGL=" + idSgl, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                if (responseContent.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    returnEntity = responseContent.ContentAsType<IList<HistorialSGLDTO>>();
                }
            }

            return returnEntity;
        }

        #endregion
    }
}
