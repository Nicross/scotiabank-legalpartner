﻿using CL.Scotiabank.LegalPartner.CabeceraSglAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.ConsultaAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.DAL;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;


namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    public class CabeceraSGLAPP : ICabeceraSGLAPP
    {
        #region Memebers

        private ITokenAPP _tokenAPP;
        private IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string urlCabeceraSGL = Resources.CabeceraSGL;
        private readonly string urlConstultaTransversal = Resources.TransversalUrl;
        private readonly string urlConsulta = Resources.Consulta;

        #endregion

        #region Constructor

        public CabeceraSGLAPP(ITokenAPP tokenAPP, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            this._tokenAPP = tokenAPP;
            this._configuration = configuration;
            this._httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Public Methods

        #region Cambiar Sociedad

      
        /// <summary>
        /// Metodo encargado de orquestar el llamado (PUT) para cambiar Tipo de Persona Juridica.
        /// </summary>
        /// <returns>IngresarSGL DTO</returns>
        public BasePropertiesDTO ActualizarPersona(IngresarSGLDTO entityToUse)
        {
            var requestEntity = new ActualizarPersonaDTORequest
            {
                CodigoPersona     = entityToUse.CodigoPersona,
                CodigoTipoPersona = entityToUse.IdTipoPersona,
                Nombre            = entityToUse.Nombre,
                ApellidoPaterno   = entityToUse.ApellidoPaterno,
                ApellidoMaterno   = entityToUse.ApellidoMaterno
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Put(urlCabeceraSGL + "/ActualizarPersona", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Put(urlCabeceraSGL + "/ActualizarPersona", requestEntity, token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<IngresarSGLDTO>>();
            }

            return returnEntity;
        }
        #endregion

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="entityToUse"></param>
        /// <returns></returns>
        public BasePropertiesDTO IngresarSGL(IngresarSGLDTO entityToUse)
        {

            var ultimoSGL = ObtenerUltimoCorrelativo(Resources.UltimoCorretaltivoSGL);
            
            var requestEntity = new CabeceraSglAPI.Transversal.DTO.IngresarSGLDTORequest
            {
                IdSGL = ultimoSGL.Resultado[0].UltimoCorrelativo + 1,
                Fecha = entityToUse.Fecha,
                Materia = entityToUse.TipoMateria,
                Estado = entityToUse.Estado,
                of_partes = entityToUse.of_partes,
                IdSucursal = entityToUse.IdSucursal,
                CodigoUsuario = entityToUse.CodigoUsuario,
            };            

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlCabeceraSGL + "/IngresarSGL", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Post(urlCabeceraSGL + "/IngresarSGL", requestEntity, token.access_token);
            }

            NuevoEstadoSGLDTORequest nuevoEstado = new NuevoEstadoSGLDTORequest()
            {
                SglId = ultimoSGL.Resultado[0].UltimoCorrelativo + 1,
                Estado = 2,
                CodigoPersonaModificador = entityToUse.CodigoUsuario
            };
            NuevoAbogadoSGLDTORequest Abogado = new NuevoAbogadoSGLDTORequest()
            {
                SglId = ultimoSGL.Resultado[0].UltimoCorrelativo + 1,
                CodigoOficinaPartes = entityToUse.of_partes,
                CodigoSucursal = entityToUse.IdSucursal,
                CodigoPersonaAbogado = entityToUse.IdAbogado
            };

            var actualizarEstado = HttpRequestHelper.Put(urlCabeceraSGL + "/ActualizarEstadoSGL", nuevoEstado, token.access_token);            
            var AsignarAbogado = HttpRequestHelper.Put(urlCabeceraSGL + "/ActualizarAbogadoSGL", Abogado, token.access_token);

            BasePropertiesDTO returnEntity = new BasePropertiesDTO {Resultado = requestEntity };
           
            return returnEntity;

            //if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            //{
               
            //}            
        }


        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="codigoSucursal"></param>
        /// <returns>Listado de SucursalConAbogadosDTOResponse</returns>
        public BasePropertiesDTO ObtenerSucursales(int codigoSucursal)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerSucursalesConAbogados?codigoSucursal=" + codigoSucursal, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerSucursalesConAbogados?codigoSucursal=" + codigoSucursal, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IList<SucursalConAbogadosDTOResponse>>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <returns>Listado de MateriasDTOResponse</returns>
        public BasePropertiesDTO ObtenerMaterias()
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerMaterias", token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerMaterias", token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IList<MateriasDTOResponse>>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="codigoSucursal"></param>
        /// <returns>Listado de AbogadoDTOResponse</returns>
        public BasePropertiesDTO ObtenerAbogadoPorSucursal(int codigoSucursal)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerAbogadosPorSucursal?codigoSucursal=" + codigoSucursal, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerAbogadosPorSucursal?codigoSucursal=" + codigoSucursal, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IList<AbogadoDTOResponse>>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="IdMateriaPadre">Id de la materia padre.</param>
        /// <returns>Listado de AbogadoDTOResponse</returns>
        public BasePropertiesDTO ObtenerTipoMateriaPorPadre(int IdMateriaPadre)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerTipoMateriaPorMateriaPadre?materiaPadre=" + IdMateriaPadre, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerTipoMateriaPorMateriaPadre?materiaPadre=" + IdMateriaPadre, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IList<ObtenerTipoMateriaDTOResponse>>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="IdUsuario">Id del usuario.</param>
        /// <returns>Listado de AbogadoDTOResponse</returns>
        public BasePropertiesDTO ObtenerDatosUsuario(int IdUsuario)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerUsuario?codigoUsuario=" + IdUsuario, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerUsuario?codigoUsuario=" + IdUsuario, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<UsuarioDTOResponse>>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="TipoCorrelativo">Id del tipo correlativo buscado.</param>
        /// <returns>Numero del ultimo correlativo correspendiente</returns>
        public BasePropertiesDTO ObtenerUltimoCorrelativo(int TipoCorrelativo)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/GetUltimoCorreltivoSGL?codigoCorrelativo=" + TipoCorrelativo, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerUltimoCorrelativoSGL?codigoCorrelativo=" + TipoCorrelativo, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ObtenerUltimoSGLDTOResponse>>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <returns>Numero del ultimo correlativo correspendiente</returns>
        public BasePropertiesDTO ObtenerListaTipoPersona()
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlConstultaTransversal + "/GetListadoTipoPersona", token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlConstultaTransversal + "/GetListadoTipoPersona", token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<TipoPersonaDTOResponse>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="rut">rut de la persona buscada</param>
        /// <returns>datos de la persona</returns>
        public BasePropertiesDTO BusquedaPorRut(int rut)
        {
            
            ConsultaPersonaDatosDTORequest request = new ConsultaPersonaDatosDTORequest
            {
                Rut = rut,
                Nombre = string.Empty,
                ApellidoPaterno = string.Empty,
                ApellidoMaterno = string.Empty,
                CantidadRegistros = 0
            };
            var responseContent = HttpRequestHelper.Post(urlConsulta + "/ConsultaPersonaDatos", request);
            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<DatosPersonaDTO>>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="IdSGL">Id de la sgl</param>
        /// <param name="IdSociedad">Id de la sociedad</param>
        /// <returns></returns>
        public BasePropertiesDTO AsociarSociedadSGL(int IdSGL, int IdSociedad)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/AsignarPersona?idSgl=" + IdSGL + "&codigoPersona=" + IdSociedad, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/AsignarPersona?idSgl=" + IdSGL + "&codigoPersona=" + IdSociedad, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<AsignarPersonaDTOResponse>>();
            }
            return returnEntity;
        }

        #region Eliminar SGL

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="IdSGL">Id de la sgl</param>
        /// <returns></returns>
        public BasePropertiesDTO ActualizarEstadoSGL(int IdSGL, int codigoPersona)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            NuevoEstadoSGLDTORequest nuevoEstado = new NuevoEstadoSGLDTORequest()
            {
                SglId = IdSGL,
                Estado = 16,
                CodigoPersonaModificador = codigoPersona
            };

            var responseContent = HttpRequestHelper.Put(urlCabeceraSGL + "/ActualizarEstadoSGL", nuevoEstado, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Put(urlCabeceraSGL + "/ActualizarEstadoSGL", nuevoEstado, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<ResultadoActualizacionEstadoSGLDTOResponse>();
            }
            return returnEntity;
        }

        #endregion

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web CabeceraSglApi
        /// para respaldar Nombre de sociedad antes de Actualizar persona Juridica.
        /// </summary>
        /// <param name="entityToUse"></param>
        /// <returns></returns>
        public BasePropertiesDTO RespaldarNombreAnterior(IngresarSGLDTO entityToUse)
        {
            var requestEntity = new RespaldarNombreAnteriorDTORequest
            {
                CodigoPersona = entityToUse.CodigoPersona,
                 NombreActual = entityToUse.NombreActual,
                RutAbogado = entityToUse.RutAbogado,
                TipoPersona = entityToUse.IdTipoPersona
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlCabeceraSGL + "/RespaldarNombreAnterior", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(urlCabeceraSGL + "/RespaldarNombreAnterior", requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<IngresarSGLDTO>>();
            }

            return returnEntity;
        }

        #region TerminarSGL

        public BasePropertiesDTO TerminarSGL(int IdSGL)
        {            
            var responseContent = HttpRequestHelper.Get(urlConsulta + "/ConsultaPersonaDatos?idSgl=" + IdSGL);
            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<TerminarSGLDTOResponse>>();
            }
            return returnEntity;
        }

        #endregion

        #endregion
    }
}
