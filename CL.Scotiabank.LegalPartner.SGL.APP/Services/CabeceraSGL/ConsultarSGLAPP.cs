﻿using CL.Scotiabank.LegalPartner.CabeceraSglAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.ConsultaAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.DAL;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    public class ConsultarSGLAPP : IConsultarSGLAPP
    {
        #region Memebers

        private readonly string urlCabeceraSGL = Resources.CabeceraSGL;
        private readonly string urlConsultaSGL = Resources.Consulta;
        private ITokenAPP _tokenAPP;
        private IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;

        #endregion

        #region Constructor

        public ConsultarSGLAPP(ITokenAPP tokenAPP, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            this._tokenAPP = tokenAPP;
            this._configuration = configuration;
            this._httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Metodos base

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="codigoSucursal"></param>
        /// <returns>Listado de SucursalConAbogadosDTOResponse</returns>
        public BasePropertiesDTO GetSucursalesConAbogados(int codigoSucursal)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerSucursalesConAbogados?codigoSucursal=" + codigoSucursal, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerSucursalesConAbogados?codigoSucursal=" + codigoSucursal, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IList<SucursalConAbogadosDTOResponse>>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param>N/A</param>
        /// <returns>Listado de Materias</returns>
        public BasePropertiesDTO ObtenerMaterias()
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerMaterias", token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerMaterias", token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IList<MateriasDTOResponse>>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="codigoSucursal"></param>
        /// <returns>Listado de AbogadoDTOResponse</returns>
        public BasePropertiesDTO GetAbogadosPorSucursal(int codigoSucursal)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerAbogadosPorSucursal?codigoSucursal=" + codigoSucursal, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerAbogadosPorSucursal?codigoSucursal=" + codigoSucursal, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IList<AbogadoDTOResponse>>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="materiaPadre"></param>
        /// <returns>Listado de Tipo Materia</returns>
        public BasePropertiesDTO ObtenerTipoMateriaPorMateriaPadre(int materiaPadre)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerTipoMateriaPorMateriaPadre?materiaPadre=" + materiaPadre, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerTipoMateriaPorMateriaPadre?materiaPadre=" + materiaPadre, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IList<ObtenerTipoMateriaDTOResponse>>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web ConsultaApi.
        /// </summary>
        /// <param name="numeroSgl"></param>
        /// <returns>Datos de SGLDTO solicitado</returns>
        public BasePropertiesDTO GetSGL(int numeroSgl)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            SGLDTO sglSinNull = null;

            var responseContent = HttpRequestHelper.Get(urlConsultaSGL + "/GetSGLPorCodigoSGL?id=" + numeroSgl, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlConsultaSGL + "/GetSGLPorCodigoSGL?id=" + numeroSgl, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IList<SGLDTO>>();

                sglSinNull = new SGLDTO();
                var cantidadPropSGL = returnEntity.Resultado.Count;
                if (cantidadPropSGL == 1 && returnEntity.Resultado[0].SucursalId == 0 && returnEntity.Resultado[0].EstadoSGL == 0)
                {
                    sglSinNull.NombrePersona = "na";
                    sglSinNull.Dv = "na";
                }
                else
                {
                    sglSinNull.NombrePersona = string.IsNullOrEmpty(returnEntity.Resultado[0].NombrePersona) ? string.Empty : returnEntity.Resultado[0].NombrePersona.Trim();
                    sglSinNull.ApellidoMaternoPersona = string.IsNullOrEmpty(returnEntity.Resultado[0].ApellidoMaternoPersona) ? string.Empty : returnEntity.Resultado[0].ApellidoMaternoPersona.Trim();
                    sglSinNull.ApellidoPaternoPersona = string.IsNullOrEmpty(returnEntity.Resultado[0].ApellidoPaternoPersona) ? string.Empty : returnEntity.Resultado[0].ApellidoPaternoPersona.Trim();
                    sglSinNull.Rut = returnEntity.Resultado[0].Rut;
                    sglSinNull.Dv = string.IsNullOrEmpty(returnEntity.Resultado[0].Dv) ? string.Empty : returnEntity.Resultado[0].Dv.Trim();
                    sglSinNull.RutPersonaSolicitante = string.IsNullOrEmpty(returnEntity.Resultado[0].RutPersonaSolicitante) ? string.Empty : returnEntity.Resultado[0].RutPersonaSolicitante.Trim();
                    sglSinNull.DvPersonaSolicitante = string.IsNullOrEmpty(returnEntity.Resultado[0].DvPersonaSolicitante) ? string.Empty : returnEntity.Resultado[0].DvPersonaSolicitante.Trim();
                    sglSinNull.NombrePersonaSolicitante = string.IsNullOrEmpty(returnEntity.Resultado[0].NombrePersonaSolicitante) ? string.Empty : returnEntity.Resultado[0].NombrePersonaSolicitante.Trim();
                    sglSinNull.ApellidoPaternoPersonaSolicitante = string.IsNullOrEmpty(returnEntity.Resultado[0].ApellidoPaternoPersonaSolicitante) ? string.Empty : returnEntity.Resultado[0].ApellidoPaternoPersonaSolicitante.Trim();
                    sglSinNull.ApellidoMaternoPersonaSolicitante = string.IsNullOrEmpty(returnEntity.Resultado[0].ApellidoMaternoPersonaSolicitante) ? string.Empty : returnEntity.Resultado[0].ApellidoMaternoPersonaSolicitante.Trim();
                    sglSinNull.CodigoPersonaAbogadoSGL = returnEntity.Resultado[0].CodigoPersonaAbogadoSGL;

                    sglSinNull.FechaInicioSGL = returnEntity.Resultado[0].FechaInicioSGL;
                    sglSinNull.SGLId = returnEntity.Resultado[0].SGLId;
                    sglSinNull.SucursalId = returnEntity.Resultado[0].SucursalId;
                    sglSinNull.DescripcionEstadoSGL = string.IsNullOrEmpty(returnEntity.Resultado[0].DescripcionEstadoSGL) ? string.Empty : returnEntity.Resultado[0].DescripcionEstadoSGL.Trim();
                    sglSinNull.EstadoSGL = returnEntity.Resultado[0].EstadoSGL;
                    sglSinNull.MateriaPadre = string.IsNullOrEmpty(returnEntity.Resultado[0].MateriaPadre) ? string.Empty : returnEntity.Resultado[0].MateriaPadre.Trim();
                    sglSinNull.MateriaId = returnEntity.Resultado[0].MateriaId;
                }

                returnEntity.Resultado = sglSinNull;
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="codigoMateria"></param>
        /// <returns>Materia filtrada</returns>
        public BasePropertiesDTO ObtenerMateriaSeleccionada(int codigoMateria)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerMateriaSeleccionada?codigoMateria=" + codigoMateria, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerMateriaSeleccionada?codigoMateria=" + codigoMateria, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IList<MateriasDTOResponse>>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="codigoMateria"></param>
        /// <returns>Tipo Materia filtrada</returns>
        public BasePropertiesDTO ObtenerTipoMateriaSeleccionada(int codigoMateria)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerTipoMateria?tipoMateriaId=" + codigoMateria, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerTipoMateria?tipoMateriaId=" + codigoMateria, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IList<ObtenerTipoMateriaDTOResponse>>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web ConsultaApi. 
        /// </summary>
        /// <param name="usuarioConsultaRequest"></param>
        /// <returns>Lista de SGLs</returns>
        public BasePropertiesDTO ConsultaPersonaDatos(UsuarioConsultaDTORequest usuarioConsultaRequest)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlConsultaSGL + "/ConsultaPersonaDatos", usuarioConsultaRequest, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Post(urlConsultaSGL + "/ConsultaPersonaDatos", usuarioConsultaRequest, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IList<ConsultaPersonaDatosDTOResponse>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Get) y el retorno de datos desde la api web ConsultaApi. 
        /// </summary>
        /// <param name="codigoPersona"></param>
        /// <returns>Lista de SGLs</returns>
        public BasePropertiesDTO GetSLGPorCodigoPersona(int codigoPersona)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlConsultaSGL + "/GetSGLPorCodigoPersona?id=" + codigoPersona.ToString(), token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlConsultaSGL + "/GetSGLPorCodigoPersona?id=" + codigoPersona.ToString(), token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IList<ConsultaSGLPorPersonaIdDTOResponse>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Ejecuta el metodo GetGrupoUsuario de la Api de Accesos.
        /// </summary>
        /// <param name="entityToUse">Entidad utilizada para crear el Request</param>
        /// <returns>BasePropertiesDTO con codigo de retorno y LoginDTO</returns>
        public BasePropertiesDTO GetGrupoUsuario(LoginDTO entityToUse)
        {
            var requestEntity = new LoginDTORequest
            {
                RutUsuario = "11111111", //entityToUse.Rut, :ToDo: borrar dato en duro
                DigitoVerificador =  "1" //entityToUse.DigitoVerificador :ToDo: borrar dato en duro
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(Resources.ModificarClaveObtenerGrupoUsuario + "?RutUsuario=" + requestEntity.RutUsuario + "&DigitoVerificador=" + requestEntity.DigitoVerificador, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { access_token = _configuration["Token:key"] });

                responseContent = HttpRequestHelper.Get(Resources.ModificarClaveObtenerGrupoUsuario + "?RutUsuario=" + requestEntity.RutUsuario + "&DigitoVerificador=" + requestEntity.DigitoVerificador, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<LoginDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Ejecuta el metodo GetClienteNombreLegal de la Api de Consulta.
        /// </summary>
        /// <param name="nombre">Nombre de </param>
        /// <returns>Datos Cliente Sociedad por Nombre</returns>
        public BasePropertiesDTO GetClienteNombreLegal(string nombre)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlConsultaSGL + "/GetClienteNombreLegal?nombre=" + nombre, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { access_token = _configuration["Token:key"] });

                responseContent = HttpRequestHelper.Get(urlConsultaSGL + "/GetClienteNombreLegal?nombre=" + nombre, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IList<ConsultaClienteNombreLegalDTOResponse>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Ejecuta el metodo ObtenerListadoSGLPorFecha de la Api de Cabecera.
        /// </summary>
        /// <param name="fecha">fecha de busqueda</param>
        /// <returns>Datos SGLs filtradaas por fecha</returns>
        public BasePropertiesDTO ObtenerListadoSGLPorFecha(string fecha)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerListadoSGLPorFecha?fechaBusqueda=" + fecha, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { access_token = _configuration["Token:key"] });

                responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerListadoSGLPorFecha?fechaBusqueda=" + fecha, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IList<SglDTOResponse>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web ConsultaAPI.
        /// </summary>
        /// <param name="sgl"></param>
        /// <returns>Sgl filtrada por codio</returns>
        public BasePropertiesDTO GetSGLPorCodigoSGL(string sgl)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlConsultaSGL + "/GetSGLPorCodigoSGL?id=" + sgl, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlConsultaSGL + "/GetSGLPorCodigoSGL?id=" + sgl, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                if (responseContent.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    returnEntity = responseContent.ContentAsType<IList<ListadoSGLDTO>>();
                }

                if (responseContent.StatusCode.Equals(System.Net.HttpStatusCode.BadRequest))
                {
                    //returnEntity = new UsuarioDTO();
                }
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web CVabeceraSGLAPI.
        /// </summary>
        /// <param name="idSgl"></param>
        /// <returns>Listado con historial de SGL solicitado</returns>
        public BasePropertiesDTO ObtenerHistorialSGL(int idSgl)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerHistorialEstadosSGL?codigoSGL=" + idSgl, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerHistorialEstadosSGL?codigoSGL=" + idSgl, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                if (responseContent.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    returnEntity = responseContent.ContentAsType<IList<HistorialSGLDTO>>();
                }

                if (responseContent.StatusCode.Equals(System.Net.HttpStatusCode.BadRequest))
                {
                    returnEntity = responseContent.ContentAsType<IList<HistorialSGLDTO>>();
                }
            }

            return returnEntity;
        }

        #endregion

    }
}
