﻿using CL.Scotiabank.LegalPartner.SGL.DAL;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Clase de servicios para la entidad SucursalBanco del sistema.
    /// </summary>
    public class SucursalBancoAPP : ISucursalBancoAPP
    {
        #region Memebers

        private ITokenAPP _tokenAPP;
        private IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string urlSucursalBancos = Resources.SucursalBancoUrl;

        #endregion

        #region Constructor

        public SucursalBancoAPP(ITokenAPP tokenAPP, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            this._tokenAPP = tokenAPP;
            this._configuration = configuration;
            this._httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de sucursalBancos.
        /// </summary>
        /// <returns>Lista de SucursalBancoDTO</returns>
        public BasePropertiesDTO Get()
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlSucursalBancos, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlSucursalBancos, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<SucursalBancoDTO>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de un dato desde la api web de sucursalBancos.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public BasePropertiesDTO Get(int id)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlSucursalBancos + "/" + id, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlSucursalBancos + "/" + id, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<SucursalBancoDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de sucursalBancos.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>SucursalBancoDTO</returns>
        public BasePropertiesDTO Create(SucursalBancoDTO entityToUse)
        {
            var requestEntity = new SucursalBancoDTORequest
            {                
                Descripcion = entityToUse.Descripcion,
                OficinaPartesCodigo = entityToUse.OficinaPartesCodigo,
                SucursalCodigoCuenta = entityToUse.SucursalCodigoCuenta
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlSucursalBancos, requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Post(urlSucursalBancos, requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<SucursalBancoDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Put) y el retorno de datos desde la api web de sucursalBancos.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>SucursalBancoDTO</returns>
        public BasePropertiesDTO Edit(SucursalBancoDTO entityToUse)
        {
            var model = new SucursalBancoDTORequest
            {
                Codigo = entityToUse.Codigo,
                Descripcion = entityToUse.Descripcion,
                OficinaPartesCodigo = entityToUse.OficinaPartesCodigo,
                SucursalCodigoCuenta = entityToUse.SucursalCodigoCuenta
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Put(urlSucursalBancos + "/" + entityToUse.Codigo, model, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Put(urlSucursalBancos + "/" + entityToUse.Codigo, model, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<SucursalBancoDTO>();
            }

            return returnEntity;
        }
        
        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) y el retorno de datos desde la api web de sucursalBancos.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>SucursalBancoDTO</returns>
        public BasePropertiesDTO Delete(SucursalBancoDTO entityToUse)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Delete(urlSucursalBancos + "/" + entityToUse.Codigo, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Delete(urlSucursalBancos + "/" + entityToUse.Codigo, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<SucursalBancoDTO>();
            }
            return returnEntity;
        }
               
        #endregion
    }
}
