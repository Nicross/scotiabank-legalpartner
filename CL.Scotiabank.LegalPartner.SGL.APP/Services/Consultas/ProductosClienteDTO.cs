﻿using System;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    public class ProductosClienteDTO
    {

        /// <summary>
        /// numero de cuenta.
        /// </summary>
        public string NumeroCuenta { get; set; }

        /// <summary>
        /// Fecha inicio
        /// </summary>
        public DateTime FechaInicio { get; set; }


        /// <summary>
        /// Fecha Fin
        /// </summary>
        public DateTime FechaFin { get; set; }

    }
}