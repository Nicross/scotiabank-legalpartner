﻿using CL.Scotiabank.LegalPartner.ConsultaAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.PoderYRevocacionesAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.DAL;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Clase de servicios para la entidad Consulta Poderes del sistema.
    /// </summary>
    public class ConsultaPoderesAPP : IConsultaPoderesAPP
    {
        #region Memebers

        private readonly ITokenAPP _tokenAPP;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string urlConsultaPoderes = Resources.ConsultaPoderesUrl;
        private readonly string urlTransversal = Resources.TransversalUrl;
        private readonly string urlPoderesRevocaciones = Resources.FacultadesUrl;


        #endregion
        #region Constructor

        public ConsultaPoderesAPP(ITokenAPP tokenAPP, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            this._tokenAPP = tokenAPP;
            this._configuration = configuration;
            this._httpContextAccessor = httpContextAccessor;
        }

        #endregion
        #region Public Methods
        public BasePropertiesDTO ObtenerPoderesFacultadesVigentes(int codigoDocumentoLegal)
        {
            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerPoderesFacultadesVigentes");

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<DocumentoLegalDTO>>();
            }

            return returnEntity;
        }

        public BasePropertiesDTO ObtenerPoderesVigentes(ConsultaPoderesDTO entityToUse)
        {
            PoderVigenteDTORequest requestEntity = new PoderVigenteDTORequest
            {
                CodigoPersona = entityToUse.CodigoPersona,
                EstadoConsulta = entityToUse.EstadoConsulta,
                FechaBusqueda = entityToUse.FechaBusqueda,
                TipoPoder = entityToUse.TipoPoder,
                TipoRespuesta = entityToUse.TipoRespuesta

            };
            var responseContent = HttpRequestHelper.Post(urlTransversal + "/ObtenerPoderesVigentes", requestEntity);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaPoderesDTO>>();
            }

            return returnEntity;
        }

        public BasePropertiesDTO ObtenerLimites()/*falta crear API */
        {
            var responseContent = HttpRequestHelper.Get(urlConsultaPoderes + "/ObtenerLimites");

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaPoderesDTO>>();
            }

            return returnEntity;
        }
        //ObtenerFacultadesVigentes

        public BasePropertiesDTO ObtenerFacultadesVigentes(int codigoPoder, string fechaConsulta)
        {

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlPoderesRevocaciones + "/ObtenerFacultadesVigentes?codigoPoder=" + codigoPoder + "&fechaConsulta=" + fechaConsulta, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<FacultadVigenteDTOResponse>>();
            }

            return returnEntity;
        }


        public BasePropertiesDTO ObtenerMontoMaximo(string codigoMoneda, decimal montoMaximo)
        {
            var responseContent = HttpRequestHelper.Get(urlConsultaPoderes + "/ObtenerMontoMaximo");

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaPoderesDTO>>();
            }

            return returnEntity;
        }

        public BasePropertiesDTO ObtenerPoderesVigentesPersona(int codigoPersona, string fechaConsulta)
        {
            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerPoderesVigentesPersona" + codigoPersona + fechaConsulta);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaPoderesDTO>>();
            }

            return returnEntity;
        }

        public BasePropertiesDTO ObtenerFormasDeActuar(int CodigoPoder)
        {
            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerFormasDeActuar" + CodigoPoder);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaPoderesDTO>>();
            }

            return returnEntity;
        }

        public BasePropertiesDTO ObtenerNumeroMandatarios(ConsultaPoderesDTO entityToUse)
        {
            ConsultaNumeroMandatarioDTORequest requestEntity = new ConsultaNumeroMandatarioDTORequest
            {
                Total = entityToUse.Total,
            };

            var responseContent = HttpRequestHelper.Post(urlTransversal + "/ObtenerNumeroMandatarios", requestEntity);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaPoderesDTO>>();
            }

            return returnEntity;
        }

        public BasePropertiesDTO ListarFacultadesPorPoder(string idPoder)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");
            IList<FacultadDTOResponse> lstNuevaFacultades = new List<FacultadDTOResponse>();
            FacultadDTOResponse paramFacultades = null;
            string descGrupo = string.Empty;

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerListadoFacultadesPorPoder?idPoder=" + idPoder, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<FacultadDTOResponse>>();

                var gruposPadres = from s in (IEnumerable<FacultadDTOResponse>)returnEntity.Resultado
                                   group s by s.DescripcionGrupo;

                foreach (var grupos in gruposPadres)
                {
                    descGrupo = grupos.Key;

                    paramFacultades = new FacultadDTOResponse
                    {
                        TipoRelacion = "Padre",
                        NombreFacultad = descGrupo
                    };

                    lstNuevaFacultades.Add(paramFacultades);

                    foreach (var item in returnEntity.Resultado)
                    {
                        if (item.DescripcionGrupo == descGrupo)
                        {
                            paramFacultades = new FacultadDTOResponse
                            {
                                CodigoFacultad = item.CodigoFacultad,
                                NombreFacultad = item.NombreFacultad,
                                DescripcionGrupo = item.DescripcionGrupo,
                                CodigoGrupo = item.CodigoGrupo,
                                TipoRelacion = item.TipoRelacion
                            };

                            lstNuevaFacultades.Add(paramFacultades);
                        }
                    }
                }

                returnEntity.Resultado = lstNuevaFacultades;
            }

            return returnEntity;
        }

        public BasePropertiesDTO ListarParticipanteConPoder(ConsultaParticipanteConPoderDTORequest entityToUse)
        {
            IEnumerable<ParticipanteDTOResponse> lstParticipantes = null;
            var requestEntity = new ConsultaParticipanteConPoderDTORequest
            {
                CodigoParticipante = entityToUse.CodigoParticipante,
                TipoParticipante = entityToUse.TipoParticipante,
                FechaConsulta = entityToUse.FechaConsulta
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlTransversal + "/ListarParticipanteConPoder", requestEntity, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ParticipanteDTOResponse>>();
                lstParticipantes = (IEnumerable<ParticipanteDTOResponse>)returnEntity.Resultado;

                returnEntity.Resultado = lstParticipantes.Where(x => x.Vigente != "N");
                //((IEnumerable<TipoPersonaDetalleDTO>)lstTipoPersona.Resultado).Where(x => x.Codigo.Equals(paramDatosPersona.CodigoTipoPersona)).FirstOrDefault().Descripcion
            }

            return returnEntity;
        }

        public BasePropertiesDTO ObtenerFirmaReciente(int codigoPersona, string numeroCuenta)
        {
            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerFirmaReciente" + codigoPersona + numeroCuenta);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaPoderesDTO>>();
            }


            return returnEntity;
        }

        public BasePropertiesDTO ObtenerParticipanteConPoderSinCta(ConsultaPoderesDTO entityToUse)
        {
            var requestEntity = new ConsultaParticipantesConPoderSinCtaDTORequest
            {
                CodigoParticipante = entityToUse.CodigoParticipante,
                TipoParticipante = entityToUse.TipoParticipante,
                FechaConsulta = entityToUse.FechaConsulta
            };

            var responseContent = HttpRequestHelper.Post(urlConsultaPoderes + "/ObtenerParticipanteConPoderSinCta", requestEntity);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaPoderesDTO>>();
            }

            return returnEntity;
        }

        public BasePropertiesDTO ObtenerFirmaRecienteSinCta()
        {
            var responseContent = HttpRequestHelper.Get(urlConsultaPoderes + "/ObtenerFirmaRecienteSinCta");

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaPoderesDTO>>();
            }

            return returnEntity;
        }

        public BasePropertiesDTO ObtenerDatosPersona(UsuarioConsultaDTORequest entityToUse)
        {
            var requestEntity = new UsuarioConsultaDTORequest
            {
                Rut = entityToUse.Rut,
                Nombre = entityToUse.Nombre,
                ApellidoPaterno = entityToUse.ApellidoPaterno,
                ApellidoMaterno = entityToUse.ApellidoMaterno,
                CantidadRegistros = entityToUse.CantidadRegistros
            };

            UsuarioConsultaDTO paramDatosPersona = new UsuarioConsultaDTO();

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlConsultaPoderes + "/ConsultaPersonaDatos", requestEntity, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<UsuarioConsultaDTO>>();
                paramDatosPersona = returnEntity.Resultado[0];

                paramDatosPersona = CargaDatosAdicionalesPersona(paramDatosPersona);
                paramDatosPersona = CargaListaDocumentos(paramDatosPersona);
                paramDatosPersona = CargaDatosEstatutoPersona(paramDatosPersona);

                returnEntity.Resultado = paramDatosPersona;

            }

            return returnEntity;
        }

        private UsuarioConsultaDTO CargaListaDocumentos(UsuarioConsultaDTO paramDatosPersona)
        {
            int codigoPersona = paramDatosPersona.CodigoPersona;
            BasePropertiesDTO lstDocumentos = ObtenerListadoDocumentos(codigoPersona);
            List<ListadoDocumentosDTO> lstNuevaDocumentos = ((List<ListadoDocumentosDTO>)lstDocumentos.Resultado);

            paramDatosPersona.ListaDocumentos = new List<ListadoDocumentosDTO>();
            paramDatosPersona.ListaDocumentos.AddRange((List<ListadoDocumentosDTO>)lstNuevaDocumentos);

            return paramDatosPersona;
        }

        private UsuarioConsultaDTO CargaDatosEstatutoPersona(UsuarioConsultaDTO paramDatosPersona)
        {
            int codigoPersona = paramDatosPersona.CodigoPersona;
            ConsultaDatosEstatutosDTORequest paramEstatutos = new ConsultaDatosEstatutosDTORequest();
            BasePropertiesDTO datosPersonaRetorno = ObtenerDatosEstatutos(codigoPersona);

            paramDatosPersona.Rut = datosPersonaRetorno.Resultado[0].Rut;
            paramDatosPersona.NombreLegalAnterior = string.IsNullOrEmpty(paramDatosPersona.Conclusion) ? string.Empty : datosPersonaRetorno.Resultado[0].NombreLegalAnterior.Trim();
            paramDatosPersona.NombreFantasia = datosPersonaRetorno.Resultado[0].NombreFantasia.Trim();
            paramDatosPersona.Objeto = datosPersonaRetorno.Resultado[0].Objeto.Trim();
            paramDatosPersona.Domicilio = datosPersonaRetorno.Resultado[0].Domicilio.Trim();
            paramDatosPersona.Duracion = datosPersonaRetorno.Resultado[0].Duracion.Trim();
            paramDatosPersona.Administracion = datosPersonaRetorno.Resultado[0].Administracion.Trim();
            paramDatosPersona.FechaActualizacion = datosPersonaRetorno.Resultado[0].FechaActualizacion.ToString("dd/MM/yyyy").Contains("3000") ? string.Empty : datosPersonaRetorno.Resultado[0].FechaActualizacion.ToString("dd/MM/yyyy").Replace("-", "/");//.Substring(0, 10).Trim();
            paramDatosPersona.Conclusion = string.IsNullOrEmpty(datosPersonaRetorno.Resultado[0].Observaciones) ? string.Empty : datosPersonaRetorno.Resultado[0].Observaciones.Trim();

            return paramDatosPersona;
        }

        private UsuarioConsultaDTO CargaDatosAdicionalesPersona(UsuarioConsultaDTO paramDatosPersona)
        {
            int codigoPersona = paramDatosPersona.CodigoPersona;
            int codLegal = 0;
            string descripcionTipoPersona = string.Empty;
            BasePropertiesDTO lstEstadoLegal = GetListaEstadoLegal();
            BasePropertiesDTO lstTipoPersona = GetListadoTipoPersonaDetalle();
            BasePropertiesDTO transversalPersonaRetorno = ConsultarDatosPersona(codigoPersona);

            if (transversalPersonaRetorno != null)
            {
                if (paramDatosPersona.CodigoTipoPersona.Equals("0"))
                {
                    paramDatosPersona.EstadoLegalDescripcion = string.Empty;
                }
                else
                {
                    descripcionTipoPersona = ((IEnumerable<TipoPersonaDetalleDTO>)lstTipoPersona.Resultado).Where(x => x.Codigo.Equals(paramDatosPersona.CodigoTipoPersona)).FirstOrDefault().Descripcion;
                    codLegal = Convert.ToInt32(((IEnumerable<TipoPersonaDetalleDTO>)lstTipoPersona.Resultado).Where(x => x.Descripcion.Trim() == descripcionTipoPersona).FirstOrDefault().CodigoLegal.Trim());

                    paramDatosPersona.TipoPersonaJuridica = descripcionTipoPersona;
                    paramDatosPersona.CodigoLegal = codLegal;
                }

                string estadoLegalDescripcion = ((IEnumerable<EstadoLegalizacionDTO>)lstEstadoLegal.Resultado).Where(x => x.Codigo.Equals(transversalPersonaRetorno.Resultado[0].EstadoLegal)).FirstOrDefault().Descripcion;
                paramDatosPersona.EstadoLegalDescripcion = estadoLegalDescripcion;
            }

            return paramDatosPersona;
        }

        public BasePropertiesDTO GetListaEstadoLegal()
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlConsultaPoderes + "/GetListaEstadoLegal", token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<EstadoLegalizacionDTO>>();
            }

            return returnEntity;
        }

        public BasePropertiesDTO GetListadoTipoPersonaDetalle()
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/GetListadoTipoPersonaDetalle", token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<TipoPersonaDetalleDTO>>();
            }

            return returnEntity;
        }

        public BasePropertiesDTO ConsultarDatosPersona(int codigoPersona)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlConsultaPoderes + "/ConsultarDatosPersona?codigoPersona=" + codigoPersona, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<UsuarioConsultaDTO>>();
            }

            return returnEntity;
        }

        public BasePropertiesDTO ObtenerListadoDocumentos(int codigoPersona)
        {
            IList<ListadoDocumentosDTO> lstDocumentos = null;

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerListadoDocumentos?IdSGL=" + codigoPersona, token.access_token);

            var returnEntity = (dynamic)null;


            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ListadoDocumentosDTO>>();
                lstDocumentos = ((IEnumerable<ListadoDocumentosDTO>)returnEntity.Resultado).Where(x => x.CodigoTipoLegal != 16 && x.CodigoTipoLegal != 2).ToList();

                //for (int i = 0; i < lstDocumentos.Count(); i++)
                //{
                //    lstDocumentos[i].ContRegistro = i + 1;
                //    lstDocumentos[i].FechaInicio = FormateaFechaString(lstDocumentos[i].FechaInicio);//item.FechaInicio.Substring(0, 10).Equals("31/12/3000") ? string.Empty : item.FechaInicio.Substring(0, 10);
                //    lstDocumentos[i].FechaDecreto = FormateaFechaString(lstDocumentos[i].FechaDecreto);//item.FechaDecreto.Substring(0, 10).Equals("31/12/3000") ? string.Empty : item.FechaDecreto.Substring(0, 10);
                //    lstDocumentos[i].FechaPublicacionDecretoString = FormateaFechaString(lstDocumentos[i].FechaPublicacionDecreto.ToString("dd/MM/yyyy"));
                //    lstDocumentos[i].FechaIncorporacion = FormateaFechaString(lstDocumentos[i].FechaIncorporacion);
                //}

                returnEntity.Resultado = lstDocumentos;
            }

            return returnEntity;
        }

        public BasePropertiesDTO ObtenerDatosEstatutos(int codigoEstatuto)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlConsultaPoderes + "/ConsultaPersonaJuridicaDatos?codigoPersonaJuridica=" + codigoEstatuto, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaPersonaJuridicaDatosDTOResponse>>();
            }
            return returnEntity;
        }

        public BasePropertiesDTO ObtenerDatosCapital(string id, string codigoTipo, string sociedad)
        {
            string formato = string.Empty;
            PatrimonioDTO paramDatosEstatuto = null;
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            BasePropertiesDTO lstTipoPersona = GetListadoTipoPersonaDetalle();
            formato = ((IEnumerable<TipoPersonaDetalleDTO>)lstTipoPersona.Resultado).Where(x => x.Descripcion.Trim() == sociedad).FirstOrDefault().CodigoCapital.Trim();

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerListadoDocumentosCapital?id=" + id, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<DocumentoCapitalDTOResponse>>();

                paramDatosEstatuto = new PatrimonioDTO
                {
                    Id = id,
                    CodigoTipo = formato,
                    Monto = returnEntity.Resultado[0].MontoCapital == null ? 0 : returnEntity.Resultado[0].MontoCapital,
                    Moneda = string.IsNullOrEmpty(returnEntity.Resultado[0].DescripcionMoneda) ? "PESO CHILENO" : returnEntity.Resultado[0].DescripcionMoneda,
                    CantidadTotalAcciones = returnEntity.Resultado[0].CantidadTotalAcciones == null ? 0 : returnEntity.Resultado[0].CantidadTotalAcciones,
                    ValorNominalAccion = returnEntity.Resultado[0].ValorNominalAccion == null ? 0 : returnEntity.Resultado[0].ValorNominalAccion,
                    Descripcion = string.IsNullOrEmpty(returnEntity.Resultado[0].Descripcion) ? string.Empty : returnEntity.Resultado[0].Descripcion
                };

                //paramDatosEstatuto = ListaParticipante(paramDatosEstatuto);
                returnEntity.Resultado = paramDatosEstatuto;

            }
            return returnEntity;
        }
       

        public BasePropertiesDTO ObtenerListaParticipante(string id, string codigoTipo)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");
            PatrimonioDTO patrimonioDTO = new PatrimonioDTO();

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ConsultarDatosParticipantes?id=" + id + "&codigoTipo=" + codigoTipo);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<DatosParticipantesDTOResponse>>();

            }
            return returnEntity;
        }


        public PatrimonioDTO ListaParticipante(PatrimonioDTO patrimonioDTO)
        {
            string id, codigoTipo;
            id = patrimonioDTO.Id;
            codigoTipo = patrimonioDTO.CodigoTipo;
          

            return patrimonioDTO;
        }

        public BasePropertiesDTO ListarPoderesVigentes(PoderVigenteDTORequest paramPoderVigente)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlTransversal + "/ListarPoderesVigentes", paramPoderVigente, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<PoderVigentePorSociedadDTOResponse>>();
            }
            return returnEntity;
        }

        public BasePropertiesDTO ObtenerDocumentoLegal(int codigoDocumentoLegal)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerDocumentoLegal?codigoDocumentoLegal=" + codigoDocumentoLegal, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaAPI.Transversal.DTO.DocumentoLegalDTOResponse>>();
            }
            return returnEntity;
        }

        public BasePropertiesDTO ObtenerPoder(int codigoPoder)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerPoder?codigoPoder=" + codigoPoder, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<PoderDTOResponse>>();
            }
            return returnEntity;
        }

        public BasePropertiesDTO ListarFacultadesVigentes(int codigoPoder, string fechaConsulta)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ListarFacultadesVigentes?codigoPoder=" + codigoPoder + "&fechaConsulta=" + fechaConsulta, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<FacultadesVigentesPorPoderDTOResponse>>();
            }
            return returnEntity;
        }

        public BasePropertiesDTO ObtenerLimites(int codigoPoder, int CodigoFacultad, string fechaConsulta)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlConsultaPoderes + "/ObtenerLimites?codigoPoder=" + codigoPoder + "&CodigoFacultad=" + CodigoFacultad + "&fechaConsulta=" + fechaConsulta, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<LimitesDTOResponse>>();
            }
            return returnEntity;
        }

        public BasePropertiesDTO ListarPoderesFacultadesVigentes(PoderFacultadVigenteDTORequest paramPoderVigente)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlTransversal + "/ListarPoderesFacultadesVigentes", paramPoderVigente, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<PoderFacultadVigenteDTORequestDTOResponse>>();
            }
            return returnEntity;
        }


        public BasePropertiesDTO ConsultarFormasDeActuar(int codigoPoder)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ConsultarFormasDeActuar?CodigoPoder=" + codigoPoder, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaFormasDeActuarDTOResponse>>();
            }
            return returnEntity;
        }

        public BasePropertiesDTO ListarPoderesVigentesPersona(int codigoPersona, string fechaConsulta)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ListarPoderesVigentesPersona?codigoPersona=" + codigoPersona + "&fechaConsulta=" + fechaConsulta, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<PoderVigentePorPersonaDTOResponse>>();
            }
            return returnEntity;
        }

        public BasePropertiesDTO ConsultarNumeroMandatarios(ConsultaNumeroMandatariosDTORequest paramNumManda)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlTransversal + "/ConsultarNumeroMandatarios", paramNumManda, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaNumeroMandatariosDTOResponse>>();
            }
            return returnEntity;
        }


        public IList<FormasActuarDTO> ObtieneFormaActuar(IEnumerable<PoderVigentePorSociedadDTOResponse> resultado, MandatarioRutResponderDTO paramMandatarios, string fecha)
        {
            IList<GlosaResponderDTO> lstGlosa = new List<GlosaResponderDTO>();
            IList<FormasActuarDTO> lstFormaActuar = new List<FormasActuarDTO>();
            FormasActuarDTO paramFormaActuar = null;

            int intCodigoPoder = 0;
            int intCodigoDocto = 0;
            string strListaFactuarValidas = string.Empty;
            string glosa = string.Empty;
            string limitaciones = string.Empty;
            string strRetorno = string.Empty;
            string[] arrMandatarios;

            lstGlosa = CargarGlosa(resultado);

            foreach (var item in resultado)
            {
                intCodigoPoder = item.IdPoder;
                intCodigoDocto = item.DocumentoId;

                arrMandatarios = paramMandatarios.NombresMandatarios.Split(',');
                strListaFactuarValidas = ObtenerFActuarValidasV2(intCodigoPoder, intCodigoDocto, fecha, paramMandatarios.CodigosMandatarios, arrMandatarios);//ObtenerFActuarValidas(item, paramMandatarios, fecha);

                if (!strListaFactuarValidas.Equals(string.Empty))
                {
                    paramFormaActuar = new FormasActuarDTO();

                    strListaFactuarValidas = strListaFactuarValidas.Substring(0, strListaFactuarValidas.Length - 1);


                    paramFormaActuar.Glosa = ObtenerGlosa2(intCodigoPoder, strListaFactuarValidas, lstGlosa);

                    paramFormaActuar.LstLimitaciones = ObtenerListaLimitaciones(paramFormaActuar.LstLimitaciones, intCodigoPoder, paramMandatarios.CodigoFacultad, paramMandatarios.Fecha);


                    var lstVerificaGlosa = lstFormaActuar.Where(x => x.Glosa.Contains(paramFormaActuar.Glosa));
                    if (lstVerificaGlosa.Count() == 0)
                    {
                        lstFormaActuar.Add(paramFormaActuar);
                    }
                }
            }
            return lstFormaActuar;
        }

        public string ObtenerFActuarValidasV2(int codigoPoder, int codigoDcto, string fechaConsulta, string lstMandatarios, string[] arregloManda)
        {
            int contAux = 0;
            int cont = 0;
            int contFActuar = 0;
            int strCodigoGrupo = 0;
            int contSiFActuar = 0;
            int contNoFActuar = 0;
            int swPrimero = 0;
            string strListaFactuarValidas = string.Empty;
            int strCodGrupoComparacion = 0;
            int indiceMandatarios = 0;
            int strCodigoFActuar = 0;
            int strNumeroFirmas = 0;
            int nGrupos = 0;
            bool sale = false;

            BasePropertiesDTO formasActuarRetorno = ConsultarFormasDeActuar(codigoPoder);

            IList<ConsultaFormasDeActuarDTOResponse> lstFormasActuar = new List<ConsultaFormasDeActuarDTOResponse>();
            IList<ConsultaFormasDeActuarDTOResponse> lstFormasActuarAux = new List<ConsultaFormasDeActuarDTOResponse>();

            lstFormasActuar = formasActuarRetorno.Resultado;
            lstFormasActuarAux = lstFormasActuar;

            if (lstFormasActuar.Count > 0)
            {
                contFActuar = 0;
                contSiFActuar = 0;
                contNoFActuar = 0;
                strCodGrupoComparacion = 0;
                indiceMandatarios = 0;
                strCodigoFActuar = lstFormasActuar[0].CodigoFormaActuar;

                foreach (var itemFormActu in lstFormasActuar)
                {
                    nGrupos = CuentaGrupos(strCodigoFActuar, lstFormasActuar);

                    if (lstMandatarios.Contains(',') && nGrupos > 1)
                        sale = true;

                    if (!sale)
                    {
                        if (strCodigoFActuar.Equals(itemFormActu.CodigoFormaActuar))
                        {
                            foreach (var itemAux in lstFormasActuarAux)
                            {
                                if (strListaFactuarValidas.Contains(strCodigoFActuar.ToString()))
                                {
                                    break;
                                }

                                if (strCodigoFActuar == itemAux.CodigoFormaActuar)
                                {
                                    strNumeroFirmas = itemAux.NumeroFirmas;
                                    strCodigoGrupo = itemAux.CodigoGrupo;

                                    ConsultaNumeroMandatariosDTORequest paramNumManda = new ConsultaNumeroMandatariosDTORequest
                                    {
                                        ListaPersonas = lstMandatarios,
                                        CodigoDocumento = codigoDcto,
                                        CodigoGrupo = strCodigoGrupo,
                                        FechaConsulta = fechaConsulta //dd/MM/yyyy
                                    };

                                    BasePropertiesDTO totalRetorno = ConsultarNumeroMandatarios(paramNumManda);

                                    if (totalRetorno != null)
                                        strCodGrupoComparacion = totalRetorno.Resultado[0].Total;

                                    if (strNumeroFirmas <= strCodGrupoComparacion)
                                        contSiFActuar = 1;
                                    else
                                        contNoFActuar = 1;

                                    strCodGrupoComparacion = 0;
                                    contFActuar++;
                                    contAux++;
                                }
                            }

                            if (contSiFActuar == 1 && contNoFActuar == 1)
                            {
                                contNoFActuar = 0;
                                contSiFActuar = 0;
                                contFActuar = 0;

                                if (indiceMandatarios == arregloManda.Length)
                                {
                                    swPrimero = 0;
                                    indiceMandatarios = 0;
                                }
                                else
                                {
                                    swPrimero = 1;
                                    indiceMandatarios = indiceMandatarios + 1;
                                }
                            }
                            else
                            {
                                if (contSiFActuar == 0 && contNoFActuar == 1)
                                {
                                    contNoFActuar = 0;
                                    contSiFActuar = 0;
                                    contFActuar = 0;
                                    swPrimero = 1;

                                    if (indiceMandatarios == arregloManda.Length)
                                    {
                                        swPrimero = 0;
                                        indiceMandatarios = 0;
                                    }
                                    else
                                    {
                                        swPrimero = 1;
                                        indiceMandatarios = indiceMandatarios + 1;
                                    }
                                }

                                if (contSiFActuar == 1 && contNoFActuar == 0)
                                {
                                    if (strListaFactuarValidas.Equals(string.Empty))
                                    {
                                        strListaFactuarValidas = strListaFactuarValidas + strCodigoFActuar + ",";
                                    }
                                    else if (!strListaFactuarValidas.Contains(strCodigoFActuar.ToString()))
                                    {
                                        strListaFactuarValidas = strListaFactuarValidas + strCodigoFActuar + ",";
                                    }

                                    indiceMandatarios = 0;
                                    contNoFActuar = 0;
                                    contSiFActuar = 0;
                                    contFActuar = 0;

                                    if (contAux == lstFormasActuarAux.Count)
                                        break;
                                    else
                                        strCodigoFActuar = lstFormasActuarAux[contAux].CodigoFormaActuar;

                                }

                                if (contSiFActuar == 0 && contNoFActuar == 1)
                                {
                                    indiceMandatarios = 0;
                                    contNoFActuar = 0;
                                    contSiFActuar = 0;
                                    contFActuar = 0;

                                    if (contAux == lstFormasActuarAux.Count)
                                        break;
                                    else
                                        strCodigoFActuar = lstFormasActuarAux[contAux].CodigoFormaActuar;
                                }
                            }
                        }
                    }

                    if (swPrimero == 1)
                        swPrimero = 0;
                    else
                    {
                        cont++;
                        if (cont < lstFormasActuar.Count)
                        {
                            strCodigoFActuar = lstFormasActuar[cont].CodigoFormaActuar;
                            cont--;
                        }
                        else
                            cont--;
                    }
                    cont++;
                }
            }

            return strListaFactuarValidas;
        }

        private int CuentaGrupos(int strCodigoFActuar, IList<ConsultaFormasDeActuarDTOResponse> lstFormasActuar)
        {
            int cuentaGrupos = 0;

            foreach (var item in lstFormasActuar)
            {
                if (item.CodigoFormaActuar == strCodigoFActuar)
                    cuentaGrupos++;
                else
                    break;
            }
            return cuentaGrupos;
        }

        public IList<GlosaResponderDTO> CargarGlosa(IEnumerable<PoderVigentePorSociedadDTOResponse> resultado)
        {
            IList<GlosaResponderDTO> lstGlosa = new List<GlosaResponderDTO>();
            GlosaResponderDTO paramGlosa = null;

            foreach (var item in resultado)
            {
                paramGlosa = new GlosaResponderDTO();
                paramGlosa.IdPoder = item.IdPoder;
                paramGlosa.Glosa = item.Glosa;
                paramGlosa.IdFactuar = item.FormaDeActuarId;

                lstGlosa.Add(paramGlosa);
            }

            return lstGlosa;
        }

        public string ObtenerGlosa2(int intCodigoPoder, string strListaFactuarValidas, IList<GlosaResponderDTO> lstGlosa)
        {
            string cadena = string.Empty;

            foreach (var item in lstGlosa)
            {
                if (item.IdPoder == intCodigoPoder)
                {
                    if (strListaFactuarValidas.Contains(item.IdFactuar.ToString()))
                    {
                        cadena = cadena + item.Glosa.Trim() + "<hr />";
                    }
                }
            }
            return cadena;
        }

        public List<Limitaciones> ObtenerListaLimitaciones(List<Limitaciones> lstLimitaciones, int intCodigoPoder, int codigoFacultad, string fecha)
        {
            string strCadena = string.Empty;
            string strLimit = string.Empty;
            string strLimitText = string.Empty;
            string strLimitMonto = string.Empty;
            string strLimitCta = string.Empty;
            string strMonto = string.Empty;
            Limitaciones paramLimi = null;

            BasePropertiesDTO limitacionFacultPoder = ObtenerLimites(intCodigoPoder, codigoFacultad, fecha);

            if (limitacionFacultPoder.Resultado != null)
            {
                foreach (var item in limitacionFacultPoder.Resultado)
                {
                    strMonto = string.Empty;
                    paramLimi = new Limitaciones();

                    switch (item.CodigoTipoLimite)
                    {
                        case 1:
                            paramLimi.LimitText = item.TextoLimite;
                            //strLimitText = strLimitText + item.TextoLimite;
                            lstLimitaciones.Add(paramLimi);
                            break;

                        case 2:
                            if (item.CodigoMoneda.Equals("999"))
                                //strMonto = item.MontoMaximo;
                                paramLimi.Monto = item.MontoMaximo;
                            else
                                paramLimi.Monto = item.MontoMaximo;
                            //strMonto = item.MontoMaximo; //ver bien la logica

                            //strLimitMonto = strLimitMonto + item.DescripcionMoneda + strMonto;
                            paramLimi.DescripcionMoneda = item.DescripcionMoneda;

                            lstLimitaciones.Add(paramLimi);
                            break;

                        case 3:
                        case 4:
                            if (item.MontoMaximo > 0)
                            {
                                if (item.CodigoMoneda.Equals("999"))
                                    paramLimi.Monto = item.MontoMaximo;
                                //strMonto = item.MontoMaximo;
                                else
                                    paramLimi.Monto = item.MontoMaximo;
                                //strMonto = item.MontoMaximo;//ver bien la logica

                                //strLimitCta = strLimitCta + item.NumeroOperacion + item.DescripcionMoneda + strMonto;
                                paramLimi.NumeroOperacion = item.NumeroOperacion;
                                paramLimi.DescripcionMoneda = item.DescripcionMoneda;
                            }
                            else
                            {
                                //strLimitCta = strLimitCta + item.NumeroOperacion;
                                paramLimi.LimitCta = item.NumeroOperacion;
                            }

                            lstLimitaciones.Add(paramLimi);
                            break;

                        default:
                            break;
                    }
                }
            }

            return lstLimitaciones;
        }

        public MandatarioRutResponderDTO RespConsultaPoderesResponder(MandatarioRutResponderDTO paramModal)
        {
            int cantidadMand = 0;
            int cantidadFacul = 0;

            paramModal.NombresMandatarios = paramModal.NombresMandatarios.Trim();
            paramModal.RutsMandatarios = paramModal.RutsMandatarios.Trim();
            paramModal.CodigosMandatarios = paramModal.CodigosMandatarios.Trim();

            paramModal.NombresMandatarios = paramModal.NombresMandatarios.Remove(paramModal.NombresMandatarios.Length - 1);//Substring(0, paramModal.NombresMandatarios.Length - 1);
            paramModal.RutsMandatarios = paramModal.RutsMandatarios.Remove(paramModal.RutsMandatarios.Length - 1);//.Substring(0, paramModal.RutsMandatarios.Length - 1);
            paramModal.Dvs = paramModal.Dvs.Remove(paramModal.Dvs.Length - 1);
            paramModal.CodigosMandatarios = paramModal.CodigosMandatarios.Remove(paramModal.CodigosMandatarios.Length - 1);//.Substring(0, paramModal.CodigosMandatarios.Length - 1);

            paramModal.Fecha = Convert.ToDateTime(paramModal.Fecha).ToString("dd/MM/yyyy");
            paramModal.ArrNombres = paramModal.NombresMandatarios.Split(',');
            paramModal.ArrRuts = paramModal.RutsMandatarios.Split(',');
            paramModal.ArrDvs = paramModal.Dvs.Split(',');
            paramModal.CantidadManda = paramModal.ArrNombres.Length;
            paramModal.CantidadFacul = string.IsNullOrEmpty(paramModal.NombreFacultad) ? 0 : 1;

            cantidadMand = paramModal.CantidadManda;
            cantidadFacul = paramModal.CantidadFacul;

            return paramModal;
        }

        public Tuple<MandatarioRutResponderDTO, string[]> RespConsultaSoloMandatarios(MandatarioRutResponderDTO paramModal)
        {
            int codigoPersona = paramModal.CodigoPersona;
            string fechaConsulta = Convert.ToDateTime(paramModal.Fecha).ToString("dd/MM/yyyy");
            string[] NombresMandatarios = null;

            Tuple<MandatarioRutResponderDTO, string[]> Respuesta = Tuple.Create<MandatarioRutResponderDTO, string[]>(paramModal, NombresMandatarios);

            paramModal.NombresMandatarios = paramModal.NombresMandatarios.Trim();
            paramModal.RutsMandatarios = paramModal.RutsMandatarios.Trim();
            paramModal.CodigosMandatarios = paramModal.CodigosMandatarios.Trim();

            paramModal.NombresMandatarios = paramModal.NombresMandatarios.Remove(paramModal.NombresMandatarios.Length - 1);//Substring(0, paramModal.NombresMandatarios.Length - 1);
            paramModal.RutsMandatarios = paramModal.RutsMandatarios.Remove(paramModal.RutsMandatarios.Length - 1);//.Substring(0, paramModal.RutsMandatarios.Length - 1);
            paramModal.Dvs = paramModal.Dvs.Remove(paramModal.Dvs.Length - 1);
            paramModal.CodigosMandatarios = paramModal.CodigosMandatarios.Remove(paramModal.CodigosMandatarios.Length - 1);//.Substring(0, paramModal.CodigosMandatarios.Length - 1);

            paramModal.Fecha = Convert.ToDateTime(paramModal.Fecha).ToString("dd/MM/yyyy");
            paramModal.ArrNombres = paramModal.NombresMandatarios.Split(',');
            paramModal.ArrRuts = paramModal.RutsMandatarios.Split(',');
            paramModal.ArrDvs = paramModal.Dvs.Split(',');
            paramModal.CantidadManda = paramModal.ArrNombres.Length;
            paramModal.CantidadFacul = string.IsNullOrEmpty(paramModal.NombreFacultad) ? 0 : 1;
            NombresMandatarios = paramModal.NombresMandatarios.Split(',');

            return Respuesta;
        }


        public string RespObtieneConsultaMandatario(MandatarioRutResponderDTO paraModal)
        {
            string strCadena = string.Empty;
            string strListaFactuarValidas = string.Empty;
            int intCodigoDocto = 0;
            string strFechaIniPoder = string.Empty;
            string strEncabezado = string.Empty;
            string strTitulo = string.Empty;
            string strCodigoMoneda = string.Empty;
            int intCodigoPoder = 0;
            string strGlosa = string.Empty;
            string strLimit = string.Empty;
            int lngDocCod = 0;
            string strNotaria = string.Empty;
            IList<GlosaResponderDTO> lstGlosa = new List<GlosaResponderDTO>();

            strEncabezado = "<table class=\"table table-striped table-bordered\" style=\"width: 100%\">";
            strEncabezado = strEncabezado + "<thead class=\"thead-dark\">";
            strEncabezado = strEncabezado + "<tr>";
            strEncabezado = strEncabezado + "<th Width=40% align=center>Facultad</th>";
            strEncabezado = strEncabezado + "<th Width=45% align=center>Limitaciones</th>";
            strEncabezado = strEncabezado + "<th Width=35% align=center>Fec.Ingreso</th>";
            strEncabezado = strEncabezado + "</tr>";
            strEncabezado = strEncabezado + "</thead>";

            BasePropertiesDTO PoderesVigentesPersona = ListarPoderesVigentesPersona(paraModal.CodigoPersona, paraModal.Fecha);

            if (PoderesVigentesPersona.Resultado.Count > 0)
            {
                lstGlosa = CargarGlosaPorFacultad(paraModal.CodigoPersona, paraModal.Fecha);

                foreach (var item in PoderesVigentesPersona.Resultado)
                {
                    lngDocCod = item.CodigoDocumento;
                    strNotaria = ObtenerDocumentoLegal(lngDocCod).Resultado[0].Notaria;
                    intCodigoPoder = item.CodigoPoder;
                    strFechaIniPoder = item.FechaInicioDocumento;

                    strListaFactuarValidas = ObtenerFActuarValidasV2(intCodigoPoder, intCodigoDocto, paraModal.Fecha, paraModal.CodigosMandatarios, paraModal.ArrNombres);

                    if (!strListaFactuarValidas.Equals(string.Empty))
                    {
                        strListaFactuarValidas = strListaFactuarValidas.Remove(strListaFactuarValidas.Length - 1);
                        strGlosa = ObtenerGlosa2(intCodigoPoder, strListaFactuarValidas, lstGlosa);

                        if (!strGlosa.Equals(string.Empty))
                        {
                            strCadena = strCadena + "<div class=\"text-left\">";
                            strCadena = strCadena + "<div class=\"form-group\">";
                            strCadena = strCadena + "<label style=\"font-size: 15px;\">Fecha Instrumento:</label>";
                            strCadena = strCadena + "<input value=\"" + item.FechaDocumento.Replace("0:00:00", "").Replace("12:00:00 AM", "").Replace("-", "/") + "\" id=\"nombre\" class=\"form-control\" readonly />";
                            strCadena = strCadena + "</div>";

                            strCadena = strCadena + "<div class=\" text-left\">";
                            strCadena = strCadena + "<div class=\"form-group\">";
                            strCadena = strCadena + "<label style=\"font-size: 15px;\">Notaria:</label>";
                            strCadena = strCadena + "<input value=\"" + strNotaria + "\" id=\"notaria\" class=\"form-control\" readonly />";
                            strCadena = strCadena + "</div>";


                            strCadena = strCadena + strTitulo;

                            strCadena = strCadena + "<tr><td colspan=3 align=left width=36%>&nbsp;" + strGlosa + "</td></tr>";

                            strLimit = ObtenerFacuYLim(intCodigoPoder, paraModal.Fecha, strFechaIniPoder);

                            strCadena = strCadena + "<div>";
                            strCadena = strCadena + strEncabezado;
                            strCadena = strCadena + "<tbody>";
                            strCadena = strCadena + strLimit;
                            strCadena = strCadena + "</tbody>";
                            strCadena = strCadena + "</table>";
                            strCadena = strCadena + "</div>";
                        }
                    }
                }
            }

            return strCadena;
        }

        public IList<GlosaResponderDTO> CargarGlosaPorFacultad(int codigoPersona, string fechaConsulta)
        {
            int tipopod = 0;
            string tipoRespu = "1";
            int estadoCsu = 0;

            IList<GlosaResponderDTO> lstGlosa = new List<GlosaResponderDTO>();
            GlosaResponderDTO paramGlosa = null;

            PoderVigenteDTORequest paramPoderesVig = new PoderVigenteDTORequest
            {
                CodigoPersona = codigoPersona,
                TipoPoder = tipopod,
                FechaBusqueda = fechaConsulta,
                TipoRespuesta = tipoRespu,
                EstadoConsulta = estadoCsu
            };

            BasePropertiesDTO PoderesVigentes = ListarPoderesVigentes(paramPoderesVig);

            foreach (var item in PoderesVigentes.Resultado)
            {
                paramGlosa = new GlosaResponderDTO();
                paramGlosa.IdPoder = item.IdPoder;
                paramGlosa.Glosa = item.Glosa;
                paramGlosa.IdFactuar = item.FormaDeActuarId;

                lstGlosa.Add(paramGlosa);
            }
            return lstGlosa;
        }

        private string ObtenerFacuYLim(int intCodPoder, string fechaConsulta, string fechaIniPoder)
        {
            string strCadena = string.Empty;
            string strLimit = string.Empty;
            int intCodGrp = 0;
            string grupo = string.Empty;
            int codFacultad = 0;

            BasePropertiesDTO FacultadesVigentes = ObtenerFacultadesVigentes(intCodPoder, fechaConsulta);


            var lstGrupos = from s in (IEnumerable<FacultadVigenteDTOResponse>)FacultadesVigentes.Resultado
                            group s by s.CodigoGrupo;

            foreach (var itemFac in lstGrupos)
            {
                intCodGrp = itemFac.Key;
                grupo = ((IEnumerable<FacultadVigenteDTOResponse>)FacultadesVigentes.Resultado).Where(x => x.CodigoGrupo == intCodGrp).FirstOrDefault().Grupo;
                codFacultad = ((IEnumerable<FacultadVigenteDTOResponse>)FacultadesVigentes.Resultado).Where(x => x.CodigoGrupo == intCodGrp).FirstOrDefault().CodigoFacultad;

                strCadena = strCadena + "<tr class=\"bg-primary text-center text-light\">";
                strCadena = strCadena + "<td style=\"font-size: 15px\" colspan='3'>" + grupo.Trim() + "</td>";
                strCadena = strCadena + "</tr>";

                foreach (var itemFacAux in FacultadesVigentes.Resultado)
                {
                    if (!itemFacAux.CodigoGrupo.Equals(intCodGrp))
                        continue;

                    strCadena = strCadena + "<tr>";
                    strCadena = strCadena + "<td>" + itemFacAux.Descripcion.Trim() + "</td>";

                    strCadena = strCadena + "<td>";
                    strLimit = string.Empty;

                    strLimit = ObtenerLimitacionesFacultad(intCodPoder, codFacultad, fechaConsulta);

                    if (!strLimit.Equals(string.Empty))
                        strCadena = strCadena + strLimit;
                    else
                        strCadena = strCadena + " ";

                    strCadena = strCadena + "</td>";

                    //Agregar fecha ini. poder
                    if (!fechaIniPoder.Equals(string.Empty))
                        strCadena = strCadena + "<td align=center>" + fechaIniPoder.Replace("0:00:00", "").Replace("12:00:00 AM", "").Replace("-", "/") + "</td>";
                    else
                        strCadena = strCadena + "</tr>";
                }
            }

            return strCadena;
        }

        public string ObtieneConsultaMandaFacul(MandatarioRutResponderDTO paramModal)
        {
            IList<GlosaResponderDTO> lstGlosa = new List<GlosaResponderDTO>();
            string strEncabezado = string.Empty;
            string strTitulo = string.Empty;
            string strCadena = string.Empty;
            int codigoPersona = paramModal.CodigoPersona;
            int codigoFacultad = paramModal.CodigoFacultad;
            string fecha = paramModal.Fecha;
            string strDescripFacu = string.Empty;
            int intCodigoPoder = 0;
            int intCodigoDocto = 0;
            string strListaFactuarValidas = string.Empty;
            string[] arrMandatarios;
            string strGlosa = string.Empty;
            string strLimit = string.Empty;

            strTitulo = "<h4>Forma de Actuar</h4>";

            strEncabezado = "<table class=\"table table-striped table-bordered\" style=\"width: 100%\">";
            strEncabezado = strEncabezado + "<thead class=\"thead-dark\">";
            strEncabezado = strEncabezado + "<tr>";
            strEncabezado = strEncabezado + "<th Width=40% align=center>Facultad</th>";
            strEncabezado = strEncabezado + "<th Width=45% align=center>Limitaciones</th>";
            strEncabezado = strEncabezado + "<th Width=35% align=center>Fec.Ingreso</th>";
            strEncabezado = strEncabezado + "</tr>";
            strEncabezado = strEncabezado + "</thead>";

            PoderFacultadVigenteDTORequest paramFacultadVig = new PoderFacultadVigenteDTORequest
            {
                CodigoPersona = codigoPersona,
                CodigoFacultad = codigoFacultad,
                FechaBusqueda = fecha
            };

            BasePropertiesDTO datosPersonaRetorno = ListarPoderesFacultadesVigentes(paramFacultadVig);

            strDescripFacu = datosPersonaRetorno.Resultado[0].DescripcionFacultad;
            lstGlosa = CargarGlosaPorFacultad(codigoPersona, fecha);

            foreach (var item in datosPersonaRetorno.Resultado)
            {
                intCodigoPoder = item.CodigoPoder;
                intCodigoDocto = item.CodigoDocumento;
                arrMandatarios = paramModal.NombresMandatarios.Split(',');

                strListaFactuarValidas = ObtenerFActuarValidasV2(intCodigoPoder, intCodigoDocto, fecha, paramModal.CodigosMandatarios, arrMandatarios);

                if (!strListaFactuarValidas.Equals(string.Empty))
                {
                    strListaFactuarValidas = strListaFactuarValidas.Remove(strListaFactuarValidas.Length - 1);
                    strGlosa = ObtenerGlosa2(intCodigoPoder, strListaFactuarValidas, lstGlosa);

                    strCadena = strCadena + strTitulo;
                    strCadena = strCadena + "<p align=\"left\">" + strGlosa + "</p>";
                    strLimit = ObtenerLimitacionesFacultad(intCodigoPoder, codigoFacultad, fecha); //ObtenerLimitaciones(intCodigoPoder, CodigoFacultad, FechaConsulta);


                    strCadena = strCadena + "<tr><td>";
                    strCadena = strCadena + strEncabezado;
                    strCadena = strCadena + "<tr>";
                    strCadena = strCadena + "<td>" + strDescripFacu.Trim() + "</td>";
                    strCadena = strCadena + "<td align=left>" + strLimit + "</td>";
                    strCadena = strCadena + "<td align=center>" + item.FechaInicioDocumento.Replace("0:00:00", "").Replace("12:00:00 AM", "").Replace("-", "/") + "</td>";
                    strCadena = strCadena + "</tr>";
                    strCadena = strCadena + "</TABLE>";
                    strCadena = strCadena + "</td></tr>";
                    strCadena = strCadena + "<tr style=&quot;background-color: transparent;&quot;><td>&nbsp;</td></tr>";
                }
            }

            return strCadena;
        }

        public string ObtenerLimitacionesFacultad(int intCodigoPoder, int codigoFacultad, string fechaConsulta)
        {
            string strMonto = string.Empty;
            string strCadena = string.Empty;
            string strLimit = string.Empty;
            string strLimitText = string.Empty;
            string strLimitMonto = string.Empty;
            string strLimitCta = string.Empty;

            BasePropertiesDTO datosPersonaRetorno = ObtenerLimites(intCodigoPoder, codigoFacultad, fechaConsulta);

            foreach (var item in datosPersonaRetorno.Resultado)
            {
                strMonto = string.Empty;

                switch (item.CodigoTipoLimite)
                {
                    case 1:
                        strLimitText = strLimitText + item.TextoLimite.Trim();
                        break;
                    case 2:
                        if (item.CodigoMoneda.Equals("999"))
                            strMonto = item.MontoMaximo;
                        else
                            strMonto = item.MontoMaximo;

                        strLimitMonto = strLimitMonto + item.DescripcionMoneda + "&nbsp;" + strMonto;

                        break;
                    case 3:
                    case 4:
                        if (item.MontoMaximo > 0)
                        {
                            if (item.CodigoMoneda.Equals("999"))
                                strMonto = item.MontoMaximo;

                            else
                                strMonto = item.MontoMaximo;



                            strLimitCta = strLimitCta + item.NumeroOperacion + " " + item.DescripcionMoneda + " " + strMonto;
                        }
                        else
                        {
                            strLimitCta = strLimitCta + item.NumeroOperacion + " ";
                        }

                        break;
                    default:
                        break;
                }
            }

            if (strLimitMonto.Length > 0)
                strCadena = strCadena + "<label>Monto Máximo</label>: " + strLimitMonto.Trim() + "<br>";
            if (strLimitCta.Length > 0)
                strCadena = strCadena + "<label>Cuenta</label>: " + strLimitCta.Trim() + "<br>";
            if (strLimitText.Length > 0)
                strCadena = strCadena + "<label>Texto</label>: " + strLimitText.Trim() + "<br>";

            return strCadena;
        }

        public string ObtenerGlosaFacultad(int intCodigoPoder, int opcion, IList<GlosaResponderDTO> lstGlosa)
        {
            string strCadena = string.Empty;

            foreach (var item in lstGlosa)
            {
                if (item.IdPoder == intCodigoPoder)
                {
                    switch (opcion)
                    {
                        case 0:
                            break;
                        case 1:
                            strCadena = strCadena + item.Glosa + "<br>";
                            break;
                        default:
                            break;
                    }
                }
            }
            return strCadena;
        }

        public string RespObtieneConsultaFacultad(int codigoPersona, int codigoFacultad, string fechaConsulta)
        {
            string strCadena = string.Empty;
            string strEncabezado = string.Empty;
            int intCodigoPoder = 0;
            string strGlosa = string.Empty;
            string strLimit = string.Empty;
            string respuesta = string.Empty;
            IList<GlosaResponderDTO> lstGlosa = new List<GlosaResponderDTO>();

            PoderFacultadVigenteDTORequest paramFacultadVig = new PoderFacultadVigenteDTORequest
            {
                CodigoPersona = codigoPersona,
                CodigoFacultad = codigoFacultad,
                FechaBusqueda = fechaConsulta
            };

            BasePropertiesDTO datosPersonaRetorno = ListarPoderesFacultadesVigentes(paramFacultadVig);

            strEncabezado = strEncabezado + "<h5>La facultad seleccionada puede ser ejercida de la siguiente forma:</h5>";
            strEncabezado = strEncabezado + "<div>";
            strEncabezado = strEncabezado + "<table class=\"table table-striped table-bordered\" style=\"width: 100%\">";
            strEncabezado = strEncabezado + "<thead class=\"thead-dark\">";
            strEncabezado = strEncabezado + "<tr>";
            strEncabezado = strEncabezado + "<th align=center>Forma de Actuar</th>";
            strEncabezado = strEncabezado + "<th align=center>Fec.Ingreso</th>";
            strEncabezado = strEncabezado + "</tr>";
            strEncabezado = strEncabezado + "</thead>";

            if (datosPersonaRetorno != null)
            {
                lstGlosa = CargarGlosaPorFacultad(codigoPersona, fechaConsulta);

                strCadena = strCadena + "<tbody>";

                foreach (var item in datosPersonaRetorno.Resultado)
                {
                    intCodigoPoder = item.CodigoPoder;

                    strGlosa = ObtenerGlosaFacultad(intCodigoPoder, 1, lstGlosa);

                    strLimit = ObtenerLimitacionesFacultad(intCodigoPoder, codigoFacultad, fechaConsulta);

                    if (!strLimit.Equals(string.Empty))
                    {
                        strGlosa = strGlosa + "<hr />" + "<label>Limitaciones:</label>" + "<br>";
                        strGlosa = strGlosa + strLimit;
                    }

                    strCadena = strCadena + "<tr><td align=left>" + strGlosa + "</td>";
                    strCadena = strCadena + "<td align=center>" + item.FechaInicioDocumento.Replace("0:00:00", "").Replace("12:00:00 AM", "").Replace("-", "/") + "</td></tr>";
                }


                strCadena = strCadena + "</tbody>";
                strCadena = strCadena + "</table>";
                strCadena = strCadena + "</div>";

                if (!strCadena.Equals(string.Empty))
                    strCadena = strEncabezado + strCadena;
            }

            if (strGlosa.Equals(string.Empty) && strLimit.Equals(string.Empty))
                respuesta = "<h3>No hay apoderados habilitados para ejercer la facultad consultada</h3>";
            else
            if (!strCadena.Equals(string.Empty))
                respuesta = strCadena;

            return respuesta;
        }

        public string ObtieneConsultaMandatario(MandatarioRutResponderDTO paraModal)
        {
            string strCadena = RespObtieneConsultaMandatario(paraModal);

            return strCadena;
        }

        #endregion


        #region Validaciones

        private string FormateaFechaString(string fecha)
        {
            if (fecha.Contains("3000"))
                return string.Empty;
            else
                return fecha.Substring(0, 10);
        }

        public ConsultaParticipanteConPoderDTORequest ValidacionFecha(int codigoParticipante, int tipoParticipante, DateTime fechaConsulta)
        {
            if (fechaConsulta.Day == 1 && fechaConsulta.Month == 1 && fechaConsulta.Year == 1)
            {
                fechaConsulta = DateTime.Now;
            }
            ConsultaParticipanteConPoderDTORequest paramParticipante = new ConsultaParticipanteConPoderDTORequest
            {
                CodigoParticipante = codigoParticipante,
                TipoParticipante = tipoParticipante,
                FechaConsulta = fechaConsulta.ToString("dd/MM/yyyy")
            };
            return paramParticipante;
        }

        #endregion
    }
}
