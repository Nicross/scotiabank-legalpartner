﻿using CL.Scotiabank.LegalPartner.SGL.DAL;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Clase de servicios para la entidad BuscarCliente del sistema.
    /// </summary>
    public class BuscarClienteAPP : IBuscarClienteAPP
    {
        #region Members

        private readonly string urlConsulta = Resources.Consulta;
        private readonly string urlSucursalBancos = Resources.SucursalBancoUrl;

        #endregion

        #region Public Methods

        /// <summary>
        /// Obtiene el cliente buscando por el nombre legal
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public BasePropertiesDTO GetListaNombreLegal(string nombre)
        {
            var responseContent = HttpRequestHelper.Get(urlConsulta + "/GetClienteNombreLegal?nombre=" + nombre);

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<BuscarClienteDTO>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Obtiene el nombre legal anterior de una empresa segun su razon social
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public BasePropertiesDTO GetListaNombreLegalAnterior(string nombre)
        {
            var responseContent = HttpRequestHelper.Get(urlConsulta + "/GetClienteNombreLegalAnterior?nombre=" + nombre);

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<BuscarClienteDTO>>();
            }

            return returnEntity;
        }

        public BasePropertiesDTO GetListaClienteNombreFantasia(string nombre)
        {
            var responseContent = HttpRequestHelper.Get(urlConsulta + "/GetClienteNombreFantasia?nombre=" + nombre);

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<BuscarClienteDTO>>();
            }

            return returnEntity;
        }

        public BasePropertiesDTO GetListaMandatorios(int IdSociedad, int IdSGL)
        {
            var responseContent = HttpRequestHelper.Get(urlConsulta + "/ConsultarAllMandatorios?Codigo=" + IdSociedad + "&CodigoTipo=" + IdSGL);
            
            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<MandatorioDTO>>();
            }

            return returnEntity;
        }

        public BasePropertiesDTO GetSucursales(int rut, int idPersona)
        {
            var returnEntity = (dynamic)null;
            var listaSucursal = (dynamic)null;
            ConsultaPersonaDatosDTO datosPersona = new ConsultaPersonaDatosDTO();
            datosPersona.Rut = rut;
            var existeCliente = BuscarCliente(datosPersona);

            if (!existeCliente.Equals(null))
            {
                var listaSgl = ListaSgl(idPersona);

                if (listaSgl != null)
                {
                    var responseContentSucursal = HttpRequestHelper.Get(urlSucursalBancos);

                    if (responseContentSucursal != null && !responseContentSucursal.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
                    {
                        listaSucursal = responseContentSucursal.ContentAsType<IEnumerable<SucursalDTO>>();
                    }

                    ((List<SGLPorPersonaDTO>)listaSgl.Resultado).ForEach(sglPersona =>
                    {
                        if (((List<SucursalDTO>)listaSucursal.Resultado).Any(sucursal => sucursal.Codigo.Equals(sglPersona.SucursalId)))
                        {
                            sglPersona.Sucursal = ((List<SucursalDTO>)listaSucursal.Resultado)
                                                    .First(sucursal => sucursal.Codigo.Equals(sglPersona.SucursalId)).Descripcion;
                        }
                    });

                    returnEntity = listaSgl;
                }
            }

            return returnEntity;
        }

        #endregion

        #region PrivateMethods 

        private BasePropertiesDTO BuscarCliente(ConsultaPersonaDatosDTO rut)
        {
            var responseContent = HttpRequestHelper.Post(urlConsulta + "/ConsultaPersonaDatos", rut);
            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaPersonaDatosDTO>>();
            }

            return returnEntity;
        }

        private BasePropertiesDTO ListaSgl(int idPersona)
        {
            var responseContent = HttpRequestHelper.Get(urlConsulta + "/GetSGLPorCodigoPersona?id=" + idPersona);
            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<SGLPorPersonaDTO>>();
            }

            return returnEntity;
        }

        #endregion
    }
}
