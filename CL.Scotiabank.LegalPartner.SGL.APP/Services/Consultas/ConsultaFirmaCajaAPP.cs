﻿using CL.Scotiabank.LegalPartner.SGL.DAL;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.Linq;
using CL.Scotiabank.LegalPartner.SGL.DAL.RequestEntities;
using System;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Clase de servicios para la entidad ConsultafirmasCaja del sistema.
    /// </summary>
    public class ConsultaFirmaCajaAPP : IConsultaFirmaCajaAPP
    {
        #region Memebers

        private readonly string urlConsulta = Resources.Consulta;

        #endregion

        #region Public Methods

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de Consulta firmas Cajas.
        /// </summary>
        /// <returns>Lista de ConsultafirmasCajaDTO</returns>
        public IEnumerable<ConsultaFirmaCajaDTO> Get()
        {
            var responseContent = HttpRequestHelper.Get(urlConsulta);

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaFirmaCajaDTO>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de Consulta firmas Cajaes.
        /// </summary>
        /// <param name="id">ConsultafirmasCajaDTO</param>
        /// <returns></returns>
        public ConsultaFirmaCajaDTO Get(ConsultaFirmaCajaDTO entityToUse)
        {
            BasePropertiesDTO respuesta;
            
            if (entityToUse.DatosPersona.Rut != null && entityToUse.DatosPersona.Rut.Length > 0)
            {
                entityToUse.DatosPersona.Rut = entityToUse.DatosPersona.Rut.Replace(".", string.Empty).Split("-")[0];
                respuesta = InvocarBusquedaPorRut(entityToUse);
                entityToUse.DatosPersona = (DatosPersonaDTO)respuesta.Resultado[0];
            }
            else
            {
                respuesta = InvocarBusquedaPorCuenta(entityToUse);
            }

            if (respuesta != null && respuesta.Resultado.Count > 0 && respuesta.Resultado[0].CodigoPersona > 0)
            {
                respuesta = ObtenerSGLPorPersona(entityToUse.DatosPersona.CodigoPersona);
                entityToUse.ListadoDatosSGL = ((IEnumerable<DatosSGLDTO>)respuesta.Resultado);
                List<string> cuentaCorrientes = ObtenerProductosCliente(entityToUse.DatosPersona.Rut+entityToUse.DatosPersona.Dv);
            }

            if (entityToUse.ListadoDatosSGL != null && entityToUse.ListadoDatosSGL.Count() > 0 && entityToUse.ListadoDatosSGL.First().SGLId > 0)
            {
                return entityToUse;
            }

            return new ConsultaFirmaCajaDTO();
        }

        public List<string> ObtenerProductosCliente(string rut)
        {
            var responseContent = HttpRequestHelper.Get(urlConsulta + "/GetProductosCliente?rut=" + rut + "&codigoProducto=CCT");
            var returnEntity = (dynamic)null;
            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ProductosClienteDTO>>();
            }
            return returnEntity;
        }

        #endregion

        #region Private Methods

        private BasePropertiesDTO ObtenerSGLPorPersona(int codigoPersona)
        {
            var responseContent = HttpRequestHelper.Get(urlConsulta + "/GetSLGPorCodigoPersona?id=" + codigoPersona);
            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<DatosSGLDTO>>();
            }
            return returnEntity;
        }

        private BasePropertiesDTO InvocarBusquedaPorCuenta(ConsultaFirmaCajaDTO entityToUse)
        {
            var responseContent = HttpRequestHelper.Post(urlConsulta + "/ConsultaTitularPorOperacion", entityToUse);
            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaFirmaCajaDTO>>();
            }
            return returnEntity;
        }

        private BasePropertiesDTO InvocarBusquedaPorRut(ConsultaFirmaCajaDTO entityToUse)
        {
            entityToUse.CantidadRegistros = 0;
            ConsultaPersonaDatosDTORequest request = new ConsultaPersonaDatosDTORequest
            {
                Rut = entityToUse.DatosPersona.Rut,
                Nombre = string.Empty,
                ApellidoPaterno = string.Empty,
                ApellidoMaterno = string.Empty,
                CantidadRegistros = 0
            };
            var responseContent = HttpRequestHelper.Post(urlConsulta + "/ConsultaPersonaDatos", request);
            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<DatosPersonaDTO>>();
            }
            return returnEntity;
        }
        
        #endregion
    }
}
