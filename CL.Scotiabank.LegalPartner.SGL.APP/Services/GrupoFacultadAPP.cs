﻿using CL.Scotiabank.LegalPartner.SGL.DAL;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Clase de servicios para la entidad GrupoFacultad del sistema.
    /// </summary>
    public class GrupoFacultadAPP : IGrupoFacultadAPP
    {
        #region Memebers

        private ITokenAPP _tokenAPP;
        private IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string urlGrupoFacultad = Resources.GrupoFacultadUrl;

        #endregion

        #region Constructor

        public GrupoFacultadAPP(ITokenAPP tokenAPP, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            this._tokenAPP = tokenAPP;
            this._configuration = configuration;
            this._httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de conservadores.
        /// </summary>
        /// <returns>Lista de GrupoFacultadDTO</returns>
        public BasePropertiesDTO Get()
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlGrupoFacultad, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlGrupoFacultad, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<GrupoFacultadDTO>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de conservadores.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>GrupoFacultadDTO</returns>
        public BasePropertiesDTO Get(int id)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlGrupoFacultad+"/"+id, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlGrupoFacultad + "/" + id, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<GrupoFacultadDTO>();
            }
            return returnEntity;
        }


        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de conservadores.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>GrupoFacultadDTO</returns>
        public BasePropertiesDTO Create(GrupoFacultadDTO entityToUse)
        {
            var requestEntity = new GrupoFacultadDTORequest
            {
                Descripcion = entityToUse.Descripcion
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlGrupoFacultad, requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Post(urlGrupoFacultad, requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<GrupoFacultadDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Put) y el retorno de datos desde la api web de conservadores.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>GrupoFacultadDTO</returns>
        public BasePropertiesDTO Edit(GrupoFacultadDTO entityToUse)
        {
            var model = new GrupoFacultadDTORequest
            {
                Codigo = entityToUse.Codigo,
                Descripcion = entityToUse.Descripcion
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Put(urlGrupoFacultad + "/" + entityToUse.Codigo, model, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Put(urlGrupoFacultad + "/" + entityToUse.Codigo, model, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<GrupoFacultadDTO>();
            }

            return returnEntity;
        }
        
        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) y el retorno de datos desde la api web de conservadores.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>GrupoFacultadDTO</returns>
        public BasePropertiesDTO Delete(GrupoFacultadDTO entityToUse)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Delete(urlGrupoFacultad + "/" + entityToUse.Codigo, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Delete(urlGrupoFacultad + "/" + entityToUse.Codigo, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<GrupoFacultadDTO>();
            }

            return returnEntity;
        }


        #endregion
    }
}
