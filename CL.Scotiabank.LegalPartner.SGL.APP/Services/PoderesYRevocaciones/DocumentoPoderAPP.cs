﻿using CL.Scotiabank.LegalPartner.ConsultaAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.PoderYRevocacionesAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.DAL;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    public class DocumentoPoderAPP : IDocumentoPoderAPP
    {
        #region Members

        private ITokenAPP _tokenAPP;
        private IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string urlTransversal = Resources.TransversalUrl;
        private readonly string urlConsultaAPI = Resources.Consulta;
        private readonly string urlCabeceraSGL = Resources.CabeceraSGL;
        private readonly string urlConsulta = Resources.ConservadorUrl;
        private readonly string urlConservador = Resources.ConservadorUrl;
        private readonly string urlPoderes = Resources.PoderesUrl;
        #endregion

        #region Constructor
        public DocumentoPoderAPP(ITokenAPP tokenAPP, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            this._tokenAPP = tokenAPP;
            this._configuration = configuration;
            this._httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Public Methods

        #region Listar

        /// <summary>
        /// Metodo encargado de listar los documentos de poder.
        /// SP: svc_rcr_doc_per (1° SP)
        /// </summary>
        /// <returns>Lista de FacultadDTO</returns>
        public BasePropertiesDTO ListarDocumentos(int id)
        {
            //id = 3456; //PRUEBA
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerListadoDocumentos?id=" + id, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerListadoDocumentos?id=" + id, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<DocumentoDTOResponse>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de listar los documentos poder.
        /// </summary>
        /// SP: svc_rcr_doc_pdr (2° sp)
        /// <returns>Lista de datos poderes </returns>
        public IList<DatosPoderesAndDesMatDTOResponse> ObtenerDatosPoderes(int id, bool checkIn, int sgl)
        {            
            int theSGL = sgl;
            //id = 3456; //PRUEBA
            bool checkInEs = checkIn;

            DatosPoderesAndDesMatDTOResponse vistaDatosPoderes = null;
            IList<DatosPoderesAndDesMatDTOResponse> lstDatosPoderesDesMat = new List<DatosPoderesAndDesMatDTOResponse>();

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ConsultarDatosPoderes?id=" + id, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlTransversal + "/ConsultarDatosPoderes?id=" + id, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaDatosPoderDTOResponse>>();
            }

            var listaObtenerDatosPoderes = returnEntity.Resultado;

            if (listaObtenerDatosPoderes.Count == Convert.ToInt32(Resources.valorCero))
            {
                return lstDatosPoderesDesMat;
            }
            else
            {
                bool poderesEs = false;
                bool revocadoEs = false;
                bool noMostrarEs = false;

                ValidacionListaDatosPoderes(theSGL, checkInEs, ref vistaDatosPoderes, lstDatosPoderesDesMat, listaObtenerDatosPoderes, ref poderesEs, ref revocadoEs, ref noMostrarEs);
                
                return lstDatosPoderesDesMat;
            }
        }

        #region Métodos Privados Listar


        /// <summary>
        /// Metodo encargado de listar los poder.
        /// </summary>
        /// SP: svc_rcr_lst_pdr (3° sp)
        /// <returns>Lista de poderes de documento poder</returns>
        private BasePropertiesDTO ListarPoderes(int codigoDocumento)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerConsultaListaPoderes?id=" + codigoDocumento, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerConsultaListaPoderes?id=" + codigoDocumento, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaListaPoderesDTOResponse>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de listar las revocaciones.
        /// </summary>
        /// SP: svc_rcr_lst_rev (4° sp)
        /// <returns>Lista de Revocaciones</returns>
        private BasePropertiesDTO ListarRevocaciones(int codigoDocumento)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ConsultarListaRevocaciones?id=" + codigoDocumento, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlTransversal + "/ConsultarListaRevocaciones?id=" + codigoDocumento, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {                
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaListaRevocacionesDTOResponseB>>();

            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web ConsultaAPI.
        /// SP: svc_rcr_sgl (5° SP) 
        /// </summary>
        /// <param name="sgl"></param>
        /// <returns>Sgl filtrada por código</returns>
        private BasePropertiesDTO GetSGLPorCodigoSGL(int idSGL)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlConsultaAPI + "/GetSGLPorCodigoSGL?id=" + idSGL, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlConsultaAPI + "/GetSGLPorCodigoSGL?id=" + idSGL, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                if (responseContent.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaSGLPorSGLIdDTOResponse>>();
                }
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de obtener lso tipso de materias codigo de documento.
        /// </summary>
        /// SP: svc_rcr_tipo_mat_sel
        /// <returns></returns>
        private BasePropertiesDTO ObtenerTipoMateria(int idMateria)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerTipoMateria?tipoMateriaId=" + idMateria, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlCabeceraSGL + "/ObtenerTipoMateria?tipoMateriaId=" + idMateria, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaSGLPorSGLIdDTOResponse>>();

            }

            return returnEntity;
        }

        /// <summary>
        /// Métedo para llamar a todos los SP para validar la segunda consulta.
        /// </summary>
        /// <param name="theSGL"></param>
        /// <param name="checkInEs"></param>
        /// <param name="vistaDatosPoderes"></param>
        /// <param name="lstDatosPoderesDesMat"></param>
        /// <param name="listaObtenerDatosPoderes"></param>
        /// <param name="poderesEs"></param>
        /// <param name="revocadoEs"></param>
        /// <param name="noMostrarEs"></param>
        private void ValidacionListaDatosPoderes(int theSGL, bool checkInEs, ref DatosPoderesAndDesMatDTOResponse vistaDatosPoderes, IList<DatosPoderesAndDesMatDTOResponse> lstDatosPoderesDesMat, dynamic listaObtenerDatosPoderes, ref bool poderesEs, ref bool revocadoEs, ref bool noMostrarEs)
        {
            foreach (var lstObtenerDatosPoderes in listaObtenerDatosPoderes)
            {
                vistaDatosPoderes = new DatosPoderesAndDesMatDTOResponse();
                DatosPoderesAndDesMatDTOResponse retornoVistaDatosPoderes = new DatosPoderesAndDesMatDTOResponse();

                poderesEs = false;
                revocadoEs = false;
                noMostrarEs = false;
                
                BasePropertiesDTO listaListarPoderes = ListarPoderes(lstObtenerDatosPoderes.CodigoDocumento);

                var lstPoderes = listaListarPoderes.Resultado;

                if (lstPoderes.Count == Convert.ToInt32(Resources.valorCero))
                {
                    poderesEs = true;
                }
                               
                BasePropertiesDTO listaListarRevocaciones = ListarRevocaciones(lstObtenerDatosPoderes.CodigoDocumento);
                
                BasePropertiesDTO listaGetSGLPorCodigoSGL = GetSGLPorCodigoSGL(lstObtenerDatosPoderes.SglId);

                foreach (var lstRevocaciones in listaListarRevocaciones.Resultado)
                {                      
                    if (lstRevocaciones.TipoRevocacion == Convert.ToInt32(Resources.valorCero))
                    {
                        revocadoEs = true;
                        break;
                    }
                }
                
                foreach (var lstGetSGL in listaGetSGLPorCodigoSGL.Resultado)
                {                    
                    if ((lstGetSGL.MateriaId == Convert.ToInt32(Resources.materiaId1) || lstGetSGL.MateriaId == Convert.ToInt32(Resources.materiaId2) ||
                        lstGetSGL.MateriaId == Convert.ToInt32(Resources.materiaId3)) && revocadoEs == true && poderesEs == true)
                    {                        
                        noMostrarEs = true;
                        break;
                    }
                }

                if (checkInEs == true && noMostrarEs == false)
                {
                    retornoVistaDatosPoderes = AdaptadorDatosPoderesMatDes(vistaDatosPoderes, lstObtenerDatosPoderes);
                    AñadirDescripcionMateria(lstDatosPoderesDesMat, retornoVistaDatosPoderes, listaGetSGLPorCodigoSGL);
                }
                else if (theSGL == lstObtenerDatosPoderes.SglId && noMostrarEs == false)
                {
                    retornoVistaDatosPoderes = AdaptadorDatosPoderesMatDes(vistaDatosPoderes, lstObtenerDatosPoderes);
                    AñadirDescripcionMateria(lstDatosPoderesDesMat, retornoVistaDatosPoderes, listaGetSGLPorCodigoSGL);
                }
            }
        }

        /// <summary>
        /// Método adaptador de DTO
        /// </summary>
        /// <param name="vistaDatosPoderes"></param>
        /// <param name="lstObtenerDatosPoderes"></param>
        private DatosPoderesAndDesMatDTOResponse AdaptadorDatosPoderesMatDes(DatosPoderesAndDesMatDTOResponse vistaDatosPoderes, dynamic lstObtenerDatosPoderes)
        {
            vistaDatosPoderes.CodNotProtoc = lstObtenerDatosPoderes.CodNotProtoc;
            vistaDatosPoderes.CodigoOrigen = lstObtenerDatosPoderes.CodigoOrigen;
            vistaDatosPoderes.Origen = lstObtenerDatosPoderes.Origen;
            vistaDatosPoderes.NombreOrigen = lstObtenerDatosPoderes.NombreOrigen;
            vistaDatosPoderes.LugarOrigen = lstObtenerDatosPoderes.LugarOrigen;
            vistaDatosPoderes.Observaciones = lstObtenerDatosPoderes.Observaciones;
            vistaDatosPoderes.doc_est_csu = lstObtenerDatosPoderes.doc_est_csu;
            vistaDatosPoderes.FechaInicio = lstObtenerDatosPoderes.FechaInicio;
            vistaDatosPoderes.FechaTerminoPoder = lstObtenerDatosPoderes.FechaTerminoPoder.ToString("dd/MM/yyyy").Contains("3000") ? string.Empty : lstObtenerDatosPoderes.FechaTerminoPoder.ToString("dd/MM/yyyy").Replace("-", "/");
            vistaDatosPoderes.rev_cod = lstObtenerDatosPoderes.rev_cod;
            vistaDatosPoderes.doc_fec_rsl_qbr = lstObtenerDatosPoderes.doc_fec_rsl_qbr;
            vistaDatosPoderes.Juzgado = lstObtenerDatosPoderes.Juzgado;
            vistaDatosPoderes.doc_fec_pbl_qbr = lstObtenerDatosPoderes.doc_fec_pbl_qbr;
            vistaDatosPoderes.UsuarioResponasble = lstObtenerDatosPoderes.UsuarioResponasble;
            vistaDatosPoderes.sol_cod_uni_ofi = lstObtenerDatosPoderes.sol_cod_uni_ofi;
            vistaDatosPoderes.doc_nom_inf = lstObtenerDatosPoderes.doc_nom_inf;
            vistaDatosPoderes.EstadoPoderes = lstObtenerDatosPoderes.EstadoPoderes;
            vistaDatosPoderes.NotariaProtocolizacion = lstObtenerDatosPoderes.NotariaProtocolizacion;
            vistaDatosPoderes.FechaProtocolizacion = lstObtenerDatosPoderes.FechaProtocolizacion;
            vistaDatosPoderes.FechaPublicacionDecreto = lstObtenerDatosPoderes.FechaPublicacionDecreto;
            vistaDatosPoderes.NumeroDecreto = lstObtenerDatosPoderes.NumeroDecreto;
            vistaDatosPoderes.SglId = lstObtenerDatosPoderes.SglId;
            vistaDatosPoderes.CodigoDocumento = lstObtenerDatosPoderes.CodigoDocumento;
            vistaDatosPoderes.CodigoPersona = lstObtenerDatosPoderes.CodigoPersona;
            vistaDatosPoderes.CodogioTipoLegalizacion = lstObtenerDatosPoderes.CodogioTipoLegalizacion;
            vistaDatosPoderes.TipoLegalizacion = lstObtenerDatosPoderes.TipoLegalizacion;
            vistaDatosPoderes.FechaDocumento = lstObtenerDatosPoderes.FechaDocumento.ToString("dd/MM/yyyy").Contains("3000") ? string.Empty : lstObtenerDatosPoderes.FechaDocumento.ToString("dd/MM/yyyy").Replace("-", "/");
            vistaDatosPoderes.NumeroDocumento = lstObtenerDatosPoderes.NumeroDocumento;
            vistaDatosPoderes.CodigoNotaria = lstObtenerDatosPoderes.CodigoNotaria;
            vistaDatosPoderes.TipoDocumentoPoder = lstObtenerDatosPoderes.TipoDocumentoPoder;

            if (lstObtenerDatosPoderes.CodigoNotaria != Convert.ToInt32(Resources.valorCero))
            {
                vistaDatosPoderes.Notaria = lstObtenerDatosPoderes.Notaria;
            }
            else
            {
                vistaDatosPoderes.Notaria = string.Empty;
            }

            vistaDatosPoderes.AnnoInscripcion = lstObtenerDatosPoderes.AnnoInscripcion;
            vistaDatosPoderes.Fojas = lstObtenerDatosPoderes.Fojas;
            vistaDatosPoderes.FojaNumero = lstObtenerDatosPoderes.FojaNumero;
            vistaDatosPoderes.Conservador = lstObtenerDatosPoderes.Conservador;
            vistaDatosPoderes.FechaPublicacion = lstObtenerDatosPoderes.FechaPublicacion;
            vistaDatosPoderes.FechaSesionDirectorio = lstObtenerDatosPoderes.FechaSesionDirectorio.ToString("dd/MM/yyyy").Contains("3000") ? string.Empty : lstObtenerDatosPoderes.FechaSesionDirectorio.ToString("dd/MM/yyyy").Replace("-", "/");
            vistaDatosPoderes.doc_fec_jun_exa_aci = lstObtenerDatosPoderes.doc_fec_jun_exa_aci;
            vistaDatosPoderes.FechaDecreto = lstObtenerDatosPoderes.FechaDecreto;
            vistaDatosPoderes.FechaInscripcion = lstObtenerDatosPoderes.FechaInscripcion;

            if (lstObtenerDatosPoderes.rev_cod > Convert.ToInt32(Resources.valorCero))
            {
                vistaDatosPoderes.DescripcionTipoPoder = lstObtenerDatosPoderes.DescripcionTipoPoder + "  " + Convert.ToString(Resources.Revocado);
            }
            else
            {
                vistaDatosPoderes.DescripcionTipoPoder = lstObtenerDatosPoderes.DescripcionTipoPoder;
            }

            return vistaDatosPoderes;
        }

        /// <summary>
        /// Método para llamar 6° SP svc_rcr_tipo_mat_sel   y añadir un campo a la lista enviada finalmente.
        /// </summary>
        /// <param name="lstDatosPoderesDesMat"></param>
        /// <param name="retornoVistaDatosPoderes"></param>
        /// <param name="listaGetSGLPorCodigoSGL"></param>
        private void AñadirDescripcionMateria(IList<DatosPoderesAndDesMatDTOResponse> lstDatosPoderesDesMat, DatosPoderesAndDesMatDTOResponse retornoVistaDatosPoderes, BasePropertiesDTO listaGetSGLPorCodigoSGL)
        {
            foreach (var lstGetSglPorCodigo in listaGetSGLPorCodigoSGL.Resultado)
            {             
                BasePropertiesDTO listaObtenerTipoMateria = ObtenerTipoMateria(lstGetSglPorCodigo.MateriaId);

                foreach (var lstTipoMateria in listaObtenerTipoMateria.Resultado)
                {
                    retornoVistaDatosPoderes.DescripcionMateria = lstTipoMateria.DescripcionMateria;
                    break;
                }
                break;
            }
            lstDatosPoderesDesMat.Add(retornoVistaDatosPoderes);
        }

        #endregion M. Privados  

        #endregion Listar

        #region Crear 

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno al crear un Documento de Poder.
        /// </summary>
        /// <returns>Ingresa DocumentopoderDTO</returns>
        public BasePropertiesDTO IngresarDocumentoPoder(DocumentoPoderDTO entityToUse)
        {

            if (entityToUse.FechaDocumento == "")
            {
                entityToUse.FechaDocumento = "31/12/3000";
            }
            if (entityToUse.FechaInscripcion == "")
            {
                entityToUse.FechaInscripcion = "31/12/3000";
            }
            if (entityToUse.FechaPublicacion == "")
            {
                entityToUse.FechaPublicacion = "31/12/3000";
            }
            if (entityToUse.FechaSesionDirectorio == "")
            {
                entityToUse.FechaSesionDirectorio = "31/12/3000";
            }
            if (entityToUse.FechaJuntaAccionista == "")
            {
                entityToUse.FechaJuntaAccionista = "31/12/3000";
            }
            if (entityToUse.FechaDecreto == "")
            {
                entityToUse.FechaDecreto = "31/12/3000";
            }
            if (entityToUse.FechaPublicacionDecreto == "")
            {
                entityToUse.FechaPublicacionDecreto = "31/12/3000";
            }
            if (entityToUse.FechaProtocolarizacion == "")
            {
                entityToUse.FechaProtocolarizacion = "31/12/3000";
            }
            if (entityToUse.FechaInicio == "")
            {
                entityToUse.FechaInicio = "31/12/3000";
            }
            if (entityToUse.FechaFinal == "")
            {
                entityToUse.FechaFinal = "31/12/3000";
            }
            if (entityToUse.FechaResolucionQuiebra == "")
            {
                entityToUse.FechaResolucionQuiebra = "31/12/3000";
            }
            if (entityToUse.FechaPublicacionQuiebra == "")
            {
                entityToUse.FechaPublicacionQuiebra = "31/12/3000";
            }
            if (entityToUse.FechaActVigencia == "")
            {
                entityToUse.FechaActVigencia = "31/12/3000";
            }
            if (entityToUse.FechaTerminaVigencia == "")
            {
                entityToUse.FechaTerminaVigencia = "31/12/3000";
            }

            var requestEntity = new IngresarDatosDocumentosDTORequest
            {
                CodigoPersona = entityToUse.CodigoPersona,
                IdSGL = entityToUse.IdSGL,
                CodigoTipoLegal = entityToUse.CodigoTipoLegal,               
                FechaDocumento = entityToUse.FechaDocumento,
                NumeroDocumento = entityToUse.NumeroDocumento,
                CodigoNotaria = entityToUse.CodigoNotaria,
                FechaInscripcion = entityToUse.FechaInscripcion,
                AnnoInscripcion = entityToUse.AnnoInscripcion,
                Fojas = entityToUse.Fojas,
                NumeroFoja = entityToUse.NumeroFoja,
                CodigoConservador = entityToUse.CodigoConservador,
                FechaPublicacion = entityToUse.FechaPublicacion,
                FechaSesionDirectorio = entityToUse.FechaSesionDirectorio,
                FechaJuntaAccionista = entityToUse.FechaJuntaAccionista,
                CodigoRectificacion = entityToUse.CodigoRectificacion,
                FechaDecreto = entityToUse.FechaDecreto,
                NumeroDecreto = entityToUse.NumeroDecreto,
                FechaPublicacionDecreto = entityToUse.FechaPublicacionDecreto,
                FechaProtocolarizacion = entityToUse.FechaProtocolarizacion,
                CodigoNotariaProtocolo = entityToUse.CodigoNotariaProtocolo,
                CodigoDocumentoOrigen = entityToUse.CodigoDocumentoOrigen,
                NombreOrigen = entityToUse.NombreOrigen,
                LugarOrigen = entityToUse.LugarOrigen,
                Observaciones = entityToUse.Observaciones,
                EstadoConsulta = entityToUse.EstadoConsulta,
                FechaInicio = entityToUse.FechaInicio,
                FechaFinal = entityToUse.FechaFinal,
                CodigoRevocacion = entityToUse.CodigoRevocacion,
                FechaResolucionQuiebra = entityToUse.FechaResolucionQuiebra,
                CodigoTribunal = entityToUse.CodigoTribunal,
                FechaPublicacionQuiebra = entityToUse.FechaPublicacionQuiebra,
                ResponsableDocumento = entityToUse.ResponsableDocumento,
                LugarDocumento = entityToUse.LugarDocumento,
                NombreInforme = entityToUse.NombreInforme,
                TipoDocumento = entityToUse.TipoDocumento,
                FechaRenovacion = entityToUse.FechaRenovacion,
                ObservacionPoder = entityToUse.ObservacionPoder,
                FechaActVigencia = entityToUse.FechaActVigencia,
                FechaTerminaVigencia = entityToUse.FechaTerminaVigencia,
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlPoderes + "/IngresarDatosDocumento", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(urlPoderes + "/IngresarDatosDocumento", requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<DocumentoPoderDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde API Transversal.
        /// </summary>
        /// <returns>Lista de DocumentoPoderDTO</returns>
        public BasePropertiesDTO ObtenerDocumentoLegal(string codigoDocumentoLegal)
        {

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerDocumentoLegal?codigoDocumentoLegal=" + codigoDocumentoLegal, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerDocumentoLegal?codigoDocumentoLegal=" + codigoDocumentoLegal, token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<DocumentoPoderDTO>>();
            }
            if (returnEntity != null)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<DocumentoPoderDTO>>();
            }

            return returnEntity;
        }


        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde API Transversal.
        /// </summary>
        /// <returns>Lista de DocumentoPoderDTO</returns>
        public BasePropertiesDTO ObtenerRespuestaLegal(string codigoDocumento)
        {

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerRespuestaLegal?codigoDocumento=" + codigoDocumento, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerRespuestaLegal?codigoDocumento=" + codigoDocumento, token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<DocumentoPoderDTO>>();
            }
            if (returnEntity != null)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<DocumentoPoderDTO>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde API Transversal.
        /// </summary>
        /// <returns>Lista de DocumentoPoderDTO</returns>
        public BasePropertiesDTO ObtenerListaTipoDocumento()
        {

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/GetListaTipoDocumento", token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Get(urlTransversal + "/GetListaTipoDocumento", token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<DocumentoPoderDTO>>();
            }
            if (returnEntity != null)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<DocumentoPoderDTO>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde API Transversal.
        /// </summary>
        /// <returns>Lista de DocumentoPoderDTO</returns>
        public BasePropertiesDTO ObtenerListaNotaria()
        {

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/GetListaNotaria", token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Get(urlTransversal + "/GetListaNotaria", token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<DocumentoPoderDTO>>();
            }
            if (returnEntity != null)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<DocumentoPoderDTO>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno al cambiar Estado SGL.
        /// </summary>
        /// <returns>Lista de GrupoDTO</returns>
        public BasePropertiesDTO CambiarEstadoSGL(DocumentoPoderDTO entityToUse)
        {
            var requestEntity = new ConsultaEstadoSGLDTORequest
            {
                IdSGL = entityToUse.IdSGL,
                EstadoId = entityToUse.EstadoId,
                CodigoPersona = entityToUse.CodigoPersona
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlTransversal + "/CambiarEstadoSGL", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(urlTransversal + "/CambiarEstadoSGL", requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<DocumentoPoderDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de conservadores.
        /// </summary>
        /// <returns>Lista de ConservadorDTO</returns>
        public BasePropertiesDTO ObtenerConservadores()
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlConservador, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlConservador, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<DocumentoPoderDTO>>();
            }

            return returnEntity;
        }

        #endregion



        #endregion
    }
}
