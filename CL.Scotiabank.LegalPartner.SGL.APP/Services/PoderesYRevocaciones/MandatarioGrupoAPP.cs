﻿using CL.Scotiabank.LegalPartner.SGL.DAL;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using CL.Scotiabank.LegalPartner.CabeceraSglAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.ConsultaAPI.Transversal.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System;
using CL.Scotiabank.LegalPartner.PoderYRevocacionesAPI.Transversal.DTO;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Clase de servicios para la entidad Mandatario, Grupo y Asociacion Mandatarios a Grupo del sistema.
    /// </summary>
    public class MandatarioGrupoAPP : IMandatarioGrupoAPP
    {
        #region Members

        private ITokenAPP _tokenAPP;
        private IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string urlTransversal = Resources.TransversalUrl;
        private readonly string urlCabeceraSGL = Resources.CabeceraSGL;
        private readonly string urlConsulta = Resources.Consulta;
        private readonly string urlMandatarios = Resources.MandatariosUrl;
        private readonly string urlGrupos = Resources.GruposUrl;
        private readonly string urlAsociacion = Resources.AsociacionMandatarioGrupoUrl;
        private readonly string urlEntradaCompleta = Resources.EntradaCompletaUrl;
        private readonly string urlSocio = Resources.SocioUrl;

        #endregion

        #region Constructor
        public MandatarioGrupoAPP(ITokenAPP tokenAPP, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            this._tokenAPP = tokenAPP;
            this._configuration = configuration;
            this._httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Public Methods

        #region Mandatarios

        #region Listar Mandatarios

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de mandatarios
        /// </summary>
        /// <returns>Lista de FacultadDTO</returns>
        public BasePropertiesDTO ListarMandatarios(GetMandatarioDTO entitytoUse)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ConsultarDatosParticipantes?codigoPersona=" + entitytoUse.CodigoPersona + "&codigoTipo=" + entitytoUse.CodigoTipo, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlTransversal + "/ConsultarDatosParticipantes?codigoPersona=" + entitytoUse.CodigoPersona + "&codigoTipo=" + entitytoUse.CodigoTipo, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<DatosParticipantesDTOResponse>>();
            }

            return returnEntity;
        }

        #endregion

        #region Ingresar Mandatarios

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de mandatarios
        /// </summary>
        /// <returns>Lista de FacultadDTO</returns>
        public BasePropertiesDTO ConsultaMandatario(MandatarioGrupoDTO entitytoUse)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var requestEntity = new UsuarioConsultaDTORequest
            {
                Rut = Convert.ToInt32(entitytoUse.Rut),
                Nombre = entitytoUse.Nombre,
                ApellidoPaterno = entitytoUse.ApellidoPaterno,
                ApellidoMaterno = entitytoUse.ApellidoMaterno,
                CantidadRegistros = entitytoUse.CantidadRegistros
            };

            var responseContent = HttpRequestHelper.Post(urlConsulta + "/ConsultaPersonaDatos", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Post(urlConsulta + "/ConsultaPersonaDatos", requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaPersonaDatosDTOResponse>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de Mandatario.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>FacultadDTO</returns>
        public BasePropertiesDTO IngresarMandatario(MandatarioGrupoDTO entityToUse)
        {
            entityToUse.CodigoTipoPersona = 0;

            entityToUse.Rut = (entityToUse.Rut).Replace("-","");
            entityToUse.Rut = entityToUse.Rut.Replace(".", "");

            var requestEntityAgregar = new AgregarPersonaDTORequest
            {
                CodigoTipoPersona = entityToUse.CodigoTipoPersona,
                RutPersona = Convert.ToInt32(entityToUse.Rut.Remove(entityToUse.Rut.Length-1,1)),             
                DvPersona = entityToUse.DvPersona,
                Nombre = entityToUse.Nombre.Trim(),
                ApellidoPaterno = entityToUse.ApellidoPaterno.Trim(),
                ApellidoMaterno = entityToUse.ApellidoMaterno.Trim()
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlCabeceraSGL + "/AgregarPersona", requestEntityAgregar, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Post(urlCabeceraSGL + "/AgregarPersona", requestEntityAgregar, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<MandatarioGrupoDTO>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Método que ingresa las observaciones del mandatario.
        /// </summary>
        /// <param name="entityToUse"></param>
        /// <returns></returns>
        public BasePropertiesDTO IngresarObservacionMandatario(MandatarioGrupoDTO entityToUse)
        {
            //Pendiente a actualizar nugget de EntradaCompleta / Socio<Controller
            var requestEntity = new IngresarBParticipanteDTORequest
            {
                CodigoPersona = entityToUse.CodigoPersona,
                CodigoParticipante = entityToUse.CodigoParticipante,
                CodigoDocumento = entityToUse.CodigoDocumento,
                CodigoTipoParticipante = entityToUse.CodigoTipoParticipante,
                Monto = entityToUse.Monto,
                Porcentaje = entityToUse.Porcentaje,
                FechaInicio = entityToUse.FechaIniciadora,
                FechaFinal = entityToUse.FechaFin,
                Limitacion = entityToUse.Limitacion,
                CodigoRevocacion = entityToUse.CodigoRevocacion
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlSocio + "/InsertarParticipante", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Post(urlSocio + "/InsertarParticipante", requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<SocioDTOResponse>();
            }

            return returnEntity;
        }

        
        #endregion

        #region Eliminar Mandatarios

        /// <summary>
        /// Método que consulta si el mandatario seleccionado sea vigente.
        /// </summary>
        /// <param name="entityToUse"></param>
        /// <returns></returns>
        public BasePropertiesDTO ObtenerMandatarioVigente(MandatarioGrupoDTO entityToUse)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var requestEntity = new ObtenerMandatarioVigenteDTORequest
            {
                CodigoPersona = entityToUse.CodigoPersona,
                CodigoParticipante = entityToUse.CodigoParticipante,
            };

            var responseContent = HttpRequestHelper.Get(urlMandatarios + "/GetMandatariosVigentes?codigoPersona=" + entityToUse.CodigoPersona + "&codigoParticipante=" + entityToUse.CodigoParticipante, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Get(urlMandatarios + "/GetMandatariosVigentes?codigoPersona=" + entityToUse.CodigoPersona + "&codigoParticipante=" + entityToUse.CodigoParticipante, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<MandatarioGrupoDTO>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Método que elimina el mandatario seleccionado
        /// </summary>
        /// <param name="requestEntity"></param>
        /// <returns></returns>
        public BasePropertiesDTO EliminarMandatario(MandatarioGrupoDTO entityToUse)
        {
            var requestEntity = new EliminarMandatarioDTORequest
            {
                CodigoCliente = entityToUse.CodigoPersona,
                CodigoParticipante = entityToUse.CodigoParticipante,
                CodigoTipo = entityToUse.CodigoTipo
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            //var responseContent = HttpRequestHelper.Delete(urlSocio + "/EliminarMandatario=" + requestEntity, token.access_token);
            var responseContent = HttpRequestHelper.Post(urlSocio + "/EliminarMandatario", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(urlSocio + "/EliminarMandatario", requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<MandatarioGrupoDTO>();
            }

            return returnEntity;
        }

        #endregion

        #region Editar Mandatarios

        /// <summary>
        /// Método que actualiza los mandatarios.
        /// </summary>
        /// <param name="entityToUse"></param>
        /// <returns></returns>
        public BasePropertiesDTO EditarMandatario(MandatarioGrupoDTO entityToUse)
        {
            var requestEntity = new ActualizarPersonaDTORequest
            {
                CodigoPersona = entityToUse.CodigoParticipante,
                CodigoTipoPersona = entityToUse.CodigoTipoPersona,
                Nombre = entityToUse.Nombre.Trim(),
                ApellidoPaterno = entityToUse.ApellidoPaterno.Trim(),
                ApellidoMaterno = entityToUse.ApellidoMaterno.Trim()
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Put(urlCabeceraSGL + "/ActualizarPersona", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Put(urlCabeceraSGL + "/ActualizarPersona", requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<MandatarioGrupoDTO>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Método que permite editar las observaciones de un Mandatario.
        /// </summary>
        /// <param name="entityToUse">Objeto de parámetros de MandatarioGrupoDTO </param>
        /// <returns></returns>
        public BasePropertiesDTO EditarObservacionesMandatario(MandatarioGrupoDTO entityToUse)
        {
            var requestEntity = new ModificarParticipanteBDTORequest
            {
                CodigoPersona = entityToUse.CodigoPersona,
                CodigoParticipante = entityToUse.CodigoParticipante,
                CodigoTipoParticipante = entityToUse.CodigoTipoParticipante,
                Monto = entityToUse.Monto,
                Porcentaje = entityToUse.Porcentaje,
                Limitacion = entityToUse.Limite
            };

            if (string.IsNullOrEmpty(requestEntity.Limitacion))
            {
                requestEntity.Limitacion = string.Empty;
            }

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlSocio + "/ModificarParticipante", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(urlSocio + "/ModificarParticipante", requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<MandatarioGrupoDTO>();
            }

            return returnEntity;
        }

        #endregion

        #region Actualizar EstadoS GL

        /// <summary>
        /// Método que actuliza el estado de la SGL.
        /// </summary>
        /// <param name="entityToUse">SglId, Estado, CodigoPersonaModificador </param>
        /// <returns></returns>
        public BasePropertiesDTO ActualizarEstadoSGL(MandatarioGrupoDTO entityToUse)
        {
            entityToUse.SglId = 4456415;
            entityToUse.Estado = 8;
            entityToUse.CodigoPersonaModificador = 717;

            var requestEntity = new NuevoEstadoSGLDTORequest
            {
                SglId = entityToUse.SglId,
                Estado = entityToUse.Estado,
                CodigoPersonaModificador = entityToUse.CodigoPersonaModificador
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Put(urlCabeceraSGL + "/ActualizarEstadoSGL", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Put(urlCabeceraSGL + "/ActualizarEstadoSGL", requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<MandatarioGrupoDTO>();
            }

            return returnEntity;
        }

        #endregion

        #endregion

        #region Grupos

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de Grupo.
        /// </summary>
        /// <returns>Lista de GrupoDTO</returns>
        public BasePropertiesDTO ConsultaSociedadGrupoMandatario(string id)
        {

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ConsultarSociedadGrupoMandatario?id=" + id, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Get(urlTransversal + "/ConsultarSociedadGrupoMandatario?id=" + id, token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<MandatarioGrupoDTO>>();
            }
            if (returnEntity != null)
            {
                var returnEntityFiltro = ((IList<MandatarioGrupoDTO>)returnEntity.Resultado).Where(x => x.Tipo == 2);

                returnEntity.Resultado = returnEntityFiltro;
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno al crear un Grupo.
        /// </summary>
        /// <returns>Lista de GrupoDTO</returns>
        public BasePropertiesDTO IngresarGrupo(MandatarioGrupoDTO entityToUse)
        {
            var requestEntity = new AgregarGrupoDTORequest
            {
                TipoGrupo = entityToUse.TipoGrupo,
                NombreGrupo = entityToUse.NombreGrupo,
                CodigoSociedad = entityToUse.CodigoSociedad
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlGrupos + "/IngresaGrupo", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(urlGrupos + "/IngresaGrupo", requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<MandatarioGrupoDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno al actualizar un Grupo.
        /// </summary>
        /// <returns>Lista de GrupoDTO</returns>
        public BasePropertiesDTO EditarGrupo(MandatarioGrupoDTO entityToUse)
        {
            var requestEntity = new ActualizarGrupoDTORequest
            {
                CodigoPer = entityToUse.CodigoPer,
                CodigoGrupo = entityToUse.CodigoGrupo,
                TipoGrupo = entityToUse.TipoGrupo,
                NombreGrupo = entityToUse.NombreGrupo
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlGrupos + "/ActualizaGrupo", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(urlGrupos + "/ActualizaGrupo", requestEntity, token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<MandatarioGrupoDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) para obtener grupos Vigentes y
        /// el retorno de datos desde la api web de Grupo.
        /// </summary>
        /// <returns>Lista de GrupoDTO</returns>
        public BasePropertiesDTO ObtenerGruposVigentes(string codigoGrupo)
        {

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlGrupos + "/ObtieneGruposVigentes?codigoGrupo=" + codigoGrupo, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Get(urlGrupos + "/ObtieneGruposVigentes?codigoGrupo=" + codigoGrupo, token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<MandatarioGrupoDTO>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) para eleiminar Grupo.
        /// </summary>
        /// <returns>Objeto de GrupoDTO</returns>
        public BasePropertiesDTO EliminarGruposVigentes(string codigoGrupo)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Delete(urlGrupos + "/EliminaGrupo?codigoGrupo=" + codigoGrupo, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Delete(urlGrupos + "/EliminaGrupo?codigoGrupo=" + codigoGrupo, token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<MandatarioGrupoDTO>();
            }

            return returnEntity;
        }

        #endregion

        #region Asociacion

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) para obtener Documento de Poder y el retorno de datos desde la api web de Grupo.
        /// </summary>
        /// <returns>Lista de DocumentoLegalDTO</returns>
        public BasePropertiesDTO GetEscrituraPoder(string CodigoDocumento)
        {

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlAsociacion + "/ObtenerEscrituraPoder?CodigoDocumento=" + CodigoDocumento, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Get(urlAsociacion + "/ObtenerEscrituraPoder?CodigoDocumneto=" + CodigoDocumento, token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ObtenerDocumentoDTO>>();
            }

            return returnEntity;
        }

        ///CreateAsociacionMandatariosGrupo
        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) para asociar mandatarios a un Grupo y 
        /// el retorno al crear un As.
        /// </summary>
        /// <returns>Lista de AsociacionDTO</returns>
        public BasePropertiesDTO IngresarAsociacion(MandatarioGrupoDTO entityToUse)
        {
            var requestEntity = new IngresaAsociacionDTORequest
            {

                CodigoGrupo = entityToUse.CodigoGrupo,
                CodigoPersona = entityToUse.CodigoParticipante,
                CodigoRevocacion = entityToUse.CodigoRevocacion,
                CodigoDocumento = entityToUse.CodigoDocumento,
                FechaInicialMandatario = entityToUse.FechaInicio,
                FechaFinalMandatario = entityToUse.FechaFinal


            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlAsociacion + "/IngresarAsociacion", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(urlAsociacion + "/IngresarAsociacion", requestEntity, token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<MandatarioGrupoDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) para obtener Lista de Grupos con Mandatarios asociados
        /// y el retorno de datos desde la api web de Asociacion.
        /// </summary>
        public BasePropertiesDTO ConsultarListaGrupoMandatorio(string id)
        {

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ConsultarListaGrupoMandatorio?id=" + id, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Get(urlTransversal + "/ConsultarListaGrupoMandatorio?id=" + id, token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<MandatarioGrupoDTO>>();
            }



            return returnEntity;
        }

        ///DeleteAsociacionMandatariosGrupo
        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) para eliminar la  asociacion mandatarios a un Grupo y 
        /// </summary>
        /// <returns>Lista de AsociacionDTO</returns>
        public BasePropertiesDTO EliminarAsociacion(MandatarioGrupoDTO entityToUse)
        {
            var requestEntity = new EliminarAsociacionDTORequest
            {
                CodigoGrupo = entityToUse.CodigoGrupo,
                CodigoMandatario = entityToUse.CodigoPersona

            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlAsociacion + "/EliminarAsociacion", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(urlAsociacion + "/EliminarAsociacion", requestEntity, token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<MandatarioGrupoDTO>();
            }

            return returnEntity;
        }

        #endregion

        #region Momentanea        


        #endregion

        #endregion
    }
}
