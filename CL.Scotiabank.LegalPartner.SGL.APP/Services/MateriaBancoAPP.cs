﻿using CL.Scotiabank.LegalPartner.SGL.DAL;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Clase de servicios para la entidad MateriaBanco del sistema.
    /// </summary>
    public class MateriaBancoAPP : IMateriaBancoAPP
    {
        #region Memebers

        private ITokenAPP _tokenAPP;
        private IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string urlMateriaBancos = Resources.MateriaBancoUrl;

        #endregion

        #region Constructor

        public MateriaBancoAPP(ITokenAPP tokenAPP, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            this._tokenAPP = tokenAPP;
            this._configuration = configuration;
            this._httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de materiaBancos.
        /// </summary>
        /// <returns>Lista de MateriaBancoDTO</returns>
        public BasePropertiesDTO Get()
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlMateriaBancos, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlMateriaBancos, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<MateriaBancoDTO>>();
            }

            IList<MateriaBancoDTO> materiasAbuelo = ((IEnumerable<MateriaBancoDTO>)returnEntity.Resultado).Where(padre => padre.MateriaPadre.ToString() == string.Empty).ToList();
            IEnumerable<MateriaBancoDTO> materiasPadres = new List<MateriaBancoDTO>();

            Parallel.ForEach(materiasAbuelo, (materiaBancoAbuelo) =>
            {
                if (string.IsNullOrEmpty(materiaBancoAbuelo.MateriaPadre.ToString()))
                {
                    materiasPadres = ((IEnumerable<MateriaBancoDTO>)returnEntity.Resultado).Where(padre => padre.MateriaPadre.Equals(materiaBancoAbuelo.Codigo));

                    foreach (MateriaBancoDTO padre in materiasPadres)
                    {
                        padre.MateriaPadreDescripcion = materiaBancoAbuelo.Descripcion;

                        materiasAbuelo.Add(padre);

                        var materiasHijos = ((IEnumerable<MateriaBancoDTO>)returnEntity.Resultado).Where(hijo => hijo.MateriaPadre.Equals(padre.Codigo));

                        foreach (MateriaBancoDTO hijo in materiasHijos)
                        {
                            hijo.MateriaPadreDescripcion = padre.Descripcion;

                            materiasAbuelo.Add(hijo);
                        }
                    }
                }
            });

            returnEntity.Resultado = materiasAbuelo.OrderBy(x=>x.Codigo);

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de materiaBancos.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>MateriaBancoDTO</returns>
        public BasePropertiesDTO Get(int id)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlMateriaBancos + "/" + id, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlMateriaBancos + "/" + id, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<MateriaBancoDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de materiaBancos.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>MateriaBancoDTO</returns>
        public BasePropertiesDTO Create(MateriaBancoDTO entityToUse)
        {
            var requestEntity = new MateriaBancoDTORequest
            {
                Descripcion = entityToUse.Descripcion,
                MateriaPadre = entityToUse.MateriaPadre
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlMateriaBancos, requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Post(urlMateriaBancos, requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<MateriaBancoDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Put) y el retorno de datos desde la api web de materiaBancos.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>MateriaBancoDTO</returns>
        public BasePropertiesDTO Edit(MateriaBancoDTO entityToUse)
        {
            var model = new MateriaBancoDTORequest
            {
                Codigo = entityToUse.Codigo,
                Descripcion = entityToUse.Descripcion,
                MateriaPadre = entityToUse.MateriaPadre
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Put(urlMateriaBancos + "/" + entityToUse.Codigo, model, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Put(urlMateriaBancos + "/" + entityToUse.Codigo, model, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<MateriaBancoDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) y el retorno de datos desde la api web de materiaBancos.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>MateriaBancoDTO</returns>
        public BasePropertiesDTO Delete(MateriaBancoDTO entityToUse)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Delete(urlMateriaBancos + "/" + entityToUse.Codigo, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Delete(urlMateriaBancos + "/" + entityToUse.Codigo, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<MateriaBancoDTO>();
            }

            return returnEntity;
        }

        #endregion
    }
}
