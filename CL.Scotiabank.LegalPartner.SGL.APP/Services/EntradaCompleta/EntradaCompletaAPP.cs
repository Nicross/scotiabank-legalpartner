﻿using CL.Scotiabank.LegalPartner.CabeceraSglAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.ConsultaAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.EntradaCompletaAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.PoderYRevocacionesAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.DAL;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO.DTO.EntradaCompleta;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using DatosEstatutosCreateDTORequest = CL.Scotiabank.LegalPartner.SGL.DAL.DatosEstatutosCreateDTORequest;
using ModificarCapitalDTORequest = CL.Scotiabank.LegalPartner.EntradaCompletaAPI.Transversal.DTO.ModificarCapitalDTORequest;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    public class EntradaCompletaAPP : IEntradaCompletaAPP
    {
        #region Memebers

        private readonly ITokenAPP _tokenAPP;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string urlConsultaPoderes = Resources.ConsultaPoderesUrl;
        private readonly string urlTransversal = Resources.TransversalUrl;
        private readonly string urlPoderesRevocaciones = Resources.FacultadesUrl;
        private readonly string urlEntradaCompletaDocEscritura = Resources.DocEscrituraUrl;
        private readonly string urlEntradaCompleta = Resources.EntradaCompletaUrl;
        private readonly string PoderesUrl = Resources.PoderesUrl;
        private readonly string SocioUrl = Resources.SocioUrl;
        private readonly string CabeceraSGLUrl = Resources.CabeceraSGL;
        private readonly string LegalizacionUrl = Resources.LegalizacionUrl;
        private readonly string RevocacionUrl = Resources.RevocacionesUrl;
        #endregion

        #region Constructor

        public EntradaCompletaAPP(ITokenAPP tokenAPP, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            this._tokenAPP = tokenAPP;
            this._configuration = configuration;
            this._httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Public Methods

        public BasePropertiesDTO ObtenerDatosPersona(UsuarioConsultaDTORequest entityToUse)
        {
            //var requestEntity = new UsuarioConsultaDTORequest
            //{
            //    Rut = entityToUse.Rut,
            //    Nombre = entityToUse.Nombre,
            //    ApellidoPaterno = entityToUse.ApellidoPaterno,
            //    ApellidoMaterno = entityToUse.ApellidoMaterno,
            //    CantidadRegistros = entityToUse.CantidadRegistros
            //};

            //UsuarioConsultaDTO paramDatosPersona = new UsuarioConsultaDTO();

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlConsultaPoderes + "/ConsultaPersonaDatos", entityToUse, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaPersonaDatosDTO>>();
                //paramDatosPersona = returnEntity.Resultado[0];

                //paramDatosPersona = CargaDatosAdicionalesPersona(paramDatosPersona);
                //paramDatosPersona = CargaListaDocumentos(paramDatosPersona);
                //paramDatosPersona = CargaDatosEstatutoPersona(paramDatosPersona);

                //returnEntity.Resultado = paramDatosPersona;

            }

            return returnEntity;
        }

        public VerInformeCapitalDTO ObtenerDatosPersonaInforme(VerInformeCapitalDTO dto)
        {
            string[] rut = dto.Rut.Split('-');
            //string[] nombre = dto.NombreCompleto.Split(' ');
            UsuarioConsultaDTORequest user = new UsuarioConsultaDTORequest
            {
                Rut = int.Parse(rut[0]),
                Nombre = string.Empty,
                ApellidoPaterno = string.Empty,
                ApellidoMaterno = string.Empty
            };
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlConsultaPoderes + "/ConsultaPersonaDatos", user, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaPersonaDatosDTO>>();
            }

            dto.NombreCompleto = returnEntity.Resultado[0].NombrePersona;
            //dto.CodigoPersona = returnEntity.Resultado[0].CodigoPersona;
            dto.TipoPersonaJuridica = returnEntity.Resultado[0].CodigoTipoPersona;

            return dto;
        }



        public BasePropertiesDTO AgregarDatoEstatuto(UsuarioConsultaDTO usuarioConsulta)
        {

            BasePropertiesDTO dTO = ObtenerDatosEstatutos(usuarioConsulta.CodigoPersona);

            string fantasiaOrigen = dTO.Resultado[0].NombreFantasia;


            if (fantasiaOrigen.Contains("(SOLO PARA DEPOSITOS)"))
            {
                if (!usuarioConsulta.NombreFantasia.Contains("(SOLO PARA DEPOSITOS)"))
                {
                    usuarioConsulta.NombreFantasia = usuarioConsulta.NombreFantasia + " (SOLO PARA DEPOSITOS)";
                }
            }

            DatosEstatutosCreateDTORequest requestEntity = new CL.Scotiabank.LegalPartner.SGL.DAL.DatosEstatutosCreateDTORequest
            {
                CodigoCliente = usuarioConsulta.CodigoPersona,
                EstadoLegal = usuarioConsulta.EstadoLegal,
                NombreFantasia = string.IsNullOrEmpty(usuarioConsulta.NombreFantasia) ? string.Empty : usuarioConsulta.NombreFantasia,
                NombreLegalAnterior = string.IsNullOrEmpty(usuarioConsulta.NombreLegalAnterior) ? string.Empty : usuarioConsulta.NombreLegalAnterior,
                Objeto = usuarioConsulta.Objeto,
                Domicilio = string.IsNullOrEmpty(usuarioConsulta.Domicilio) ? string.Empty : usuarioConsulta.Domicilio,
                Duracion = string.IsNullOrEmpty(usuarioConsulta.Duracion) ? string.Empty : usuarioConsulta.Duracion,
                Administracion = string.IsNullOrEmpty(usuarioConsulta.Administracion) ? string.Empty : usuarioConsulta.Administracion,
                Fuente = string.IsNullOrEmpty(usuarioConsulta.ComunidadFuente) ? string.Empty : usuarioConsulta.ComunidadFuente,
                Conclusiones = string.IsNullOrEmpty(usuarioConsulta.Conclusion) ? string.Empty : usuarioConsulta.Conclusion,
                FechaUltimoEstudio = string.IsNullOrEmpty(usuarioConsulta.FechaUltimoEstudio) ? string.Empty : usuarioConsulta.FechaUltimoEstudio,
                FechaProximaVigencia = string.IsNullOrEmpty(usuarioConsulta.FechaActualizacion) ? string.Empty : usuarioConsulta.FechaActualizacion,
                IndicadorInforme = string.IsNullOrEmpty(usuarioConsulta.IndicadorInforme) ? string.Empty : usuarioConsulta.IndicadorInforme,
                EstadoConsulta = string.IsNullOrEmpty(usuarioConsulta.EstadoConsulta) ? string.Empty : usuarioConsulta.EstadoConsulta
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlEntradaCompleta + "/AddDatosEstatuto", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                //_httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                //responseContent = HttpRequestHelper.Post(urlEntradaCompleta + "/AddDatosEstatuto/", requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<DatosEstatutosCreateDTO>();
            }

            return returnEntity;
        }

        private UsuarioConsultaDTO CargaDatosAdicionalesPersona(UsuarioConsultaDTO paramDatosPersona)
        {
            int codigoPersona = paramDatosPersona.CodigoPersona;
            int codLegal = 0;
            string descripcionTipoPersona = string.Empty;
            BasePropertiesDTO lstEstadoLegal = GetListaEstadoLegal();
            BasePropertiesDTO lstTipoPersona = GetListadoTipoPersonaDetalle();
            BasePropertiesDTO transversalPersonaRetorno = ConsultarDatosPersona(codigoPersona);

            if (transversalPersonaRetorno != null)
            {
                if (paramDatosPersona.CodigoTipoPersona.Equals("0"))
                {
                    paramDatosPersona.EstadoLegalDescripcion = string.Empty;
                }
                else
                {
                    descripcionTipoPersona = ((IEnumerable<TipoPersonaDetalleDTO>)lstTipoPersona.Resultado).Where(x => x.Codigo.Equals(paramDatosPersona.CodigoTipoPersona)).FirstOrDefault().Descripcion;
                    codLegal = Convert.ToInt32(((IEnumerable<TipoPersonaDetalleDTO>)lstTipoPersona.Resultado).Where(x => x.Descripcion.Trim() == descripcionTipoPersona).FirstOrDefault().CodigoLegal.Trim());

                    paramDatosPersona.TipoPersonaJuridica = descripcionTipoPersona;
                    paramDatosPersona.CodigoLegal = codLegal;
                }

                string estadoLegalDescripcion = ((IEnumerable<EstadoLegalizacionDTO>)lstEstadoLegal.Resultado).Where(x => x.Codigo.Equals(transversalPersonaRetorno.Resultado[0].EstadoLegal)).FirstOrDefault().Descripcion;
                paramDatosPersona.EstadoLegalDescripcion = estadoLegalDescripcion;
            }

            return paramDatosPersona;
        }

        public BasePropertiesDTO GetListaEstadoLegal()
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlConsultaPoderes + "/GetListaEstadoLegal", token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaListaEstadoLegalDTOResponse>>();
            }

            return returnEntity;
        }

        public BasePropertiesDTO ConsultarDatosPersona(int codigoPersona)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlConsultaPoderes + "/ConsultarDatosPersona?codigoPersona=" + codigoPersona, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<UsuarioConsultaDTO>>();
            }

            return returnEntity;
        }

        public VerInformeCapitalDTO ConsultaDatosPersonaInforme(VerInformeCapitalDTO dto)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlConsultaPoderes + "/ConsultarDatosPersona?codigoPersona=" + dto.CodigoPersona, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<UsuarioConsultaDTO>>();
            }

            //dto.NombreFantasia = returnEntity.Resultado[0].NombreFantasia;
            //dto.Domicilio = returnEntity.Resultado[0].Domicilio;
            //dto.Duracion = returnEntity.Resultado[0].Duracion;
            //dto.Objeto = returnEntity.Resultado[0].Objeto;
            //dto.CodigoPersona = 0;
            //dto.TipoPersonaJuridica = returnEntity.Resultado[0].CodigoTipoPersona;
            //VerInformeCapitalDTO dto = new VerInformeCapitalDTO
            //{
            dto.Rut = FormatearRut(returnEntity.Resultado[0].Rut + returnEntity.Resultado[0].Dv);
            //= returnEntity.Resultado[0].Rut + returnEntity.Resultado[0].Dv;
            //dto.Dv = returnEntity.Resultado[0].Dv;

            dto.NombreCompleto = returnEntity.Resultado[0].NombrePersona;
            //NombreFantasia = returnEntity.Resultado[0].NombreFantasia,
            //Domicilio = returnEntity.Resultado[0].Domicilio,
            //Administracion = returnEntity.Resultado[0].Administracion,
            //Objeto = returnEntity.Resultado[0].Objeto,
            //Duracion = returnEntity.Resultado[0].Duracion,
            dto.CodigoPersonaJuridica = returnEntity.Resultado[0].CodigoTipoPersona;
            dto.TipoPersonaJuridica = returnEntity.Resultado[0].TipoPersonaDescripcion;

            /*returnEntity.Resultado[0].CodigoTipoPersona,*/
            //CodigoPersona = codigoPersona
            //dto.CodigoPersona = CodigoPersona
            //};

            return dto;
        }

        public string FormatearRut(string rut)
        {
            int cont = 0;
            string format;
            if (rut.Length == 0)
            {
                return "";
            }
            else
            {
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                format = "-" + rut.Substring(rut.Length - 1);
                for (int i = rut.Length - 2; i >= 0; i--)
                {
                    format = rut.Substring(i, 1) + format;
                    cont++;
                    if (cont == 3 && i != 0)
                    {
                        format = "." + format;
                        cont = 0;
                    }
                }
                return format;
            }

        }

        public VerInformeCapitalDTO ObtenerListadoMoneda(VerInformeCapitalDTO dto)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerListadoMoneda", token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<CL.Scotiabank.LegalPartner.SGL.Transversal.DTO.MonedaDTO>>();
                returnEntity.Resultado = ((IEnumerable<CL.Scotiabank.LegalPartner.SGL.Transversal.DTO.MonedaDTO>)returnEntity.Resultado);
                dto.listaMoneda = returnEntity.Resultado;
            }

            return dto;

        }

        public BasePropertiesDTO GetListadoTipoPersonaDetalle()
        {
            string[] arrCodigo = { "1", "8", "17", "18", "19", "20" };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/GetListadoTipoPersonaDetalle", token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<TipoPersonaDetalleDTOResponse>>();
                returnEntity.Resultado = ((IEnumerable<TipoPersonaDetalleDTOResponse>)returnEntity.Resultado).Where(x => !arrCodigo.Contains(x.Codigo));
            }

            return returnEntity;
        }



        private UsuarioConsultaDTO CargaListaDocumentos(UsuarioConsultaDTO paramDatosPersona)
        {
            int codigoPersona = paramDatosPersona.CodigoPersona;
            BasePropertiesDTO lstDocumentos = ObtenerListadoDocumentos(codigoPersona);
            List<ListadoDocumentosDTO> lstNuevaDocumentos = ((List<ListadoDocumentosDTO>)lstDocumentos.Resultado);

            paramDatosPersona.ListaDocumentos = new List<ListadoDocumentosDTO>();
            paramDatosPersona.ListaDocumentos.AddRange(lstNuevaDocumentos);

            return paramDatosPersona;
        }

        private UsuarioConsultaDTO CargaDatosEstatutoPersona(UsuarioConsultaDTO paramDatosPersona)
        {
            int codigoPersona = paramDatosPersona.CodigoPersona;
            ConsultaDatosEstatutosDTORequest paramEstatutos = new ConsultaDatosEstatutosDTORequest();
            BasePropertiesDTO datosPersonaRetorno = ConsultaPersonaJuridicaDatos(codigoPersona);

            string[] rut = datosPersonaRetorno.Resultado[0].Rut.Split('-');

            paramDatosPersona.Rut = string.Format("{0:#,0}", Convert.ToInt32(rut[0])) + "-" + rut[1];
            paramDatosPersona.NombreLegalAnterior = string.IsNullOrEmpty(paramDatosPersona.Conclusion) ? string.Empty : datosPersonaRetorno.Resultado[0].NombreLegalAnterior.Trim();
            paramDatosPersona.NombreFantasia = datosPersonaRetorno.Resultado[0].NombreFantasia.Trim();
            paramDatosPersona.Objeto = datosPersonaRetorno.Resultado[0].Objeto.Trim();
            paramDatosPersona.Domicilio = datosPersonaRetorno.Resultado[0].Domicilio.Trim();
            paramDatosPersona.Duracion = datosPersonaRetorno.Resultado[0].Duracion.Trim();
            paramDatosPersona.Administracion = datosPersonaRetorno.Resultado[0].Administracion.Trim();
            paramDatosPersona.FechaActualizacion = FormateaFechaString(datosPersonaRetorno.Resultado[0].FechaActualizacion.ToString("dd/MM/yyyy"));//.Substring(0, 10).Trim();
            paramDatosPersona.Conclusion = string.IsNullOrEmpty(datosPersonaRetorno.Resultado[0].Observaciones) ? string.Empty : datosPersonaRetorno.Resultado[0].Observaciones.Trim();

            return paramDatosPersona;
        }

        public BasePropertiesDTO ConsultaPersonaJuridicaDatos(int codigoEstatuto)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlConsultaPoderes + "/ConsultaPersonaJuridicaDatos?codigoPersonaJuridica=" + codigoEstatuto, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaPersonaJuridicaDatosDTOResponse>>();
            }

            return returnEntity;
        }

        public VerInformeCapitalDTO ConsultaPersonaJuridicaDatosInforme(VerInformeCapitalDTO dto)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlConsultaPoderes + "/ConsultaPersonaJuridicaDatos?codigoPersonaJuridica=" + dto.CodigoPersona, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaPersonaJuridicaDatosDTOResponse>>();
            }

            if (returnEntity.Resultado == null)
            {
                dto.DatosValidos = false;
            }
            else
            {
                dto.DatosValidos = true;
            }

            dto.Duracion = returnEntity.Resultado[0].Duracion;
            dto.Objeto = returnEntity.Resultado[0].Objeto;
            dto.Administracion = returnEntity.Resultado[0].Administracion;
            dto.Domicilio = returnEntity.Resultado[0].Domicilio;
            dto.NombreFantasia = returnEntity.Resultado[0].NombreFantasia;
            dto.Observaciones = returnEntity.Resultado[0].Observaciones;
            return dto;
        }


        public BasePropertiesDTO ConsultaDocumentosCapital(string id)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerListadoDocumentosCapital?id=" + id, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = (dynamic)responseContent.ContentAsType<IEnumerable<DocumentoCapitalDTOResponse>>();
            }

            return returnEntity;
        }

        public VerInformeCapitalDTO ConsultaDocumentosCapitalInforme(VerInformeCapitalDTO dto)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerListadoDocumentosCapital?id=" + dto.CodigoPersona, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = (dynamic)responseContent.ContentAsType<IEnumerable<DocumentoCapitalDTOResponse>>();
            }

            BasePropertiesDTO baseDTO = GetListadoTipoPersonaDetalle();


            var lstCodCapital = from s in (IEnumerable<TipoPersonaDetalleDTOResponse>)baseDTO.Resultado
                                where s.Codigo == dto.TipoPersonaJuridica
                                group s by new { s.CodigoCapital };

            foreach (var item in lstCodCapital)
            {
                if (item.Any(x => x.CodigoCapital.TrimEnd() == "0"))
                {
                    dto.Formato = 0;
                }
            }
            dto.MontoCapital = returnEntity.Resultado[0].MontoCapital;

            dto.DescripcionMoneda = returnEntity.Resultado[0].DescripcionMoneda;
            dto.CantidadTotalAcciones = (int)returnEntity.Resultado[0].CantidadTotalAcciones;
            dto.ValorNominalAccion = (int)returnEntity.Resultado[0].ValorNominalAccion;
            dto.CodigoMoneda = returnEntity.Resultado[0].CodigoMoneda;
            if (dto.CodigoMoneda.Equals(""))
            {
                dto.CodigoMoneda = "999";
            }
            dto.Descripcion = returnEntity.Resultado[0].Descripcion;

            return dto;
        }

        public VerInformeCapitalDTO ConsultaDocumentosGeneralInforme(VerInformeCapitalDTO dto)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerListadoDocumentos?id=" + dto.CodigoPersona, token.access_token);

            var returnEntity = (dynamic)null;
            string fechaIncorporacion = string.Empty;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = (dynamic)responseContent.ContentAsType<IEnumerable<DocumentoDTO>>();
                dto.ListaDocumentos = ((IEnumerable<DocumentoDTO>)returnEntity.Resultado).Where(x => x.CodigoTipoLegal != 16 && x.CodigoTipoLegal != 2).ToList();

                if (dto.ListaDocumentos.FirstOrDefault().CodigoTipoLegal == 1)
                {
                    _httpContextAccessor.HttpContext.Session.SetString("_FechaDocumento",Convert.ToString(dto.ListaDocumentos.FirstOrDefault().FechaDocumento));
                }

                foreach (var item in dto.ListaDocumentos)
                {
                    item.FechaIncorporacionString = item.FechaIncorporacion.ToString("dd/MM/yyyy").Replace("-","/");
                    
                    item.FechaDocumentoString = item.FechaDocumento.ToString("dd/MM/yyyy").Replace("-","/");
                    item.FechaPublicacionDecretoString = item.FechaPublicacionDecreto.ToString("dd/MM/yyyy").Replace("-","/");
                    item.FechaPublicacionString = item.FechaPublicacion.ToString("dd/MM/yyyy").Replace("-","/");
                    item.FechaPublicacionQuiebraString = item.FechaPublicacionQuiebra.ToString("dd/MM/yyyy").Replace("-","/");
                }
            }
            return dto;
        }

        public VerInformeCapitalDTO ObtenerAbogado(VerInformeCapitalDTO dto)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlEntradaCompleta + "/ObtenerAbogado?codigoPersona=" + dto.CodigoPersonaAbogado, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = (dynamic)responseContent.ContentAsType<IEnumerable<LegalizacionDTOResponse>>();
            }

            //dto.NombreAbogado = returnEntity.Resultado[0].NombreCompleto;
            if (returnEntity.Resultado.Count == 0)
            {
                dto.NombreAbogado = string.Empty;
            }
            else
            {
                dto.NombreAbogado = returnEntity.Resultado[0].NombreCompleto;
            }
            //if (dto.NombreAbogado == null)
            //{
            //    dto.NombreAbogado = string.Empty;
            //}


            return dto;
        }

        public VerInformeCapitalDTO ObtenerSGLUSuarioInforme(VerInformeCapitalDTO dto)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var ResponseContent = HttpRequestHelper.Get(urlConsultaPoderes + "/GetSGLPorCodigoSGL?id=" + dto.SGL.ToString(), token.access_token);

            var returnEntity = (dynamic)null;

            if (ResponseContent != null && ResponseContent.IsSuccessStatusCode)
            {
                returnEntity = (dynamic)ResponseContent.ContentAsType<IEnumerable<ConsultaSGLPorSGLIdDTOResponse>>();
            }
            dto.CodigoPersonaAbogado = returnEntity.Resultado[0].CodigoPersonaAbogado;
            return dto;

        }

        public VerInformeCapitalDTO ConsultaDatosParticipante(VerInformeCapitalDTO dto)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ConsultarDatosParticipantes?codigoPersona=" + dto.CodigoPersona + "&codigoTipo=1", token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ParticipanteDTO>>();
                dto.listaParticipantes = ((IEnumerable<ParticipanteDTO>)returnEntity.Resultado).ToList();
                dto.CodigoPersona = dto.CodigoPersona;
                //dtoInforme.TipoPersonaJuridica = dto.TipoPersonaJuridica;
            }

            return dto;
        }

        public VerInformeCapitalDTO ConsultaDatosParticipanteInforme(VerInformeCapitalDTO dto)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ConsultarDatosParticipantes?codigoPersona=" + dto.CodigoPersona + "&codigoTipo=" + dto.CodigoTipoParticipante, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ParticipanteDTO>>();
                dto.listaParticipantes = ((IEnumerable<ParticipanteDTO>)returnEntity.Resultado).ToList();
            }

            foreach (var item in dto.listaParticipantes)
            {

                item.Rut = item.Rut + item.Dv;
                item.Rut = FormatearRut(item.Rut);
                if (item.MontoAporte != "0")
                {
                    if (dto.CodigoMoneda == "999")
                    {
                        item.MontoAporte = item.MontoAporte.Insert(2, ".");

                    }
                    else
                    {
                        item.MontoAporte = item.MontoAporte.Insert(2, ".");
                        item.MontoAporte = item.MontoAporte + ",00";
                    }
                }
            }


            return dto;
        }

        public IList<ListaDocumentoDTO> MapeaListaDocumentos(IEnumerable<DocumentoDTOResponse> resultado)
        {
            IList<ListaDocumentoDTO> lstDocumentos = new List<ListaDocumentoDTO>();
            ListaDocumentoDTO paramDocum = null;
            int cont = 1;

            foreach (var item in resultado)
            {
                paramDocum = new ListaDocumentoDTO();
                paramDocum.contador = cont;
                paramDocum.FechaFinal = item.FechaFinal.ToString("dd/MM/yyyy").Contains("3000") ? string.Empty : item.FechaFinal.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocum.CodigoRevocacion = item.CodigoRevocacion;
                paramDocum.FechaResolucionQuiebra = item.FechaResolucionQuiebra.ToString("dd/MM/yyyy").Contains("3000") ? string.Empty : item.FechaResolucionQuiebra.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocum.Juzgado = item.Juzgado;
                paramDocum.FechaPublicacionQuiebra = item.FechaPublicacionQuiebra.ToString().Contains("3000") ? string.Empty : item.FechaPublicacionQuiebra.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocum.UsuarioLogin = item.UsuarioLogin;
                paramDocum.sol_cod_uni_ofi = item.sol_cod_uni_ofi;
                paramDocum.NombreInforme = item.NombreInforme;
                paramDocum.CodigoConservador = item.CodigoConservador;
                paramDocum.FechaRenovacionDocumento = item.FechaRenovacionDocumento.ToString("dd/MM/yyyy").Contains("3000") ? string.Empty : item.FechaRenovacionDocumento.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocum.IdSGL = item.IdSGL;
                paramDocum.Notaria = item.Notaria;
                paramDocum.NumeroDocumento = item.NumeroDocumento;
                paramDocum.FechaDocumento = item.FechaDocumento.ToString("dd/MM/yyyy").Contains("3000") ? string.Empty : item.FechaDocumento.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocum.DescripcionTipoLegal = item.DescripcionTipoLegal;
                paramDocum.CodigoTipoLegal = item.CodigoTipoLegal;
                paramDocum.FechaInicio = item.FechaInicio.ToString().Contains("3000") ? string.Empty : item.FechaInicio.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocum.EstadoCsu = item.EstadoCsu;
                paramDocum.Observaciones = item.Observaciones;
                paramDocum.LugarOrigen = item.LugarOrigen;
                paramDocum.FechaIncorporacion = item.FechaIncorporacion.ToString().Contains("3000") ? string.Empty : item.FechaIncorporacion.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocum.AnioIncorporacion = item.AnioIncorporacion;
                paramDocum.Foja = item.Foja;
                paramDocum.FojaNumero = item.FojaNumero;
                paramDocum.Conservador = item.Conservador;
                paramDocum.FechaPublicacion = item.FechaPublicacion.ToString("dd/MM/yyyy").Contains("3000") ? string.Empty : item.FechaPublicacion.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocum.FechaSesionDirectorio = item.FechaSesionDirectorio;
                paramDocum.CodigoDocumento = item.CodigoDocumento;
                paramDocum.FechaJuntaExtraordinariaAccionistas = item.FechaJuntaExtraordinariaAccionistas.ToString("dd/MM/yyyy").Contains("3000") ? string.Empty : item.FechaJuntaExtraordinariaAccionistas.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocum.NumeroDecreto = item.NumeroDecreto;
                paramDocum.FechaPublicacionDecreto = item.FechaPublicacionDecreto.ToString("dd/MM/yyyy").Contains("3000") ? string.Empty : item.FechaPublicacionDecreto.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocum.FechaProtocolizacion = item.FechaProtocolizacion.ToString("dd/MM/yyyy").Contains("3000") ? string.Empty : item.FechaProtocolizacion.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocum.NombreNotariaProtocolizacion = item.NombreNotariaProtocolizacion;
                paramDocum.CodigoOrigen = item.CodigoOrigen;
                paramDocum.DescripcionOrigen = item.DescripcionOrigen;
                paramDocum.NombreOrigen = item.NombreOrigen;
                paramDocum.FechaDecreto = item.FechaDecreto.ToString().Contains("3000") ? string.Empty : item.FechaDecreto.ToString("dd/MM/yyyy").Replace("-", "/");
                paramDocum.CodigoPersona = item.CodigoPersona;

                lstDocumentos.Add(paramDocum);

                cont++;
            }

            return lstDocumentos;
        }

        public BasePropertiesDTO ObtenerListadoDocumentos(int codigoPersona)
        {
            IList<DocumentoDTOResponse> lstDocumentos = null;


            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerListadoDocumentos?id=" + codigoPersona, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<DocumentoDTOResponse>>();
                lstDocumentos = ((IEnumerable<DocumentoDTOResponse>)returnEntity.Resultado).Where(x => x.CodigoTipoLegal != 16 && x.CodigoTipoLegal != 2).ToList();

                returnEntity.Resultado = lstDocumentos;
            }

            return returnEntity;
        }

        public VerInformeCapitalDTO ObtenerListadoDocumentosInforme(int codigoPer)
        {
            VerInformeCapitalDTO infoDto = new VerInformeCapitalDTO();
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");
            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerListadoDocumentos?IdSGL=" + codigoPer, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<DocumentoDTO>>();
                infoDto.ListaDocumentos = ((IEnumerable<DocumentoDTO>)returnEntity.Resultado).ToList();
            }

            return infoDto;
        }

        public void IngresarCapitalSocio(VerInformeCapitalDTO dto)
        {
            dto.MontoCapital = Convert.ToDecimal(dto.MontoCapitalIngresa);
            //Convert.ToString(dto.MontoCapitalIngresa);
            dto.CantidadTotalAcciones = dto.CantidadTotalAccionesIngresa;
            dto.ValorNominalAccion = dto.ValorNominalAccionIngresa;

            ModificarCapitalDTORequest enviar = new ModificarCapitalDTORequest
            {
                MontoCapital = dto.MontoCapital,
                CodigoPersona = dto.CodigoPersona,
                CodigoMoneda = dto.CodigoMoneda,
                CantidadTotal = dto.CantidadTotalAcciones,
                ValorNominal = dto.ValorNominalAccion,
                ObservacionCapital = dto.Descripcion
            };
            //int largo = enviar.ObservacionCapital.Length;
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(SocioUrl + "/ModificarIngresarCapitalSocio", enviar, token.access_token);

        }

        /// <summary>
        /// Obtiene los datos del participante segun codigo persona y tipo 
        /// </summary>
        /// <param name="codigoPersona"></param>
        /// <param name="codigoTipo"></param>
        /// <returns></returns>
        public BasePropertiesDTO ConsultaDatosParticipante(int codigoPersona, string codigoTipo)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ConsultarDatosParticipantes?codigoPersona=" + codigoPersona + "&codigoTipo=" + codigoTipo, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<DatosParticipantesDTOResponse>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Método que elimina el mandatario seleccionado
        /// </summary>
        /// <param name="requestEntity"></param>
        /// <returns></returns>
        public BasePropertiesDTO EliminarMandatario(MandatarioGrupoDTO entityToUse)
        {
            var requestEntity = new CL.Scotiabank.LegalPartner.PoderYRevocacionesAPI.Transversal.DTO.EliminarMandatarioDTORequest
            {
                CodigoCliente = entityToUse.CodigoPersona,
                CodigoParticipante = entityToUse.CodigoParticipante,
                CodigoTipo = entityToUse.CodigoTipo
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            //var responseContent = HttpRequestHelper.Delete(urlSocio + "/EliminarMandatario=" + requestEntity, token.access_token);
            var responseContent = HttpRequestHelper.Post(SocioUrl + "/EliminarMandatario", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(SocioUrl + "/EliminarMandatario", requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<MandatarioGrupoDTO>();
            }

            return returnEntity;
        }

        public void IngresarInforme(IngresarDocDTO dto)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");
            var requestEntity = new IngresarDocumentoDTORequest {
                IdSgl = dto.IdSgl,

            };


            var responseContent = HttpRequestHelper.Post(urlTransversal + "/IngresarDocumentoWeb" , dto, token.access_token);
            
        }


        /// <summary>
        /// Metodo encargado de orquestar el llamado (PUT) para cambiar Tipo de Persona Juridica.
        /// </summary>
        /// <returns>IngresarSGL DTO</returns>
        public BasePropertiesDTO ActualizarPersona(IngresarSGLDTO entityToUse)
        {
            var requestEntity = new ActualizarPersonaDTORequest
            {
                CodigoPersona = entityToUse.CodigoPersona,
                CodigoTipoPersona = entityToUse.IdTipoPersona,
                Nombre = entityToUse.Nombre,
                ApellidoPaterno = entityToUse.ApellidoPaterno,
                ApellidoMaterno = entityToUse.ApellidoMaterno
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Put(CabeceraSGLUrl + "/ActualizarPersona", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Put(CabeceraSGLUrl + "/ActualizarPersona", requestEntity, token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<IngresarSGLDTO>>();
            }

            return returnEntity;
        }

        public BasePropertiesDTO GetTipoLegalizacion(string codigoTipoPersona, string codigoEstLegal)
        {

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlEntradaCompletaDocEscritura + "/GetTipoLegalizacion?codigoTipoPersona=" + codigoTipoPersona + "&codigoEstLegal=" + codigoEstLegal, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<TipoLegalizacionDTO>>();
            }

            return returnEntity;
        }

        public BasePropertiesDTO GetListaNotaria()
        {

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/GetListaNotaria", token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ObtenerListaNotariaDTOResponse>>();
            }

            return returnEntity;
        }


        public BasePropertiesDTO ObtenerRegistros()
        {

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlEntradaCompletaDocEscritura + "/ObtenerRegistros", token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ObtenerRegistrosDTO>>();
            }

            return returnEntity;
        }

        public BasePropertiesDTO ObtenerDatosEstatutos(int codigoPersonaJuridica)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlConsultaPoderes + "/ConsultaPersonaJuridicaDatos?codigoPersonaJuridica=" + codigoPersonaJuridica, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaPersonaJuridicaDatosDTOResponse>>();
            }
            return returnEntity;
        }

        public VerInformeCapitalDTO ObtenerDatosEstatutosInforme(VerInformeCapitalDTO dto)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlConsultaPoderes + "/ConsultaPersonaJuridicaDatos?codigoPersonaJuridica=" + dto.CodigoPersona, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaPersonaJuridicaDatosDTOResponse>>();
            }

            dto.FechaActualizacion = returnEntity.Resultado[0].FechaActualizacion;
            dto.FechaActualizacionString = dto.FechaActualizacion.ToString("dd/MM/yyyy");

            if (dto.FechaActualizacionString == "31/12/3000")
            {
                dto.FechaActualizacionString = "";
            }
            else
            {
                dto.FechaActualizacionString = dto.FechaActualizacion.ToString("dd/MM/yyyy");
            }

            return dto;
        }


        public BasePropertiesDTO GetDatosOrigen(string codigoTipoDocumento, string codigoTipoPersona)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(CabeceraSGLUrl + "/GetDatosOrigen?codigoTipoDocumento=" + codigoTipoDocumento + "&codigoTipoPersona=" + codigoTipoPersona, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ObtenerDatosOrigenDTOResponse>>();
            }

            return returnEntity;
        }


        public BasePropertiesDTO ConsultarListaPoderes(string id)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ConsultarListaPoderes?id=" + id, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaListaPoderesDTOResponse>>();
            }

            return returnEntity;
        }


        public BasePropertiesDTO ObtieneCantidadGruposDocumento(string codigoDocumento, int tipoGrupo)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

#if DEBUG 
            //var responseContent = HttpRequestHelper.Get("http://localhost:30009/api/Transversal" + "/ObtieneCantidadGruposDocumento?codigoDocumento=" + codigoDocumento + "&tipoGrupo=" + tipoGrupo, token.access_token);
#endif
            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtieneCantidadGruposDocumento?codigoDocumento=" + codigoDocumento + "&tipoGrupo=" + tipoGrupo, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ObtieneCantidadGruposDocumentoDTO>>();
            }

            return returnEntity;
        }


        public BasePropertiesDTO GetCantidadParticipantesDocumento(string codigoDocumento, string tipoParticipante)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlEntradaCompletaDocEscritura + "/GetCantidadParticipantesDocumento?codigoDocumento=" + codigoDocumento + "&tipoParticipante=" + tipoParticipante, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ObtenerCantParticipantesDocDTO>>();
            }

            return returnEntity;
        }

        public BasePropertiesDTO DeleteDocumento(string codigoDocumento)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Delete(urlEntradaCompletaDocEscritura + "/DeleteDocumento?codigoDocumento=" + codigoDocumento, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<BasePropertiesDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo que retorna una lista de revocaciones
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public BasePropertiesDTO ConsultarListaRevocaciones(string id)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ConsultarListaRevocaciones?id=" + id, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaRevocacionesDTOResponse>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de agregar un nuevo documento.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>UsuarioDTO</returns>
        public BasePropertiesDTO IngresarDatosDocumento(IngresarDatosDocumentosDTO entityToUse)
        {
            var requestEntity = new IngresarDatosDocumentosDTORequest
            {
                NombreOrigen = entityToUse.NombreOrigen,
                LugarOrigen = entityToUse.LugarOrigen,
                Observaciones = entityToUse.Observaciones,
                EstadoConsulta = entityToUse.EstadoConsulta,
                FechaInicio = entityToUse.FechaInicio,
                FechaFinal = entityToUse.FechaFinal,
                CodigoRevocacion = entityToUse.CodigoRevocacion,
                CodigoDocumentoOrigen = entityToUse.CodigoDocumentoOrigen,
                FechaResolucionQuiebra = entityToUse.FechaResolucionQuiebra,
                FechaPublicacionQuiebra = entityToUse.FechaPublicacionQuiebra,
                ResponsableDocumento = entityToUse.ResponsableDocumento,
                LugarDocumento = entityToUse.LugarDocumento,
                NombreInforme = entityToUse.NombreInforme,
                TipoDocumento = entityToUse.TipoDocumento,
                FechaRenovacion = entityToUse.FechaRenovacion,
                ObservacionPoder = entityToUse.Observaciones,
                CodigoTribunal = entityToUse.CodigoTribunal,
                CodigoNotariaProtocolo = entityToUse.CodigoNotariaProtocolo,
                FechaProtocolarizacion = entityToUse.FechaProtocolarizacion,
                FechaPublicacionDecreto = entityToUse.FechaPublicacionDecreto,
                CodigoPersona = entityToUse.CodigoPersona,
                IdSGL = entityToUse.IdSGL,
                CodigoTipoLegal = entityToUse.CodigoTipoLegal,
                FechaDocumento = entityToUse.FechaDocumento,
                NumeroDocumento = entityToUse.NumeroDocumento,
                CodigoNotaria = entityToUse.CodigoNotaria,
                FechaInscripcion = entityToUse.FechaInscripcion,
                AnnoInscripcion = entityToUse.AnnoInscripcion,
                Fojas = entityToUse.Fojas,
                NumeroFoja = entityToUse.NumeroFoja,
                CodigoConservador = entityToUse.CodigoConservador,
                FechaPublicacion = entityToUse.FechaPublicacion,
                FechaSesionDirectorio = entityToUse.FechaSesionDirectorio,
                FechaJuntaAccionista = entityToUse.FechaJuntaAccionista,
                CodigoRectificacion = entityToUse.CodigoRectificacion,
                FechaDecreto = entityToUse.FechaDecreto.ToString(),
                NumeroDecreto = entityToUse.NumeroDecreto,
                FechaActVigencia = entityToUse.FechaActVigencia,
                FechaTerminaVigencia = entityToUse.FechaTerminaVigencia

            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(PoderesUrl + "/IngresarDatosDocumento/", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(urlPoderesRevocaciones + "/IngresarDatosDocumento/", requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null)//problema al  retornar statusCode badrequest 500 la validacion de que ya esta creado en sistema    && responseContent.IsSuccessStatusCode
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<IngresarDatosDocumentosDTOResponse>>();
                //returnEntity = responseContent.ContentAsType<IngresarDatosDocumentosDTOResponse>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de Modificar un documento
        /// </summary>
        /// <param name="entityToUse"></param>
        /// <returns>UsuarioDTO</returns>
        public BasePropertiesDTO ActualizarDatosDocumento(IngresarDatosDocumentosDTO entityToUse)
        {
            var requestEntity = new ModificarDocumentoLegalDTORequest
            {
                CodigoDocumento = entityToUse.CodDocumento,
                DescripcionOrigen = entityToUse.NombreOrigen,
                LugarOrigen = entityToUse.LugarOrigen,
                Observaciones = entityToUse.Observaciones,
                EstadoConsulta = entityToUse.EstadoConsulta,
                FechaInicio = entityToUse.FechaInicio,
                FechaFinal = entityToUse.FechaFinal,
                CodigoRevocion = entityToUse.CodigoRevocacion,
                CodigoOrigenDocumento = entityToUse.CodigoDocumentoOrigen,
                FechaQuiebraResolucion = entityToUse.FechaResolucionQuiebra,
                FechaQuiebraPbl = entityToUse.FechaPublicacionQuiebra,
                Responsable = entityToUse.ResponsableDocumento,
                LugarDocumento = entityToUse.LugarDocumento,
                NombreInforme = entityToUse.NombreInforme,
                TipoDocumento = entityToUse.TipoDocumento,
                FechaRenovacion = entityToUse.FechaRenovacion,
                //ObservacionPoder = entityToUse.Observaciones,
                CodigoTribunal = entityToUse.CodigoTribunal,
                NumeroNotariaProtocolo = entityToUse.CodigoNotariaProtocolo,
                FechaProtocolo = entityToUse.FechaProtocolarizacion,
                FechaPblDecreto = entityToUse.FechaPublicacionDecreto,
                //CodigoPersona = entityToUse.CodigoPersona,
                //IdSGL = entityToUse.IdSGL,
                CodigoTipoLegalizacion = entityToUse.CodigoTipoLegal,
                FechaDocumento = entityToUse.FechaDocumento,
                NumeroDocumneto = entityToUse.NumeroDocumento,
                CodigoNotaria = entityToUse.CodigoNotaria,
                FechaInscripcion = entityToUse.FechaInscripcion,
                AnyoInscripcion = entityToUse.AnnoInscripcion,
                Fojas = entityToUse.Fojas,
                NumeroFojas = entityToUse.NumeroFoja,
                CodigoConservador = entityToUse.CodigoConservador,
                FechaPublicacion = entityToUse.FechaPublicacion,
                FechaSessionDirectorio = entityToUse.FechaSesionDirectorio,
                FechaJuntaAccion = entityToUse.FechaJuntaAccionista,
                CodigoRectificacion = entityToUse.CodigoRectificacion,
                FechaDecreto = entityToUse.FechaDecreto.ToString(),
                NumeroDecreto = entityToUse.NumeroDecreto,
                FechaVigenciaActiva = entityToUse.FechaActVigencia,
                FechaTerminoVigencia = entityToUse.FechaTerminaVigencia

            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            //LegalizacionUrl
            var responseContent = HttpRequestHelper.Post(LegalizacionUrl + "/UpdateDocumentoLegal/", requestEntity, token.access_token);
            //var responseContent = HttpRequestHelper.Post("http://localhost:57011/api/Legalizacion" + "/UpdateDocumentoLegal/", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(LegalizacionUrl + "/UpdateDocumentoLegal/", requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null)//problema al  retornar statusCode badrequest 500 la validacion de que ya esta creado en sistema    && responseContent.IsSuccessStatusCode
            {
                returnEntity = responseContent.ContentAsType<LegalizacionDTOResponse>();
                //returnEntity = responseContent.ContentAsType<IngresarDatosDocumentosDTOResponse>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Actualiza el tribunal del documento
        /// </summary>
        /// <param name="codDocumento"></param>
        /// <param name="glosa"></param>
        /// <returns></returns>
        public BasePropertiesDTO ActualizaTribunal(string codDocumento, string glosa)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(LegalizacionUrl + "/UpdateTribunal?codDocumento=" + codDocumento + "&glosa=" + glosa, token.access_token);
            //var responseContent = HttpRequestHelper.Get("http://localhost:57011/api/Legalizacion" + "/UpdateTribunal?codDocumento=" + codDocumento + "&glosa=" + glosa, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<TipoLegalizacionDTOResponse>>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Agrega tribunal
        /// </summary>
        /// <param name="tribunal"></param>
        /// <returns></returns>
        public BasePropertiesDTO AgregarTribunal(string tribunal)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(LegalizacionUrl + "/AddTribunal?tribunal=" + tribunal, token.access_token);
            //var responseContent = HttpRequestHelper.Get("http://localhost:57011/api/Legalizacion" + "/AddTribunal?tribunal=" + tribunal, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<TipoLegalizacionDTOResponse>>();
            }
            return returnEntity;
        }

        public BasePropertiesDTO ObtenerDocumentoLegal(int codigoDocumentoLegal)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ObtenerDocumentoLegal?codigoDocumentoLegal=" + codigoDocumentoLegal, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaAPI.Transversal.DTO.DocumentoLegalDTOResponse>>();
            }
            return returnEntity;
        }

        private string FormateaFechaString(string fecha)
        {
            if (string.IsNullOrEmpty(fecha))
                return fecha;

            if (fecha.Contains("3000"))
                return string.Empty;
            else
                return fecha.Substring(0, 10);
        }

        #region Cambiar Estado SGL

        /// <summary>
        /// ]Metodo que ejecuta la API para obtener datos de la SGL filtrada por codigo
        /// </summary>
        /// <param name="codigoSGL"></param>
        /// <returns>Codigo SGL y codigo Estaod</returns>
        public EstadoSGLDTO GetSGL(string codigoSGL)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlConsultaPoderes + "/GetSGLPorCodigoSGL?id=" + codigoSGL, token.access_token);

            EstadoSGLDTO estadoSGL = new EstadoSGLDTO();

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlConsultaPoderes + "/GetSGLPorCodigoSGL?id=" + codigoSGL, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = (dynamic)responseContent.ContentAsType<IEnumerable<ConsultaSGLPorSGLIdDTOResponse>>();
            }

            estadoSGL.CodigoSGL = returnEntity.Resultado[0].SGLId;
            estadoSGL.CodigoEstadoSGL = returnEntity.Resultado[0].EstadoSGL;

            return estadoSGL;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdSGL">Codigo identificador de SGL</param>
        /// <param name="codigoEstado">Codigo Estado para actualizar SGL</param>
        /// <param name="codigoUsusario">Codigo del usuario conectado</param>
        /// <returns></returns>
        public BasePropertiesDTO ActualizarEstadoSGL(int IdSGL, int codigoEstado, int codigoUsusario)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            NuevoEstadoSGLDTORequest nuevoEstado = new NuevoEstadoSGLDTORequest()
            {
                SglId = IdSGL,
                Estado = codigoEstado,
                CodigoPersonaModificador = codigoUsusario
            };

            var responseContent = HttpRequestHelper.Put(CabeceraSGLUrl + "/ActualizarEstadoSGL", nuevoEstado, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Put(CabeceraSGLUrl + "/ActualizarEstadoSGL", nuevoEstado, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<ResultadoActualizacionEstadoSGLDTOResponse>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Agrega Revocacion
        /// </summary>
        /// <param name="tipoRevocacion"></param>
        /// <param name="descripcion"></param>
        /// <param name="codDoc"></param>
        /// <returns></returns>
        public BasePropertiesDTO AgregarRevocacion(int tipoRevocacion, string descripcion, int codDoc)
        {
            AgregarRevocacionDTORequest requestEntity = new AgregarRevocacionDTORequest
            {
                CodigoDocumento = codDoc,
                Descripcion = descripcion,
                TipoRevocacion = tipoRevocacion
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(RevocacionUrl + "/AgregarRevocacion/", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(RevocacionUrl + "/AgregarRevocacion/", requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null)//problema al  retornar statusCode badrequest 500 la validacion de que ya esta creado en sistema    && responseContent.IsSuccessStatusCode
            {
                returnEntity = responseContent.ContentAsType<RevocacionDTOResponse>();
                //returnEntity = responseContent.ContentAsType<IngresarDatosDocumentosDTOResponse>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Obtiene Datos Poderes por id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public BasePropertiesDTO ConsultarDatosPoderes(string id)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlTransversal + "/ConsultarDatosPoderes?id=" + id, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<ConsultaDatosPoderDTOResponse>>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Graba la revocacion
        /// </summary>
        /// <param name="paramRevo"></param>
        /// <returns></returns>
        public BasePropertiesDTO GrabarRevocacionEscritura(RevocacionEscrituraDTO paramRevo)
        {
            GrabarRevocacionEscrituraDTORequest requestEntity = new GrabarRevocacionEscrituraDTORequest
            {
                CodigoDocumento = paramRevo.CodigoDocumento,
                CodigoRevocacion = paramRevo.CodigoRevocacion,
                Estado = paramRevo.Estado,
                FechaFinal = paramRevo.FechaFinal
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(CabeceraSGLUrl + "/GrabarRevocacionEscritura/", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(CabeceraSGLUrl + "/GrabarRevocacionEscritura/", requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null)//problema al  retornar statusCode badrequest 500 la validacion de que ya esta creado en sistema    && responseContent.IsSuccessStatusCode
            {
                returnEntity = responseContent.ContentAsType<GrabarRevocacionEscrituraDTOResponse>();
                //returnEntity = responseContent.ContentAsType<IngresarDatosDocumentosDTOResponse>();
            }

            return returnEntity;
        }

        #endregion

        #region Terminar SGL

        /// <summary>
        /// Cambia el estado de una SGL a terminado(SGL UP Terminado)
        /// </summary>
        /// <param name="IdSGL">Id de la SGL</param>
        /// <returns></returns>
        public BasePropertiesDTO TerminarSGL(int IdSGL)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(CabeceraSGLUrl + "/TerminarSGL?idSgl=" + IdSGL, token.access_token);

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<TerminarSGLDTOResponse>>();
            }
            return returnEntity;
        }



        #endregion

        #endregion
    }
}
