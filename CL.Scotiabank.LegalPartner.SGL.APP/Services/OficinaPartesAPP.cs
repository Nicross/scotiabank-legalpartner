﻿using CL.Scotiabank.LegalPartner.SGL.DAL;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Clase de servicios para la entidad OficinaPartes del sistema.
    /// </summary>
    public class OficinaPartesAPP : IOficinaPartesAPP
    {
        #region Memebers

        private ITokenAPP _tokenAPP;
        private IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string urlOficinaPartess = Resources.OficinaPartesUrl;

        #endregion

        #region Constructor

        public OficinaPartesAPP(ITokenAPP tokenAPP, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            this._tokenAPP = tokenAPP;
            this._configuration = configuration;
            this._httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de oficinaPartess.
        /// </summary>
        /// <returns>Lista de OficinaPartesDTO</returns>
        public BasePropertiesDTO Get()
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlOficinaPartess, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlOficinaPartess, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<OficinaPartesDTO>>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de un dato desde la api web de oficinaPartess.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public BasePropertiesDTO Get(int id)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlOficinaPartess + "/" + id, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlOficinaPartess + "/" + id, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<OficinaPartesDTO>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de oficinaPartess.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>OficinaPartesDTO</returns>
        public BasePropertiesDTO Create(OficinaPartesDTO entityToUse)
        {
            var requestEntity = new OficinaPartesDTORequest
            {
                Codigo = entityToUse.Codigo,
                Descripcion = entityToUse.Descripcion
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlOficinaPartess, requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Post(urlOficinaPartess, requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<OficinaPartesDTO>();
            }
            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Put) y el retorno de datos desde la api web de oficinaPartess.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>OficinaPartesDTO</returns>
        public BasePropertiesDTO Edit(OficinaPartesDTO entityToUse)
        {
            var model = new OficinaPartesDTORequest
            {
                Codigo = entityToUse.Codigo
                ,Descripcion = entityToUse.Descripcion
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Put(urlOficinaPartess + "/" + entityToUse.Codigo, model, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Put(urlOficinaPartess + "/" + entityToUse.Codigo, model, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<OficinaPartesDTO>();
            }

            return returnEntity;
        }
        
        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) y el retorno de datos desde la api web de oficinaPartess.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>OficinaPartesDTO</returns>
        public BasePropertiesDTO Delete(OficinaPartesDTO entityToUse)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Delete(urlOficinaPartess + "/" + entityToUse.Codigo, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Delete(urlOficinaPartess + "/" + entityToUse.Codigo, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<OficinaPartesDTO>();
            }

            return returnEntity;
        }



        #endregion
    }
}
