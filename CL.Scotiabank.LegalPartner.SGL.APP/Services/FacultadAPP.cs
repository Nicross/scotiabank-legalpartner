﻿using CL.Scotiabank.LegalPartner.SGL.DAL;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Clase de servicios para la entidad Facultad del sistema.
    /// </summary>
    public class FacultadAPP : IFacultadAPP
    {
        #region Members

        private ITokenAPP _tokenAPP;
        private IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string urlFacultad = Resources.FacultadUrl;

        #endregion

        #region Constructor

        public FacultadAPP(ITokenAPP tokenAPP, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            this._tokenAPP = tokenAPP;
            this._configuration = configuration;
            this._httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de conservadores.
        /// </summary>
        /// <returns>Lista de FacultadDTO</returns>
        public BasePropertiesDTO Get()
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlFacultad, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlFacultad, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<FacultadDTO>>();
            }

            return returnEntity;

        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de conservadores.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>FacultadDTO</returns>
        public BasePropertiesDTO Get(int id)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlFacultad+"/"+id, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(urlFacultad + "/" + id, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<FacultadDTO>();
            }
            return returnEntity;
        }
        
        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de conservadores.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>FacultadDTO</returns>
        public BasePropertiesDTO Create(FacultadDTO entityToUse)
        {
            FacultadDTORequest requestEntity = new FacultadDTORequest
            {
                Descripcion = entityToUse.Descripcion
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            System.Net.Http.HttpResponseMessage responseContent = HttpRequestHelper.Post(urlFacultad, requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Post(urlFacultad, requestEntity, token.access_token);
            }

            dynamic returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<FacultadDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Put) y el retorno de datos desde la api web de conservadores.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>FacultadDTO</returns>
        public BasePropertiesDTO Edit(FacultadDTO entityToUse)
        {
            var model = new FacultadDTORequest
            {
                Codigo = entityToUse.Codigo
                ,Descripcion = entityToUse.Descripcion
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Put(urlFacultad + "/" + entityToUse.Codigo, model, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Put(urlFacultad + "/" + entityToUse.Codigo, model, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<FacultadDTO>();
            }

            return returnEntity;
        }
        
        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) y el retorno de datos desde la api web de conservadores.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>FacultadDTO</returns>
        public BasePropertiesDTO Delete(FacultadDTO entityToUse)
        {
            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Delete(urlFacultad + "/" + entityToUse.Codigo, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Delete(urlFacultad + "/" + entityToUse.Codigo, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<FacultadDTO>();
            }

            return returnEntity;
        }


        #endregion
    }
}
