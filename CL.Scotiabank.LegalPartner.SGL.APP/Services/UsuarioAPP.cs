﻿using CL.Scotiabank.LegalPartner.MantenedoresAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.DAL;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Web;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Clase de servicios para la entidad Usuario del sistema.
    /// </summary>
    public class UsuarioAPP : IUsuarioAPP
    {
       
        #region Memebers

        private ITokenAPP _tokenAPP;
        private IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string urlUsuarios = Resources.UsuarioUrl;

        #endregion

        #region Constructor

        public UsuarioAPP(ITokenAPP tokenAPP, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            this._tokenAPP = tokenAPP;
            this._configuration = configuration;
            this._httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <returns>Lista de ConservadorDTO</returns>
        public BasePropertiesDTO GetUsuarios()
        {         

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlUsuarios + "/GetUsuarios", token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Get(urlUsuarios + "/GetUsuarios", token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<UsuarioDTO>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <param name="id">UsuarioDTO</param>
        /// <returns></returns>
        public BasePropertiesDTO GetUsuario(UsuarioDTO entityToUse)
        {
            var requestEntity = new UsuarioDTORequest
            {
                RutUsuario = Convert.ToInt32(entityToUse.RutUsuario) 

            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlUsuarios + "/GetUsuario/", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(urlUsuarios + "/GetUsuario/", requestEntity, token.access_token);

            }           

            var returnEntity = (dynamic)null;
                       
            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                if (responseContent.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    returnEntity = responseContent.ContentAsType<UsuarioDTO>();
                }

                if (responseContent.StatusCode.Equals(System.Net.HttpStatusCode.BadRequest))
                {
                    returnEntity = responseContent.ContentAsType<IEnumerable<UsuarioDTO>>();
                }

                //returnEntity = responseContent.ContentAsType<UsuarioDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos de  Perfil desde la api web de Usuario.
        /// </summary>
        /// <returns>Lista de ConservadorDTO</returns>
        public BasePropertiesDTO GetPerfil()
        {            

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlUsuarios + "/GetPerfil", token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Get(urlUsuarios + "/GetPerfil", token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<UsuarioDTO>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos de  Perfil desde la api web de Usuario.
        /// </summary>
        /// <returns>Lista de ConservadorDTO</returns>
        public BasePropertiesDTO GetSucursal()
        {
           

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlUsuarios + "/GetSucursal", token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Get(urlUsuarios + "/GetSucursal", token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<UsuarioDTO>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos de  Perfil y Claves desde la api web de Usuario.
        /// </summary>
        /// <returns>Lista de UsuarioDTO</returns>
        public BasePropertiesDTO GetUsuarioRelacionPerfilClaves()
        {
            

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(urlUsuarios + "/GetUsuarioRelacionPerfilClaves", token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Get(urlUsuarios + "/GetUsuarioRelacionPerfilClaves", token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<UsuarioDTO>>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <param name = "entityToUse" ></ param >
        /// <returns>UsuarioDTO</returns>
        public BasePropertiesDTO GetUsuarioRut(UsuarioDTO entityToUse)
        {
            var requestEntity = new UsuarioConsultaRutDTORequest
            {
                RutUsuario= Convert.ToInt32(entityToUse.RutUsuario)

            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlUsuarios + "/GetUsuarioRut/", requestEntity, token.access_token);
           
            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(urlUsuarios + "/GetUsuarioRut/", requestEntity, token.access_token);

            }                      

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<UsuarioDTO>();
            }

            return returnEntity;
        }

     
        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <param name = "entityToUse" ></ param >
        /// <returns>UsuarioDTO</returns>
        public BasePropertiesDTO GetUsuarioVigencia(UsuarioDTO entityToUse)
        {
            var rut = entityToUse.RutUsuario;

            var requestEntity = new UsuarioDTORequest
            {
                RutUsuario = Convert.ToInt32(rut)

            };            

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlUsuarios + "/GetUsuarioVigencia/", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(urlUsuarios + "/GetUsuarioVigencia/", requestEntity, token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<UsuarioDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <param name="entityToUse"></param>
        /// <returns>UsuarioDTO</returns>
        public BasePropertiesDTO GetSucursalPorCodigoUsuario(UsuarioDTO entityToUse)
        {           

            var requestEntity = new UsuarioDTORequest
            {
                Codigo = entityToUse.Codigo
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");
                    
            var responseContent = HttpRequestHelper.Post(urlUsuarios + "/GetSucursalesPorCodigoUsuario/", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(urlUsuarios + "/GetSucursalesPorCodigoUsuario/", requestEntity, token.access_token);
            }                     

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<UsuarioDTO>>();//IEnumerable
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <param name = "entityToUse" ></ param >
        /// <returns>UsuarioDTO</returns>
        public BasePropertiesDTO GetConsultaSucursal(UsuarioDTO entityToUse)
        {
            var requestEntity = new UsuarioDTORequest
            {
                RutUsuario = Convert.ToInt32(entityToUse.RutUsuario),
                IdSucursal = entityToUse.IdSucursal
            };                     

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlUsuarios + "/GetConsultaSucursal/", requestEntity, token.access_token);
          
            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(urlUsuarios + "/GetConsultaSucursal/", requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<UsuarioDTO>();
            }

            return returnEntity;
        }
        
        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>UsuarioDTO</returns>
        public BasePropertiesDTO CreateUsuario(UsuarioDTO entityToUse)
        {
            

            var requestEntity = new UsuarioDTORequest
            {                
                RutUsuario = Convert.ToInt32(entityToUse.RutUsuario),
                DigitoVerificador = entityToUse.DigitoVerificador,
                Nombre = entityToUse.Nombre,
                ApellidoPaterno = entityToUse.ApellidoPaterno,
                ApellidoMaterno = entityToUse.ApellidoMaterno,
                Telefono = entityToUse.Telefono,
                Email = entityToUse.Email,
                Contraseña = entityToUse.Contraseña,
                CodigoPerfil = entityToUse.CodigoPerfil,
                IdSucursal = entityToUse.IdSucursal,
                UsuarioSession = entityToUse.SesionUsuario
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlUsuarios + "/CreateUsuario/", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(urlUsuarios + "/CreateUsuario/", requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null)//problema al  retornar statusCode badrequest 500 la validacion de que ya esta creado en sistema    && responseContent.IsSuccessStatusCode
            {
                returnEntity = responseContent.ContentAsType<UsuarioDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (CreateReingresarUsuario) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>UsuarioDTO</returns>
        public BasePropertiesDTO CreateReingresarUsuario(UsuarioDTO entityToUse)
        {
            var requestEntity = new UsuarioDTORequest
            {
                RutUsuario = Convert.ToInt32(entityToUse.RutUsuario),           
                Telefono = entityToUse.Telefono,
                Email = entityToUse.Email,
                Contraseña = entityToUse.Contraseña,
                CodigoPerfil = entityToUse.CodigoPerfil,
                IdSucursal = entityToUse.IdSucursal
            };            

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlUsuarios + "/CreateReingresarUsuario/", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(urlUsuarios + "/CreateReingresarUsuario/", requestEntity, token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null)
            {
                returnEntity = responseContent.ContentAsType<UsuarioDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (UpdateUsuarioNuevo) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>UsuarioDTO</returns>
        public BasePropertiesDTO UpdateUsuarioNuevo(UsuarioDTO entityToUse)
        {
            var requestEntity = new UsuarioDTORequest
            {
                Codigo = entityToUse.Codigo,                
                CodigoPerfil = entityToUse.CodigoNuevoPerfil,
                IdSucursal = entityToUse.IdSucursal
            };           

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlUsuarios + "/UpdateGrabaUsuarioNuevo/", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(urlUsuarios + "/UpdateGrabaUsuarioNuevo/", requestEntity, token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null)
            {
                returnEntity = responseContent.ContentAsType<UsuarioDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>UsuarioDTO</returns>
        public BasePropertiesDTO CreateRelacionPerfil(UsuarioDTO entityToUse)
        {
            var requestEntity = new UsuarioDTORequest
            {
                RutUsuario = Convert.ToInt32(entityToUse.RutUsuario),
                DigitoVerificador = entityToUse.DigitoVerificador,
                CodigoPerfil = entityToUse.CodigoPerfil,
                IdSucursal = entityToUse.IdSucursal
            };

        

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlUsuarios + "/CreateRelacionPerfil/", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(urlUsuarios + "/CreateRelacionPerfil/", requestEntity, token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<UsuarioDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Put) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>UsuarioDTO</returns>
        public BasePropertiesDTO UpdateUsuario(UsuarioDTO entityToUse)
        {
            var requestEntity = new UsuarioDTORequest
            {
                Codigo = entityToUse.Codigo,
                RutUsuario = Convert.ToInt32(entityToUse.RutUsuario),
                DigitoVerificador = entityToUse.DigitoVerificador,
                Nombre = entityToUse.Nombre,
                ApellidoPaterno = entityToUse.ApellidoPaterno,
                ApellidoMaterno = entityToUse.ApellidoMaterno,
                Telefono = entityToUse.Telefono,
                Email = entityToUse.Email,
                CodigoPerfil = entityToUse.CodigoPerfil,
                IdSucursal = entityToUse.IdSucursal,
                UsuarioSession = "11111111"
            };



            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Put(urlUsuarios + "/UpdateUsuario/", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Put(urlUsuarios + "/UpdateUsuario/", requestEntity, token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<UsuarioDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Put) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>UsuarioDTO</returns>
        public BasePropertiesDTO UpdateUsuarioSucursal( UsuarioDTO entityToUse)
        {
            var requestEntity = new UsuarioDTORequest
            {
                Codigo = entityToUse.Codigo,
                IdNuevoSucursal = entityToUse.IdNuevoSucursal,
                IdSucursal = entityToUse.IdSucursal
            };          

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Put(urlUsuarios + "/UpdateUsuarioSucursal/", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Put(urlUsuarios + "/UpdateUsuarioSucursal/", requestEntity, token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<UsuarioDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Put) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>UsuarioDTO</returns>
        public BasePropertiesDTO UpdateUsuarioPerfil(UsuarioDTO entityToUse)
        {
            var requestEntity = new UsuarioDTORequest
            {
                Codigo = entityToUse.Codigo, 
                CodigoPerfil = entityToUse.CodigoPerfil                
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Put(urlUsuarios + "/UpdateUsuarioPerfil/" + entityToUse.Codigo , requestEntity, token.access_token);


            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Put(urlUsuarios + "/UpdateUsuarioPerfil/" + entityToUse.Codigo, requestEntity, token.access_token);

            }


            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<UsuarioDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) y el retorno de datos desde la api web de conservadores.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>ConservadorDTO</returns>
        public BasePropertiesDTO DeleteUsuario(UsuarioDTO entityToUse)
        {
            var requestEntity = new UsuarioDTORequest
            {
                Codigo = entityToUse.Codigo,
                RutUsuario = Convert.ToInt32(entityToUse.RutUsuario)
            };           

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlUsuarios + "/DeleteUsuario/", requestEntity, token.access_token);


            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(urlUsuarios + "/DeleteUsuario/", requestEntity, token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<UsuarioDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) y el retorno de datos desde la api web de conservadores.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>ConservadorDTO</returns>
        public BasePropertiesDTO DeleteUsuarioConSucursal(UsuarioDTO entityToUse)
        {
            var requestEntity = new UsuarioDTORequest
            {
                Codigo = entityToUse.Codigo,
                RutUsuario = Convert.ToInt32(entityToUse.RutUsuario),
                IdSucursal = entityToUse.IdSucursal
            };           

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlUsuarios + "/DeleteUsuarioSucursal/", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(urlUsuarios + "/DeleteUsuarioSucursal/", requestEntity, token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<UsuarioDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <param name = "entityToUse" ></ param >
        /// <returns>UsuarioDTO</returns>
        public BasePropertiesDTO GetPerfilUsuario(UsuarioDTO entityToUse)
        {
            var requestEntity = new UsuarioDTORequest
            {
                Codigo = Convert.ToInt32(entityToUse.Codigo)
            };

           

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Post(urlUsuarios + "/GetPerfilCodigoUsuario/", requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                responseContent = HttpRequestHelper.Post(urlUsuarios + "/GetPerfilCodigoUsuario/", requestEntity, token.access_token);

            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<UsuarioDTO>>();
            }

            return returnEntity;
        }
        #endregion

    }
}
