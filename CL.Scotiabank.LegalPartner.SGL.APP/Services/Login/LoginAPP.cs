﻿using CL.Scotiabank.LegalPartner.SGL.BLL;
using CL.Scotiabank.LegalPartner.SGL.DAL;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Clase de servicios para la entidad Conservador del sistema.
    /// </summary>
    public class LoginAPP : ILoginAPP
    {
        #region Members

        private ILoginBLL _loginBLL;
        private ITokenAPP _tokenAPP;
        private IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;

        #endregion

        #region Constructor

        public LoginAPP(ILoginBLL loginBLL, ITokenAPP tokenAPP, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            this._loginBLL = loginBLL;
            this._tokenAPP = tokenAPP;
            this._configuration = configuration;
            this._httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Métodos Privados

        /// <summary>
        /// Ejecuta el metodo GetUsuarioAdmin de la Api de Accesos.
        /// </summary>
        /// <param name="entityToUse">Entidad utilizada para crear el Request</param>
        /// <returns>BasePropertiesDTO con codigo de retorno y LoginDTO</returns>
        private BasePropertiesDTO GetUsuarioAdmin(LoginDTO entityToUse)
        {
            var requestEntity = new LoginDTORequest
            {
                RutUsuario = entityToUse.Rut,
                PasswordUsuario = EncodeDecode.Encrypt(_configuration["Encrypt:key"], entityToUse.Password)
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(Resources.LoginObtenerUsuarioAdmim + "?RutUsuario=" + requestEntity.RutUsuario + "&PasswordUsuario=" + requestEntity.PasswordUsuario, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(Resources.LoginObtenerUsuarioAdmim + "?RutUsuario=" + requestEntity.RutUsuario + "&PasswordUsuario=" + requestEntity.PasswordUsuario, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<LoginDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Ejecuta el metodo GetUsuarioLogin de la Api de Accesos.
        /// </summary>
        /// <param name="entityToUse">Entidad utilizada para crear el Request</param>
        /// <returns>BasePropertiesDTO con codigo de retorno y LoginDTO</returns>
        private BasePropertiesDTO GetUsuarioLogin(LoginDTO entityToUse)
        {
            var requestEntity = new LoginDTORequest
            {
                RutUsuario = entityToUse.Rut,
                PasswordUsuario = EncodeDecode.Encrypt(_configuration["Encrypt:key"], entityToUse.Password)
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(Resources.LoginObtenerUsuarioLogin + "?RutUsuario=" + requestEntity.RutUsuario + "&PasswordUsuario=" + requestEntity.PasswordUsuario, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { access_token = _configuration["Token:key"] });

                responseContent = HttpRequestHelper.Get(Resources.LoginObtenerUsuarioLogin + "?RutUsuario=" + requestEntity.RutUsuario + "&PasswordUsuario=" + requestEntity.PasswordUsuario, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<LoginDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Ejecuta el metodo GetUsuarioPassword de la Api de Accesos.
        /// </summary>
        /// <param name="entityToUse">Entidad utilizada para crear el Request</param>
        /// <returns>BasePropertiesDTO con codigo de retorno y LoginDTO</returns>
        private BasePropertiesDTO GetUsuarioPassword(LoginDTO entityToUse)
        {
            var requestEntity = new LoginDTORequest
            {
                RutUsuario = entityToUse.Rut
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(Resources.LoginObtenerPassword + "?RutUsuario=" + requestEntity.RutUsuario, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { access_token = _configuration["Token:key"] });

                responseContent = HttpRequestHelper.Get(Resources.LoginObtenerPassword + "?RutUsuario=" + requestEntity.RutUsuario, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<LoginDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Ejecuta el metodo GetUsuario de la Api de Accesos.
        /// </summary>
        /// <param name="entityToUse">Entidad utilizada para crear el Request</param>
        /// <returns>BasePropertiesDTO con codigo de retorno y LoginDTO</returns>
        private BasePropertiesDTO GetUsuario(LoginDTO entityToUse)
        {
            var requestEntity = new LoginDTORequest
            {
                CodigoUsuario = entityToUse.Codigo
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(Resources.ModificarClaveObtenerUsuario + "/" + requestEntity.CodigoUsuario, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { access_token = _configuration["Token:key"] });

                responseContent = HttpRequestHelper.Get(Resources.ModificarClaveObtenerUsuario + "/" + requestEntity.CodigoUsuario, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<LoginDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Ejecuta el metodo GetUsuario de la Api de Accesos.
        /// </summary>
        /// <param name="entityToUse">Entidad utilizada para crear el Request</param>
        /// <returns>BasePropertiesDTO con codigo de retorno y LoginDTO</returns>
        private BasePropertiesDTO GetPasswordAnterior(LoginDTO entityToUse)
        {
            var requestEntity = new LoginDTORequest
            {
                CodigoUsuario = entityToUse.Codigo
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(Resources.ModificarClaveObtenerPasswordAnterior + "/" + requestEntity.CodigoUsuario, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { access_token = _configuration["Token:key"] });

                responseContent = HttpRequestHelper.Get(Resources.ModificarClaveObtenerPasswordAnterior + "/" + requestEntity.CodigoUsuario, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && responseContent.IsSuccessStatusCode)
            {
                returnEntity = responseContent.ContentAsType<LoginDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Ejecuta el metodo ActualizarHistorialClave de la Api de Accesos.
        /// </summary>
        /// <param name="entityToUse">Entidad utilizada para crear el Request</param>
        /// <returns>BasePropertiesDTO con codigo de retorno y LoginDTO</returns>
        private BasePropertiesDTO ActualizarHistorialClave(LoginDTO entityToUse)
        {
            var requestEntity = new LoginDTORequest
            {
                CodigoUsuario = entityToUse.Codigo,
                PasswordUsuario = EncodeDecode.Encrypt(_configuration["Encrypt:key"], entityToUse.Password)
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Put(Resources.ModificarClaveActualizarHistorialClaves + "/" + requestEntity.CodigoUsuario, requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { access_token = _configuration["Token:key"] });

                responseContent = HttpRequestHelper.Put(Resources.ModificarClaveActualizarHistorialClaves + "/" + requestEntity.CodigoUsuario, requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<LoginDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Ejecuta el metodo ActualizarClave de la Api de Accesos.
        /// </summary>
        /// <param name="entityToUse">Entidad utilizada para crear el Request</param>
        /// <returns>BasePropertiesDTO con codigo de retorno y LoginDTO</returns>
        private BasePropertiesDTO ActualizarClave(LoginDTO entityToUse)
        {
            var requestEntity = new LoginDTORequest
            {
                CodigoUsuario = entityToUse.Codigo,
                PasswordUsuario = EncodeDecode.Encrypt(_configuration["Encrypt:key"], entityToUse.Password)
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Put(Resources.ModificarClaveActualizarClave + "/" + requestEntity.CodigoUsuario, requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { access_token = _configuration["Token:key"] });

                responseContent = HttpRequestHelper.Put(Resources.ModificarClaveActualizarClave + "/" + requestEntity.CodigoUsuario, requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<LoginDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Ejecuta el metodo ActualizarClaveInicio de la Api de Accesos.
        /// </summary>
        /// <param name="entityToUse">Entidad utilizada para crear el Request</param>
        /// <returns>BasePropertiesDTO con codigo de retorno y LoginDTO</returns>
        private BasePropertiesDTO ActualizarClaveInicio(LoginDTO entityToUse)
        {
            var requestEntity = new LoginDTORequest
            {
                CodigoUsuario = entityToUse.Codigo,
                PasswordUsuario = EncodeDecode.Encrypt(_configuration["Encrypt:key"], entityToUse.Password)
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Put(Resources.ModificarClaveActualizarClaveInicio + "/" + requestEntity.CodigoUsuario, requestEntity, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { access_token = _configuration["Token:key"] });

                responseContent = HttpRequestHelper.Put(Resources.ModificarClaveActualizarClaveInicio + "/" + requestEntity.CodigoUsuario, requestEntity, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<LoginDTO>();
            }

            return returnEntity;
        }

        /// <summary>
        /// Ejecuta el metodo GetGrupoUsuario de la Api de Accesos.
        /// </summary>
        /// <param name="entityToUse">Entidad utilizada para crear el Request</param>
        /// <returns>BasePropertiesDTO con codigo de retorno y LoginDTO</returns>
        private BasePropertiesDTO GetGrupoUsuario(LoginDTO entityToUse)
        {
            var requestEntity = new LoginDTORequest
            {
                RutUsuario = entityToUse.Rut,
                DigitoVerificador = entityToUse.DigitoVerificador
            };

            TokenDTO token = _httpContextAccessor.HttpContext.Session.GetObjectFromJson<TokenDTO>("token");

            var responseContent = HttpRequestHelper.Get(Resources.ModificarClaveObtenerGrupoUsuario + "?RutUsuario=" + requestEntity.RutUsuario + "&DigitoVerificador=" + requestEntity.DigitoVerificador, token.access_token);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { access_token = _configuration["Token:key"] });

                responseContent = HttpRequestHelper.Get(Resources.ModificarClaveObtenerGrupoUsuario + "?RutUsuario=" + requestEntity.RutUsuario + "&DigitoVerificador=" + requestEntity.DigitoVerificador, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<LoginDTO>();
            }

            return returnEntity;
        }

        #endregion

        #region Métodos de Servicio

        /// <summary>
        /// Método utilizado para realizar la modificacion de la clave del usuario
        /// cuando ya ha caducado por validación del sistema.
        /// </summary>
        /// <param name="entityToUse">Identificación y nuevos datos del usuario</param>
        /// <returns>BasePropertiesDTO con el codigo de retorno</returns>
        public BasePropertiesDTO ModificarClave(LoginDTO entityToUse)
        {
            if (this._loginBLL.ValidaExistenciaPreviaClave(entityToUse))
            {
                return new BasePropertiesDTO()
                {
                    CodigoRetorno = (int)LoginEstados.ClavePreviamenteIngresada,
                    Resultado = null
                };
            }

            string respaldoPassword = entityToUse.Password;
            entityToUse.Password = entityToUse.PasswordAnterior;

            BasePropertiesDTO loginRetornoUsuarioLogin = this.GetUsuarioLogin(entityToUse);

            entityToUse.Password = respaldoPassword;

            if (loginRetornoUsuarioLogin.CodigoRetorno == 0 || loginRetornoUsuarioLogin.CodigoRetorno == 6 || loginRetornoUsuarioLogin.CodigoRetorno == 7)
            {
                //sva_act_clv_ant_2 este SP no entrega un codigo de retorno indicando o no el exito
                //ademas tiene un Set NoCount Off por lo cual no se puede obtener el numero de filas afectadas.
                BasePropertiesDTO retornoActualizarLogin = this.ActualizarHistorialClave(entityToUse);

                //Adm_UpdateClave
                BasePropertiesDTO retornoActualizarClave = this.ActualizarClave(entityToUse);

                retornoActualizarClave.CodigoRetorno = 5;

                return retornoActualizarClave;
            }
            else if (loginRetornoUsuarioLogin.CodigoRetorno == 5)
            {
                //sva_act_clv_ant_2
                BasePropertiesDTO retornoActualizarLogin = this.ActualizarHistorialClave(entityToUse);

                //Adm_UpdateClave_Inicio
                BasePropertiesDTO retornoActualizarClave = this.ActualizarClaveInicio(entityToUse);

                retornoActualizarClave.CodigoRetorno = 5;

                return retornoActualizarClave;
            }
            else if (loginRetornoUsuarioLogin.CodigoRetorno == 1)
            {
                //resultado = 1 no es el mismo valor que devuelve UpdateClave,
                //asi que lo cambiare por cualquier otro valor que me sirva 3.
                loginRetornoUsuarioLogin.CodigoRetorno = 3;
            }
            else if (loginRetornoUsuarioLogin.CodigoRetorno == 4)
            {
                //en el sistema anterior se utilizaba -4, pero el valor 4 nunca se estaba utilizando
                //asique se dejo el valor para continuar la susecion.
                loginRetornoUsuarioLogin.CodigoRetorno = 4;
            }

            return loginRetornoUsuarioLogin;
        }

        /// <summary>
        /// Metodo utilizado para obtener la informacion del usuario al momento de realizar el Login.
        /// </summary>
        /// <param name="entityToUse">Identificación para obtener al usuario</param>
        /// <returns>BasePropertiesDTO con el codigo de retorno y el loguinDTO</returns>
        public BasePropertiesDTO ObtenerInformacionModificarClave(LoginDTO entityToUse)
        {
            BasePropertiesDTO loginRetornoPassword = this.GetPasswordAnterior(entityToUse);

            BasePropertiesDTO loginRetornoUsuario = this.GetUsuario(entityToUse);

            loginRetornoUsuario.Resultado.CadenaPasswordsAnteriores = loginRetornoPassword.Resultado.CadenaPasswordsAnteriores;

            return loginRetornoUsuario;
        }

        /// <summary>
        /// Metodo para loguearse en el administrador de usuarios y tablas.
        /// </summary>
        /// <param name="entityToUse">loginDTO</param>
        /// <returns>BasePropertiesDTO con el codigo de retorno y el loguinDTO</returns>
        public BasePropertiesDTO Login(LoginDTO entityToUse)
        {
            BasePropertiesDTO loginRetornoUsuarioAdmin = this.GetUsuarioAdmin(entityToUse);

            if (loginRetornoUsuarioAdmin.CodigoRetorno == 0)
            {
                BasePropertiesDTO loginRetornoUsuarioPassword = this.GetUsuarioPassword(entityToUse);

                BasePropertiesDTO loginRetornoUsuarioLogin = this.GetUsuarioLogin(entityToUse);

                //Se asigna en duro ya que el procedimiento de obtener grupos de usuario
                //pregunta por todos los grupos que no sean 1 tambien en duro.
                loginRetornoUsuarioPassword.Resultado.GrupoPerfilId = 1;
                loginRetornoUsuarioPassword.Resultado.GrupoPerfilDescripcion = "FULL ADMINISTRADOR";

                if (loginRetornoUsuarioLogin.CodigoRetorno == 0)
                {
                    return loginRetornoUsuarioPassword;
                }
                else
                {
                    loginRetornoUsuarioPassword.CodigoRetorno = loginRetornoUsuarioLogin.CodigoRetorno;
                    loginRetornoUsuarioPassword.Resultado.DiasRetorno = loginRetornoUsuarioLogin.Resultado.DiasRetorno;

                    return loginRetornoUsuarioPassword;
                }
            }

            return loginRetornoUsuarioAdmin;
        }

        /// <summary>
        /// Método utilizado para loguearse en el sistema para administrar SGL.
        /// </summary>
        /// <param name="entityToUse"></param>
        /// <returns>BasePropertiesDTO con el codigo de retorno y el loguinDTO</returns>
        public BasePropertiesDTO LoginSGL(LoginDTO entityToUse)
        {
            BasePropertiesDTO loginRetornoUsuarioAdmin = this.GetUsuarioLogin(entityToUse);

            if (loginRetornoUsuarioAdmin.CodigoRetorno == 0)
            {
                BasePropertiesDTO loginRetornoUsuarioPassword = this.GetUsuarioPassword(entityToUse);

                BasePropertiesDTO loginRetornoUsuario = this.GetUsuario(loginRetornoUsuarioPassword.Resultado);

                if (loginRetornoUsuarioPassword != null)
                {
                    if (loginRetornoUsuario != null)
                    {
                        if (loginRetornoUsuario.Resultado.GrupoPerfilId == 11 || loginRetornoUsuario.Resultado.GrupoPerfilId == 22)
                        {
                            //Response.Redirect "pagNoAdministrador.asp"
                            loginRetornoUsuarioPassword.CodigoRetorno = 2;

                            return loginRetornoUsuarioPassword;
                        }
                        else
                        {
                            BasePropertiesDTO loginRetornoGrupoUsuario = this.GetGrupoUsuario(entityToUse);

                            loginRetornoUsuarioPassword.Resultado.PerfilId = loginRetornoGrupoUsuario.Resultado.PerfilId;
                            loginRetornoUsuarioPassword.Resultado.GrupoPerfilId = loginRetornoGrupoUsuario.Resultado.GrupoPerfilId;
                            loginRetornoUsuarioPassword.Resultado.GrupoPerfilDescripcion = loginRetornoGrupoUsuario.Resultado.GrupoPerfilDescripcion;

                            loginRetornoUsuarioPassword.CodigoRetorno = 0;

                            return loginRetornoUsuarioPassword;
                        }
                    }
                    else
                    {
                        //-1 codigo de error
                        loginRetornoUsuarioPassword.CodigoRetorno = -1;

                        return loginRetornoUsuarioPassword;
                    }
                }
                else
                {
                    //-1 codigo de error
                    loginRetornoUsuarioPassword.CodigoRetorno = -1;

                    return loginRetornoUsuarioPassword;
                }
            }
            else if (loginRetornoUsuarioAdmin.CodigoRetorno == 1)
            {
                return loginRetornoUsuarioAdmin;
                //Response.Redirect "pagNoCoincideClave.asp"
            }
            else if (loginRetornoUsuarioAdmin.CodigoRetorno == 2)
            {
                return loginRetornoUsuarioAdmin;
                //Response.Redirect "pagNoAdministrador.asp"
            }
            else if (loginRetornoUsuarioAdmin.CodigoRetorno == 3)
            {
                return loginRetornoUsuarioAdmin;
                //Response.Redirect "pagNoCliente.asp"
            }
            else if (loginRetornoUsuarioAdmin.CodigoRetorno == 4)
            {
                return loginRetornoUsuarioAdmin;
                //Response.Redirect "pagBloqueado.asp"
            }
            else if (loginRetornoUsuarioAdmin.CodigoRetorno == 5)
            {
                BasePropertiesDTO loginRetornoUsuarioPassword = this.GetUsuarioPassword(entityToUse);

                if (loginRetornoUsuarioPassword != null)
                {
                    //CodGrp = Consultar.ObtenerGrupoUsuario(cstr(strUsername), cstr(strDrt), Cstr(strUsername), Cstr("NoValue"), Cstr(request.ServerVariables("REMOTE_ADDR")))
                    BasePropertiesDTO loginRetornoGrupoUsuario = this.GetGrupoUsuario(entityToUse);

                    loginRetornoUsuarioPassword.Resultado.PerfilId = loginRetornoGrupoUsuario.Resultado.PerfilId;
                    loginRetornoUsuarioPassword.Resultado.GrupoPerfilId = loginRetornoGrupoUsuario.Resultado.GrupoPerfilId;
                    loginRetornoUsuarioPassword.Resultado.GrupoPerfilDescripcion = loginRetornoGrupoUsuario.Resultado.GrupoPerfilDescripcion;

                    loginRetornoUsuarioPassword.CodigoRetorno = 5;

                    return loginRetornoUsuarioPassword;
                    //pagina = "Syp_ModificarClaves.asp"
                }
                else
                {
                    loginRetornoUsuarioAdmin.CodigoRetorno = -1;
                    return loginRetornoUsuarioAdmin;
                    //Response.Redirect "PagAdvertenciaIngreso.asp"	
                }
            }
            else if (loginRetornoUsuarioAdmin.CodigoRetorno == 6)
            {
                BasePropertiesDTO loginRetornoUsuarioPassword = this.GetUsuarioPassword(entityToUse);

                if (loginRetornoUsuarioPassword != null)
                {
                    //CodGrp = Consultar.ObtenerGrupoUsuario(cstr(strUsername), cstr(strDrt), Cstr(strUsername), Cstr("NoValue"), Cstr(request.ServerVariables("REMOTE_ADDR")))
                    BasePropertiesDTO loginRetornoGrupoUsuario = this.GetGrupoUsuario(entityToUse);

                    loginRetornoUsuarioPassword.Resultado.PerfilId = loginRetornoGrupoUsuario.Resultado.PerfilId;
                    loginRetornoUsuarioPassword.Resultado.GrupoPerfilId = loginRetornoGrupoUsuario.Resultado.GrupoPerfilId;
                    loginRetornoUsuarioPassword.Resultado.GrupoPerfilDescripcion = loginRetornoGrupoUsuario.Resultado.GrupoPerfilDescripcion;


                    loginRetornoUsuarioPassword.Resultado.DiasRetorno = loginRetornoUsuarioAdmin.Resultado.DiasRetorno;
                    loginRetornoUsuarioPassword.CodigoRetorno = 6;

                    return loginRetornoUsuarioPassword;
                    //pagina = "Syp_ModificarClaves.asp"
                }
                else
                {
                    loginRetornoUsuarioAdmin.CodigoRetorno = -1;
                    return loginRetornoUsuarioAdmin;
                    //Response.Redirect "PagAdvertenciaIngreso.asp"	
                }
                //Response.Redirect "pagBloqueado.asp"
            }
            else if (loginRetornoUsuarioAdmin.CodigoRetorno == 7)
            {
                BasePropertiesDTO loginRetornoUsuarioPassword = this.GetUsuarioPassword(entityToUse);

                if (loginRetornoUsuarioPassword != null)
                {
                    //modificar la clave
                    loginRetornoUsuarioPassword.CodigoRetorno = 7;

                    return loginRetornoUsuarioPassword;
                    //pagina = "Syp_ModificarClaves.asp"
                }
                else
                {
                    loginRetornoUsuarioAdmin.CodigoRetorno = -1;
                    return loginRetornoUsuarioAdmin;
                    //Response.Redirect "PagAdvertenciaIngreso.asp"	
                }
                //Response.Redirect "pagBloqueado.asp"
            }
            else
            {
                return loginRetornoUsuarioAdmin;
            }
        }

        #endregion
    }
}
