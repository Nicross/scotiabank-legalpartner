﻿using CL.Scotiabank.LegalPartner.SGL.DAL;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    public class TokenAPP : ITokenAPP
    {
        public TokenDTO CreateToken(TokenDTO entityToUse)
        {
            var requestEntity = new TokenDTORequest
            {
                ClaveToken = entityToUse.ClaveSecreta
            };

            var responseContent = HttpRequestHelper.Get(Resources.TokenCreateToken + "?apiToken=" + requestEntity.ClaveToken);

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = JsonConvert.DeserializeObject<TokenDTO>(responseContent.Content.ReadAsStringAsync().Result);
            }

            return returnEntity;
        }
    }
}
