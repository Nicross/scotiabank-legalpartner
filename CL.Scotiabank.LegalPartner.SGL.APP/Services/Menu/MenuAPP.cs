﻿using CL.Scotiabank.LegalPartner.SGL.BLL;
using CL.Scotiabank.LegalPartner.SGL.DAL;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Clase de servicios para la entidad Menu del sistema.
    /// </summary>
    public class MenuAPP : IMenuAPP
    {
        #region Memebers
        
        private ITokenAPP _tokenAPP;

        private readonly IHttpContextAccessor _httpContextAccessor;
        private IConfiguration _configuration;

        #endregion

        #region Constructor

        public MenuAPP(ITokenAPP tokenAPP, IHttpContextAccessor httpContextAccessor, IConfiguration configuration)
        {
            this._tokenAPP = tokenAPP;
            this._httpContextAccessor = httpContextAccessor;
            this._configuration = configuration;
        }

        #endregion

        #region Public Methods

        #region Metodos de Servicio

        /// <summary>
        /// Método encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de conservadores.
        /// </summary>
        /// <returns>Lista de ConservadorDTO</returns>
        public BasePropertiesDTO GetMenusPorCodigoGrupoUsuario(int id)
        {
            var requestEntity = new MenuDTORequest
            {
                CodigoGrupoUsuario = id
            };

            TokenDTO token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

            var responseContent = HttpRequestHelper.Get(Resources.MenuObtenerMenus + "?CodigoGrupoUsuario=" + requestEntity.CodigoGrupoUsuario);

            if (responseContent != null && responseContent.StatusCode.Equals(System.Net.HttpStatusCode.Unauthorized))
            {
                token = _tokenAPP.CreateToken(new TokenDTO() { ClaveSecreta = _configuration["Token:key"] });

                _httpContextAccessor.HttpContext.Session.SetObjectAsJson<TokenDTO>("token", token);

                responseContent = HttpRequestHelper.Get(Resources.MenuObtenerMenus + "?CodigoGrupoUsuario=" + requestEntity.CodigoGrupoUsuario, token.access_token);
            }

            var returnEntity = (dynamic)null;

            if (responseContent != null && !responseContent.StatusCode.Equals(System.Net.HttpStatusCode.InternalServerError))
            {
                returnEntity = responseContent.ContentAsType<IEnumerable<MenuDTO>>();
            }

            return returnEntity;
        }

        #endregion

        #endregion
    }
}
