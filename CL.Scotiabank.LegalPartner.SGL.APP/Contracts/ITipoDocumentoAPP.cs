﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad Tipo de Documento del sistema.
    /// </summary>
    public interface ITipoDocumentoAPP
    {
        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de tipos de documentos.
        /// </summary>
        /// <returns>Lista de TipoDocumentoDTO</returns>
        BasePropertiesDTO Get();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de tipos de documentos.
        /// </summary>
        /// <returns>TipoDocumentoDTO</returns>
        BasePropertiesDTO Get(int id);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de tipos de documentos.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>TipoDocumentoDTO</returns>
        BasePropertiesDTO Create(TipoDocumentoDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Put) y el retorno de datos desde la api web de tipos de documentos.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>TipoDocumentoDTO</returns>
        BasePropertiesDTO Edit(TipoDocumentoDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) y el retorno de datos desde la api web de tipos de documentos.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>TipoDocumentoDTO</returns>
        BasePropertiesDTO Delete(TipoDocumentoDTO entityToUse);
    }
}
