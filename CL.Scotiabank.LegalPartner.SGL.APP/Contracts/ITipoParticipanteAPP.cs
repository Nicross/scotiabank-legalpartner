﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad Tipo de Participante del sistema.
    /// </summary>
    public interface ITipoParticipanteAPP
    {
        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de tipos de participantes.
        /// </summary>
        /// <returns>Lista de TipoParticipanteDTO</returns>
        BasePropertiesDTO Get();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de tipos de participantes.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>TipoParticipanteDTO</returns>
        BasePropertiesDTO Get(int id);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de tipos de participantes.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>TipoParticipanteDTO</returns>
        BasePropertiesDTO Create(TipoParticipanteDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Put) y el retorno de datos desde la api web de tipos de participantes.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>TipoParticipanteDTO</returns>
        BasePropertiesDTO Edit(TipoParticipanteDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) y el retorno de datos desde la api web de tipos de participantes.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>TipoParticipanteDTO</returns>
        BasePropertiesDTO Delete(TipoParticipanteDTO entityToUse);
    }
}
