﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad PerfilUsuario del sistema.
    /// </summary>
    public interface IPerfilUsuarioAPP
    {
        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de perfilUsuarios.
        /// </summary>
        /// <returns>Lista de PerfilUsuarioDTO</returns>
        BasePropertiesDTO Get();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno un dato desde la api web de perfilUsuarios.
        /// </summary>
        /// <returns>PerfilUsuarioDTO</returns>
        BasePropertiesDTO Get(int id);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de perfilUsuarios.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>PerfilUsuarioDTO</returns>
        BasePropertiesDTO Create(PerfilUsuarioDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Put) y el retorno de datos desde la api web de perfilUsuarios.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>PerfilUsuarioDTO</returns>
        BasePropertiesDTO Edit(PerfilUsuarioDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) y el retorno de datos desde la api web de perfilUsuarios.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>PerfilUsuarioDTO</returns>
        BasePropertiesDTO Delete(PerfilUsuarioDTO entityToUse);
    }
}
