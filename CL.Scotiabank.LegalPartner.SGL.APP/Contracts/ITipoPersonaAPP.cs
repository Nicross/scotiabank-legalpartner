﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad Tipo de Persona del sistema.
    /// </summary>
    public interface ITipoPersonaAPP
    {
        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de tipos de personas.
        /// </summary>
        /// <returns>Lista de TipoPersonaDTO</returns>
        BasePropertiesDTO Get();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de tipos de personas.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>TipoPersonaDTO</returns>
        BasePropertiesDTO  Get(int id);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de tipos de personas.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>TipoPersonaDTO</returns>
        BasePropertiesDTO Create(TipoPersonaDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Put) y el retorno de datos desde la api web de tipos de personas.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>TipoPersonaDTO</returns>
        BasePropertiesDTO Edit(TipoPersonaDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) y el retorno de datos desde la api web de tipos de personas.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>TipoPersonaDTO</returns>
        BasePropertiesDTO Delete(TipoPersonaDTO entityToUse);
    }
}
