﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    public interface ICabeceraSGLAPP
    {

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="entityToUse"></param>
        /// <returns></returns>
        BasePropertiesDTO RespaldarNombreAnterior(IngresarSGLDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (PUT) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="entityToUse"></param>
        /// <returns></returns>
        BasePropertiesDTO ActualizarPersona(IngresarSGLDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="entityToUse"></param>
        /// <returns></returns>
        BasePropertiesDTO IngresarSGL(IngresarSGLDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="codigoSucursal"></param>
        /// <returns>Listado de SucursalConAbogadosDTOResponse</returns>
        BasePropertiesDTO ObtenerSucursales(int IdSucursal);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <returns>Listado de MateriasDTOResponse</returns>
        BasePropertiesDTO ObtenerMaterias();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="codigoSucursal"></param>
        /// <returns>Listado de AbogadoDTOResponse</returns>
        BasePropertiesDTO ObtenerAbogadoPorSucursal(int codigoSucursal);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="IdMateriaPadre">Id de la materia padre.</param>
        /// <returns>Listado de AbogadoDTOResponse</returns>
        BasePropertiesDTO ObtenerTipoMateriaPorPadre(int IdTipoMateria);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="IdUsuario">Id del usuario.</param>
        /// <returns>Listado de AbogadoDTOResponse</returns>
        BasePropertiesDTO ObtenerDatosUsuario(int IdUsuario);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <returns>Numero del ultimo correlativo correspendiente</returns>
        BasePropertiesDTO ObtenerListaTipoPersona();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="rut">rut de la persona buscada</param>
        /// <returns>datos de la persona</returns>
        BasePropertiesDTO BusquedaPorRut(int rut);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="IdSGL">Id de la sgl</param>
        /// <param name="IdSociedad">Id de la sociedad</param>
        /// <returns></returns>
        BasePropertiesDTO AsociarSociedadSGL(int IdSGL, int IdSociedad);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="IdSGL">Id de la sgl</param>
        /// <param name="IdSociedad">Id de la sociedad</param>
        /// <returns></returns>
        BasePropertiesDTO ActualizarEstadoSGL(int IdSGL, int codigoPersona);
    }
}
