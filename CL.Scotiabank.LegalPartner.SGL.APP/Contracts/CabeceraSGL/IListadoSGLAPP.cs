﻿using CL.Scotiabank.LegalPartner.CabeceraSglAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad Tipo de Documento del sistema.
    /// </summary>
    public interface IListadoSGLAPP
    {
        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="codigoUsuario"></param>
        /// <returns>Listado de UsuarioDTOResponse</returns>
        BasePropertiesDTO GetUsuarios(int codigoUsuario);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="codigoSucursal"></param>
        /// <returns>Listado de SucursalConAbogadosDTOResponse</returns>
        BasePropertiesDTO GetSucursalesConAbogados(int codigoSucursal);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="codigoSucursal"></param>
        /// <returns>Listado de AbogadoDTOResponse</returns>
        BasePropertiesDTO GetAbogadosPorSucursal(int codigoSucursal);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web ConsultasApi.
        /// </summary>
        /// <param></param>
        /// <returns>Listado de EstadoSGLDTOResponse</returns>
        BasePropertiesDTO GetListadoEstadosSGL();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web CabeceraAPI. 
        /// </summary>
        /// <param name="listadoSglDTO"></param>
        /// <returns>Lista de SGL</returns>
        BasePropertiesDTO ObtenerCantidadSGL(ObtenerCantidadSGLDTORequest listadoSglDTO);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web CabeceraAPI. 
        /// </summary>
        /// <param name="listadoSglDTO"></param>
        /// <returns>Lista de SGLs</returns>
        BasePropertiesDTO ObtenerTodasSGL(ObtenerTodosSGLDTORequest listadoSglDTO);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web ConsultaAPI.
        /// </summary>
        /// <param name="idSucursal"></param>
        /// <returns>Lista de usuarios por suscursal</returns>
        BasePropertiesDTO GetListadoUsuarioPorSucursal(string idSucursal);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web ConsultaAPI.
        /// </summary>
        /// <param name="sgl"></param>
        /// <returns>Sgl filtrada por codio</returns>
        BasePropertiesDTO GetSGLPorCodigoSGL(string sgl);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web CVabeceraSGLAPI.
        /// </summary>
        /// <param name="idSgl"></param>
        /// <returns>Listado con historial de SGL solicitado</returns>
        BasePropertiesDTO ObtenerHistorialSGL(int idSgl);
    }
}
