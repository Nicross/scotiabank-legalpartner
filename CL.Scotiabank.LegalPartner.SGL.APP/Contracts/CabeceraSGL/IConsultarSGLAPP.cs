﻿using CL.Scotiabank.LegalPartner.ConsultaAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    public interface IConsultarSGLAPP
    {
        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="codigoSucursal"></param>
        /// <returns>Listado de SucursalConAbogadosDTOResponse</returns>
        BasePropertiesDTO GetSucursalesConAbogados(int codigoSucursal);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param>N/A</param>
        /// <returns>Listado de Materias</returns>
        BasePropertiesDTO ObtenerMaterias();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="codigoSucursal"></param>
        /// <returns>Listado de AbogadoDTOResponse</returns>
        BasePropertiesDTO GetAbogadosPorSucursal(int codigoSucursal);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="materiaPadre"></param>
        /// <returns>Listado de Tipo Materia</returns>
        BasePropertiesDTO ObtenerTipoMateriaPorMateriaPadre(int materiaPadre);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web ConsultaApi.
        /// </summary>
        /// <param name="numeroSgl"></param>
        /// <returns>Datos de SGLDTO solicitado</returns>
        BasePropertiesDTO GetSGL(int numeroSgl);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="codigoMateria"></param>
        /// <returns>Materia filtrada</returns>
        BasePropertiesDTO ObtenerMateriaSeleccionada(int codigoMateria);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web CabeceraSglApi.
        /// </summary>
        /// <param name="codigoMateria"></param>
        /// <returns>Tipo Materia filtrada</returns>
        BasePropertiesDTO ObtenerTipoMateriaSeleccionada(int codigoMateria);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web ConsultaApi. 
        /// </summary>
        /// <param name="usuarioConsultaRequest"></param>
        /// <returns>Lista de SGLs</returns>
        BasePropertiesDTO ConsultaPersonaDatos(UsuarioConsultaDTORequest usuarioConsultaRequest);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Get) y el retorno de datos desde la api web ConsultaApi. 
        /// </summary>
        /// <param name="codigoPersona"></param>
        /// <returns>Lista de SGLs</returns>
        BasePropertiesDTO GetSLGPorCodigoPersona(int codigoPersona);

        /// <summary>
        /// Ejecuta el metodo GetClienteNombreLegal de la Api de Consulta.
        /// </summary>
        /// <param name="nombre">Nombre de </param>
        /// <returns>Datos Cliente Sociedad por Nombre</returns>
        BasePropertiesDTO GetClienteNombreLegal(string nombre);

        /// <summary>
        /// Ejecuta el metodo GetGrupoUsuario de la Api de Accesos.
        /// </summary>
        /// <param name="entityToUse">Entidad utilizada para crear el Request</param>
        /// <returns>BasePropertiesDTO con codigo de retorno y LoginDTO</returns>
        BasePropertiesDTO GetGrupoUsuario(LoginDTO entityToUse);

        /// <summary>
        /// Ejecuta el metodo ObtenerListadoSGLPorFecha de la Api de Cabecera.
        /// </summary>
        /// <param name="fecha">fecha de busqueda</param>
        /// <returns>Datos SGLs filtradaas por fecha</returns>
        BasePropertiesDTO ObtenerListadoSGLPorFecha(string fecha);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web ConsultaAPI.
        /// </summary>
        /// <param name="sgl"></param>
        /// <returns>Sgl filtrada por codio</returns>
        BasePropertiesDTO GetSGLPorCodigoSGL(string sgl);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web CVabeceraSGLAPI.
        /// </summary>
        /// <param name="idSgl"></param>
        /// <returns>Listado con historial de SGL solicitado</returns>
        BasePropertiesDTO ObtenerHistorialSGL(int idSgl);
    }
}
