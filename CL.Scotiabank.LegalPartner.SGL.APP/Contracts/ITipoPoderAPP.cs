﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad Tipo de Poder del sistema.
    /// </summary>
    public interface ITipoPoderAPP
    {
        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de tipos de poderes.
        /// </summary>
        /// <returns>Lista de TipoPoderDTO</returns>
        BasePropertiesDTO Get();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de tipos de poderes.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>TipoPoderDTO</returns>
        BasePropertiesDTO Get(int id);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de tipos de poderes.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>TipoPoderDTO</returns>
        BasePropertiesDTO Create(TipoPoderDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Put) y el retorno de datos desde la api web de tipos de poderes.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>TipoPoderDTO</returns>
        BasePropertiesDTO Edit(TipoPoderDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) y el retorno de datos desde la api web de tipos de poderes.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>TipoPoderDTO</returns>
        BasePropertiesDTO Delete(TipoPoderDTO entityToUse);
    }
}
