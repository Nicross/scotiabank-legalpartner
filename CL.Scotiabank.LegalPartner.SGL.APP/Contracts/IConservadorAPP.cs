﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad Conservador del sistema.
    /// </summary>
    public interface IConservadorAPP
    {
        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de conservadores.
        /// </summary>
        /// <returns>Lista de ConservadorDTO</returns>
        //IEnumerable<ConservadorDTO> Get();
        BasePropertiesDTO Get();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de dato desde la api web de conservadores.
        /// </summary>
        /// <returns>ConservadorDTO</returns>
        BasePropertiesDTO Get(int id);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de conservadores.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>ConservadorDTO</returns>
        BasePropertiesDTO Create(ConservadorDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Put) y el retorno de datos desde la api web de conservadores.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>ConservadorDTO</returns>
        BasePropertiesDTO Edit(ConservadorDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) y el retorno de datos desde la api web de conservadores.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>ConservadorDTO</returns>
        BasePropertiesDTO Delete(ConservadorDTO entityToUse);
    }
}
