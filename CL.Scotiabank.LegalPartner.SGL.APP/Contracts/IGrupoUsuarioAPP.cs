﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad GrupoUsuario del sistema.
    /// </summary>
    public interface IGrupoUsuarioAPP
    {
        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de grupoUsuarios.
        /// </summary>
        /// <returns>Lista de GrupoUsuarioDTO</returns>
        BasePropertiesDTO Get();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno un dato desde la api web de grupoUsuarios.
        /// </summary>
        /// <returns>GrupoUsuarioDTO</returns>
        BasePropertiesDTO Get(int id);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de grupoUsuarios.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>GrupoUsuarioDTO</returns>
        BasePropertiesDTO Create(GrupoUsuarioDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Put) y el retorno de datos desde la api web de grupoUsuarios.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>GrupoUsuarioDTO</returns>
        BasePropertiesDTO Edit(GrupoUsuarioDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) y el retorno de datos desde la api web de grupoUsuarios.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>GrupoUsuarioDTO</returns>
        BasePropertiesDTO Delete(GrupoUsuarioDTO entityToUse);
    }
}
