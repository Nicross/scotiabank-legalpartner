﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad SucursalBanco del sistema.
    /// </summary>
    public interface ISucursalBancoAPP
    {
        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de sucursalBancos.
        /// </summary>
        /// <returns>Lista de SucursalBancoDTO</returns>
        BasePropertiesDTO Get();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno un dato desde la api web de sucursalBancos.
        /// </summary>
        /// <returns>SucursalBancoDTO</returns>
        BasePropertiesDTO Get(int id);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de sucursalBancos.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>SucursalBancoDTO</returns>
        BasePropertiesDTO Create(SucursalBancoDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Put) y el retorno de datos desde la api web de sucursalBancos.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>SucursalBancoDTO</returns>
        BasePropertiesDTO Edit(SucursalBancoDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) y el retorno de datos desde la api web de sucursalBancos.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>SucursalBancoDTO</returns>
        BasePropertiesDTO Delete(SucursalBancoDTO entityToUse);
    }
}
