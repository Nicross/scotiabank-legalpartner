﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad Notaria del sistema.
    /// </summary>
    public interface INotariaAPP
    {
        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de notarias.
        /// </summary>
        /// <returns>Lista de NotariaDTO</returns>
        BasePropertiesDTO Get();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de notarias.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        BasePropertiesDTO Get(int id);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de notarias.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>NotariaDTO</returns>
        BasePropertiesDTO Create(NotariaDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Put) y el retorno de datos desde la api web de notarias.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>NotariaDTO</returns>
        BasePropertiesDTO Edit(NotariaDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) y el retorno de datos desde la api web de notarias.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>NotariaDTO</returns>
        BasePropertiesDTO Delete(NotariaDTO entityToUse);
    }
}
