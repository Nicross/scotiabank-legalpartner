﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad MateriaBanco del sistema.
    /// </summary>
    public interface IMateriaBancoAPP
    {
        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de materiaBancos.
        /// </summary>
        /// <returns>Lista de MateriaBancoDTO</returns>
        BasePropertiesDTO Get();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de materiaBancos.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        BasePropertiesDTO Get(int id);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de materiaBancos.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>MateriaBancoDTO</returns>
        BasePropertiesDTO Create(MateriaBancoDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Put) y el retorno de datos desde la api web de materiaBancos.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>MateriaBancoDTO</returns>
        BasePropertiesDTO Edit(MateriaBancoDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) y el retorno de datos desde la api web de materiaBancos.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>MateriaBancoDTO</returns>
        BasePropertiesDTO Delete(MateriaBancoDTO entityToUse);
    }
}
