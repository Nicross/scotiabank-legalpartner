﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad menu del sistema.
    /// </summary>
    public interface IMenuAPP
    {
        BasePropertiesDTO GetMenusPorCodigoGrupoUsuario(int id);
    }
}
