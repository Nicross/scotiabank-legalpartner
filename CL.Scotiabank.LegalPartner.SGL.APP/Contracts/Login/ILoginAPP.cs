﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad Login del sistema.
    /// </summary>
    public interface ILoginAPP
    {
        //LoginDTO GetUsuarioAdmin(LoginDTO entityToUse);

        //LoginDTO GetUsuarioLogin(LoginDTO entityToUse);

        //LoginDTO GetUsuarioPassword(LoginDTO entityToUse);

        //LoginDTO GetUsuario(LoginDTO entityToUse);

        //LoginDTO GetUsuario(int id);

        //LoginDTO GetPasswordAnterior(LoginDTO entityToUse);

        BasePropertiesDTO ObtenerInformacionModificarClave(LoginDTO entityToUse);

        BasePropertiesDTO Login(LoginDTO entityToUse);

        BasePropertiesDTO ModificarClave(LoginDTO entityToUse);

        BasePropertiesDTO LoginSGL(LoginDTO entityToUse);
    }
}
