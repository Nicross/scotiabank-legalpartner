﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad OficinaPartes del sistema.
    /// </summary>
    public interface IOficinaPartesAPP
    {
        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de oficinaPartess.
        /// </summary>
        /// <returns>Lista de OficinaPartesDTO</returns>
        BasePropertiesDTO Get();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno un dato desde la api web de oficinaPartess.
        /// </summary>
        /// <returns>OficinaPartesDTO</returns>
        BasePropertiesDTO Get(int id);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de oficinaPartess.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>OficinaPartesDTO</returns>
        BasePropertiesDTO Create(OficinaPartesDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Put) y el retorno de datos desde la api web de oficinaPartess.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>OficinaPartesDTO</returns>
        BasePropertiesDTO Edit(OficinaPartesDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) y el retorno de datos desde la api web de oficinaPartess.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>OficinaPartesDTO</returns>
        BasePropertiesDTO Delete(OficinaPartesDTO entityToUse);
    }
}
