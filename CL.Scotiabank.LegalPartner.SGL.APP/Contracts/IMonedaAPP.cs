﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad Moneda del sistema.
    /// </summary>
    public interface IMonedaAPP
    {
        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de monedas.
        /// </summary>
        /// <returns>Lista de MonedaDTO</returns>
        BasePropertiesDTO Get();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno un dato desde la api web de monedas.
        /// </summary>
        /// <returns>MonedaDTO</returns>
        BasePropertiesDTO Get(int id);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de monedas.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>MonedaDTO</returns>
        BasePropertiesDTO Create(MonedaDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Put) y el retorno de datos desde la api web de monedas.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>MonedaDTO</returns>
        BasePropertiesDTO Edit(MonedaDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) y el retorno de datos desde la api web de monedas.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>MonedaDTO</returns>
        BasePropertiesDTO Delete(MonedaDTO entityToUse);
    }
}
