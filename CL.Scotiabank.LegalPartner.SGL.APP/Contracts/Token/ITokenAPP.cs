﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    public interface ITokenAPP
    {
        TokenDTO CreateToken(TokenDTO entityToUse);
    }
}
