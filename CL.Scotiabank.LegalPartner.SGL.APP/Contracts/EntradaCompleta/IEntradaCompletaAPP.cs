﻿using CL.Scotiabank.LegalPartner.ConsultaAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.DAL;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO.DTO.EntradaCompleta;
using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    public interface IEntradaCompletaAPP
    {
        BasePropertiesDTO ObtenerDatosPersona(UsuarioConsultaDTORequest entityToUse);

        BasePropertiesDTO ObtenerListadoDocumentos(int codigoPersona);

        BasePropertiesDTO GetListaEstadoLegal();

        BasePropertiesDTO ConsultaPersonaJuridicaDatos(int codigoEstatuto);

        BasePropertiesDTO GetListadoTipoPersonaDetalle();

        BasePropertiesDTO GetTipoLegalizacion(string codigoTipoPersona, string codigoEstLegal);

        BasePropertiesDTO GetListaNotaria();

        BasePropertiesDTO ObtenerRegistros();

        //BasePropertiesDTO IngresarDatoEstatuto(DatosEstatutosCreateDTO datosEstatutosDTO);

        BasePropertiesDTO AgregarDatoEstatuto(UsuarioConsultaDTO datosEstatutosDTO);

        BasePropertiesDTO ObtenerDatosEstatutos(int codigoPersonaJuridica);
        BasePropertiesDTO IngresarDatosDocumento(IngresarDatosDocumentosDTO entityToUse);


        //BasePropertiesDTO ConsultaDocumentosCapital(BasePropertiesDTO dto);

        VerInformeCapitalDTO ConsultaDatosParticipante(VerInformeCapitalDTO dto);

        BasePropertiesDTO ConsultaDocumentosCapital(string id);

        VerInformeCapitalDTO ConsultaDocumentosCapitalInforme(VerInformeCapitalDTO dto);

        BasePropertiesDTO ConsultarDatosPersona(int codigoPersona);

        VerInformeCapitalDTO ConsultaDatosPersonaInforme(VerInformeCapitalDTO dto);

        VerInformeCapitalDTO ConsultaDatosParticipanteInforme(VerInformeCapitalDTO dto);

        VerInformeCapitalDTO ObtenerAbogado(VerInformeCapitalDTO dto);

        VerInformeCapitalDTO ConsultaDocumentosGeneralInforme(VerInformeCapitalDTO dto);

        VerInformeCapitalDTO ObtenerSGLUSuarioInforme(VerInformeCapitalDTO dto);

        VerInformeCapitalDTO ObtenerDatosPersonaInforme(VerInformeCapitalDTO dto);

        void IngresarCapitalSocio(VerInformeCapitalDTO dto);

        VerInformeCapitalDTO ObtenerDatosEstatutosInforme(VerInformeCapitalDTO dto);
        BasePropertiesDTO GetDatosOrigen(string codigoTipoDocumento, string codigoTipoPersona);

        BasePropertiesDTO ConsultarListaPoderes(string id);

        BasePropertiesDTO ObtieneCantidadGruposDocumento(string codigoDocumento, int tipoGrupo);

        BasePropertiesDTO GetCantidadParticipantesDocumento(string codigoDocumento, string tipoParticipante);

        BasePropertiesDTO ConsultarListaRevocaciones(string id);

        BasePropertiesDTO DeleteDocumento(string codigoDocumento);

        IList<ListaDocumentoDTO> MapeaListaDocumentos(IEnumerable<DocumentoDTOResponse> resultado);

        VerInformeCapitalDTO ConsultaPersonaJuridicaDatosInforme(VerInformeCapitalDTO dto);

        BasePropertiesDTO ObtenerDocumentoLegal(int codigoDocumentoLegal);

        VerInformeCapitalDTO ObtenerListadoMoneda(VerInformeCapitalDTO dto);

        /// <summary>
        /// ]Metodo que ejecuta la API para obtener datos de la SGL filtrada por codigo
        /// </summary>
        /// <param name="codigoSGL"></param>
        /// <returns>Codigo SGL y codigo Estaod</returns>
        EstadoSGLDTO GetSGL(string codigoSGL);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdSGL">Codigo identificador de SGL</param>
        /// <param name="codigoEstado">Codigo Estado para actualizar SGL</param>
        /// <param name="codigoUsusario">Codigo del usuario conectado</param>
        /// <returns></returns>
        BasePropertiesDTO ActualizarEstadoSGL(int IdSGL, int codigoEstado, int codigoUsusario);

        /// <summary>
        /// Metodo encargado de Modificar un documento
        /// </summary>
        /// <param name="entityToUse"></param>
        /// <returns>UsuarioDTO</returns>
        BasePropertiesDTO ActualizarDatosDocumento(IngresarDatosDocumentosDTO entityToUse);

        /// <summary>
        /// Actualiza el tribunal del documento
        /// </summary>
        /// <param name="codDocumento"></param>
        /// <param name="glosa"></param>
        /// <returns></returns>
        BasePropertiesDTO ActualizaTribunal(string codDocumento, string glosa);

        /// <summary>
        /// Agrega tribunal
        /// </summary>
        /// <param name="tribunal"></param>
        /// <returns></returns>
        BasePropertiesDTO AgregarTribunal(string tribunal);

        /// <summary>
        /// Agrega Revocacion
        /// </summary>
        /// <param name="tipoRevocacion"></param>
        /// <param name="descripcion"></param>
        /// <param name="codDoc"></param>
        /// <returns></returns>
        BasePropertiesDTO AgregarRevocacion(int tipoRevocacion, string descripcion, int codDoc);

        /// <summary>
        /// Obtiene Datos Poderes por id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        BasePropertiesDTO ConsultarDatosPoderes(string id);

        /// <summary>
        /// Graba la revocacion
        /// </summary>
        /// <param name="paramRevo"></param>
        /// <returns></returns>
        BasePropertiesDTO GrabarRevocacionEscritura(RevocacionEscrituraDTO paramRevo);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (PUT) para cambiar Tipo de Persona Juridica.
        /// </summary>
        /// <returns>IngresarSGL DTO</returns>
        BasePropertiesDTO ActualizarPersona(IngresarSGLDTO entityToUse);

        /// <summary>
        /// Obtiene los datos del participante segun codigo persona y tipo 
        /// </summary>
        /// <param name="codigoPersona"></param>
        /// <param name="codigoTipo"></param>
        /// <returns></returns>
        BasePropertiesDTO ConsultaDatosParticipante(int codigoPersona, string codigoTipo);

        /// <summary>
        /// Método que elimina el mandatario seleccionado
        /// </summary>
        /// <param name="requestEntity"></param>
        /// <returns></returns>
        BasePropertiesDTO EliminarMandatario(MandatarioGrupoDTO entityToUse);

        BasePropertiesDTO TerminarSGL(int IdSGL);

        void IngresarInforme(IngresarDocDTO dto);
    }
}
