﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad Facultad del sistema.
    /// </summary>
    public interface IFacultadAPP
    {
        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de facultades.
        /// </summary>
        /// <returns>Lista de FacultadDTO</returns>
        BasePropertiesDTO Get();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de facultades.
        /// </summary>
        /// <returns>Lista de FacultadDTO</returns>
        BasePropertiesDTO Get(int id);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de facultades.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>FacultadDTO</returns>
        BasePropertiesDTO Create(FacultadDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Put) y el retorno de datos desde la api web de facultades.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>FacultadDTO</returns>
        BasePropertiesDTO Edit(FacultadDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) y el retorno de datos desde la api web de facultades.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>FacultadDTO</returns>
        BasePropertiesDTO Delete(FacultadDTO entityToUse);
    }
}
