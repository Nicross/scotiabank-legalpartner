﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad MandatarioGrupo.
    /// </summary>
    public interface IMandatarioGrupoAPP
    {
        #region Mandatarios

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de mandatarios
        /// </summary>
        /// <returns>Lista de FacultadDTO</returns>
        BasePropertiesDTO ListarMandatarios(GetMandatarioDTO entitytoUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de mandatarios
        /// </summary>
        /// <returns>Lista de FacultadDTO</returns>
        BasePropertiesDTO ConsultaMandatario(MandatarioGrupoDTO entitytoUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de Mandatario.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>FacultadDTO</returns>
        BasePropertiesDTO IngresarMandatario(MandatarioGrupoDTO entityToUse);

        /// <summary>
        /// Método que ingresa las observaciones del mandatario.
        /// </summary>
        /// <param name="entityToUse"></param>
        /// <returns></returns>
        BasePropertiesDTO IngresarObservacionMandatario(MandatarioGrupoDTO entityToUse);

        /// <summary>
        /// Método que actuliza el estado de la SGL.
        /// </summary>
        /// <param name="entityToUse"></param>
        /// <returns></returns>
        BasePropertiesDTO ActualizarEstadoSGL(MandatarioGrupoDTO entityToUse);

        /// <summary>
        /// Método que consulta si el mandatario seleccionado sea vigente.
        /// </summary>
        /// <param name="entityToUse"></param>
        /// <returns></returns>
        BasePropertiesDTO ObtenerMandatarioVigente(MandatarioGrupoDTO entityToUse);

        /// <summary>
        /// Método que elimina el mandatario seleccionado
        /// </summary>
        /// <param name="requestEntity"></param>
        /// <returns></returns>
        BasePropertiesDTO EliminarMandatario(MandatarioGrupoDTO entityToUse);

        /// <summary>
        /// Método que actualiza los mandatarios.
        /// </summary>
        /// <param name="entityToUse"></param>
        /// <returns></returns>
        BasePropertiesDTO EditarMandatario(MandatarioGrupoDTO entityToUse);

        /// <summary>
        /// Método que permite editar las observaciones de un Mandatario.
        /// </summary>
        /// <param name="entityToUse">Objeto de parámetros de MandatarioGrupoDTO </param>
        /// <returns></returns>
        BasePropertiesDTO EditarObservacionesMandatario(MandatarioGrupoDTO entityToUse);

        #endregion

        #region Grupos

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de Grupo.
        /// </summary>
        /// <returns>Lista de GrupoDTO</returns>
        BasePropertiesDTO ConsultaSociedadGrupoMandatario(string id);

        ///CreateGrupo
        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno al crear un Grupo.
        /// </summary>
        /// <returns>Lista de GrupoDTO</returns>
        BasePropertiesDTO IngresarGrupo(MandatarioGrupoDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno al actualizar un Grupo.
        /// </summary>
        /// <returns>Lista de GrupoDTO</returns>
        BasePropertiesDTO EditarGrupo(MandatarioGrupoDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) para obtener grupos Vigentes y
        /// el retorno de datos desde la api web de Grupo.
        /// </summary>
        /// <returns>Lista de GrupoDTO</returns>
        BasePropertiesDTO ObtenerGruposVigentes(string codigoGrupo);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) para eleiminar Grupo.
        /// </summary>
        /// <returns>Objeto de GrupoDTO</returns>
        BasePropertiesDTO EliminarGruposVigentes(string codigoGrupo);

        #endregion

        #region Asociacion
        
        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) para obtener Documento de Poder y el retorno de datos desde la api web de Grupo.
        /// </summary>
        /// <returns>Lista de DocumentoLegalDTO</returns>
        BasePropertiesDTO GetEscrituraPoder(string CodigoDocumento);

        ///CreateAsociacionMandatariosGrupo
        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) para asociar mandatarios a un Grupo y 
        /// el retorno al crear un As.
        /// </summary>
        /// <returns>Lista de AsociacionDTO</returns>
        BasePropertiesDTO IngresarAsociacion(MandatarioGrupoDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) para obtener Lista de Grupos con Mandatarios asociados
        /// y el retorno de datos desde la api web de Asociacion.
        /// </summary>
        /// <returns>Lista de GrupoDTO</returns>
        BasePropertiesDTO ConsultarListaGrupoMandatorio(string id);

        ///DeleteAsociacionMandatariosGrupo
        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) para eliminar la  asociacion mandatarios a un Grupo y 
        /// </summary>
        /// <returns>Lista de AsociacionDTO</returns>
        BasePropertiesDTO EliminarAsociacion(MandatarioGrupoDTO entityToUse);        

        #endregion
    }
}
