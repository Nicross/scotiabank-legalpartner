﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad MandatarioGrupo.
    /// </summary>
    public interface IDocumentoPoderAPP
    {
        #region Listar

        /// <summary>
        /// Metodo encargado de listar los documentos de poder.
        /// SP: svc_rcr_doc_per (1° SP)
        /// </summary>
        /// <returns>Lista de FacultadDTO</returns>
        BasePropertiesDTO ListarDocumentos(int id);

        /// <summary>
        /// Metodo encargado de listar los ddocumentos poder.
        /// </summary>
        /// SP: svc_rcr_doc_pdr (2° sp)
        /// <returns>Lista de FacultadDTO</returns>
        IList<DatosPoderesAndDesMatDTOResponse> ObtenerDatosPoderes(int id, bool checkIn, int sgl);

        #endregion

        #region Grabar
        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde API Transversal.
        /// </summary>
        /// <returns>Lista de DocumentoPoderDTO</returns>
        BasePropertiesDTO ObtenerRespuestaLegal(string codigoDocumento);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde API Transversal.
        /// </summary>
        /// <returns>Lista de DocumentoPoderDTO</returns>
        BasePropertiesDTO ObtenerListaTipoDocumento();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde API Transversal.
        /// </summary>
        /// <returns>Lista de DocumentoPoderDTO</returns>
        BasePropertiesDTO ObtenerListaNotaria();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno al cambiar Estado SGL.
        /// </summary>
        /// <returns>Lista de GrupoDTO</returns>
        BasePropertiesDTO CambiarEstadoSGL(DocumentoPoderDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de conservadores.
        /// </summary>
        /// <returns>Lista de ConservadorDTO</returns>
        BasePropertiesDTO ObtenerConservadores();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde API Transversal.
        /// </summary>
        /// <returns>Lista de DocumentoPoderDTO</returns>
        BasePropertiesDTO ObtenerDocumentoLegal(string codigoDocumentoLegal);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno al crear un Documento de Poder.
        /// </summary>
        /// <returns>Ingresa DocumentopoderDTO</returns>
      BasePropertiesDTO IngresarDocumentoPoder(DocumentoPoderDTO entityToUse);
        #endregion


    }
}
