﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad Consulta del sistema.
    /// </summary>
    public interface IBuscarClienteAPP 
    {
        BasePropertiesDTO GetListaNombreLegal(string nombre);

        BasePropertiesDTO GetListaNombreLegalAnterior(string nombre);

        BasePropertiesDTO GetListaClienteNombreFantasia(string nombre);

        BasePropertiesDTO GetListaMandatorios(int IdSociedad, int IdSGL);

        BasePropertiesDTO GetSucursales(int rut, int idPersona);

    }
}
