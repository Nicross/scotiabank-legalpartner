﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad Consulta del sistema.
    /// </summary>
    public interface IConsultaFirmaCajaAPP
    {
        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de Consultaes.
        /// </summary>
        /// <returns>Lista de ConsultaDTO</returns>
        IEnumerable<ConsultaFirmaCajaDTO> Get();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de dato desde la api web de Consultaes.
        /// </summary>
        /// <returns>ConsultaDTO</returns>
        ConsultaFirmaCajaDTO Get(ConsultaFirmaCajaDTO entityToUse);
        List<string> ObtenerProductosCliente(string rut);
    }
}
