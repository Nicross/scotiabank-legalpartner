﻿using CL.Scotiabank.LegalPartner.ConsultaAPI.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.DAL;
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;

using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad Consulta Poderes del sistema.
    /// </summary>
    public interface IConsultaPoderesAPP
    {
        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes.
        /// </summary>
        /// <returns>Lista de ConsultaPoderesDTO</returns>
        BasePropertiesDTO ObtenerPoderesFacultadesVigentes(int codigoDocumentoLegal);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes.
        /// </summary>
        /// <returns>Lista de ConsultaPoderesDTO</returns>
        BasePropertiesDTO ObtenerPoderesVigentes(ConsultaPoderesDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes.
        /// </summary>
        /// <returns>Lista de ConsultaPoderesDTO</returns>
        BasePropertiesDTO ObtenerMontoMaximo(string codigoMoneda, decimal montoMaximo);


        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes.
        /// </summary>
        /// <returns>Lista de ConsultaPoderesDTO</returns>
        BasePropertiesDTO ObtenerDocumentoLegal(int codigoDocumentoLegal);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes.
        /// </summary>
        /// <returns>Lista de ConsultaPoderesDTO</returns>
        BasePropertiesDTO ObtenerFormasDeActuar(int CodigoPoder);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web de ConsultaPoderes.
        /// </summary>
        /// <returns>Lista de ConsultaPoderesDTO</returns>
        BasePropertiesDTO ObtenerNumeroMandatarios(ConsultaPoderesDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes.
        /// </summary>
        /// <returns>Lista de ConsultaPoderesDTO</returns>
        BasePropertiesDTO ListarFacultadesPorPoder(string idPoder);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web de ConsultaPoderes.
        /// </summary>
        /// <returns>Lista de ConsultaPoderesDTO</returns>
        BasePropertiesDTO ListarParticipanteConPoder(ConsultaParticipanteConPoderDTORequest entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes.
        /// </summary>
        /// <returns>Lista de ConsultaPoderesDTO</returns>
        BasePropertiesDTO ObtenerFirmaReciente(int codigoPersona, string numeroCuenta);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web de ConsultaPoderes.
        /// </summary>
        /// <returns>Lista de ConsultaPoderesDTO</returns>
        BasePropertiesDTO ObtenerParticipanteConPoderSinCta(ConsultaPoderesDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes.
        /// </summary>
        /// <returns>Lista de ConsultaPoderesDTO</returns>
        BasePropertiesDTO ObtenerFirmaRecienteSinCta();

        /// <summary>
        ///  Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes.
        /// </summary>
        /// <param name="codigoPersona"></param>
        /// <returns></returns>
        BasePropertiesDTO ObtenerDatosPersona(UsuarioConsultaDTORequest entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes.
        /// </summary>
        /// <returns></returns>
        BasePropertiesDTO GetListaEstadoLegal();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes.
        /// </summary>
        /// <returns></returns>
        BasePropertiesDTO GetListadoTipoPersonaDetalle();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes.
        /// </summary>
        /// <param name="codigoPersona"></param>
        /// <returns></returns>
        BasePropertiesDTO ConsultarDatosPersona(int codigoPersona);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes.
        /// </summary>
        /// <param name="codigoPersona"></param>
        /// <returns></returns>
        BasePropertiesDTO ObtenerListadoDocumentos(int codigoPersona);

        /// <summary>
        ///  Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes.
        /// </summary>
        /// <param name="codigoPersona"></param>
        /// <returns></returns>
        BasePropertiesDTO ObtenerDatosEstatutos(int codigoEstatuto);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes
        /// </summary>
        /// <param name="codigoPersona"></param>
        /// <param name="codigoTipo"></param>
        /// <returns></returns>
        BasePropertiesDTO ObtenerDatosCapital(string id, string codigoTipo, string sociedad);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes
        /// </summary>
        /// <param name="patrimonioDTO"></param>
        /// <returns></returns>
        PatrimonioDTO ListaParticipante(PatrimonioDTO patrimonioDTO);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes
        /// </summary>
        /// <param name="id"></param>
        /// <param name="codigoTipo"></param>
        /// <returns></returns>
        BasePropertiesDTO ObtenerListaParticipante(string id, string codigoTipo);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes
        /// </summary>
        /// <param name="paramPoderVigente"></param>
        /// <returns></returns>
        BasePropertiesDTO ListarPoderesVigentes(PoderVigenteDTORequest paramPoderVigente);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes
        /// </summary>
        /// <param name="codigoPoder"></param>
        /// <returns></returns>
        BasePropertiesDTO ObtenerPoder(int codigoPoder);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes
        /// </summary>
        /// <param name="codigoPoder"></param>
        /// <param name="fechaConsulta"></param>
        /// <returns></returns>
        BasePropertiesDTO ListarFacultadesVigentes(int codigoPoder, string fechaConsulta);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes
        /// </summary>
        /// <param name="codigoPoder"></param>
        /// <param name="CodigoFacultad"></param>
        /// <param name="fechaConsulta"></param>
        /// <returns></returns>
        BasePropertiesDTO ObtenerLimites(int codigoPoder, int CodigoFacultad, string fechaConsulta);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes
        /// </summary>
        /// <param name="paramPoderVigente"></param>
        /// <returns></returns>
        BasePropertiesDTO ListarPoderesFacultadesVigentes(PoderFacultadVigenteDTORequest paramPoderVigente);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes
        /// </summary>
        /// <param name="codigoPoder"></param>
        /// <returns></returns>
        BasePropertiesDTO ConsultarFormasDeActuar(int codigoPoder);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de ConsultaPoderes
        /// </summary>
        /// <param name="paramNumManda"></param>
        /// <returns></returns>
        BasePropertiesDTO ConsultarNumeroMandatarios(ConsultaNumeroMandatariosDTORequest paramNumManda);

        /// <summary>
        /// Obtiene Formas de actuar para modal
        /// </summary>
        /// <param name="resultado"></param>
        /// <param name="paramMandatarios"></param>
        /// <param name="fecha"></param>
        /// <returns></returns>
        IList<FormasActuarDTO> ObtieneFormaActuar(IEnumerable<PoderVigentePorSociedadDTOResponse> resultado, MandatarioRutResponderDTO paramMandatarios, string fecha);

        /// <summary>
        /// Carga Glosa
        /// </summary>
        /// <param name="resultado"></param>
        /// <returns></returns>
        IList<GlosaResponderDTO> CargarGlosa(IEnumerable<PoderVigentePorSociedadDTOResponse> resultado);

        List<Limitaciones> ObtenerListaLimitaciones(List<Limitaciones> lstLimitaciones, int intCodigoPoder, int codigoFacultad, string fecha);

        BasePropertiesDTO ObtenerPoderesVigentesPersona(int codigoPersona, string fechaConsulta);

        string ObtenerGlosa2(int intCodigoPoder, string strListaFactuarValidas, IList<GlosaResponderDTO> lstGlosa);



        BasePropertiesDTO ListarPoderesVigentesPersona(int codigoPersona, string fechaConsulta);

        string ObtenerFActuarValidasV2(int codigoPoder, int codigoDcto, string fechaConsulta, string lstMandatarios, string[] arregloManda);

        BasePropertiesDTO ObtenerFacultadesVigentes(int codigoPoder, string fechaConsulta);

        ConsultaParticipanteConPoderDTORequest ValidacionFecha(int codigoParticipante, int tipoParticipante, DateTime fechaConsulta);

        MandatarioRutResponderDTO RespConsultaPoderesResponder(MandatarioRutResponderDTO paramModal);

        Tuple<MandatarioRutResponderDTO, string[]> RespConsultaSoloMandatarios(MandatarioRutResponderDTO paramModal);

        string RespObtieneConsultaMandatario(MandatarioRutResponderDTO paraModal);

        string ObtenerLimitacionesFacultad(int intCodigoPoder, int codigoFacultad, string fechaConsulta);

        string ObtenerGlosaFacultad(int intCodigoPoder, int opcion, IList<GlosaResponderDTO> lstGlosa);

        string RespObtieneConsultaFacultad(int codigoPersona, int codigoFacultad, string fechaConsulta);

        string ObtieneConsultaMandatario(MandatarioRutResponderDTO paraModal);

        IList<GlosaResponderDTO> CargarGlosaPorFacultad(int codigoPersona, string fechaConsulta);

        string ObtieneConsultaMandaFacul(MandatarioRutResponderDTO paramModal);
    }
}
