﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad GrupoFacultad del sistema.
    /// </summary>
    public interface IGrupoFacultadAPP
    {
        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de facultades.
        /// </summary>
        /// <returns>Lista de GrupoFacultadDTO</returns>
       BasePropertiesDTO Get();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de facultades.
        /// </summary>
        /// <returns>Lista de GrupoFacultadDTO</returns>
        BasePropertiesDTO Get(int id);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de facultades.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>GrupoFacultadDTO</returns>
        BasePropertiesDTO Create(GrupoFacultadDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Put) y el retorno de datos desde la api web de facultades.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>GrupoFacultadDTO</returns>
        BasePropertiesDTO Edit(GrupoFacultadDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Delete) y el retorno de datos desde la api web de facultades.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>GrupoFacultadDTO</returns>
        BasePropertiesDTO Delete(GrupoFacultadDTO entityToUse);
    }
}
