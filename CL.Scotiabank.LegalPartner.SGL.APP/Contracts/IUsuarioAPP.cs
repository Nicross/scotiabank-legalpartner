﻿
using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.APP
{
    /// <summary>
    /// Interfaz para la clase de servicios para la entidad Usuario del sistema.
    /// </summary>
    public interface IUsuarioAPP
    {
        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <returns>Lista de UsuarioDTO</returns>
        BasePropertiesDTO GetUsuarios();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos  desde la api web de Usuario por Id.
        /// </summary>
        /// <returns>Datos de un de UsuarioDTO</returns>
        BasePropertiesDTO GetUsuario(UsuarioDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos de Perfil desde la api web de Usuario.
        /// </summary>
        /// <returns>Lista de Perfil</returns>
        BasePropertiesDTO GetPerfil();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos de Sucursal desde la api web de Usuario.
        /// </summary>
        /// <returns>Lista de UsuarioDTO</returns>
        BasePropertiesDTO GetSucursal();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (GET) y el retorno de datos de Relacion Perfil Claves desde la api web de Usuario.
        /// </summary>
        /// <returns>Lista de UsuarioDTO</returns>
        BasePropertiesDTO GetUsuarioRelacionPerfilClaves();

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos  desde la api web de Usuario por Rut.
        /// </summary>
        /// <returns>Datos de un de UsuarioDTO</returns>
        BasePropertiesDTO GetUsuarioRut(UsuarioDTO entityToUse);


        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos de Vigencia de Usuario desde la api web de Usuario.
        /// </summary>
        /// <returns>Lista de UsuarioDTO</returns>
        BasePropertiesDTO GetUsuarioVigencia(UsuarioDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos  desde la api web de Usuario por Codigo de usuario.
        /// </summary>
        /// <returns>Datos de Sucursal de UsuarioDTO</returns>
        BasePropertiesDTO GetSucursalPorCodigoUsuario(UsuarioDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos  desde la api web de Usuario por Rut de usuario y Id de Sucursal.
        /// </summary>
        /// <returns>Datos de Sucursal de UsuarioDTO</returns>
        BasePropertiesDTO GetConsultaSucursal(UsuarioDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>UsuarioDTO</returns>
        BasePropertiesDTO CreateUsuario(UsuarioDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (Create) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>UsuarioDTO</returns>
        BasePropertiesDTO CreateRelacionPerfil(UsuarioDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (CreateReingresarUsuario) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <param name="entityToUse">Entidad a crear</param>
        /// <returns>UsuarioDTO</returns>
        BasePropertiesDTO CreateReingresarUsuario(UsuarioDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (UpdateUsuarioNuevo) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>UsuarioDTO</returns>
        BasePropertiesDTO UpdateUsuarioNuevo(UsuarioDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (PUT) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>UsuarioDTO</returns>
        BasePropertiesDTO UpdateUsuario(UsuarioDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (PUT) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>UsuarioDTO</returns>
        BasePropertiesDTO UpdateUsuarioSucursal(UsuarioDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (PUT) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <param name="entityToUse">Entidad a editar</param>
        /// <returns>UsuarioDTO</returns>
        BasePropertiesDTO UpdateUsuarioPerfil(UsuarioDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>UsuarioDTO</returns>
        BasePropertiesDTO DeleteUsuario(UsuarioDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos desde la api web de Usuario.
        /// </summary>
        /// <param name="entityToUse">Entidad a eliminar</param>
        /// <returns>UsuarioDTO</returns>
        BasePropertiesDTO DeleteUsuarioConSucursal(UsuarioDTO entityToUse);

        /// <summary>
        /// Metodo encargado de orquestar el llamado (POST) y el retorno de datos de Perfil de un usuario desde la API web de Usuario.
        /// </summary>
        /// <returns>Datos de un de UsuarioDTO</returns>
        BasePropertiesDTO GetPerfilUsuario(UsuarioDTO entityToUse);

    }
}
