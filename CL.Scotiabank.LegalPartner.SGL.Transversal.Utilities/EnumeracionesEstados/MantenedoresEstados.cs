﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities
{
    public enum MantenedoresEstados
    {
        Exitoso = 0,
        RegistroExistente = 1,
        RegistroNoExistente = 2,
        RegistroUtilizado = 3,
        PerfilExistente = 4,
        ErrorInesperado = 5,
        UsuarioBloqueado = 6,
        SucursalExistente = 7

    };
}
