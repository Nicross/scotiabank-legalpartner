﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities
{
    public enum LoginEstados
    {
        Exitoso = 0,
        ClaveIncorrecta = 1,
        PerfilInvalido = 2,
        UsuarioNoRegistrado = 3,
        UsuarioBloqueado = 4,
        AdvertenciaCambiarClave = 5,
        ExpiracionProntaClave = 6,
        ClaveExpirada = 7,
        ClavePreviamenteIngresada = 8
    }
}
