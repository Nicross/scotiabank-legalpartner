using Newtonsoft.Json;
using System.Net.Http;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities
{
    public class JsonContent : StringContent
    {
        public JsonContent(object value)
            : base (JsonConvert.SerializeObject(value), Encoding.UTF8, "application/json")
        {
        }

        public JsonContent(object value, string mediaType)
            : base(JsonConvert.SerializeObject(value), Encoding.UTF8, mediaType)
        {
        }
    }
}