﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using Newtonsoft.Json;
using System;
using System.Net.Http;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities
{
    public static class HttpResponseExtensions
    {
        public static BasePropertiesDTO ContentAsType<T>(this HttpResponseMessage response)
        {
            var data = response.Content.ReadAsStringAsync().Result;

            var objetoResultado = string.IsNullOrWhiteSpace(data) ? default(BasePropertiesDTO) : JsonConvert.DeserializeObject<BasePropertiesDTO>(data);

            objetoResultado.Resultado = objetoResultado.Resultado == null || string.IsNullOrWhiteSpace(objetoResultado.Resultado.ToString()) ? default(T) : JsonConvert.DeserializeObject<T>(objetoResultado.Resultado.ToString());

            return objetoResultado;

        }
        
        public static string ContentAsJson(this HttpResponseMessage response)
        {
            var data = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.SerializeObject(data);
        }

        public static string ContentAsString(this HttpResponseMessage response)
        {
            return response.Content.ReadAsStringAsync().Result;
        }
    }
}
