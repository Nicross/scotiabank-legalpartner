﻿using System;
using System.Data;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities
{
    public class Utilities
    {
        /// <summary>
        /// Metodo utilizado para convertir un tipo de dato de C# a SqlDbType.
        /// </summary>
        /// <param name="tipoDato">Tipo de Dato C#</param>
        /// <returns>Tipo de Dato SqlDbType</returns>
        public static SqlDbType ConvertirTipoDeDatoASqlDbType(string tipoDato)
        {
            switch (tipoDato.ToLower())
            {
                case "int32":
                    {
                        return SqlDbType.Int;
                    }
                case "string":
                    {
                        return SqlDbType.Text;
                    }
                case "datetime":
                    {
                        return SqlDbType.DateTime;
                    }
                case "char":
                    {
                        return SqlDbType.Char;
                    }
                case "decimal":
                    {
                        return SqlDbType.Decimal;
                    }
                case "double":
                    {
                        return SqlDbType.Float;
                    }
                case "int16":
                    {
                        return SqlDbType.SmallInt;
                    }
                case "byte[]":
                    {
                        return SqlDbType.Binary;
                    }
                case "byte":
                    {
                        return SqlDbType.TinyInt;
                    }
                default:
                    return SqlDbType.VarChar;
            }
        }

        /// <summary>
        /// Metodo utilizado para convertir un tipo de dato SQL a c#
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <param name="valor"></param>
        /// <returns></returns>
        public static dynamic ConvertirObjetoAValorConTipoDeDato(PropertyInfo propertyInfo, object valor)
        {
            switch (propertyInfo.PropertyType.Name.ToLower())
            {
                case "int32":
                    {
                        return Convert.ToInt32(valor);
                    }
                case "string":
                    {
                        return Convert.ToString(valor);
                    }
                case "datetime":
                    {
                        return Convert.ToDateTime(valor);
                    }
                case "char":
                    {
                        return Convert.ToString(valor);
                    }
                case "decimal":
                    {
                        return Convert.ToDecimal(valor);
                    }
                case "double":
                    {
                        return Convert.ToDouble(valor);
                    }
                case "int16":
                    {
                        return Convert.ToInt16(valor);
                    }
                case "byte[]":
                    {
                        BinaryFormatter bf = new BinaryFormatter();
                        using (MemoryStream ms = new MemoryStream())
                        {
                            bf.Serialize(ms, valor);
                            return ms.ToArray();
                        }
                    }
                case "image":
                    {
                        BinaryFormatter bf = new BinaryFormatter();
                        using (MemoryStream ms = new MemoryStream())
                        {
                            bf.Serialize(ms, valor);
                            return ms.ToArray();
                        }
                    }
                case "byte":
                    {
                        return Convert.ToByte(valor);
                    }
            }
            return null;
        }


        /// <summary>
        /// Metodo utilizado para convertir un tipo de dato SQL a c#
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <param name="valor"></param>
        /// <returns></returns>
        public static dynamic ConvertirObjetoAValorConTipoDeDato(string tipo, object valor)
        {
            switch (tipo.ToLower())
            {
                case "int32":
                    {
                        if (Convert.IsDBNull(valor))
                            return 0;
                        return Convert.ToInt32(valor);
                    }
                case "int":
                    {
                        if (Convert.IsDBNull(valor))
                            return 0;
                        return Convert.ToInt32(valor);
                    }
                case "string":
                    {
                        if (Convert.IsDBNull(valor))
                            return string.Empty;
                        return Convert.ToString(valor).Trim();
                    }
                case "datetime":
                    {
                        if (Convert.IsDBNull(valor))
                            return default(DateTime);
                        return Convert.ToDateTime(valor);
                    }
                case "char":
                    {
                        if (Convert.IsDBNull(valor))
                            return string.Empty;
                        return Convert.ToString(valor).Trim();
                    }
                case "decimal":
                    {
                        if (Convert.IsDBNull(valor))
                            return default(decimal);
                        return Convert.ToDecimal(valor);
                    }
                case "double":
                    {
                        if (Convert.IsDBNull(valor))
                            return default(double);
                        return Convert.ToDouble(valor);
                    }
                case "int16":
                    {
                        if (Convert.IsDBNull(valor))
                            return default(Int16);
                        return Convert.ToInt16(valor);
                    }
                case "byte[]":
                    {
                        if (Convert.IsDBNull(valor))
                            return default(Byte[]);
                        BinaryFormatter bf = new BinaryFormatter();
                        using (MemoryStream ms = new MemoryStream())
                        {
                            bf.Serialize(ms, valor);
                            return ms.ToArray();
                        }
                    }
                case "image":
                    {
                        if (Convert.IsDBNull(valor))
                            return default(Byte[]);
                        BinaryFormatter bf = new BinaryFormatter();
                        using (MemoryStream ms = new MemoryStream())
                        {
                            bf.Serialize(ms, valor);
                            return ms.ToArray();
                        }
                    }
                case "byte":
                    {
                        if (Convert.IsDBNull(valor))
                            return default(Byte);
                        return Convert.ToByte(valor);
                    }
            }
            return null;
        }
    }
}
