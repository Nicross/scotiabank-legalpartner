using System.Collections.Generic;

namespace CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities
{
    public static class Resources
    {
        #region  Api Mantenedores - Mantenedores Url

        //public const string endPointMantenedores = "http://localhost:57010";
        //public const string endPointMantenedores = "http://localhost:62200";
        public const string endPointMantenedores = "http://172.17.206.20:8082";
        
        public static readonly string ConservadorUrl = endPointMantenedores+"/api/Conservadores";
        public static readonly string FacultadUrl = endPointMantenedores+"/api/Facultades";
        public static readonly string NotariaUrl = endPointMantenedores+"/api/Notarias";
        public static readonly string TipoDocumentoUrl = endPointMantenedores+"/api/TiposDocumentos";
        public static readonly string TipoPoderUrl = endPointMantenedores+"/api/TiposPoderes";
        public static readonly string TipoPersonaUrl = endPointMantenedores+"/api/TiposPersonas";
        public static readonly string TipoParticipanteUrl = endPointMantenedores+"/api/TiposParticipantes";        
        public static readonly string GrupoFacultadesUrl = endPointMantenedores + "/api/GrupoFacultades";
        public static readonly string MateriaBancoUrl = endPointMantenedores + "/api/MateriasBanco";
        public static readonly string MonedaUrl = endPointMantenedores + "/api/Monedas";
        public static readonly string OficinaPartesUrl = endPointMantenedores + "/api/OficinaPartes";
        public static readonly string SucursalBancoUrl = endPointMantenedores + "/api/SucursalBanco";
        public static readonly string GrupoUsuariosUrl = endPointMantenedores + "/api/GrupoUsuarios";
        public static readonly string PerfilUsuarioUrl = endPointMantenedores + "/api/PerfilesUsuarios";
        public static readonly string GrupoFacultadUrl = endPointMantenedores + "/api/GrupoFacultades";
        public static readonly string UsuarioUrl = endPointMantenedores + "/api/Usuarios";

        #endregion

        #region  Api Token - Token Url

        //public const string ambienteToken = "http://localhost:49598";
        public const string ambienteToken = "http://172.17.206.20:8081";

        public static readonly string TokenCreateToken = ambienteToken + "/Token";

        #endregion 

        #region Api Accesos - Login Url

        //public const string endPointAccesos = "http://localhost:30009";
        //public const string endPointAccesos = "http://localhost:30008";
        public const string endPointAccesos = "http://172.17.206.20:8084";

        public static readonly string LoginObtenerUsuarioAdmim = endPointAccesos + "/api/Login/GetUsuarioAdmin";
        public static readonly string LoginObtenerUsuarioLogin = endPointAccesos + "/api/Login/GetUsuarioLogin";
        public static readonly string LoginObtenerPassword = endPointAccesos + "/api/Login/GetPassword";

        public static readonly string ModificarClaveObtenerUsuario = endPointAccesos + "/api/Login/GetUsuario";
        public static readonly string ModificarClaveObtenerPasswordAnterior = endPointAccesos + "/api/Login/GetPasswordAnterior";
        public static readonly string ModificarClaveActualizarClave = endPointAccesos + "/api/Login/ActualizarClave";
        public static readonly string ModificarClaveActualizarClaveInicio = endPointAccesos + "/api/Login/ActualizarClaveInicio";
        public static readonly string ModificarClaveActualizarHistorialClaves = endPointAccesos + "/api/Login/ActualizarHistorialClaves";
        public static readonly string ModificarClaveObtenerGrupoUsuario = endPointAccesos + "/api/Login/GetGrupoUsuario";

        public static readonly string MenuObtenerMenus = endPointAccesos + "/api/Menu/GetMenusPorCodigoGrupoUsuario";

        #endregion

        #region Consultas

        //public const string endPointConsultas = "http://localhost:30009";
        public const string endPointConsultas = "http://172.17.206.20:8085";

        public static readonly string Consulta = endPointConsultas + "/api/Consulta";
        public static readonly string TransversalUrl = endPointConsultas + "/api/Transversal";
        public static readonly string ConsultaPoderesUrl = endPointConsultas + "/api/Consulta";

        #endregion

        #region Api CabeceraSGL - CabeceraSGL Url

        public const string endPointCabeceraSGL = "http://172.17.206.20:8087";
        //public const string endPointCabeceraSGL = "http://localhost:57011";
        //public const string endPointCabeceraSGL = "http://localhost:57010";

        public static readonly string CabeceraSGL = endPointCabeceraSGL + "/api/SGL";

        #region Constantes utilizadas por los metodos de cabeceraSGL

        public static readonly int UltimoCorretaltivoSGL = 10;

        #endregion

        #endregion

        #region Api Poderes Y Revocaciones - Mantenedores/Grupo Url

        //public const string endPointPoderesYrevocaciones = "http://localhost:57010";
        public const string endPointPoderesYrevocaciones = "http://172.17.206.20:8088";
        public static readonly string AsociacionMandatarioGrupoUrl = endPointPoderesYrevocaciones + "/api/AsociacionMandatarioGrupo";
        public static readonly string GruposUrl = endPointPoderesYrevocaciones + "/api/Grupos";
        public static readonly string MandatariosUrl = endPointPoderesYrevocaciones + "/api/Mandatarios";
        public static readonly string FacultadesUrl = endPointPoderesYrevocaciones + "/api/Facultades";
        public static readonly string PoderesUrl = endPointPoderesYrevocaciones + "/api/Poderes";
        public static readonly string RevocacionesUrl = endPointPoderesYrevocaciones + "/api/Revocaciones";

        #region Constantes

        #region Listar Documento Poder

        /// <summary>
        /// Materias que filtran el documento para "mat_id"
        /// </summary>
        public const string materiaId1 = "344";
        public const string materiaId2 = "352";
        public const string materiaId3 = "354";
        /// <summary>
        /// Estado de revocacion
        /// if rev_cod > 0
        /// </summary>
        public const string Revocado = "(Revocado)";


        #endregion


        ///summary
        ///Constantes 
        ///summary
        public const string constanteDos = "2";

        /// <summary>
        /// Constante para evaluar codigoTipo de ListaMandatario
        /// Controller: MandatarioGrupo
        /// </summary>
        public const string valorCero = "0";

        public const string valorUno = "-1";

        public static readonly string tipoGrupo = constanteDos;

        #endregion

        #endregion

        #region Api Entrada Completa

        public const string endPointEntradaCompleta = "http://172.17.206.20:8089";
        //public const string endPointEntradaCompleta = "http://localhost:57011";       
        //public const string endPointEntradaCompleta = "http://localhost:57010";

        public static readonly string EntradaCompletaUrl = endPointEntradaCompleta + "/api/DatosEstatutos";
        public static readonly string DocEscrituraUrl = endPointEntradaCompleta + "/api/DocEscritura";
        public static readonly string SocioUrl = endPointEntradaCompleta + "/api/Socio";
        public static readonly string LegalizacionUrl = endPointEntradaCompleta + "/api/Legalizacion";

        #endregion

        #region Mensaje Error
        
        public const string TituloMensajeError = "Mensaje: Error";
        public const string CuerpoMensajeError = "Ha ocurrido un error inesperado, int�ntelo de nuevo m�s tarde o con�ctese con el administrador del sistema.";

        #endregion
    }
}