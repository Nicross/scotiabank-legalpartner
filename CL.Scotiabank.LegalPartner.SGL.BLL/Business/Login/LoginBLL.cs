﻿using CL.Scotiabank.LegalPartner.SGL.Transversal.DTO;
using CL.Scotiabank.LegalPartner.SGL.Transversal.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CL.Scotiabank.LegalPartner.SGL.BLL
{
    public class LoginBLL : ILoginBLL
    {
        /// <summary>
        /// Metodo encargado de validar la existencia previa de la clave ingresada, en caso de existir retornara un True.
        /// </summary>
        /// <param name="entityToValidate">Entidad a Validar</param>
        /// <returns>boleano</returns>
        public bool ValidaExistenciaPreviaClave(LoginDTO entityToValidate)
        {
            if (entityToValidate.CadenaPasswordsAnteriores.Split(';').Select(x => EncodeDecode.Decrypt("claveSecreta", x)).Contains(entityToValidate.Password))
            {
                return true;
            }

            return false;
        }
    }
}
